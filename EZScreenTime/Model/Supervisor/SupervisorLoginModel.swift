//
//  SupervisorLoginModel.swift
//  EZScreenTime
//
//  Created by cedcoss on 24/03/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import Foundation

// MARK: - Welcome
struct SupervisorLoginModel: Codable {
    let status: Bool?
    let data: SupervisorLoginDataClass?
    let message: String?
    let response: Bool?
}

// MARK: - DataClass
struct SupervisorLoginDataClass: Codable {
    let accessToken: String?
    let supervisor: SupervisorDetail?

    enum CodingKeys: String, CodingKey {
        case accessToken = "access_token"
        case supervisor
    }
}

// MARK: - Supervisor
struct SupervisorDetail: Codable {
    let id, icon, tz, name: String?
}
