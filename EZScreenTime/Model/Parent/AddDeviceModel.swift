//
//  AddDeviceModel.swift
//  EZScreenTime
//
//  Created by cedcoss on 08/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import Foundation

// MARK: - AddDevice
struct AddDevicesModel:Codable {
    let status:   Bool?
    let message:  String?
    let response: Bool?
    var data: AllData?
}

// MARK: - DataClass
struct AllData:Codable  {
    let suggestedDevices: [SuggestedDevice]?
    var parentDevices: [ParentDevice]?
}

// MARK: - ParentDevice
struct ParentDevice:Codable  {
    let id, name, icon: String?
}

// MARK: - SuggestedDevice
struct SuggestedDevice:Codable  {
    let icon, name: String?
}

