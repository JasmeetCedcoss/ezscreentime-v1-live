//
//  LoginModel.swift
//  EZScreenTime
//
//  Created by cedcoss on 06/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import Foundation

// MARK: - Welcome
struct LoginWelcome: Codable {
    let message: String?
    let status, response: Bool?
    let data: DataClass?
}

// MARK: - DataClass
struct DataClass: Codable {
    let info: Info?
    let accessToken: String?

    private enum CodingKeys : String, CodingKey {
        case accessToken = "access_token"
        case info = "info"
    }

    init(info: Info?,accessToken: String?) {
        self.info = info
        self.accessToken = accessToken
    }
}

// MARK: - Info
struct Info: Codable {
    let name, isNew: String?
}
