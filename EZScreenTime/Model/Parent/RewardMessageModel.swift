//
//  RewardMessageModel.swift
//  EZScreenTime
//
//  Created by cedcoss on 17/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import Foundation

// MARK: - Welcome
struct RewardMessageModel: Codable {
    var data: RewardMessageDataClass?
    let message: String?
    let response, status: Bool?
}

// MARK: - DataClass
struct RewardMessageDataClass: Codable {
    let accessToken: String?
    var categories: [Category]?
}

// MARK: - Category
struct Category: Codable {
    var id, name: String?
    var messages: [Message]?
}

// MARK: - Message
struct Message: Codable {
    let selected: Bool?
    let catID, id, isCustom, message: String?
}
