//
//  AddKidsModel.swift
//  EZScreenTime
//
//  Created by cedcoss on 13/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import Foundation


// MARK: - AddKidsModel
struct AddKidsModel: Codable {
    var data: AddKidsDataClass?
    let response, status: Bool?
}

// MARK: - AddKidsDataClass
struct AddKidsDataClass: Codable {
    let accessToken: String?
    let devices: [DataDevice]?
    var kids: [Kid]?
}

// MARK: - DataDevice
struct DataDevice: Codable {
    let name, id, icon,valMinutes: String?

    enum CodingKeys: String, CodingKey {
        case name,id,icon,valMinutes
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
         name        = try container.decode(String.self, forKey: CodingKeys.name)
         id          = try container.decode(String.self, forKey: CodingKeys.id)
         icon        = try container.decode(String.self, forKey: CodingKeys.icon)
         let minutes     = try container.decode(String.self, forKey: CodingKeys.valMinutes)

        if let index = minutes.range(of: ".")?.lowerBound {
            let substring = minutes[..<index]
            let string = String(substring)
            valMinutes = string
        }else{
            valMinutes = minutes
        }
    }
}

// MARK: - Kid
struct Kid: Codable {
    var age, name, icon: String?
    let devices: [KidDevice]?
    var id: String?
}

// MARK: - KidDevice
struct KidDevice: Codable {
    let coins, isActive, id: String?
}



