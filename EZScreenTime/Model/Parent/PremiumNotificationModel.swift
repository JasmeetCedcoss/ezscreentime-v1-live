//
//  PremiumNotificationModel.swift
//  EZScreenTime
//
//  Created by cedcoss on 26/03/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import Foundation

//// MARK: - Welcome
//struct PremiumNotificationModel: Codable {
//    let status: Bool?
//    let message: String?
//    let response: Bool?
//    let data: PremiumNotificationDataClass?
//}
//
//// MARK: - DataClass
//struct PremiumNotificationDataClass: Codable {
//    let devices: [NotiDevice]?
//    let isActive: Bool?
//    let accessToken: String?
//
//    enum CodingKeys: String, CodingKey {
//        case devices, isActive
//        case accessToken = "access_token"
//    }
//}
//
//// MARK: - Device
//struct NotiDevice: Codable {
//    let color, icon, name, id: String?
//    let valMinutes: String?
//
//    enum CodingKeys: String, CodingKey {
//        case color, icon, name, id
//        case valMinutes = "val_minutes"
//    }
//}


// MARK: - Welcome
struct PremiumNotificationModel: Codable {
    let data: PremiumNotificationDataClass?
    let response, status: Bool?
    let message: String?
}

// MARK: - DataClass
struct PremiumNotificationDataClass: Codable {
    let devices: [NotiDevice]?
    let notificationData: [NotificationDatum]?
    let accessToken: String?
    let isActive: Bool?

    enum CodingKeys: String, CodingKey {
        case devices
        case notificationData = "notification_data"
        case accessToken = "access_token"
        case isActive
    }
}

// MARK: - Device
struct NotiDevice: Codable {
    let icon, valMinutes, color, name: String?
    let id: String?

    enum CodingKeys: String, CodingKey {
        case icon
        case valMinutes = "val_minutes"
        case color, name, id
    }
}

// MARK: - NotificationDatum
struct NotificationDatum: Codable {
    let deviceId, addedOn, isActive: String?
    //let updatedOn: JSONNull?
    let pId, timeBefore, id: String?

    enum CodingKeys: String, CodingKey {
        case deviceId = "device_id"
        case addedOn = "added_on"
        case isActive = "is_active"
        //case updatedOn = "updated_on"
        case pId = "p_id"
        case timeBefore = "time_before"
        case id
    }
}

