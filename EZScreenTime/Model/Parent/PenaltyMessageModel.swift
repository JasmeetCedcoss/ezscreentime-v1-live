//
//  PenaltyMessageModel.swift
//  EZScreenTime
//
//  Created by cedcoss on 18/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import Foundation


// MARK: - Welcome
struct PenaltyMessageModel: Codable {
    let status, response: Bool?
    let message: String?
    var data: PenaltyDataClass?
}

// MARK: - DataClass
struct PenaltyDataClass: Codable {
    var messages: [PenaltyMessage]?
    let accessToken: String?
}

// MARK: - Message
struct PenaltyMessage: Codable {
    let isCustom, message: String?
    let selected: Bool?
    let id: String?
}
