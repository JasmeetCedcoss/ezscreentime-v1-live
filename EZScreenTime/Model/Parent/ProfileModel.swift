//
//  ProfileModel.swift
//  EZScreenTime
//
//  Created by cedcoss on 12/03/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import Foundation


// MARK: - Welcome
struct ProfileModel: Codable {
    let status: Bool?
    let message: String?
    let data: ProfileDataClass?
    let response: Bool?
}

// MARK: - DataClass
struct ProfileDataClass: Codable {
    let accessToken: String?
    let profileData: ProfileData?
}

// MARK: - ProfileData
struct ProfileData: Codable {
    let tz, name, icon, lang: String?
    
    init(from decoder: Decoder) throws {
        let container       = try decoder.container(keyedBy: CodingKeys.self)
         tz                 = try container.decode(String.self, forKey: CodingKeys.tz)
         name               = try container.decode(String.self, forKey: CodingKeys.name)
         lang               = try container.decode(String.self, forKey: CodingKeys.lang)
         let iconRecived    = try container.decode(String.self, forKey: CodingKeys.icon)
         icon = iconRecived == "" ? "new_icon" : iconRecived
    }
}
