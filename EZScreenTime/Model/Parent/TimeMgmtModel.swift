//
//  TimeMgmtModel.swift
//  EZScreenTime
//
//  Created by cedcoss on 18/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//
//
//


import Foundation

// MARK: - TimeMgmtModel
struct TimeMgmtModel: Codable {
    let response, status: Bool?
    let message: String?
    let data: TimeMgmtDataClass?
}

// MARK: - DataClass
struct TimeMgmtDataClass: Codable {
    let systemSettings: SystemSettings?
    let parentDevices: [TimeParentDevice]?
    let savedSettings: SavedSettings?
    let accessToken: String?
}

// MARK: - ParentDevice
struct TimeParentDevice: Codable {
    let id, icon, name, color: String?
    let valMinutes: String?
}

// MARK: - SavedSettings
struct SavedSettings: Codable {
    let period, durationType: String?
   // let durationValue: [DurationValue]?
}

// MARK: - DurationValue
struct DurationValue: Codable {
    let deviceID, allDays: Int?
}

// MARK: - SystemSettings
struct SystemSettings: Codable {
    let periods, durations: [Duration]?
}

// MARK: - Duration
struct Duration: Codable {
    let key: Int?
    let value: String?
}




// MARK: - DurationValue
struct MySavedSettings: Codable {
    var durationValue: [DurationValueForCase1]?

}

struct DurationValueForCase1:Codable {
    let deviceId,allDays : Int?
}


// MARK: - SavedSettingscase 2
struct MySavedSettingsCase2: Codable {
    let durationValue: [DurationValueForCase2]?
}
// MARK: - DurationValue
struct DurationValueForCase2: Codable {
    let monday, tuesday, wednesday, thursday: [Day]?
    let friday, saturday, sunday: [Day]?
}
// MARK: - Day
struct Day: Codable {
    let deviceId, maxMinutes: String?
}

