//
//  ActionModel.swift
//  EZScreenTime
//
//  Created by cedcoss on 22/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import Foundation

// MARK: - AcctionModel
struct AcctionModel: Codable {
    let data: KidsDataClass?
    let message: String?
    let response, status: Bool?
}

// MARK: - KidsDataClass
struct KidsDataClass: Codable {
    let accessToken: String?
    let kids: [KidForAction]?

    enum CodingKeys: String, CodingKey {
        case accessToken = "access_token"
        case kids

    }
}

// MARK: - Kid
struct KidForAction: Codable {
    let name, icon, id, age: String?
}

