//
//  ParentDashboardModel.swift
//  EZScreenTime
//
//  Created by cedcoss on 05/03/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import Foundation



// MARK: - Welcome
struct ParentDashboardModel: Codable {
    let data: ParentDashboardDataClass?
    let message: String?
    let response, status: Bool?
}

// MARK: - DataClass
struct ParentDashboardDataClass: Codable {
    let accessToken: String?
    let kids: [ParentDashboardKid]?
    let dayNumber, weekNumber: String?

    
}

// MARK: - Kid
struct ParentDashboardKid: Codable {
    let icon, name: String?
    let devices: [ParentDashboardDevice]?
    let id, age: String?
}

// MARK: - Device
struct ParentDashboardDevice: Codable {

    let name, icon : String?
    let id: Int?
    let isActive: Int?
    let totalTimeLeft: String?
    let coinsReceived,coinsAvailable: String?

    enum CodingKeys: String, CodingKey {
        case id, name, icon, coinsAvailable,coinsReceived,totalTimeLeft,isActive
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        name                     = try container.decode(String.self, forKey: CodingKeys.name)
        id                       = try container.decode(Int.self, forKey: CodingKeys.id)
        icon                     = try container.decode(String.self, forKey: CodingKeys.icon)
        isActive                 = try container.decode(Int.self, forKey: CodingKeys.isActive)

        let coinsReceivedTotal   = try container.decode(String.self, forKey: CodingKeys.coinsReceived)
        let coinsReceivedString  = Double(coinsReceivedTotal)
        let finalcoinsReceived   = String(Int(coinsReceivedString!))
        coinsReceived            = finalcoinsReceived

        let coins               = try container.decode(String.self, forKey: CodingKeys.coinsAvailable)
        let coinString          = Double(coins)!
        //let coinRounded         = (coinString?.rounded(.awayFromZero))!
        let coinRounded         = round(coinString)
        let finalCoin           = String(Int(coinRounded))
        coinsAvailable          = finalCoin

        let totalTimeLeftInt    = try container.decode(Int.self, forKey: CodingKeys.totalTimeLeft)
        let interval            = (totalTimeLeftInt * 60)
        let formatter           = DateComponentsFormatter()
        formatter.allowedUnits  = [.hour, .minute]
        formatter.unitsStyle    = .brief
        let formattedString     = formatter.string(from: TimeInterval(interval))!
        totalTimeLeft           = formattedString
    }
}
