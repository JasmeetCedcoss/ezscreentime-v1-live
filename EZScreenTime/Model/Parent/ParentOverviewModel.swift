//
//  ParentOverviewModel.swift
//  EZScreenTime
//
//  Created by cedcoss on 27/03/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import Foundation


// MARK: - Welcome
struct ParentOverviewModel: Codable {
    let response: Bool?
    let data: ParentOverviewDataClass?
    let status: Bool?
    let message: String?
}

// MARK: - DataClass
struct ParentOverviewDataClass: Codable {
    let accessToken: String?
    let devicesUsage: [DevicesUsage]?

    enum CodingKeys: String, CodingKey {
        case accessToken = "access_token"
        case devicesUsage
    }
}

// MARK: - DevicesUsage
struct DevicesUsage: Codable {
    let usage: [Usage]?
    let name, totalUsageTime: String?
    let isActive: Int?
    let coinsSpend, id: String?
}

// MARK: - Usage
struct Usage: Codable {
    let minutes, started, ended: String?

    init(from decoder: Decoder) throws {

        let container = try decoder.container(keyedBy: CodingKeys.self)
        minutes              = try container.decode(String.self, forKey: CodingKeys.minutes)
        let startTimestamp   = try container.decode(String.self, forKey: CodingKeys.started)
        started              = convert(time: startTimestamp)
        let endedTimestamp   = try container.decode(String.self, forKey: CodingKeys.ended)
        ended                = convert(time: endedTimestamp)
    }
}

func convert(time: String)->String?{
      let dateFormatterGet = DateFormatter()
      dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"

      let dateFormatterPrint = DateFormatter()
      dateFormatterPrint.dateFormat = "h:mm a"

      if let date = dateFormatterGet.date(from: time) {
          print(dateFormatterPrint.string(from: date))
          return dateFormatterPrint.string(from: date)
      } else {
          print("There was an error decoding the string")
          return nil
      }
  }

