//
//  ActionStep2Model.swift
//  EZScreenTime
//
//  Created by cedcoss on 24/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import Foundation

// MARK: - ActionStep2Model
struct ActionStepTwoModel: Codable {
    let data: ActionStepTwoDataClass?
    let status: Bool?
    let message: String?
    let response: Bool?
}

// MARK: - ActionStep2DataClass
struct ActionStepTwoDataClass: Codable {
    let accessToken: String?
    let kidDevices: [ActionKidDevice]?
    let actionMessages: ActionMessages?
}

// MARK: - ActionMessages
struct ActionMessages: Codable {
    let reward, consume, discipline: [Consume]?
}

// MARK: - Consume
struct Consume: Codable {
    let id, message: String?
}

// MARK: - ActionKidDevice
struct ActionKidDevice: Codable {
    let name, icon, coinsAvailable, id: String?
    let coins: String?

    init(from decoder: Decoder) throws {
        let container                = try decoder.container(keyedBy: CodingKeys.self)
        name                         = try container.decode(String.self, forKey: CodingKeys.name)
        id                           = try container.decode(String.self, forKey: CodingKeys.id)
        icon                         = try container.decode(String.self, forKey: CodingKeys.icon)
        let coinsVal                 = try container.decode(String.self, forKey: CodingKeys.coinsAvailable)
        let coinString               = Double(coinsVal)!
        let coinRounded              = round(coinString)
        let finalCoin                = String(Int(coinRounded))
        coinsAvailable               = finalCoin
        coins                        = try container.decode(String.self, forKey: CodingKeys.coins)
    }
}


struct CoinDeductionBrain{
    var deductionSelected = AppSetUp.coinsDeductionChoice.first

    mutating func update(val:String){
        deductionSelected = val
    }
}
