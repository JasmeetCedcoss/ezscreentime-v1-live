//
//  SupervisorModel.swift
//  EZScreenTime
//
//  Created by cedcoss on 16/03/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import Foundation

// MARK: - SupervisorModel
struct SupervisorModel: Codable {
    let response, status: Bool?
    let data: SupervisorModelDataClass?
}

// MARK: - DataClass
struct SupervisorModelDataClass: Codable {
    let accessToken: String?
    let supervisors: [Supervisor]?

    enum CodingKeys: String, CodingKey {
        case accessToken = "access_token"
        case supervisors
    }
}

// MARK: - Supervisor
struct Supervisor: Codable {
    let updatedOn, email: String?
    let isNew: Bool?
    let pId: String?
    let isActivated: Bool?
    let id, tz, icon, name: String?
    let addedOn, receiveMail, locale: String?
    let status: Bool?

}

