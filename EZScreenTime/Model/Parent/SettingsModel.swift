//
//  SettingsModel.swift
//  EZScreenTime
//
//  Created by cedcoss on 06/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import Foundation


// MARK: - Welcome
struct SettingsModel: Codable {
    let data: DataClassSet?
}

// MARK: - DataClass
struct DataClassSet: Codable {
    let configurationArray: [String]?
    let premiumUser: Bool?
}

