//
//  ActivateKidDeviceModel.swift
//  EZScreenTime
//
//  Created by cedcoss on 27/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import Foundation

// MARK: - ActivateKidDeviceModel
struct ActivateKidDeviceModel: Codable {
    let status: Bool?
    let data: ActivateKidDeviceDataClass?
    let response: Bool?
    let message: String?
}

// MARK: - ActivateKidDeviceDataClass
struct ActivateKidDeviceDataClass: Codable {
    let accessToken: String?
    let kidsSessions: [KidsSession]?
}

// MARK: - KidsSession
struct KidsSession: Codable {
    let icon, age, isActivated: String?
    let isNew: Bool?
    let name, id: String?
}
