//
//  NotificationModel.swift
//  EZScreenTime
//
//  Created by cedcoss on 17/03/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import Foundation

struct NotificationModel{
    let title: String?
    let message: String?
    let reason: String?
    let parentIcon: String?
    let typeOfDistribution: String?
    let kidID: String?
    let numberOfCoins: String?

    init(title: String = "",message: String = "",reason: String = "",parentIcon: String = "",typeOfDistribution: String = "",kidID: String = "",numberOfCoins: String = "") {
        
        self.title      = title
        self.message    = message
        self.parentIcon = parentIcon
        self.reason     = reason
        self.typeOfDistribution = typeOfDistribution
        self.kidID      = kidID
        self.numberOfCoins = numberOfCoins
    }
}
