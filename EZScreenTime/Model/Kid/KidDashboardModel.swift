//
//  KidDashboardModel.swift
//  EZScreenTime
//
//  Created by cedcoss on 29/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import Foundation


// MARK: - Welcome
struct KidsDashboardModel: Codable {
    var data: KidDashboardDataClass?
    var status: Bool?
}

// MARK: - DataClass
struct KidDashboardDataClass: Codable {
    var settings: [Setting]?
    let weekNumber, dayNumber: String?

}

// MARK: - Setting
struct Setting: Codable {
    let todayTimeLimit: String?
    let timerData: TimerData?
    let coinsAvailable, coinColor, totalTimeLeft: String?
    var isActiveTimer: Int?
    let id, icon, coinsReceived, name: String?
    let coinMinutesValue: String?
    var todayTimeLimitInInt: Int?

    var totalTimeleftInDouble: Int?

    init(from decoder: Decoder) throws {
        let container                = try decoder.container(keyedBy: CodingKeys.self)
        name                         = try container.decode(String.self, forKey: CodingKeys.name)
        id                           = try container.decode(String.self, forKey: CodingKeys.id)
        icon                         = try container.decode(String.self, forKey: CodingKeys.icon)
        isActiveTimer                = try container.decode(Int.self, forKey: CodingKeys.isActiveTimer)
        coinMinutesValue             = try container.decode(String.self, forKey: CodingKeys.coinMinutesValue)
        timerData                    = try container.decode(TimerData.self, forKey: CodingKeys.timerData)
        coinColor                    = try container.decode(String.self, forKey: CodingKeys.coinColor)

        let coinsReceivedTotal       = try container.decode(String.self, forKey: CodingKeys.coinsReceived)
        let coinsReceivedString      = Double(coinsReceivedTotal)
        let finalcoinsReceived       = String(Int(coinsReceivedString!))
        coinsReceived                = finalcoinsReceived

        let coins                    = try container.decode(String.self, forKey: CodingKeys.coinsAvailable)
        let coinString               = Double(coins)!
        //let coinRounded              = (coinString?.rounded(.awayFromZero))!
        let coinRounded              = round(coinString)
        let finalCoin                = String(Int(coinRounded))
        coinsAvailable               = finalCoin

        let todayTimeLimitInt        = try container.decode(Int.self, forKey: CodingKeys.todayTimeLimit)
        var interval                 = (todayTimeLimitInt * 60)
        todayTimeLimitInInt          = interval
        todayTimeLimit               = timeformatter(timeInterval: interval)

        let totalTimeLeftInt         = try container.decode(Int.self, forKey: CodingKeys.totalTimeLeft)
        interval                     = (totalTimeLeftInt * 60)
        totalTimeleftInDouble        = totalTimeLeftInt
        totalTimeLeft                = timeformatter(timeInterval: interval)

    }
}

// MARK: - TimerData
struct TimerData: Codable {
    let timeSpendToday, timeInRunningSession: String?
    var timeSpendTodayInInt: Int?
    enum CodingKeys: String, CodingKey {
        case timeSpendToday, timeInRunningSession,timeSpendTodayInInt
    }

    init(from decoder: Decoder) throws {
        let container               = try decoder.container(keyedBy: CodingKeys.self)

        let timeSpendTodayInt       = try container.decode(Int.self, forKey: CodingKeys.timeSpendToday)
        let interval                = (timeSpendTodayInt * 60)
        timeSpendTodayInInt         = interval
        timeSpendToday              = timeformatter(timeInterval: interval)

        let timeInRunningSessionInt = try container.decode(Int.self, forKey: CodingKeys.timeInRunningSession)
        let timeInRunningSessionString = String(timeInRunningSessionInt)
        timeInRunningSession = timeInRunningSessionString
    }
}


func timeformatter(timeInterval:Int)->String{
    let formatter               = DateComponentsFormatter()
    formatter.allowedUnits      = [.hour,.minute]
    formatter.unitsStyle        = .brief
    let formattedString         = formatter.string(from: TimeInterval(timeInterval))!
    return formattedString
}


