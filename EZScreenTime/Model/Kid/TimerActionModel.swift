//
//  TimerActionModel.swift
//  EZScreenTime
//
//  Created by cedcoss on 13/03/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import Foundation

struct TimerActionModel: Codable {
    let data: TimerActionDataClass?
    let response: Bool?
    let message: String?
    let status: Bool?
}

// MARK: - DataClass
struct TimerActionDataClass: Codable {
    let deviceData: TimerDeviceData?
}

// MARK: - DeviceData
struct TimerDeviceData: Codable {
    let timerData: TimerActionData?
    let coinsReceived, coinsAvailable: String?
    let totalTimeLeft, todayTimeLimit: Int?
    let coinMinutesValue: String?
    let isActiveTimer: Int?
    let coinColor: String?
    let deviceId: Int?
}

// MARK: - TimerActionData
struct TimerActionData: Codable {
    let timeInRunningSession, timeSpendToday: Int?
}
