//
//  KidLoginModel.swift
//  EZScreenTime
//
//  Created by cedcoss on 28/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import Foundation

// MARK: - KidLoginModel
struct KidLoginModel: Codable {
    let response: Bool?
    let message: String?
    let status: Bool?
    let data: KidModelDataClass?
}

// MARK: - KidModelDataClass
struct KidModelDataClass: Codable {
    let accessToken: String?
    let kid: KidModel?

    enum CodingKeys: String, CodingKey {
        case accessToken = "access_token"
        case kid
    }
}

// MARK: - Kid
struct KidModel: Codable {
    let age, id, customCoins, name: String?
    let tz, icon: String?
}
