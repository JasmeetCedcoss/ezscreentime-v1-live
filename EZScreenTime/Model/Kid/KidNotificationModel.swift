//
//  KidNotificationModel.swift
//  EZScreenTime
//
//  Created by cedcoss on 14/03/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import Foundation


// MARK: - KidNotificationModel
struct KidNotificationModel: Codable {
    let response, status: Bool?
    let message: String?
    let data: KidNotificationDataClass?
}

// MARK: - DataClass
struct KidNotificationDataClass: Codable {
    let notificationHistory: [NotificationHistory]?
}

// MARK: - NotificationHistory
struct NotificationHistory: Codable {
    let timestamp, name, title, numberOfCoins,parentIcon: String?
    let typeOfDistribution, reason, message: String?
    let typeOfMessage: Int?
    let icon: String?
    let color: String?

    enum CodingKeys: String, CodingKey {
        case timestamp, name,title, numberOfCoins,parentIcon,
        typeOfDistribution, reason, message,
        typeOfMessage,
        icon,
        color
    }

    init(from decoder: Decoder) throws {

        let container = try decoder.container(keyedBy: CodingKeys.self)
        name                = try container.decode(String.self, forKey: CodingKeys.name)
        parentIcon                = try container.decode(String.self, forKey: CodingKeys.parentIcon)
        timestamp                = try container.decode(String.self, forKey: CodingKeys.timestamp)
        title                = try container.decode(String.self, forKey: CodingKeys.title)
        numberOfCoins                = try container.decode(String.self, forKey: CodingKeys.numberOfCoins)
        typeOfDistribution                = try container.decode(String.self, forKey: CodingKeys.typeOfDistribution)
        reason                = try container.decode(String.self, forKey: CodingKeys.reason)
        message                = try container.decode(String.self, forKey: CodingKeys.message)
        typeOfMessage                = try container.decode(Int.self, forKey: CodingKeys.typeOfMessage)
        icon                = try container.decode(String.self, forKey: CodingKeys.icon)
        color                = title == "Awarded" ? "green": "red"

    }
}
