//
//  SupervisorListViewModel.swift
//  EZScreenTime
//
//  Created by cedcoss on 16/03/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import Foundation
import UIKit
struct SupervisorListViewModel{

    let updatedOn, email: String?
    let isNew: Bool?
    let pId: String?
    let isActivated: Bool?
    var id, tz, icon, name: String?
    var addedOn, receiveMail, locale: String?
    var activationStatus: Bool?

    //  MARK: - CUSTOM VALUES
    //
    var generateCodeText:String?
    var receiveMailCustom: String?
    var cellHeight: CGFloat?

    init(supervisor:Supervisor){
        self.updatedOn          = supervisor.updatedOn
        self.email              = supervisor.email
        self.isNew              = supervisor.isNew
        self.pId                = supervisor.pId
        self.isActivated        = supervisor.isActivated
        self.id                 = supervisor.id
        self.tz                 = supervisor.tz
        self.icon               = supervisor.icon
        self.name               = supervisor.name
        self.addedOn            = supervisor.addedOn
        self.receiveMail        = supervisor.receiveMail
        self.locale             = supervisor.locale
        self.activationStatus   = supervisor.status
        guard let activated     = isActivated else {return}
        self.generateCodeText   = activated ? "***RESET***" : "***NEW CODE***"
        guard let receiveMailVal     = receiveMail else {return}
        self.receiveMailCustom  = receiveMailVal == "0" ? "No" : "Yes"
        if AppSetUp.isIpad{
            self.cellHeight         = receiveMailVal == "0" ? CGFloat(370) : CGFloat(420)
        }else{
            self.cellHeight         = receiveMailVal == "0" ? CGFloat(290) : CGFloat(340)
        }
    }
}
