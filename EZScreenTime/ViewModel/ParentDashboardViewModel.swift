//
//  KidDashboardViewModel.swift
//  EZScreenTime
//
//  Created by cedcoss on 29/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import Foundation

struct KidDashboardViewModel{

    var kidDashboardModel  : Setting?
    var todayTimeLimitInInt: Int?
    var isActiveTimer: Int?
    var timeSpendTodayInInt: Int?
    var difference: Int
    init(DashboardModel:Setting){
        self.kidDashboardModel = DashboardModel
        self.todayTimeLimitInInt = DashboardModel.todayTimeLimitInInt
        self.isActiveTimer       = DashboardModel.isActiveTimer
        self.timeSpendTodayInInt = DashboardModel.timerData?.timeSpendTodayInInt
        self.difference          = (DashboardModel.todayTimeLimitInInt ?? 0) - (DashboardModel.timerData?.timeSpendTodayInInt ?? 0)
    }
}
