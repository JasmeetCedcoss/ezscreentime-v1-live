//
//  AppInfo+CoreDataProperties.swift
//  
//
//  Created by cedcoss on 06/02/20.
//
//

import Foundation
import CoreData


extension AppInfo {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<AppInfo> {
        return NSFetchRequest<AppInfo>(entityName: "AppInfo")
    }

    @NSManaged public var getLatestToken: String?
}
