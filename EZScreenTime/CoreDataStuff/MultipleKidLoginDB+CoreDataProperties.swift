//
//  MultipleKidLoginDB+CoreDataProperties.swift
//  
//
//  Created by cedcoss on 29/02/20.
//
//

import Foundation
import CoreData

extension MultipleKidLoginDB {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<MultipleKidLoginDB> {
        return NSFetchRequest<MultipleKidLoginDB>(entityName: "MultipleKidLoginDB")
    }

    @NSManaged public var kid_age: String?
    @NSManaged public var kid_icon: String?
    @NSManaged public var kid_id: String?
    @NSManaged public var kid_token: String?
    @NSManaged public var kid_name: String?


}
