//
//  CalendarPopup.swift
//  EZScreenTime
//
//  Created by cedcoss on 21/04/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit
import FSCalendar

class CalendarPopup: NSObject {

    let blackView = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffect.Style.regular))

    lazy var backView: UIView = {
        let backView                                        = UIView()
        backView.translatesAutoresizingMaskIntoConstraints  = false
        backView.backgroundColor                            = .white
        backView.layer.cornerRadius                         = 5
        return backView
    }()

    lazy var calendarView: FSCalendar = {
        let calendarView                                        = FSCalendar()
        calendarView.scope                                      = .week
        calendarView.translatesAutoresizingMaskIntoConstraints  = false
        
        return calendarView
    }()

    func showCalendarPopup(){
        if let window = UIApplication.shared.keyWindow {
            blackView.backgroundColor = UIColor(white: 0, alpha: 0.5)
            blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleDismiss)))
            window.addSubview(blackView)
            blackView.frame = window.frame
            blackView.alpha = 0
            backView.alpha  = 0
            window.addSubview(backView)

            //  MARK: - Setting up backview
            //
            backView.centerYAnchor.constraint(equalTo: window.centerYAnchor).isActive = true
            backView.centerXAnchor.constraint(equalTo: window.centerXAnchor).isActive = true
            backView.heightAnchor.constraint(equalToConstant: 260).isActive           = true
            backView.widthAnchor.constraint(equalToConstant:window.frame.width - 50).isActive = true

            backView.addSubview(calendarView)
            calendarView.snp.makeConstraints { (make) in
                make.edges.equalToSuperview().inset(UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10))
            }

            UIView.animate(withDuration: 0.5, animations: {
                self.blackView.alpha = 1
                self.backView.alpha  = 1
            })
        }
    }

    @objc func handleDismiss() {
        UIView.animate(withDuration: 0.5) {
            self.blackView.alpha = 0
            self.backView.alpha  = 0
        }
    }

    override init() {
        super.init()
        //start doing something here maybe....
    }
}
