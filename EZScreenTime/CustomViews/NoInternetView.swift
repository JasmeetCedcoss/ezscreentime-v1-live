//
//  NoInternetView.swift
//  EZScreenTime
//
//  Created by cedcoss on 07/04/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import Foundation
import AVFoundation

class NoInternetView: NSObject {

    lazy var backView: UIView = {
        let backView                                        = UIView()
        backView.translatesAutoresizingMaskIntoConstraints  = false
        backView.backgroundColor                            = AppSetUp.controllerBackgroundColor
        backView.layer.cornerRadius                         = 5
        return backView
    }()

    lazy var header: UILabel = {
        let header                                        = UILabel()
        header.textColor                                  = AppSetUp.textColor
        header.translatesAutoresizingMaskIntoConstraints  = false
        header.numberOfLines                              = 0
        header.text                                       = "No internet connection"
        header.textAlignment                              = .center
        header.font                                       = UIFont(name: "Poppins-Regular", size: 22)
        return header
    }()

    lazy var noInternetImage: UIImageView = {
        let noInternetImage                                        = UIImageView()
        noInternetImage.image                                      = UIImage(named: "nointernet")
        noInternetImage.contentMode                                = .scaleAspectFit
        noInternetImage.translatesAutoresizingMaskIntoConstraints  = false
        return noInternetImage
    }()

    lazy var subHeading: UILabel = {
        let subHeading                                        = UILabel()
        subHeading.textColor                                  = AppSetUp.textColor
        subHeading.translatesAutoresizingMaskIntoConstraints  = false
        subHeading.numberOfLines                              = 0
        subHeading.text                                       = "Oops, your connection seems off..."
        subHeading.font                                       = UIFont(name: "Poppins-Regular", size: 22)
        subHeading.textAlignment                              = .center
        return subHeading
    }()

    lazy var message: UILabel = {
        let message                                        = UILabel()
        message.textColor                                  = AppSetUp.textColor
        message.translatesAutoresizingMaskIntoConstraints  = false
        message.numberOfLines                              = 0
        message.text                                       = "Check you internet connection and try again"
        message.font                                       = UIFont(name: "Poppins-Regular", size: 17)
        message.textAlignment                              = .center
        return message
    }()

    lazy var refreshButton: UIButton = {
        let refreshButton                                        = UIButton()
        refreshButton.backgroundColor                            = AppSetUp.colorAccent
        refreshButton.setTitle("    REFRESH    ", for: .normal)
        refreshButton.translatesAutoresizingMaskIntoConstraints  = false
        refreshButton.titleLabel?.font                           = UIFont(name: "Poppins-SemiBold", size: 17)
        refreshButton.layer.cornerRadius                         = 5
        return refreshButton
    }()


    func displayNoInternetView(controller:UIViewController){

        Vibration.error.vibrate()
        if let window = UIApplication.shared.keyWindow {

            window.addSubview(backView)
            //  MARK: - Setting up backview
            //
            backView.snp.makeConstraints { (make) in
                make.edges.equalTo(controller.view)
            }

            backView.addSubview(header)
            header.snp.makeConstraints { (make) in
                make.top.equalTo(20)
                make.left.equalTo(10)
                make.right.equalTo(-10)
                make.height.equalTo(50)
            }

            backView.addSubview(noInternetImage)
            noInternetImage.snp.makeConstraints { (make) in
                make.top.equalTo(self.header.snp.bottom).offset(20)
                make.centerX.equalTo(backView)
                make.height.equalTo(150)
                make.width.equalTo(170)
            }
            backView.addSubview(subHeading)
            subHeading.snp.makeConstraints { (make) in
                make.top.equalTo(self.noInternetImage.snp.bottom).offset(20)
                make.left.equalTo(10)
                make.right.equalTo(-10)
            }

            backView.addSubview(message)
            message.snp.makeConstraints { (make) in
                make.top.equalTo(self.subHeading.snp.bottom).offset(0)
                make.left.equalTo(10)
                make.right.equalTo(-10)
                make.height.equalTo(80)
            }

            backView.addSubview(refreshButton)
            //refreshButton.topAnchor.constraint(equalTo: message.bottomAnchor, constant: 20).isActive = true
            refreshButton.centerXAnchor.constraint(equalTo: backView.centerXAnchor).isActive = true
            refreshButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
            refreshButton.bottomAnchor.constraint(equalTo: backView.bottomAnchor, constant: -20).isActive = true
            refreshButton.rx.tap.subscribe { (onTap) in
                self.handleDismiss()
            }.disposed(by: AppSetUp.disposeBag)

            UIView.animate(withDuration: 0.5, animations: {
                self.backView.alpha  = 1
            })
        }
    }


    func makeitErrorView(){
        self.header.text            = "Error occured"
        self.subHeading.text        = "Oops something went wrong."
        self.noInternetImage.image  = UIImage(named: "erroroccured")
        self.message.text           = "Please try again."
    }

    @objc func handleDismiss() {
        UIView.animate(withDuration: 0.5) {
            self.backView.alpha  = 0
        }
    }


    override init() {
        super.init()
        //start doing something here maybe....
    }
}
