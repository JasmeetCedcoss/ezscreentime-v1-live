//
//  ParentOverviewPopup.swift
//  EZScreenTime
//
//  Created by cedcoss on 27/03/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import Foundation

class ParentOverviewPopup: NSObject {

    var dataPopup: ParentOverviewModel?{
        didSet{
            backView.overviewData = dataPopup
        }
    }

    var backView = ParentOverviewXib()
    let blackView = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffect.Style.regular))


    func showLoginPopup(kidName: String){
        if let window = UIApplication.shared.keyWindow {
            backView = ParentOverviewXib(frame: CGRect(x: 0, y: 0, width: window.bounds.width - 50, height: window.bounds.height - 100))
            backView.center                 = window.center
            blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleDismiss)))
            window.addSubview(blackView)
            blackView.frame                 = window.frame
            blackView.alpha                 = 0
            backView.alpha                  = 0

            //  MARK: - Data set
            backView.name.text              = """
            \(kidName)

            Today's overview:
            """
            backView.close.rx.tap.subscribe { (onTap) in
                self.handleDismiss()
            }.disposed(by: AppSetUp.disposeBag)

            window.addSubview(backView)

            UIView.animate(withDuration: 0.5, animations: {
                self.blackView.alpha    = 1
                self.backView.alpha   = 1
            })
        }
    }

    @objc func handleDismiss() {
        UIView.animate(withDuration: 0.5) {
            self.blackView.alpha = 0
            self.backView.alpha  = 0
        }
    }

    override init() {
        super.init()
        //start doing something here maybe....
    }
}




