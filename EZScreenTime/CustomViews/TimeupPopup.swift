//
//  TimeupPopup.swift
//  EZScreenTime
//
//  Created by cedcoss on 23/04/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit
import AVFoundation


class TimeupPopup: NSObject {

    let blackView = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffect.Style.regular))

    lazy var backView: UIView = {
        let backView                                        = UIView()
        backView.translatesAutoresizingMaskIntoConstraints  = false
        backView.backgroundColor                            = .white
        backView.layer.cornerRadius                         = 10
        return backView
    }()

    lazy var timerImage: UIImageView = {
        let timerImage                                        = UIImageView()
        timerImage.image                                      = UIImage(named: "clock")
        timerImage.translatesAutoresizingMaskIntoConstraints  = false
        return timerImage
    }()

    lazy var customLabel: UILabel = {
        let customLabel                                        = UILabel()
        customLabel.textColor                                  = UIColor.black
        customLabel.translatesAutoresizingMaskIntoConstraints  = false
        customLabel.numberOfLines                              = 0
        customLabel.text                                       = "TIMER IS UP FOR TODAY"
        customLabel.font                                       = UIFont(name: "Poppins-Regular", size: 18)
        return customLabel
    }()

    func showTimeupPopup(){

        Vibration.error.vibrate()

        if let window = UIApplication.shared.keyWindow {
            blackView.backgroundColor = UIColor(white: 0, alpha: 0.5)
            blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleDismiss)))
            window.addSubview(blackView)
            blackView.frame = window.frame
            blackView.alpha = 0
            backView.alpha  = 0
            window.addSubview(backView)

            //  MARK: - Setting up backview
            //
            backView.centerYAnchor.constraint(equalTo: window.centerYAnchor).isActive = true
            backView.centerXAnchor.constraint(equalTo: window.centerXAnchor).isActive = true
            backView.heightAnchor.constraint(equalToConstant: 260).isActive           = true
            backView.widthAnchor.constraint(equalToConstant:window.frame.width - 50).isActive = true

            backView.addSubview(timerImage)
            timerImage.snp.makeConstraints { (make) in
                make.centerX.equalTo(backView.snp.centerX)
                make.height.width.equalTo(150)
                make.top.equalTo(20)
            }


            backView.addSubview(customLabel)
            customLabel.snp.makeConstraints { (make) in
                make.centerX.equalTo(backView.snp.centerX)
                make.top.equalTo(timerImage.snp.bottom).offset(20)
            }

            UIView.animate(withDuration: 0.5, animations: {
                self.blackView.alpha = 1
                self.backView.alpha  = 1
            })
        }
    }

    @objc func handleDismiss() {
        UIView.animate(withDuration: 0.5) {
            self.blackView.alpha = 0
            self.backView.alpha  = 0
        }
    }

    override init() {
        super.init()
        //start doing something here maybe....
    }
}
