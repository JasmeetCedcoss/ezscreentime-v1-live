//
//  showKidLoginPopup.swift
//  EZScreenTime
//
//  Created by cedcoss on 28/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import Foundation
import UIKit

class ShowLoginPopup: NSObject {

    //let blackView = UIView()
    let blackView = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffect.Style.regular))
    lazy var backView: UIView = {
        let backView                                        = UIView()
        backView.translatesAutoresizingMaskIntoConstraints  = false
        backView.backgroundColor                            = .white
        backView.layer.cornerRadius                         = 5
        return backView
    }()

    lazy var customLabel: UILabel = {
        let customLabel                                        = UILabel()
        customLabel.textColor                                  = UIColor.white
        customLabel.translatesAutoresizingMaskIntoConstraints  = false
        customLabel.numberOfLines                              = 0
        customLabel.text                                       = "LOGIN AS KID"
        customLabel.textAlignment                              = .center
        customLabel.backgroundColor                            = AppSetUp.colorPrimary
        if AppSetUp.isIpad{
            customLabel.font                                   = UIFont(name: "Poppins-SemiBold", size: 20)
        }else{
            customLabel.font                                   = UIFont(name: "Poppins-SemiBold", size: 17)
        }
        return customLabel
    }()


    lazy var containerView: UIView = {
        let containerView                                        = UIView()
        containerView.translatesAutoresizingMaskIntoConstraints  = false
        containerView.layer.cornerRadius                         = 5
        containerView.layer.borderColor                          = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
        containerView.layer.borderWidth                          = 2

        return containerView
    }()

    lazy var imageButton: UIButton = {
        let imageButton                                        = UIButton()
        imageButton.backgroundColor                            = AppSetUp.controllerBackgroundColor
        imageButton.translatesAutoresizingMaskIntoConstraints  = false
        imageButton.setImage(UIImage(named: "lock_64"), for: .normal)
        imageButton.imageEdgeInsets                            = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        return imageButton
    }()

    lazy var codeTextField: UITextField = {
        let codeTextField                                        = UITextField()
        codeTextField.placeholder                                = "Your unique code"
        codeTextField.translatesAutoresizingMaskIntoConstraints  = false
        if AppSetUp.isIpad{
            codeTextField.font = UIFont(name: "Poppins-SemiBold", size: 20)
        }else{
            codeTextField.font = UIFont(name: "Poppins-SemiBold", size: 17)
        }
        return codeTextField
    }()

    lazy var LoginButton: UIButton = {
        let kidLoginButton                                        = UIButton()
        kidLoginButton.backgroundColor                            = AppSetUp.colorPrimary
        kidLoginButton.setTitle("LOGIN", for: .normal)
        kidLoginButton.translatesAutoresizingMaskIntoConstraints  = false
        kidLoginButton.layer.cornerRadius                         = 5
        if AppSetUp.isIpad{
            kidLoginButton.titleLabel?.font = UIFont(name: "Poppins-SemiBold", size: 20)
        }else{
            kidLoginButton.titleLabel?.font = UIFont(name: "Poppins-SemiBold", size: 17)
        }
        return kidLoginButton
    }()

    func showLoginPopup(){
        if let window = UIApplication.shared.keyWindow {
            blackView.backgroundColor = UIColor(white: 0, alpha: 0.5)
            blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleDismiss)))
            window.addSubview(blackView)
            blackView.frame = window.frame
            blackView.alpha = 0
            backView.alpha  = 0
            window.addSubview(backView)

            //  MARK: - Setting up backview
            //
            backView.centerYAnchor.constraint(equalTo: window.centerYAnchor).isActive = true
            backView.centerXAnchor.constraint(equalTo: window.centerXAnchor).isActive = true
            backView.heightAnchor.constraint(equalToConstant: 260).isActive           = true
            backView.widthAnchor.constraint(equalToConstant:window.frame.width - 50).isActive = true

            backView.addSubview(customLabel)
            customLabel.topAnchor.constraint(equalTo: backView.topAnchor).isActive = true
            customLabel.leadingAnchor.constraint(equalTo: backView.leadingAnchor).isActive = true
            customLabel.trailingAnchor.constraint(equalTo: backView.trailingAnchor).isActive = true
            customLabel.heightAnchor.constraint(equalToConstant: 60).isActive = true

            backView.addSubview(containerView)
            containerView.topAnchor.constraint(equalTo: customLabel.bottomAnchor, constant: 30).isActive = true
            containerView.leadingAnchor.constraint(equalTo: backView.leadingAnchor, constant: 20).isActive = true
            containerView.trailingAnchor.constraint(equalTo: backView.trailingAnchor, constant: -20).isActive = true
            containerView.heightAnchor.constraint(equalToConstant: 50).isActive = true

            containerView.addSubview(imageButton)
            imageButton.leadingAnchor.constraint(equalTo: containerView.leadingAnchor).isActive = true
            imageButton.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
            imageButton.bottomAnchor.constraint(equalTo: containerView.bottomAnchor).isActive = true
            imageButton.widthAnchor.constraint(equalToConstant:50).isActive = true

            containerView.addSubview(codeTextField)
            codeTextField.leadingAnchor.constraint(equalTo: imageButton.trailingAnchor, constant: 8).isActive = true
            codeTextField.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: 0).isActive = true
            codeTextField.bottomAnchor.constraint(equalTo: containerView.bottomAnchor).isActive = true
            codeTextField.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true

            backView.addSubview(LoginButton)
            LoginButton.topAnchor.constraint(equalTo: containerView.bottomAnchor, constant: 30).isActive = true
            LoginButton.centerXAnchor.constraint(equalTo: backView.centerXAnchor).isActive = true
            LoginButton.widthAnchor.constraint(equalToConstant: 170).isActive = true
            LoginButton.heightAnchor.constraint(equalToConstant: 50).isActive = true

            UIView.animate(withDuration: 0.5, animations: {
                self.blackView.alpha = 1
                self.backView.alpha  = 1
            })
        }
    }

    @objc func handleDismiss() {
        UIView.animate(withDuration: 0.5) {
            self.blackView.alpha = 0
            self.backView.alpha  = 0
        }
    }

    override init() {
        super.init()
        //start doing something here maybe....
    }
}
