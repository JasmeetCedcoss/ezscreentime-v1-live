//
//  ForgotPasswordPopup.swift
//  EZScreenTime
//
//  Created by cedcoss on 25/03/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import Foundation

class ForgotPasswordPopup: NSObject {

    let blackView = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffect.Style.regular))
    lazy var backView: UIView = {
        let backView                                        = UIView()
        backView.translatesAutoresizingMaskIntoConstraints  = false
        backView.backgroundColor                            = .white
        backView.layer.cornerRadius                         = 5
        return backView
    }()

    lazy var customHeader: UILabel = {
        let customHeader                                        = UILabel()
        customHeader.textColor                                  = AppSetUp.textColor
        if AppSetUp.isIpad{
            customHeader.font = UIFont(name: "Poppins-SemiBold", size: 22)
        }else{
            customHeader.font = UIFont(name: "Poppins-SemiBold", size: 16)
        }
        customHeader.text                                       = "You forgot your password"
        customHeader.translatesAutoresizingMaskIntoConstraints  = false
        //customHeader.font = UIFont.systemFont(ofSize: 25, weight: .medium)
        customHeader.textAlignment                              = .center
        customHeader.numberOfLines                              = 0

        return customHeader
    }()

    lazy var customSubHeading: UILabel = {
        let customSubHeading                                        = UILabel()
        customSubHeading.textColor                                  = AppSetUp.textColor
        customSubHeading.translatesAutoresizingMaskIntoConstraints  = false
        customSubHeading.numberOfLines                              = 0
        if AppSetUp.isIpad{
            customSubHeading.font = UIFont(name: "Poppins-Regular", size: 20)
        }else{
            customSubHeading.font = UIFont(name: "Poppins-Regular", size: 16)
        }
        customSubHeading.text                                       = "No worries enter your registered email below and we will send you reset code."
        customSubHeading.textAlignment                              = .center
        return customSubHeading
    }()

    lazy var stackView: UIStackView = {
        let stackView                                        = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints  = false
        stackView.distribution                               = .fillEqually
        stackView.spacing                                    = 5
        stackView.alignment                                  = .fill
        stackView.axis                                       = .vertical
        return stackView
    }()

    lazy var emailTextField: UITextField = {
        let emailTextField                                        = UITextField()
        emailTextField.placeholder                                = "Enter your email"
        emailTextField.translatesAutoresizingMaskIntoConstraints  = false
        emailTextField.borderStyle                                = .roundedRect
        return emailTextField
    }()

    lazy var getResetCode: UIButton = {
        let getResetCode                                        = UIButton()
        getResetCode.backgroundColor                            = AppSetUp.colorAccent
        getResetCode.setTitle("   SEND RESET CODE   ", for: .normal)
        getResetCode.translatesAutoresizingMaskIntoConstraints  = false
        getResetCode.titleLabel?.font                           = UIFont(name: "Poppins-SemiBold", size: 17)
        getResetCode.layer.cornerRadius                         = 5
        return getResetCode
    }()

    func showLoginPopup(){
        if let window = UIApplication.shared.keyWindow {
            blackView.backgroundColor = UIColor(white: 0, alpha: 0.5)
            blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleDismiss)))
            window.addSubview(blackView)
            blackView.frame = window.frame
            blackView.alpha = 0
            backView.alpha  = 0
            window.addSubview(backView)

            //  MARK: - Setting up backview
            //
            backView.centerYAnchor.constraint(equalTo: window.centerYAnchor).isActive = true
            backView.centerXAnchor.constraint(equalTo: window.centerXAnchor).isActive = true
            backView.heightAnchor.constraint(equalToConstant: 280).isActive           = true
            backView.widthAnchor.constraint(equalToConstant:window.frame.width - 50).isActive = true

            backView.addSubview(customHeader)
            customHeader.topAnchor.constraint(equalTo: backView.topAnchor, constant: 20).isActive = true
            customHeader.centerXAnchor.constraint(equalTo: backView.centerXAnchor).isActive = true
            customHeader.heightAnchor.constraint(equalToConstant: 30).isActive = true

            backView.addSubview(customSubHeading)
            customSubHeading.topAnchor.constraint(equalTo: customHeader.bottomAnchor, constant: 0).isActive = true
            customSubHeading.leadingAnchor.constraint(equalTo: backView.leadingAnchor, constant: 15).isActive = true
            customSubHeading.trailingAnchor.constraint(equalTo: backView.trailingAnchor, constant: -15).isActive = true

            backView.addSubview(stackView)
            stackView.topAnchor.constraint(equalTo: customSubHeading.bottomAnchor, constant: 0).isActive = true
            stackView.leadingAnchor.constraint(equalTo: backView.leadingAnchor, constant: 15).isActive = true
            stackView.trailingAnchor.constraint(equalTo: backView.trailingAnchor, constant: -15).isActive = true
            stackView.heightAnchor.constraint(equalToConstant: 50).isActive = true
            stackView.addArrangedSubview(emailTextField)

            backView.addSubview(getResetCode)
            getResetCode.topAnchor.constraint(equalTo: stackView.bottomAnchor, constant: 20).isActive = true
            getResetCode.centerXAnchor.constraint(equalTo: backView.centerXAnchor).isActive = true
            getResetCode.heightAnchor.constraint(equalToConstant: 50).isActive = true
            getResetCode.bottomAnchor.constraint(equalTo: backView.bottomAnchor, constant: -20).isActive = true

            UIView.animate(withDuration: 0.5, animations: {
                self.blackView.alpha = 1
                self.backView.alpha  = 1
            })
        }
    }


    func makeOTPView(){
        customHeader.text = "Check your inbox"
        customSubHeading.text = "A 6-digit confirmation code has been sent to your email."
        emailTextField.text = ""
        emailTextField.placeholder = "Enter OTP"
        getResetCode.setTitle("  SUBMIT OTP  ", for: .normal)
    }

    @objc func handleDismiss() {
        UIView.animate(withDuration: 0.5) {
            self.blackView.alpha = 0
            self.backView.alpha  = 0
        }
    }

    func backToBasic(){
        customHeader.text = "You forgot your password"
        customSubHeading.text = "No worries enter your registered email below and we will send you reset code."
        emailTextField.text = ""
        emailTextField.placeholder = "Enter your email"
        getResetCode.setTitle("   SEND RESET CODE   ", for: .normal)
    }

    func makeUpdatePasswordView(){
        customHeader.text = "Update password"
        customSubHeading.text = "Enter your new password here"
        getResetCode.setTitle("  UPDATE PASSWORD  ", for: .normal)
        emailTextField.text = ""
        emailTextField.placeholder = "Enter new password"

    }

    override init() {
        super.init()
    }
}
