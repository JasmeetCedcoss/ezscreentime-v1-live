//
//  ParentOverviewXib.swift
//  EZScreenTime
//
//  Created by cedcoss on 27/03/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

class ParentOverviewXib: UIView {
    var view: UIView!

    var overviewData: ParentOverviewModel?{
        didSet{
            print("See==",overviewData)
            tableView.reloadData()
        }
    }

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var close: UIButton!

    override init(frame: CGRect)
    {
        // 1. setup any properties here
        // 2. call super.init(frame:)
        super.init(frame: frame)

        // 3. Setup view from .xib file
        xibSetup()
    }

    required init?(coder aDecoder: NSCoder)
    {
        // 1. setup any properties here
        // 2. call super.init(coder:)
        super.init(coder: aDecoder)

        // 3. Setup view from .xib file
        xibSetup()
    }

    func xibSetup()
    {
        view = loadViewFromNib()
        // use bounds not frame or it'll be offset
        view.frame = bounds
        // Make the view stretch with containing view
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
        view.layer.cornerRadius     = 10
        close.layer.cornerRadius    = 5
        tableView.delegate          = self
        tableView.dataSource        = self
        tableView.register(ParentOverviewTVCell.self, forCellReuseIdentifier: "ParentOverviewTVCell")
        tableView.separatorStyle    = .none
        name.font = UIFont(name: "Poppins-SemiBold", size: 17)
    }

    func loadViewFromNib() -> UIView
    {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName:"ParentOverviewXib", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
}

extension ParentOverviewXib:UITableViewDelegate,UITableViewDataSource{

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let count = overviewData?.data?.devicesUsage?.count else{return 0}
        print("===",count)
        return count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ParentOverviewTVCell", for: indexPath) as! ParentOverviewTVCell
        let data = overviewData?.data?.devicesUsage?[indexPath.row]
        cell.data = data
        cell.backgroundColor    = AppSetUp.controllerBackgroundColor
        guard let deviceName = data?.name, let totalCoin = data?.totalUsageTime, let coinSpend = data?.coinsSpend else {return UITableViewCell()}
        cell.headerLabel.text   = "  \(deviceName)  \(totalCoin) min  (\(coinSpend)coins)"
        cell.setupView()
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (CGFloat(overviewData?.data?.devicesUsage?[indexPath.row].usage?.count ?? 0) * 60) + CGFloat(50)
    }
}
