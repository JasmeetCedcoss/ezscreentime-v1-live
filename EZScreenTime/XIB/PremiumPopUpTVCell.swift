//
//  PremiumPopUpTVCell.swift
//  EZScreenTime
//
//  Created by cedcoss on 15/04/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

class PremiumPopUpTVCell: UITableViewCell {

    private(set) var disposeBag = DisposeBag()

    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag() // because life cicle of every cell ends on prepare for reuse
    }

    @IBOutlet weak var noButton: UIButton!
    @IBOutlet weak var yesHelp: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        noButton.layer.cornerRadius = 5
        yesHelp.layer.cornerRadius  = 5
    }
}
