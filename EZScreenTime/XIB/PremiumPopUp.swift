//
//  PremiumPopUp.swift
//  EZScreenTime
//
//  Created by cedcoss on 15/04/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import Foundation

class PremiumPopUp: UIView {

    var view: UIView!

    @IBOutlet weak var tableView: UITableView!

    override init(frame: CGRect)
    {
        // 1. setup any properties here

        // 2. call super.init(frame:)
        super.init(frame: frame)

        // 3. Setup view from .xib file
        xibSetup()
    }

    required init?(coder aDecoder: NSCoder)
    {
        // 1. setup any properties here
        // 2. call super.init(coder:)
        super.init(coder: aDecoder)

        // 3. Setup view from .xib file
        xibSetup()
    }

    func xibSetup()
    {
        view = loadViewFromNib()
        // use bounds not frame or it'll be offset
        view.frame = bounds
        // Make the view stretch with containing view
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
        view.layer.cornerRadius     = 10
        tableView.delegate          = self
        tableView.dataSource        = self
        let nibCell = UINib(nibName: "PremiumPopUpTVCell", bundle: nil)
        tableView.register(nibCell, forCellReuseIdentifier: "PremiumPopUpTVCell")
        tableView.separatorStyle    = .none
        tableView.backgroundColor   = AppSetUp.controllerBackgroundColor
    }

    func loadViewFromNib() -> UIView
    {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName:"PremiumPopUp", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
}

extension PremiumPopUp:UITableViewDelegate,UITableViewDataSource{

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PremiumPopUpTVCell", for: indexPath) as! PremiumPopUpTVCell
        cell.noButton.rx.tap.subscribe { (onTap) in
           print("Hello")
        }.disposed(by: cell.disposeBag)
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 1080
    }
}

