//
//  CustomDevice.swift
//  EZScreenTime
//
//  Created by cedcoss on 11/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

class CustomDevice: UIView {

    var view: UIView!
    var parent  = AddDevicesController()

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var deviceNameField: UITextField!
    @IBOutlet weak var selectDevice: UIButton!
    @IBOutlet weak var addCustomDevice: UIButton!

    override init(frame: CGRect)
    {
        // 1. setup any properties here

        // 2. call super.init(frame:)
        super.init(frame: frame)

        // 3. Setup view from .xib file
        xibSetup()
    }

    required init?(coder aDecoder: NSCoder)
    {
        // 1. setup any properties here

        // 2. call super.init(coder:)
        super.init(coder: aDecoder)

        // 3. Setup view from .xib file
        xibSetup()
    }

    func xibSetup()
    {
        view = loadViewFromNib()
        // use bounds not frame or it'll be offset
        view.frame = bounds
        // Make the view stretch with containing view
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addCustomDevice.JSCustomView()
        // mainView.JSCustomView()
//        mainView.layer.borderColor     = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
//        mainView.layer.borderWidth     = 2
//        mainView.layer.cornerRadius    = 10
        addCustomDevice.backgroundColor = AppSetUp.colorAccent
        self.tag = 500
        addSubview(view)
    }

    func loadViewFromNib() -> UIView
    {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName:"CustomDevice", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }


}
