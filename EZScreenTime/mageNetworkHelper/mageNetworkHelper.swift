//
//  mageHelper.swift
//  Swister
//
//  Created by Macmini on 11/06/19.
//  Copyright © 2019 Macmini. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class mageHelper : NSObject {

    var latestToken     : [AppInfo]?
    let noInternetView  = NoInternetView()

    func showAlert (view:UIViewController,msg:String,title:String,completion: @escaping(String?)->Void){
        let alertView = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        alertView.addAction(UIAlertAction(title: "OK", style: .default, handler: {
            Void in
            completion("OK")
        }))
        alertView.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { Void in
            completion("Cancel")
        }))
        view.present(alertView, animated: true, completion: nil)
    }

    //  MARK: - PARENT

    func callHttpRequest(controller:UIViewController,endPoints:String,parameters:Dictionary<String,String>?,sendAuthHeader: Bool,showLoader:Bool = true,postData:Bool = false,addStringToParam:String? = nil,completion:@escaping(_ data: Data)->Void){

        if showLoader{
            controller.view.addLoader()
        }else{
            print("Dont Show Loader")
        }
        let url              = AppSetUp.baseUrl+endPoints
        var jsonStringParams :NSString = ""

        guard let nsUrl         = URL(string:url) else {return}
        var request             = URLRequest(url: nsUrl)
        request.httpMethod      = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Rd8cDf3g8A2SQgTF", forHTTPHeaderField: "api_key")
        if sendAuthHeader{
            print("LatestToken==",AppSetUp().getLatestToken())
            guard let getLatestToken = AppSetUp().getLatestToken() else {return}
            request.setValue("Bearer " + getLatestToken, forHTTPHeaderField: "Authorization")
        }

        if(parameters != nil){
            if addStringToParam != nil{
                jsonStringParams = [addStringToParam!:parameters].convtToJson()
            }else{
                jsonStringParams=parameters!.convtToJson()
            }
            request.httpMethod = "POST"
        }

        print(url)
        print(jsonStringParams)

        if postData {
            request.httpMethod  = "POST"
            request.httpBody    = jsonStringParams.data(using: String.Encoding.utf8.rawValue)
        }

        Alamofire.request(request).responseJSON(completionHandler: {
            response in
            DispatchQueue.main.async {
                switch response.result {
                case .success:
                    controller.view.stopLoader()
                    guard let data = response.data else {return}
                    do {
                        print(NSString(data: data, encoding: String.Encoding.utf8.rawValue))
                        completion(data)
                    } catch let error {
                        print(error.localizedDescription)
                        print("magenetweork parsing catch block")
                        print(error)
                    }

                case .failure(let error):
                    controller.view.stopLoader()
                    //  MARK: - place the error occured image here
                    print(error.localizedDescription)
                    print(error)
                    print("You are in mageNetworkHelper class error block")
                    if error.localizedDescription == "The Internet connection appears to be offline."{
                        self.noInternetView.displayNoInternetView(controller: controller)
                    }else{
                        self.noInternetView.displayNoInternetView(controller: controller)
                        self.noInternetView.makeitErrorView()
                    }
                    guard let data = response.data else {return}
                    print(NSString(data: data, encoding: String.Encoding.utf8.rawValue))
                    print(error.localizedDescription)
                }
            }
        })
    }

    func callHttpRequestDelete(controller:UIViewController,endPoints:String,parameters:Dictionary<String,String>?,sendAuthHeader: Bool,showLoader:Bool = true,postData:Bool = false, completion:@escaping(_ data: Data)->Void){

        if showLoader{
            controller.view.addLoader()
        }else{
            print("Dont Show Loader")
        }
        let url              = AppSetUp.baseUrl+endPoints
        var jsonStringParams :NSString = ""

        guard let nsUrl         = URL(string:url) else {return}
        var request             = URLRequest(url: nsUrl)
        request.httpMethod      = "DELETE"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Rd8cDf3g8A2SQgTF", forHTTPHeaderField: "api_key")
        if sendAuthHeader{
            print("LatestToken==",AppSetUp().getLatestToken())
            guard let getLatestToken = AppSetUp().getLatestToken() else {return}
            request.setValue("Bearer " + getLatestToken, forHTTPHeaderField: "Authorization")
        }

        if(parameters != nil){
            request.httpMethod = "POST"
            jsonStringParams=parameters!.convtToJson()
        }

        print(url)
        print(jsonStringParams)

        if postData {
            request.httpMethod  = "POST"
            request.httpBody    = jsonStringParams.data(using: String.Encoding.utf8.rawValue)
        }

        Alamofire.request(request).responseJSON(completionHandler: {
            response in
            DispatchQueue.main.async {
                switch response.result {
                case .success:
                    controller.view.stopLoader()
                    guard let data = response.data else {return}
                    do {
                        print(NSString(data: data, encoding: String.Encoding.utf8.rawValue))
                        completion(data)
                    } catch let error {
                        print(error.localizedDescription)
                        print("magenetweork parsing catch block")
                        print(error)
                    }

                case .failure(let error):
                    controller.view.stopLoader()
                    print("You are in mageNetworkHelper class error block")
                    print(error.localizedDescription)
                    print(error)

                    if error.localizedDescription == "The Internet connection appears to be offline."{
                        self.noInternetView.displayNoInternetView(controller: controller)
                    }else{
                        self.noInternetView.displayNoInternetView(controller: controller)
                        self.noInternetView.makeitErrorView()
                    }

                    guard let data = response.data else {return}
                    print(NSString(data: data, encoding: String.Encoding.utf8.rawValue))
                    print(error.localizedDescription)
                }
            }
        })
    }

    func callHttpRequestForTimeMgmt(controller:UIViewController,endPoints:String,parameters:String?,sendAuthHeader: Bool,showLoader:Bool = true,postData:Bool = false,addStringToParam:String? = nil,completion:@escaping(_ data: Data)->Void){

        if showLoader{
            controller.view.addLoader()
        }else{
            print("Dont Show Loader")
        }
        let url              = AppSetUp.baseUrl+endPoints
        var jsonStringParams :NSString = ""

        guard let nsUrl         = URL(string:url) else {return}
        var request             = URLRequest(url: nsUrl)
        request.httpMethod      = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Rd8cDf3g8A2SQgTF", forHTTPHeaderField: "api_key")
        if sendAuthHeader{
            print("LatestToken==",AppSetUp().getLatestToken())
            guard let getLatestToken = AppSetUp().getLatestToken() else {return}
            request.setValue("Bearer " + getLatestToken, forHTTPHeaderField: "Authorization")
        }

        print(url)
        print(jsonStringParams)

        if postData {
            request.httpMethod  = "POST"
            request.httpBody    = parameters!.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        }

        Alamofire.request(request).responseJSON(completionHandler: {
            response in
            DispatchQueue.main.async {
                switch response.result {
                case .success:
                    controller.view.stopLoader()
                    guard let data = response.data else {return}
                    do {
                        print(NSString(data: data, encoding: String.Encoding.utf8.rawValue))
                        completion(data)
                    } catch let error {
                        print(error.localizedDescription)
                        print("magenetweork parsing catch block")
                        print(error)
                    }

                case .failure(let error):

                    controller.view.stopLoader()
                    //  MARK: - place the error occured image here
                    print(error.localizedDescription)
                    print(error)
                    print("You are in mageNetworkHelper class error block")
                    if error.localizedDescription == "The Internet connection appears to be offline."{
                        self.noInternetView.displayNoInternetView(controller: controller)
                    }else{
                        self.noInternetView.displayNoInternetView(controller: controller)
                        self.noInternetView.makeitErrorView()
                    }

                    guard let data = response.data else {return}
                    print(NSString(data: data, encoding: String.Encoding.utf8.rawValue))
                    print(error.localizedDescription)
                }
            }
        })
    }



    //  MARK:  Get settings request
    func callSettings(controller:UIViewController,endPoints: String = "parent/settings/configuration",sendAuthHeader: Bool = true,showLoader:Bool = true,completion:@escaping(_ data: Bool)->Void){

        if showLoader{
            controller.view.addLoader()
        }else{
            print("Dont Show Loader")
        }
        let url              = AppSetUp.baseUrl+endPoints
        let jsonStringParams :NSString = ""

        guard let nsUrl         = URL(string:url) else {return}
        var request             = URLRequest(url: nsUrl)
        request.httpMethod      = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Rd8cDf3g8A2SQgTF", forHTTPHeaderField: "api_key")
        if sendAuthHeader{
            print("LatestToken==",AppSetUp().getLatestToken())
            guard let getLatestToken = AppSetUp().getLatestToken() else {return}
            request.setValue("Bearer " + getLatestToken, forHTTPHeaderField: "Authorization")
        }
        print(url)
        print(jsonStringParams)

        Alamofire.request(request).responseJSON(completionHandler: {
            response in
            DispatchQueue.main.async {
                switch response.result {
                case .success:
                    controller.view.stopLoader()
                    guard let data = response.data else {return}
                    do {

                        print(NSString(data: data, encoding: String.Encoding.utf8.rawValue))
                        let json = try JSON(data: data)
                        if json["status"].boolValue == true{
                            //  MARK: - Save latest token
                            //
                            let token = json["data"]["access_token"].stringValue
                            AppSetUp().saveToken(token)
                            var settingConfigurationData    = [String:String]()
                            for (key,value) in json["data"]["configuration"] {
                                print("seee",key)
                                settingConfigurationData[key] = value.stringValue
                            }
                            if settingConfigurationData.values.contains("false"){
                                print("❌❌ Settings Incomplete ❌❌")
                                completion(false)
                            }else{
                                print("✅✅ Settings Complete ✅✅")
                                completion(true)
                            }
                        }
                    } catch let error {
                        print(error.localizedDescription)
                        print("magenetweork parsing catch block")
                        print(error)
                    }

                case .failure(let error):
                    controller.view.stopLoader()
                    //  MARK: - place the error occured image here
                    print(error.localizedDescription)
                    print(error)
                    print("You are in mageNetworkHelper class error block")
                    if error.localizedDescription == "The Internet connection appears to be offline."{
                        self.noInternetView.displayNoInternetView(controller: controller)
                    }else{
                        self.noInternetView.displayNoInternetView(controller: controller)
                        self.noInternetView.makeitErrorView()
                    }
                    guard let data = response.data else {return}
                    print(NSString(data: data, encoding: String.Encoding.utf8.rawValue))
                    print(error.localizedDescription)
                }
            }
        })
    }


    //  MARK: - KID SECTION REQUESTS
    //
    func callHttpRequestForKid(controller:UIViewController,endPoints:String,parameters:Dictionary<String,String>?,sendAuthHeader: Bool,showLoader:Bool = true,postData:Bool = false,kidID: String,addStringToParam:String? = nil,completion:@escaping(_ data: Data)->Void){
        if showLoader{
            controller.view.addLoader()
        }else{
            print("Dont Show Loader")
        }
        let url              = AppSetUp.baseUrl+endPoints
        var jsonStringParams :NSString = ""

        guard let nsUrl         = URL(string:url) else {return}
        var request             = URLRequest(url: nsUrl)
        request.httpMethod      = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Rd8cDf3g8A2SQgTF", forHTTPHeaderField: "api_key")
        if sendAuthHeader{
            print("token requested for kidid == ", kidID)
            guard let getLatestToken = KidSetupSettings().getTokenForKidFromDB(with: kidID) else {return}
            request.setValue("Bearer " + getLatestToken, forHTTPHeaderField: "Authorization")
            print("LatestToken==",getLatestToken)
        }

        if(parameters != nil){
            if addStringToParam != nil{
                jsonStringParams = [addStringToParam!:parameters].convtToJson()
            }else{
                jsonStringParams=parameters!.convtToJson()
            }
            request.httpMethod = "POST"
        }

        print(url)
        print(jsonStringParams)

        if postData {
            request.httpMethod  = "POST"
            request.httpBody    = jsonStringParams.data(using: String.Encoding.utf8.rawValue)
        }

        Alamofire.request(request).responseJSON(completionHandler: {
            response in
            DispatchQueue.main.async {
                switch response.result {
                case .success:
                    controller.view.stopLoader()
                    guard let data = response.data else {return}
                    do{
                        print(NSString(data: data, encoding: String.Encoding.utf8.rawValue))
                        completion(data)
                    }catch let error {
                        print(error.localizedDescription)
                        print("magenetweork parsing catch block")
                        print(error)
                    }
                case .failure(let error):

                    controller.view.stopLoader()
                    //  MARK: - place the error occured image here
                    print(error.localizedDescription)
                    print(error)
                    print("You are in mageNetworkHelper class error block")
                    if error.localizedDescription == "The Internet connection appears to be offline."{
                        self.noInternetView.displayNoInternetView(controller: controller)
                    }else{
                        self.noInternetView.displayNoInternetView(controller: controller)
                        self.noInternetView.makeitErrorView()
                    }
                    guard let data = response.data else {return}
                    print(NSString(data: data, encoding: String.Encoding.utf8.rawValue))
                    print(error.localizedDescription)
                }
            }
        })
    }



    //  MARK: - SUPERVISOR SECTION
    //

    func callHttpRequestSupervisor (controller:UIViewController,endPoints:String,parameters:Dictionary<String,String>?,sendAuthHeader: Bool,showLoader:Bool = true,postData:Bool = false,addStringToParam:String? = nil,completion:@escaping(_ data: Data)->Void){

        if showLoader{
            controller.view.addLoader()
        }else{
            print("Dont Show Loader")
        }
        let url              = AppSetUp.baseUrl+endPoints
        var jsonStringParams :NSString = ""

        guard let nsUrl         = URL(string:url) else {return}
        var request             = URLRequest(url: nsUrl)
        request.httpMethod      = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Rd8cDf3g8A2SQgTF", forHTTPHeaderField: "api_key")
        if sendAuthHeader{
            print("LatestToken==",AppSetUp().getLatestToken())
            guard let getLatestToken = SupervisorSetupSettings.getsupervisorToken else {return}
            request.setValue("Bearer " + getLatestToken, forHTTPHeaderField: "Authorization")
        }

        if(parameters != nil){
            if addStringToParam != nil{
                jsonStringParams = [addStringToParam!:parameters].convtToJson()
            }else{
                jsonStringParams=parameters!.convtToJson()
            }
            request.httpMethod = "POST"
        }

        print(url)
        print(jsonStringParams)

        if postData {
            request.httpMethod  = "POST"
            request.httpBody    = jsonStringParams.data(using: String.Encoding.utf8.rawValue)
        }

        Alamofire.request(request).responseJSON(completionHandler: {
            response in
            DispatchQueue.main.async {
                switch response.result {
                case .success:
                    controller.view.stopLoader()
                    guard let data = response.data else {return}
                    do {
                        print(NSString(data: data, encoding: String.Encoding.utf8.rawValue))
                        completion(data)
                    } catch let error {
                        print(error.localizedDescription)
                        print("magenetweork parsing catch block")
                        print(error)
                    }

                case .failure(let error):
                    controller.view.stopLoader()
                    //  MARK: - place the error occured image here
                    print(error.localizedDescription)
                    print(error)
                    print("You are in mageNetworkHelper class error block")
                    if error.localizedDescription == "The Internet connection appears to be offline."{
                        self.noInternetView.displayNoInternetView(controller: controller)
                    }else{
                        self.noInternetView.displayNoInternetView(controller: controller)
                        self.noInternetView.makeitErrorView()
                    }
                    guard let data = response.data else {return}
                    print(NSString(data: data, encoding: String.Encoding.utf8.rawValue))
                    print(error.localizedDescription)
                }
            }
        })
    }
}









