//
//  KidNotificationHistoryController.swift
//  EZScreenTime
//
//  Created by cedcoss on 14/03/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

class KidNotificationHistoryController: BaseViewControllerForKid {

    lazy var backView: UIView = {
        let backView                                        = UIView()
        backView.backgroundColor                            = AppSetUp.controllerBackgroundColor
        backView.translatesAutoresizingMaskIntoConstraints  = false
        return backView
    }()

    lazy var tableView: UITableView = {
        let tableView                                        = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints  = false
        return tableView
    }()

    var kidNotificationModel: KidNotificationModel?{
        didSet{
            self.tableView.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTable()
        setupDesign()
        makeNetworkCall()
        hideAd()
    }

    func setupTable(){
        tableView.separatorStyle    = .none
        tableView.delegate          = self
        tableView.dataSource        = self
        tableView.register(HeaderTVCell.self, forCellReuseIdentifier: "HeaderTVCell")
        tableView.register(KidNotificationTVCell.self, forCellReuseIdentifier: "KidNotificationTVCell")
        tableView.backgroundColor   = AppSetUp.controllerBackgroundColor
    }

    func setupDesign(){
        self.navigationItem.setHidesBackButton(true, animated: true);
        self.view.addSubview(backView)
        backView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        backView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        backView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        backView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -50).isActive = true

        backView.addSubview(tableView)
        tableView.topAnchor.constraint(equalTo: self.backView.topAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: self.backView.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: self.backView.trailingAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: self.backView.bottomAnchor).isActive = true
    }

    func makeNetworkCall(){
        mageHelper().callHttpRequestForKid(controller: self, endPoints: "kid/notificationHistory", parameters: nil,sendAuthHeader: true,kidID: CurrentLoggedKid.id) { (data) in
            do {
                let json = try JSON(data: data)
                print(json)
                if json["status"].boolValue == true{
                    AppSetUp.decoder.keyDecodingStrategy = .convertFromSnakeCase
                    self.kidNotificationModel = try? AppSetUp.decoder.decode(KidNotificationModel.self, from: data)
                    print("kidNotificationModel====",self.kidNotificationModel as Any)
                }else{
                    self.view.makeToast(json["message"].stringValue, duration: 3.0)
                }
            }
            catch let error{
                print(error.localizedDescription)
            }
        }
    }
}

extension KidNotificationHistoryController: UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return self.kidNotificationModel?.data?.notificationHistory?.count ?? 0
        default:
            return 0
        }
    }


    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderTVCell", for: indexPath) as! HeaderTVCell
            cell.setupView(header: "Notification",image: "settings",subheader: "")
            cell.backIcon.isHidden = true
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "KidNotificationTVCell", for: indexPath) as! KidNotificationTVCell
            cell.setupDesign()
            let notificationHistory     = self.kidNotificationModel?.data?.notificationHistory?[indexPath.row]
            cell.notificationHistory    = notificationHistory
            return cell
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case  1:
           if let selectedRows = tableView.indexPathsForSelectedRows, selectedRows.contains(indexPath) {
                return 200
            } else {
                return 140
            }
        default:
            return UITableView.automaticDimension
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.beginUpdates()
        tableView.endUpdates()
    }
}
