//
//  KidNotificationTVCell.swift
//  EZScreenTime
//
//  Created by cedcoss on 14/03/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

class KidNotificationTVCell: UITableViewCell {

    lazy var backView: UIView = {
        let backView                                        = UIView()
        backView.layer.borderWidth                          = 4
        backView.translatesAutoresizingMaskIntoConstraints  = false
        backView.layer.cornerRadius                         = 8
        return backView
    }()

    lazy var messagelabel: UILabel = {
        let messagelabel                                        = UILabel()
        messagelabel.textColor                                  = UIColor.black
        messagelabel.translatesAutoresizingMaskIntoConstraints  = false
        messagelabel.numberOfLines                              = 0
        if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular{
            messagelabel.font = UIFont(name: "Poppins-Regular", size: 22)
        }else{
            messagelabel.font = UIFont(name: "Poppins-Regular", size: 17)
        }
        return messagelabel
    }()

    lazy var parentImage: UIButton = {
        let parentImage                                           = UIButton()
        parentImage.backgroundColor                               = AppSetUp.controllerBackgroundColor
        parentImage.layer.borderColor                             = #colorLiteral(red: 0.007843137255, green: 0.337254902, blue: 0.5098039216, alpha: 1)
        parentImage.layer.borderWidth                             = 5
        parentImage.translatesAutoresizingMaskIntoConstraints     = false
        parentImage.imageEdgeInsets                               = UIEdgeInsets(top: 15, left: 18, bottom: 15, right: 18)
        parentImage.layer.cornerRadius                            = AppSetUp.isIpad ? 100 * 0.5 : 70 * 0.5
        parentImage.clipsToBounds                                 = true
        parentImage.contentMode                                   = .scaleAspectFit
        return parentImage
    }()


    var notificationHistory: NotificationHistory?{
        didSet{
            guard let title = notificationHistory?.title, let coins = notificationHistory?.numberOfCoins, let reason = notificationHistory?.reason, let message = notificationHistory?.message, let device = notificationHistory?.name,let colorSet = notificationHistory?.color else {return}
            messagelabel.text = "***\(title.uppercased())***  \n\n \(message) for  \(device)  \n\n \(reason)"
            parentImage.setImage(UIImage(named: notificationHistory?.parentIcon ?? ""), for: .normal)
            backView.backgroundColor = title == "Awarded" ? #colorLiteral(red: 0.721568644, green: 0.8862745166, blue: 0.5921568871, alpha: 1) : #colorLiteral(red: 0.9568627477, green: 0.6588235497, blue: 0.5450980663, alpha: 1)
            backView.layer.borderColor = title == "Awarded" ? #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1) : #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle                 = .none
        self.contentView.backgroundColor    = AppSetUp.controllerBackgroundColor

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

    func setupDesign(){
        self.contentView.backgroundColor    = AppSetUp.controllerBackgroundColor
        self.contentView.addSubview(backView)
        backView.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 10).isActive = true
        backView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 10).isActive = true
        backView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -10).isActive = true
        backView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -10).isActive = true

        self.backView.addSubview(parentImage)
        parentImage.centerYAnchor.constraint(equalTo: self.backView.centerYAnchor, constant: 0).isActive = true
        parentImage.leadingAnchor.constraint(equalTo: self.backView.leadingAnchor, constant: 10).isActive = true

        if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular{
            parentImage.widthAnchor.constraint(equalToConstant: 100).isActive = true
            parentImage.heightAnchor.constraint(equalToConstant: 100).isActive = true
        }else{
            parentImage.widthAnchor.constraint(equalToConstant: 70).isActive = true
            parentImage.heightAnchor.constraint(equalToConstant: 70).isActive = true
        }


        self.backView.addSubview(messagelabel)
        messagelabel.leadingAnchor.constraint(equalTo: self.parentImage.trailingAnchor, constant: 20).isActive = true
        messagelabel.trailingAnchor.constraint(equalTo: self.backView.trailingAnchor, constant: -20).isActive = true
        messagelabel.bottomAnchor.constraint(equalTo: self.backView.bottomAnchor, constant: -10).isActive = true
        messagelabel.topAnchor.constraint(equalTo: self.backView.topAnchor, constant: 10).isActive = true
    }
}
