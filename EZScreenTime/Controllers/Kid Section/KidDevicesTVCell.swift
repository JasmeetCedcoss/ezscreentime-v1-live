//
//  KidDevicesTVCell.swift
//  EZScreenTime
//
//  Created by cedcoss on 19/03/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

protocol addCoinTokidDevice {
    func addCoinTokidDevice(_cell: KidDevicesTVCell,sender: Any, device_id: String)
}

class KidDevicesTVCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var proceedButton: UIButton!


    private(set) var disposeBag = DisposeBag()

       override func prepareForReuse() {
           super.prepareForReuse()
           disposeBag = DisposeBag() // because life cicle of every cell ends on prepare for reuse
       }


    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.delegate     = self
        collectionView.dataSource   = self

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    var delegate    : addCoinTokidDevice?
    var kidDevices  : [Setting]?{
        didSet{
            self.collectionView.reloadData()
        }
    }
}

extension KidDevicesTVCell: UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return kidDevices?.count ?? 0
    }

    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "KidDevicesNotiCVCell", for: indexPath) as! KidDevicesNotiCVCell
        let device              = self.kidDevices?[indexPath.row]
        cell.deviceIcon.image   = UIImage(named: device?.icon ?? "")?.withRenderingMode(.alwaysTemplate)
        cell.deviceIcon.tintColor   = .black
        guard let tag           = Int((device?.id) ?? "") else {return UICollectionViewCell()}
        cell.tag                = tag
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/3 - 10, height: collectionView.frame.height)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let device = self.kidDevices?[indexPath.row]
        guard let device_id = device?.id else {return}
        print("\(self.kidDevices?[indexPath.row].id)")
        self.delegate?.addCoinTokidDevice(_cell: self, sender: (Any).self, device_id: device_id)
    }
}
