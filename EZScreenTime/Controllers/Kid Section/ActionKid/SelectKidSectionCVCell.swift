//
//  SelectKidSectionCVCell.swift
//  EZScreenTime
//
//  Created by cedcoss on 02/03/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

class SelectKidSectionCVCell: UICollectionViewCell {

    lazy var backView: UIView = {
         let backView                                        = UIView()
         backView.translatesAutoresizingMaskIntoConstraints  = false
         return backView
     }()

     lazy var customImageView: UIImageView = {
         let customImageView                                        = UIImageView()
         customImageView.contentMode                                = .scaleAspectFit
         customImageView.clipsToBounds                              = true
         customImageView.translatesAutoresizingMaskIntoConstraints  = false
         return customImageView
     }()

    lazy var customLabel: UILabel = {
        let customLabel                                        = UILabel()
        customLabel.textColor                                  = UIColor.black
        customLabel.translatesAutoresizingMaskIntoConstraints  = false
        customLabel.numberOfLines                              = 0
        customLabel.textAlignment                              = .center
        customLabel.adjustsFontSizeToFitWidth                  = true
        return customLabel
    }()

     override init(frame: CGRect) {
         super.init(frame:frame)
         setupView()
     }

     required init?(coder aDecoder: NSCoder) {
         fatalError("")
     }

    override var isSelected: Bool {
           didSet {
               if isSelected { // Selected cell
                self.layer.borderColor = #colorLiteral(red: 0.007843137255, green: 0.337254902, blue: 0.5098039216, alpha: 1)
               } else { // Normal cell
                self.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
               }
           }
       }

     func setupView(){
         self.layer.cornerRadius    = 5
         self.layer.borderWidth     = 2
         self.layer.borderColor     = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
         self.addSubview(backView)
         backView.topAnchor.constraint(equalTo: self.topAnchor,constant: 0).isActive = true
         backView.leadingAnchor.constraint(equalTo: self.leadingAnchor,constant: 0).isActive = true
         backView.trailingAnchor.constraint(equalTo: self.trailingAnchor,constant: 0).isActive = true
         backView.bottomAnchor.constraint(equalTo: self.bottomAnchor,constant: 0).isActive = true

         backView.addSubview(customImageView)
         customImageView.topAnchor.constraint(equalTo: backView.topAnchor,constant: 10).isActive = true
         customImageView.leadingAnchor.constraint(equalTo: backView.leadingAnchor,constant: 0).isActive = true
         customImageView.trailingAnchor.constraint(equalTo: backView.trailingAnchor,constant: 0).isActive = true
        backView.addSubview(customLabel)

        customLabel.topAnchor.constraint(equalTo: customImageView.bottomAnchor, constant: 0).isActive = true
         customLabel.leadingAnchor.constraint(equalTo: backView.leadingAnchor,constant: 5).isActive = true
                customLabel.trailingAnchor.constraint(equalTo: backView.trailingAnchor,constant: -5).isActive = true
        customLabel.bottomAnchor.constraint(equalTo: backView.bottomAnchor, constant: -5).isActive = true
        customLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        customImageView.bottomAnchor.constraint(equalTo: customLabel.topAnchor,constant: 0).isActive = true
     }


    var allKids: MultipleKidLoginDB?{
        didSet{
            customImageView.image = UIImage(named: allKids?.kid_icon ?? "placeholder")
            customLabel.text      = allKids?.kid_name?.uppercased()
        }
    }
}
