//
//  KidLoginWelcomeScreenTwo.swift
//  EZScreenTime
//
//  Created by cedcoss on 03/06/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

class KidLoginWelcomeScreenTwo: BaseViewControllerForKid {

    @IBOutlet weak var descriptiontext: UILabel!
    @IBOutlet weak var kidName: UILabel!
    @IBOutlet weak var kidIcon: UIImageView!
    @IBOutlet weak var kidIconContainer: UIView!
    //@IBOutlet weak var headerContainer: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var goToKidDashboard: UIButton!

    var kid: MultipleKidLoginDB?

    override func viewDidLoad() {
        super.viewDidLoad()
        hideAd()
        designSetup()
    }

    @IBAction func goToKidDashboard(_ sender: UIButton) {
        let viewControl = KidSetupSettings.storyBoard.instantiateViewController(withIdentifier: "KidDashboardController") as! KidDashboardController
        self.navigationController?.setViewControllers([viewControl], animated: true)
    }

    func designSetup(){
        kidName.text    = kid?.kid_name
        kidIcon.image   = UIImage(named: kid?.kid_icon ?? "placeholder")
        customTabbarView.isHidden = true
        kidIconContainer.layer.borderColor                       = #colorLiteral(red: 0.007843137255, green: 0.337254902, blue: 0.5098039216, alpha: 1)
        kidIconContainer.layer.borderWidth                       = 5
        kidIconContainer.layer.cornerRadius                      = AppSetUp.isIpad ? 130 * 0.5 : 90 * 0.5
        containerView.layer.maskedCorners                        = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        goToKidDashboard.layer.cornerRadius                      = 5
        guard let name                                           = kid?.kid_name else {return}
        descriptiontext.text                = "Congratulations \(name)"
    }
}
