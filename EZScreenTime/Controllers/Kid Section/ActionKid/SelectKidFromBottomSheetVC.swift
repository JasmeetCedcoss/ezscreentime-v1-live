//
//  SelectKidFromBottomSheetVC.swift
//  EZScreenTime
//
//  Created by cedcoss on 02/03/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit
import BottomPopup

protocol selectedKid {
    func selectedKid(_controller:SelectKidFromBottomSheetVC,sender:Any,id:String)
    func newKidLogin(_controller:SelectKidFromBottomSheetVC)
}


class SelectKidFromBottomSheetVC: BottomPopupViewController {

     var delegate                       : selectedKid?
     var height                         : CGFloat?
     var topCornerRadius                : CGFloat?
     var presentDuration                : Double?
     var dismissDuration                : Double?
     var shouldDismissInteractivelty    : Bool?
     var allKids                        : [MultipleKidLoginDB]?

    lazy var collectionView: UICollectionView = {
        let layout                  = UICollectionViewFlowLayout()
        layout.scrollDirection      = .horizontal
        let collection              = UICollectionView(frame: CGRect(x: 0, y: 0, width: 0, height: 0), collectionViewLayout: layout)
        collection.translatesAutoresizingMaskIntoConstraints = false
        collection.isScrollEnabled  = true
        collection.backgroundColor  = UIColor.white
        collection.showsHorizontalScrollIndicator = false
        return collection
    }()

    lazy var customLabel: UILabel = {
        let customLabel                                        = UILabel()
        customLabel.textColor                                  = UIColor.black
        customLabel.translatesAutoresizingMaskIntoConstraints  = false
        customLabel.numberOfLines                              = 0
        customLabel.text                                       = "Switch account"
        customLabel.font                                       = UIFont(name: "Poppins-SemiBold", size: 17)
        return customLabel
    }()

    lazy var newKidLoginButton: UIButton = {
           let newKidLoginButton                                        = UIButton()
           newKidLoginButton.backgroundColor                            = AppSetUp.colorAccent
           newKidLoginButton.setTitle("NEW KID LOGIN", for: .normal)
           newKidLoginButton.titleLabel?.font                           = UIFont(name: "Poppins-Regular", size: 17)
           newKidLoginButton.translatesAutoresizingMaskIntoConstraints  = false
           newKidLoginButton.layer.cornerRadius                            = 5
        newKidLoginButton.rx.tap.subscribe { (onTap) in
            self.dismiss(animated: true) {
                self.delegate?.newKidLogin(_controller: self)
            }
        }.disposed(by: AppSetUp.disposeBag)
           return newKidLoginButton
       }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        self.view.backgroundColor = .white
        getKids()
        // Do any additional setup after loading the view.
    }

    func getKids(){
        allKids = KidSetupSettings().getAllKidsFromDB()
    }

      func getPopupHeight() -> CGFloat {
          return height ?? CGFloat(300)
      }

      func getPopupTopCornerRadius() -> CGFloat {
          return topCornerRadius ?? CGFloat(10)
      }

      func getPopupPresentDuration() -> Double {
          return presentDuration ?? 1.0
      }

      func getPopupDismissDuration() -> Double {
          return dismissDuration ?? 1.0
      }

      func shouldPopupDismissInteractivelty() -> Bool {
          return shouldDismissInteractivelty ?? true
      }

    func setupView(){

        self.view.addSubview(customLabel)
        customLabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        customLabel.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 20).isActive = true

        self.view.addSubview(collectionView)
        collectionView.topAnchor.constraint(equalTo: customLabel.bottomAnchor, constant: 20).isActive       = true
        collectionView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        collectionView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 5).isActive    = true
        collectionView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -5).isActive   = true
        collectionView.register(SelectKidSectionCVCell.self, forCellWithReuseIdentifier: "SelectKidSectionCVCell")
        collectionView.register(TitleCVCell.self, forCellWithReuseIdentifier: "TitleCVCell")
        collectionView.delegate     = self
        collectionView.dataSource   = self

        self.view.addSubview(newKidLoginButton)
        newKidLoginButton.snp.makeConstraints { (make) in
            make.top.equalTo(collectionView.snp.bottom).offset(20)
            make.centerX.equalTo(self.view.snp.centerX)
            make.width.equalTo(180)
            make.height.equalTo(50)
        }
    }
}

extension SelectKidFromBottomSheetVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section{
        case 0:
            return 0
        default:
            return allKids?.count ?? 0
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        switch indexPath.section {
        case 1:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectKidSectionCVCell", for: indexPath) as! SelectKidSectionCVCell
            cell.setupView()
            cell.allKids = allKids?[indexPath.row]
            return cell
        default:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TitleCVCell", for: indexPath) as! TitleCVCell
            cell.setupView()
            cell.customLabel.text = "Select an image"
            return cell
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch indexPath.section {
        case 1:
            return CGSize(width: 150 , height: 200)
        default:
            return CGSize(width: collectionView.frame.width , height: 120)
        }
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.section {
        case 1:
            self.dismiss(animated: true) {
                self.delegate?.selectedKid(_controller: self, sender: (Any).self, id: self.allKids?[indexPath.row].kid_id ?? "")
            }
        default:
            print("default")
        }
    }
}
