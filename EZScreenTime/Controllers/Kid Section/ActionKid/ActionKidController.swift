//
//  ActionKidController.swift
//  EZScreenTime
//
//  Created by cedcoss on 04/03/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

class ActionKidController: BaseViewControllerForKid {

    @IBOutlet weak var kidName: UILabel!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var kidLogOut: UIButton!
    @IBOutlet weak var kidIcon: UIImageView!
    @IBOutlet weak var kidIconContainer: UIView!
    var selectedImage  = String()
    let tapGestureIcon = UITapGestureRecognizer()

    override func viewDidLoad() {
        super.viewDidLoad()
        designSetup()
        dataBinding()
        hideAd()

    }

    func designSetup(){
        self.navigationItem.setHidesBackButton(true, animated: true);
        kidIconContainer.layer.borderColor     = #colorLiteral(red: 0.007843137255, green: 0.337254902, blue: 0.5098039216, alpha: 1)
        kidIconContainer.backgroundColor       = #colorLiteral(red: 0.9333333333, green: 0.9333333333, blue: 0.9333333333, alpha: 1)
        kidIconContainer.layer.borderWidth     = 5
        kidIconContainer.layer.cornerRadius    = AppSetUp.isIpad ? 180 * 0.5
            : 130 * 0.5

        saveButton.layer.cornerRadius       = 5
        kidLogOut.layer.cornerRadius        = 5

        kidLogOut.rx.tap.subscribe { (onTap) in
            if AppSetUp().doLogOut(){
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let viewControl = storyBoard.instantiateViewController(withIdentifier: "ParentPageMenuViewController") as! ParentPageMenuViewController
                self.navigationController?.setViewControllers([viewControl], animated: true)
            }
        }.disposed(by: AppSetUp.disposeBag)
        kidLogOut.isHidden = true
    }

    func dataBinding(){

        let kid = KidSetupSettings().getDataForKidFromDB(with: CurrentLoggedKid.id)
        kidName.text = "Choose new avatar for \(kid?.kid_name ?? "")"
        kidIcon.image = UIImage(named: kid?.kid_icon ?? "placeholder")
        kidIcon.restorationIdentifier = kid?.kid_icon ?? "placeholder"
        //  MARK: - RXTAP
        //
        kidIconContainer.addGestureRecognizer(tapGestureIcon)

        tapGestureIcon.rx.event.bind(onNext: { recognizer in
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            guard let viewControl = storyBoard.instantiateViewController(withIdentifier: "SelectCustomDeviceBottomSheetVC") as? SelectCustomDeviceBottomSheetVC else { return }
            let screenSize: CGRect       = UIScreen.main.bounds
            let screenHeight             = screenSize.height
            viewControl.height           = screenHeight/2
            viewControl.topCornerRadius  = 20
            viewControl.delegate         = self as selectedDeviceImage
            viewControl.isFrom           = "AddKids"
            viewControl.presentDuration  = 0.30
            viewControl.dismissDuration  = 0.30
            self.navigationController?.present(viewControl, animated: true, completion: nil)
        }).disposed(by: AppSetUp.disposeBag)

        saveButton.rx.tap.subscribe { (onTap) in
            self.updateKidIcon()
        }.disposed(by: AppSetUp.disposeBag)
    }

    //  MARK: - UPDATE KID ICON AND DB ALONG WITH IT
    //

    func updateKidIcon(){
        mageHelper().callHttpRequestForKid(controller: self, endPoints: "kid/dashboard/actions/icon", parameters: ["icon":selectedImage],sendAuthHeader: true,postData: true,kidID: CurrentLoggedKid.id) { (data) in
            do {
                let json = try JSON(data: data)
                print(json)
                if json["status"].boolValue == true{
                    self.view.makeToast(json["message"].stringValue, duration: 3.0)
                    KidSetupSettings().updateKidIcon(icon: self.selectedImage, id: CurrentLoggedKid.id)
                }else{
                    self.view.makeToast(json["message"].stringValue, duration: 3.0)
                }
            }
            catch let error{
                print(error.localizedDescription)
            }
        }
    }
}

extension ActionKidController:selectedDeviceImage {

    func selectedDeviceImage(_controller: SelectCustomDeviceBottomSheetVC, sender: Any, image: String) {
        print(image)
        selectedImage = image
        kidIcon.image = UIImage(named: image)
        kidIcon.restorationIdentifier = image
    }
}
