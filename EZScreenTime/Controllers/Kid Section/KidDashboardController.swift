//
//  KidDashboardController.swift
//  EZScreenTime
//
//  Created by cedcoss on 29/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

class KidDashboardController: BaseViewControllerForKid {

    let notificationCenter = UNUserNotificationCenter.current()

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mainView: UIView!

    lazy var addKidFloatingButton: UIButton = {
        let addKidFloatingButton                                        = UIButton()
        addKidFloatingButton.backgroundColor                            = AppSetUp.colorPrimary
        addKidFloatingButton.translatesAutoresizingMaskIntoConstraints  = false
        addKidFloatingButton.setImage(UIImage(named: "bell")?.withRenderingMode(.alwaysTemplate), for: .normal)
        addKidFloatingButton.tintColor                                  = .white
        addKidFloatingButton.imageEdgeInsets                            = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
        addKidFloatingButton.floatButtonShadow()
        return addKidFloatingButton
    }()

    lazy var accountSwitcher: UIButton = {
        let accountSwitcher                                        = UIButton()
        accountSwitcher.backgroundColor                            = AppSetUp.colorPrimary
        accountSwitcher.translatesAutoresizingMaskIntoConstraints  = false
        accountSwitcher.setImage(UIImage(named: "swap")?.withRenderingMode(.alwaysTemplate), for: .normal)
        accountSwitcher.tintColor                                  = .white
        accountSwitcher.imageEdgeInsets                            = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
        accountSwitcher.floatButtonShadow()
        return accountSwitcher
    }()
    
    //  MARK: -  EVERYTHING TO BE MANAGED WITH THE HELP OF SINGLETON KIDID CLASS

    var kidsDashboardModel        : KidsDashboardModel?{
        didSet{
            self.tableView.reloadData()
        }
    }

    var kidId: String? {
        return CurrentLoggedKid.id
    }

    var kidDashboardViewModel     = [KidDashboardViewModel]()
    var timerActionModel          : TimerActionModel?
    var kidFromDB                 : MultipleKidLoginDB?
    var notificationModel         : NotificationModel?
    var timeupPopup               = TimeupPopup()
    var isChecked                 = false
    var showCoinSpendMessage      = false
    var paramsToAddCoin           = [String:String]()
    var isInProcess               = false
    let appdel                    = UIApplication.shared.delegate as! AppDelegate


    override func viewDidLoad() {
        super.viewDidLoad()
        guard let kidID = kidId else {return}
        getKid(id: kidID)
        setupTable()
        hideAd()
        makeNetworkCall()
        subscribeToKidDashBoardNotification()
        appdel.autoDeactivationDelegate = self
    }


    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // made this change on 2 June
        self.tableView.reloadSections([0], with: .none)
    }

    func subscribeToKidDashBoardNotification(){
        NotificationCenter.default.addObserver(self, selector: #selector(performKidDashUpdateForCoin(notification: )), name: .performKidDashUpdateForCoin, object: nil)
    }

    func AutoDeactivateDevice(deviceID:String,kidID:String,endPoint:String,completion: @escaping(Bool, Error?) -> ()){
        print("AutoDeactivation--KIDID===",kidID)
        mageHelper().callHttpRequestForKid(controller: self, endPoints: endPoint, parameters: ["device_id":deviceID],sendAuthHeader: true,postData: true,kidID: kidID) { (data) in
            do {
                let json = try JSON(data: data)
                print(json)
                if json["status"].boolValue == true{
                    completion(true,nil)
                }else{
                    completion(false,nil)
                }
            }
            catch let error{
                print(error.localizedDescription)
            }
        }
    }

    @objc func performKidDashUpdateForCoin(notification:Notification){
        print(notification.userInfo ?? "")
        if let dict = notification.userInfo as NSDictionary? {
            if let notificationData = dict["PenaltyCoinUpdate"] as? NotificationModel{
                self.notificationModel = notificationData
                if receivedKid{
                    self.tableView.reloadSections([0], with: .none)
                    self.showMessage()
                }else{
                    print("Don't show")
                }
            }
        }
    }

    var receivedKid: Bool{
        return self.notificationModel?.kidID == CurrentLoggedKid.id ? true : false
    }

    func getKid(id: String){
        let kid = KidSetupSettings().getDataForKidFromDB(with: id)
        self.kidFromDB = kid
    }
}

//  MARK: - NETWORK LAYERS
//
extension KidDashboardController{
    
    func makeNetworkCall(){
        self.kidsDashboardModel?.data?.settings?.removeAll()
        var kidID       = String()
        guard let kidId = kidFromDB?.kid_id else {return}
        kidID           = kidId
        print(kidID)
        mageHelper().callHttpRequestForKid(controller: self, endPoints: "kid/dashboard", parameters: nil,sendAuthHeader: true,kidID: kidID) { (data) in
            do {
                let json = try JSON(data: data)
                print(json)
                if json["status"].boolValue == true{
                    AppSetUp.decoder.keyDecodingStrategy = .convertFromSnakeCase
                    self.kidsDashboardModel = try? AppSetUp.decoder.decode(KidsDashboardModel.self, from: data)
                    //  MARK: - Pushing data to view model for managing time functionality done on 30th march
                    self.kidDashboardViewModel.removeAll()
                    self.kidDashboardViewModel +=  (self.kidsDashboardModel?.data?.settings?.map({
                        return KidDashboardViewModel(DashboardModel: $0)
                    }))!
                    print("kidsDashboardModel====Viewmodel====",self.kidDashboardViewModel as Any)
                    print("kidsDashboardModel====",self.kidsDashboardModel as Any)
                }else{
                    self.view.makeToast(json["message"].stringValue, duration: 3.0,position:. top)
                }
            }
            catch let error{
                print(error.localizedDescription)
            }
        }
    }

    //  MARK: - ACTIVATE/DEACTIVATE KID DEVICE N/W FUNCTION
    //

    func activate_DeactivateDevice(deviceID:String,endPoint:String,completion: @escaping(Bool, Error?) -> ()){

        var kidID       = String()
        guard let kidId = kidFromDB?.kid_id else {return}
        kidID           = kidId

        mageHelper().callHttpRequestForKid(controller: self, endPoints: endPoint, parameters: ["device_id":deviceID],sendAuthHeader: true,postData: true,kidID: kidID) { (data) in
            do {
                let json = try JSON(data: data)
                print(json)
                if json["status"].boolValue == true{
                    AppSetUp.decoder.keyDecodingStrategy = .convertFromSnakeCase
                    self.timerActionModel = try? AppSetUp.decoder.decode(TimerActionModel.self, from: data)
                    print("timerActionModel====",self.timerActionModel as Any)
                    completion(true,nil)
                    self.view.makeToast(json["message"].stringValue, duration: 3.0,position:.top)
                }else{
                    completion(false,nil)
                    if json["message"].stringValue == "You have reached the daily limit of using this device."{
                        self.timeupPopup.showTimeupPopup()
                        return
                    }
                    self.view.makeToast(json["message"].stringValue, duration: 3.0,position:.top)
                }
            }
            catch let error{
                print(error.localizedDescription)
            }
        }
    }

    //  MARK: - BACKGROUND SYNC API TO BE CALLED EVERY 30 SEC TO MAINTAIN ACTIVE DEVICE SESSION
    //

    func backgroundSync(deviceID:String,completion: @escaping(Bool, Error?) -> ()){
        var kidID       = String()
        guard let kidId = kidFromDB?.kid_id else {return}
        kidID           = kidId

        mageHelper().callHttpRequestForKid(controller: self, endPoints: "kid/dashboard/timer/sync", parameters: ["device_id":deviceID],sendAuthHeader: true,postData: true,kidID: kidID) { (data) in
            do {
                let json = try JSON(data: data)
                print(json)
                if json["status"].boolValue == true{
                    completion(true,nil)
                    self.view.makeToast(json["message"].stringValue, duration: 3.0)
                }else{
                    completion(false,nil)
                    self.view.makeToast(json["message"].stringValue, duration: 3.0)
                }
            }
            catch let error{
                print(error.localizedDescription)
            }
        }
    }

    func addCoins(params:[String:String]){
        self.view.isUserInteractionEnabled = false
        mageHelper().callHttpRequestForKid(controller: self, endPoints: "kid/dashboard/actions/reward", parameters: paramsToAddCoin,sendAuthHeader: true,postData: true,kidID: CurrentLoggedKid.id) { (data) in
            do {
                let json = try JSON(data: data)
                print(json)
                if json["status"].boolValue == true{
                    self.view.makeToast(json["message"].stringValue, duration: 3.0,position:.top)
                    self.showCoinSpendMessage = false

                    //  MARK: - Today 18th April
                    guard let cell = self.tableView.cellForRow(at: [0,0]) as? KidsHeaderTVCell else {return}
                    cell.parentIcon.isHidden = true
                    cell.mainStack.isHidden = true
                    self.notificationModel = nil
                    //29th April start
                    //self.tableView.reloadSections([0,0], with: .automatic)
                    //self.makeNetworkCall()
                    self.view.isUserInteractionEnabled = true
                    let viewControl = KidSetupSettings.storyBoard.instantiateViewController(withIdentifier: "KidDashboardController") as! KidDashboardController
                    self.navigationController?.setViewControllers([viewControl], animated: false)
                    //29th April end
                }else{
                    self.view.makeToast(json["message"].stringValue, duration: 3.0,position:.top)
                }
            }
            catch let error{
                print(error.localizedDescription)
            }
        }
    }
}

//  MARK: - VIEW LAYERS
//
extension KidDashboardController: UITableViewDataSource,UITableViewDelegate{

    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section{
        case 0:
            return self.showCoinSpendMessage ? self.notificationModel?.typeOfDistribution == "1" ? 2 : 1 : 1
        case 1:
            return self.kidsDashboardModel?.data?.settings?.count ?? 0
        default:
            return 0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section{
        case 0:
            switch indexPath.row{
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "KidsHeaderTVCell", for: indexPath) as! KidsHeaderTVCell
                cell.setupView()
                cell.parent             = self
                cell.kidName.text       = kidFromDB?.kid_name
                cell.delegate           = self as selectedKid
                cell.kidIcon.image     = UIImage(named: kidFromDB?.kid_icon ?? "placeholder")
                cell.parentIcon.isHidden = true

                //  MARK: - TIME SPEND ALERT MESSAGE
                //
                if let timeSpend = self.timerActionModel?.data?.deviceData?.timerData?.timeInRunningSession{
                    cell.alertMessage.text = "You have played \(timeSpend) minutes in last session."
                }

                //  MARK: - COIN ACTION MESSAGES
                //
                if self.notificationModel != nil{
                    cell.parentIcon.isHidden = false
                    guard let title = self.notificationModel?.title,let message = self.notificationModel?.message,let reason = self.notificationModel?.reason, let _ = self.notificationModel?.parentIcon else{return UITableViewCell()}
                    cell.alertMessage.text = "***\(title)***\n\(message)\n\nReason: \(reason)"
                    cell.parentIcon.setImage(UIImage(named: self.notificationModel?.parentIcon ?? ""), for: .normal)
                }
                return cell

            case 1: //Show devices
                let cell = tableView.dequeueReusableCell(withIdentifier: "KidDevicesTVCell", for: indexPath) as! KidDevicesTVCell
                let kidDevices  = self.kidsDashboardModel?.data?.settings
                cell.kidDevices = kidDevices
                cell.delegate   = self as addCoinTokidDevice

                cell.proceedButton.rx.tap.subscribe { (onTap) in
                    print("")
                    self.addCoins(params: self.paramsToAddCoin)
                }.disposed(by: cell.disposeBag)
                return cell
            default:
                return UITableViewCell()
            }
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "KidTimerTVCell", for: indexPath) as! KidTimerTVCell
            let devices     = self.kidsDashboardModel?.data?.settings?[indexPath.row]
            cell.devices    = devices
            
            //  MARK: IN CASE ISACTIVE TIMER IS 1 AND ALREADY RUNNING
            //
            //First i was using devices?.isactiveTimer

            if kidDashboardViewModel[indexPath.row].isActiveTimer == 1{
                print("Current Time===",cell.timerLabel.isFinished)
                print("Current Time==",cell.timerLabel.timeRemaining)
                let timeCounted = cell.timerLabel.timeCounted
                print("WHEN TIMER IS ALREADY ACTIVE")
                print("TimeCounted===",timeCounted)
                let roundingOfTimeCounted = round(timeCounted)
                print("Rounding of TimeCounted===",roundingOfTimeCounted)
                let time = Int(roundingOfTimeCounted)
                let originalValue = kidDashboardViewModel[indexPath.row].timeSpendTodayInInt
                print("OriginalTIME==",originalValue)
                kidDashboardViewModel[indexPath.row].timeSpendTodayInInt = (originalValue ?? 0) + time
                print("Time to be added==",time)
                print("ORIGINAL VALUE AFTER ADDING TIME==",kidDashboardViewModel[indexPath.row].timeSpendTodayInInt)
                cell.timerButton.setTitle(" STOP TIMER ", for: .normal)
                cell.timerButton.backgroundColor  = .red
                cell.timerLabel.isHidden          = false

                if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular{
                    cell.timerLabelHeight.constant    = 60
                }else{
                    cell.timerLabelHeight.constant    = 40
                }

                guard let todayLimit = devices?.todayTimeLimitInInt, let timeSpendToday = kidDashboardViewModel[indexPath.row].timeSpendTodayInInt else{return UITableViewCell()}
                let diff = todayLimit - timeSpendToday
                print("See the difference ==",diff)
                cell.timerLabel.setCountDownTime(minutes: Double(diff))
                cell.timerLabel.start()
            }else{
                cell.timerLabel.isHidden          = true
                cell.timerLabelHeight.constant    = 0
                cell.timerButton.setTitle(" START TIMER ", for: .normal)
                cell.timerButton.backgroundColor  = AppSetUp.colorAccent
            }
            //  MARK: - TIMER ACTIVATION ON BUTTON TAP
            //
            cell.timerButton.rx.tap.subscribe { (onTap) in
                UNUserNotificationCenter.current().getPendingNotificationRequests { (requests) in
                    for request in requests {
                        print("Check123==",request)
                    }
                }
                print("Timer Tapped")
                if cell.timerButton.currentTitle == " START TIMER "{self.isChecked = true}else{self.isChecked = false}
                //self.isChecked = !self.isChecked
                if self.isChecked{
                    //20May modi start
                    if devices?.coinsAvailable == "0" {
                        self.view.makeToast("You don't have enough coins to start with this device",position:.top)
                        return
                    }
                    // 20May modi end
                    self.activate_DeactivateDevice(deviceID: (devices?.id)!, endPoint: "kid/dashboard/timer/activate") { (success, error) in
                        if error == nil && success == true{
                            // 22 may start
                            let totalTime = (Double((devices?.totalTimeleftInDouble)!)) * 60
                            print("NewTime==",totalTime)
                            let options: UNAuthorizationOptions = [.alert, .sound]
                            self.notificationCenter.requestAuthorization(options: options) {
                                (didAllow, error) in
                                if !didAllow {
                                    print("User has declined notifications")
                                }
                            }
                            let content = UNMutableNotificationContent()
                            content.title = "Auto Deactivation"
                            content.body  = "Timer has deactivated"
                            content.userInfo = ["kidID": self.kidId!,"deviceID":(devices?.id)!]
                            content.sound = UNNotificationSound.default

                            let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: totalTime, repeats: false)
                            let identifier = "ADLN-\(self.kidId!)"
                            let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)

                            self.notificationCenter.add(request) { (error) in
                                if let error = error {
                                    print("Error \(error.localizedDescription)")
                                }
                            }
                            //22 may end
                            print("Show",indexPath.row)
                            cell.timerButton.setTitle(" STOP TIMER ", for: .normal)
                            cell.timerButton.backgroundColor  = .red
                            //  MARK: - modi 30march
                            print("ON TIMER BUTTON TAP")
                            let timeCounted = cell.timerLabel.timeCounted
                            print("TimeCounted===",timeCounted)
                            let roundingOfTimeCounted = round(timeCounted)
                            let time = Int(roundingOfTimeCounted)
                            let originalValue = self.kidDashboardViewModel[indexPath.row].timeSpendTodayInInt
                            print("OriginalTIME==",originalValue)
                            // 29th April change 
                            //self.kidDashboardViewModel[indexPath.row].timeSpendTodayInInt = (originalValue ?? 0) + time
                            self.kidDashboardViewModel[indexPath.row].timeSpendTodayInInt = (originalValue ?? 0)
                            print("Time to be added==",time)
                            print("ORIGINAL VALUE AFTER ADDING TIME==",self.kidDashboardViewModel[indexPath.row].timeSpendTodayInInt)
                            guard let todayLimit = devices?.todayTimeLimitInInt, let timeSpendToday = devices?.timerData?.timeSpendTodayInInt else {return}
                            let diff = todayLimit - timeSpendToday

                            cell.timerLabel.setCountDownTime(minutes: Double(diff))
                            cell.timerLabel.start()
                            cell.timerLabel.isHidden          = false

                            if self.traitCollection.horizontalSizeClass == .regular && self.traitCollection.verticalSizeClass == .regular{
                                cell.timerLabelHeight.constant    = 60
                            }else{
                                cell.timerLabelHeight.constant    = 40
                            }
                            //  MARK: - modification
                            self.kidDashboardViewModel[indexPath.row].isActiveTimer = 1
                        }
                    }
                }else{
                    self.activate_DeactivateDevice(deviceID: (devices?.id)!, endPoint: "kid/dashboard/timer/deactivate") { (success, error) in
                        if error == nil && success == true{
                            print("Hide")
                            //22 MAY START
                            UNUserNotificationCenter.current().getPendingNotificationRequests { (requests) in
                                for request in requests {
                                    print("Check123==",request)
                                    if request.identifier == "ADLN-\(self.kidId!)" {
                                        UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["ADLN-\(self.kidId!)"])
                                    }
                                }
                            }
                            //22 MAY STOP
                            cell.timerButton.setTitle(" START TIMER ", for: .normal)
                            cell.timerButton.backgroundColor  = AppSetUp.colorAccent
                            cell.timerLabel.cancel()
                            cell.timerLabel.isHidden          = true
                            cell.timerLabelHeight.constant    = 0
                            //  MARK: - modification
                            self.kidDashboardViewModel[indexPath.row].isActiveTimer = 0
                            self.showMessage()
                        }
                    }
                }
            }.disposed(by: cell.disposeBag)
            return cell
        default:
            return UITableViewCell()
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section{
        case 0:
            if self.showCoinSpendMessage{
                if self.notificationModel?.typeOfDistribution == "1"{
                    switch indexPath.row{
                    case 1:  return 275
                    default: return UITableView.automaticDimension
                    }
                }else{
                    return UITableView.automaticDimension
                }
            }else{
                if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular{
                    return 160
                }else{
                    return 100
                }
            }
        case 1:
            if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular{
                return 400
            }else{
                return 260
            }
        default:
            if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular{
                return 160
            }else{
                return 100
            }
        }
    }

    func showMessage(){
        self.showCoinSpendMessage = true
        self.tableView.reloadSections([0,0], with: .automatic)

        if self.notificationModel?.typeOfDistribution == "1" {
            self.showCoinSpendMessage = true
            print("Dont auto hide")
        }else{
            DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                self.showCoinSpendMessage = false
                self.tableView.reloadSections([0,0], with: .automatic)

                //  MARK: - Today 18th April
                let cell = self.tableView.cellForRow(at: [0,0]) as? KidsHeaderTVCell
                cell?.parentIcon.isHidden = true
                cell?.mainStack.isHidden = true
                self.notificationModel = nil
                //29thApril
                let viewControl = KidSetupSettings.storyBoard.instantiateViewController(withIdentifier: "KidDashboardController") as! KidDashboardController
                self.navigationController?.setViewControllers([viewControl], animated: false)
            }
        }
    }
}

//  MARK: - PROTOCOLS
//
extension KidDashboardController:selectedKid{
    func newKidLogin(_controller: SelectKidFromBottomSheetVC) {
        if AppSetUp().doLogOut(){
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewControl = storyBoard.instantiateViewController(withIdentifier: "ParentPageMenuViewController") as! ParentPageMenuViewController
            self.navigationController?.setViewControllers([viewControl], animated: true)
        }
    }

    func selectedKid(_controller: SelectKidFromBottomSheetVC, sender: Any, id: String) {
        print("id==",id)
        getKid(id: id)
        CurrentLoggedKid.id = id
        //  MARK: - Have replaced network call with setting up vc because of UX bug on 9June
        //makeNetworkCall()
        let viewControl = KidSetupSettings.storyBoard.instantiateViewController(withIdentifier: "KidDashboardController") as! KidDashboardController
        self.navigationController?.setViewControllers([viewControl], animated: false)
    }
}

extension KidDashboardController:addCoinTokidDevice{
    func addCoinTokidDevice(_cell: KidDevicesTVCell, sender: Any, device_id: String) {
        guard let coin = self.notificationModel?.numberOfCoins else {return}
        paramsToAddCoin = ["kid_id":CurrentLoggedKid.id,"type":"award","coins":coin ,"remark":"","send_msg":"0","customChoice":"","device_id":device_id]
    }
}

extension KidDashboardController: AutoDeactivationDelegate{
    func AutoDeactivationDelegate(_ param: [AnyHashable : Any]) {
        print(param)
        guard let deviceID = param["deviceID"] as? String, let kidID = param["kidID"] as? String else {return}
        self.AutoDeactivateDevice(deviceID: deviceID, kidID: kidID, endPoint: "kid/dashboard/timer/deactivate") { (success, error) in
            if error == nil && success == true{
                self.makeNetworkCall()
            }else{
                print("Failure case")
            }
        }
    }
}
