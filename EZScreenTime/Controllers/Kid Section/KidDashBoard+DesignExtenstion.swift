//
//  KidDashBoard+DesignExtenstion.swift
//  EZScreenTime
//
//  Created by cedcoss on 29/04/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

extension KidDashboardController{
    func setupTable(){
        tableView.dataSource        = self
        tableView.delegate          = self
        tableView.backgroundColor   = AppSetUp.controllerBackgroundColor
        tableView.separatorStyle    = .none

        //  MARK: - Floating button setup
        //
        self.mainView.addSubview(addKidFloatingButton)
        if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular{
            addKidFloatingButton.widthAnchor.constraint(equalToConstant: 80).isActive = true
            addKidFloatingButton.heightAnchor.constraint(equalToConstant: 80).isActive = true
            addKidFloatingButton.layer.cornerRadius  = 80 * 0.5
            addKidFloatingButton.imageEdgeInsets     = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        }else{
            addKidFloatingButton.widthAnchor.constraint(equalToConstant: 60).isActive = true
            addKidFloatingButton.heightAnchor.constraint(equalToConstant: 60).isActive = true
            addKidFloatingButton.layer.cornerRadius  = 60 * 0.5
        }
        addKidFloatingButton.bottomAnchor.constraint(equalTo: self.mainView.bottomAnchor, constant: -20).isActive = true
        addKidFloatingButton.trailingAnchor.constraint(equalTo: self.mainView.trailingAnchor, constant: -18).isActive = true

        addKidFloatingButton.rx.tap.subscribe { (onTapCall) in
            let viewControl = KidNotificationHistoryController()
            //self.navigationController?.setViewControllers([viewControl], animated: true)
            self.navigationController?.pushViewController(viewControl, animated: true)
        }.disposed(by: AppSetUp.disposeBag)

        //  MARK: - Floating button setup
        //
        self.mainView.addSubview(accountSwitcher)
        if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular{
            accountSwitcher.widthAnchor.constraint(equalToConstant: 80).isActive = true
            accountSwitcher.heightAnchor.constraint(equalToConstant: 80).isActive = true
            accountSwitcher.layer.cornerRadius                         = 80 * 0.5
            accountSwitcher.imageEdgeInsets                            = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        }else{
            accountSwitcher.widthAnchor.constraint(equalToConstant: 60).isActive = true
            accountSwitcher.heightAnchor.constraint(equalToConstant: 60).isActive = true
            accountSwitcher.layer.cornerRadius                            = 60 * 0.5
        }
        accountSwitcher.bottomAnchor.constraint(equalTo: self.mainView.bottomAnchor, constant: -20).isActive = true
        accountSwitcher.leadingAnchor.constraint(equalTo: self.mainView.leadingAnchor, constant: 18).isActive = true


        accountSwitcher.rx.tap.subscribe { (onTapCall) in

            let viewControl              = SelectKidFromBottomSheetVC()
            let screenSize: CGRect       = UIScreen.main.bounds
            let screenHeight             = screenSize.height
            viewControl.height           = screenHeight/2.5
            viewControl.topCornerRadius  = 20
            viewControl.delegate         = self as selectedKid
            viewControl.presentDuration  = 0.30
            viewControl.dismissDuration  = 0.30
            self.navigationController?.present(viewControl, animated: true, completion: nil)
        }.disposed(by: AppSetUp.disposeBag)
    }
}


