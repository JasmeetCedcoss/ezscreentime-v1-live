//
//  KidsHeaderTVCell.swift
//  EZScreenTime
//
//  Created by cedcoss on 29/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

protocol selectedKidId {
    func selectedKidId(_cell: KidsHeaderTVCell,sender: Any,id:String)
}

class KidsHeaderTVCell: UITableViewCell {


    @IBOutlet weak var kidImageContainer: UIView!
    @IBOutlet weak var mainStack: UIStackView!
    @IBOutlet weak var alertMessageContainer: UIView!
    @IBOutlet weak var parentIcon: UIButton!
    @IBOutlet weak var kidName: UILabel!
    @IBOutlet weak var kidIcon: UIImageView!
    @IBOutlet weak var alertMessage: UILabel!
    @IBOutlet weak var containerView: UIView!


    private(set) var disposeBag = DisposeBag()
    var parent                  = UIViewController()
    var delegate                : selectedKid?

    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag() // because life cicle of every cell ends on prepare for reuse
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setupView(){
        alertMessageContainer.layer.borderColor                       = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        alertMessageContainer.layer.borderWidth                       = 3
        alertMessageContainer.layer.cornerRadius                      = 10

        self.selectionStyle                                           = .none
        kidImageContainer.layer.borderColor                           = #colorLiteral(red: 0.007843137255, green: 0.337254902, blue: 0.5098039216, alpha: 1)
        kidImageContainer.layer.borderWidth                           = 5

        alertMessage.textAlignment                                    = .center
        containerView.layer.maskedCorners                             = [.layerMinXMinYCorner, .layerMaxXMinYCorner]

        parentIcon.layer.borderColor                                  = #colorLiteral(red: 0.007843137255, green: 0.337254902, blue: 0.5098039216, alpha: 1)
        parentIcon.layer.borderWidth                                  = 5
        parentIcon.layer.cornerRadius                                 = parentIcon.bounds.height * 0.5

        if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular{
            kidImageContainer.layer.cornerRadius                      = 130 * 0.5
        }else{
            kidImageContainer.layer.cornerRadius                      = 90 * 0.5
        }
    }
}

