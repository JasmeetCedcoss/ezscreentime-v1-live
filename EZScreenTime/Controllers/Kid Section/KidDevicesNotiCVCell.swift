//
//  KidDevicesNotiCVCell.swift
//  EZScreenTime
//
//  Created by cedcoss on 19/03/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

class KidDevicesNotiCVCell: UICollectionViewCell {

    @IBOutlet weak var deviceIcon: UIImageView!

    override var isSelected: Bool {
        didSet {
            if isSelected { // Selected cell
             self.layer.borderColor = #colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1)
                self.contentView.alpha = 1.0
            } else { // Normal cell
             self.layer.borderColor = #colorLiteral(red: 0.9333333333, green: 0.9333333333, blue: 0.9333333333, alpha: 1)
                self.contentView.alpha = 0.5
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.layer.cornerRadius = 5
        self.contentView.alpha = 0.5
    }
}
