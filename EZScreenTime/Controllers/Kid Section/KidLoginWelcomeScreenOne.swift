//
//  KidLoginWelcomeScreenOne.swift
//  EZScreenTime
//
//  Created by cedcoss on 28/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

class KidLoginWelcomeScreenOne: BaseViewControllerForKid {

    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerContainer: UIView!
    @IBOutlet weak var kidName: UILabel!
    @IBOutlet weak var confirmationButton: UIButton!
    @IBOutlet weak var stackViewHeight: NSLayoutConstraint!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var kidIconContainer: UIView!
    @IBOutlet weak var kidIcon: UIImageView!
    @IBOutlet weak var logOut: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        designSetup()
        dataBinding()
        hideAd()
        confirmationButton.rx.tap.subscribe { (onTap) in
            let viewControl = KidSetupSettings.storyBoard.instantiateViewController(withIdentifier: "KidLoginWelcomeScreenTwo") as! KidLoginWelcomeScreenTwo
            let kid = KidSetupSettings().getDataForKidFromDB(with: CurrentLoggedKid.id)
            viewControl.kid = kid
            self.navigationController?.pushViewController(viewControl, animated: true)
        }.disposed(by: AppSetUp.disposeBag)

        logOut.rx.tap.subscribe { (onTap) in
            if AppSetUp().doLogOut(){
                KidSetupSettings().deleteKidLoggingOut(CurrentLoggedKid.id)
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let viewControl = storyBoard.instantiateViewController(withIdentifier: "ParentPageMenuViewController") as! ParentPageMenuViewController
                self.navigationController?.setViewControllers([viewControl], animated: true)
            }
        }.disposed(by: AppSetUp.disposeBag)
    }

    func designSetup(){
        customTabbarView.isHidden = true
        kidIconContainer.layer.borderColor                       = #colorLiteral(red: 0.007843137255, green: 0.337254902, blue: 0.5098039216, alpha: 1)
        kidIconContainer.layer.borderWidth                       = 5
        kidIconContainer.layer.cornerRadius                      = AppSetUp.isIpad ? 130 * 0.5 : 90 * 0.5
        confirmationButton.backgroundColor                       = #colorLiteral(red: 0.007843137255, green: 0.337254902, blue: 0.5098039216, alpha: 1)
        headerContainer.JSCustomView()
        confirmationButton.layer.cornerRadius                    = 5
        containerView.layer.maskedCorners                        = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }

    func dataBinding(){
        let kid = KidSetupSettings().getDataForKidFromDB(with: CurrentLoggedKid.id)
        kidIcon.image = UIImage(named: kid?.kid_icon ?? "placeholder")
        kidName.text  = kid?.kid_name
    }
}
