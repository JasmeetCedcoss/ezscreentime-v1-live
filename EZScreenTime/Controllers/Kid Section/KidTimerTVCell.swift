//
//  KidTimerTVCell.swift
//  EZScreenTime
//
//  Created by cedcoss on 02/03/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

class KidTimerTVCell: UITableViewCell {

    private(set) var disposeBag = DisposeBag()
    var coinAdded               = false

    @IBOutlet weak var icon:                    UIImageView!
    @IBOutlet weak var deviceName:              UILabel!
    @IBOutlet weak var timerButton:             UIButton!
    @IBOutlet weak var todayTime:               UILabel!
    @IBOutlet weak var timeSpendToday:          UILabel!
    @IBOutlet weak var bottomLabel:             UILabel!
    @IBOutlet weak var timerLabel:              CountdownLabel!
    @IBOutlet weak var coinStack:               UIStackView!
    @IBOutlet var coinSatckWidth:               NSLayoutConstraint!
    @IBOutlet var timerLabelHeight:             NSLayoutConstraint!
    @IBOutlet weak var timerContainerView:      UIView!


    override func awakeFromNib() {
        super.awakeFromNib()
        //  MARK: - DESIGNING STUFF
        //
        timerButton.layer.cornerRadius          = 5
        timerLabel.timeFormat                   = "HH:mm:ss"
        timerLabel.animationType                = .Evaporate
        timerContainerView.layer.cornerRadius   = 5
        timerContainerView.layer.borderColor    = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        timerContainerView.layer.borderWidth    = 1
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }

    //  MARK: - DATA BINDING
    //
    var devices: Setting?{
        didSet{
            if devices?.icon == "coin_silver"{
                icon.image = UIImage(named: devices?.icon ?? "placeholder")
            }else{
                icon.image = UIImage(named: devices?.icon ?? "placeholder")?.withRenderingMode(.alwaysTemplate)
                icon.tintColor = .black
            }
            deviceName.text         = devices?.name
            todayTime.text          = "Today: " + (devices?.todayTimeLimit)!
            timeSpendToday.text     = "Spend: " + (devices?.timerData?.timeSpendToday)!
            guard let coinsLeft     = devices?.coinsAvailable, let totalCoins = devices?.coinsReceived,let totalTime = devices?.totalTimeLeft else {return}

            guard let coinCountInt  = Int(coinsLeft) else {return}
            bottomLabel.text        = coinsLeft + " coins left / " + totalCoins + " (\(totalTime))"
            setupCoinStack(coinCountInt)
        }
    }

    fileprivate func setupCoinStack(_ coinCountInt: Int) {

        coinSatckWidth.constant = 0
        coinStack.arrangedSubviews
            .forEach({ $0.removeFromSuperview() })

        for val in 0..<(coinCountInt){
            if val <= 5 {
                let coinImageView                                       = UIImageView()
                coinImageView.contentMode                               = .scaleAspectFit
                coinImageView.image                                     = UIImage(named: (devices?.coinColor ?? "placeholder"))
                coinImageView.translatesAutoresizingMaskIntoConstraints = false
                coinStack.addArrangedSubview(coinImageView)

                if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular{
                    coinSatckWidth.constant += 65
                }else{
                    coinSatckWidth.constant += 40
                }
            }
        }

        if coinCountInt > 6 {
            let label                                        = UILabel()
            label.text                                       = "+" + String(coinCountInt - 6)
            label.translatesAutoresizingMaskIntoConstraints  = false
            coinStack.addArrangedSubview(label)

            if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular{
                coinSatckWidth.constant += 65
                label.font = UIFont(name: "Poppins-Regular", size: 21)
            }else{
                coinSatckWidth.constant += 40
            }
        }
    }
}
