//
//  BaseViewControllerSupervisor.swift
//  EZScreenTime
//
//  Created by cedcoss on 24/03/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit
import DropDown

import CoreData
import GoogleMobileAds

class BaseViewControllerSupervisor: UIViewController {

    let dropDown = DropDown()
    lazy var customTabbarView: UIView = {
        let customTabbarView                                        = UIView()
        customTabbarView.backgroundColor                            = UIColor.red
        customTabbarView.translatesAutoresizingMaskIntoConstraints  = false
        return customTabbarView
    }()

    lazy var popUpMenu: UIButton = {
        let popUpMenu                                        = UIButton()
        popUpMenu.backgroundColor                            = AppSetUp.colorPrimaryDark
        popUpMenu.translatesAutoresizingMaskIntoConstraints  = false
        popUpMenu.rx.tap.subscribe { (onTap) in
            self.dropDown.show()
        }.disposed(by: AppSetUp.disposeBag)
        popUpMenu.setImage(UIImage(named: "menu"), for: .normal)
        popUpMenu.tintColor                                  = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        popUpMenu.imageEdgeInsets                            = UIEdgeInsets(top: 10, left: 25, bottom: 10, right: 25)
        return popUpMenu
    }()

    lazy var homeIcon: UIImageView = {
        let homeIcon                                        = UIImageView()
        homeIcon.image                                      = UIImage(named: "homeicon")
        homeIcon.translatesAutoresizingMaskIntoConstraints  = false
        homeIcon.contentMode                                = .scaleAspectFit
        return homeIcon
    }()
    
    lazy var stackView: UIStackView = {
        let stackView                                        = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints  = false
        stackView.distribution                               = .fillEqually
        stackView.spacing                                    = 0
        stackView.alignment                                  = .fill
        stackView.axis                                       = .horizontal
        return stackView
    }()

    lazy var homeButton: UIButton = {
        let homeButton                                        = UIButton()
        homeButton.backgroundColor                            = AppSetUp.colorAccent
        homeButton.translatesAutoresizingMaskIntoConstraints  = false
        homeButton.rx.tap.subscribe { (onTap) in
         let viewControl = SupervisorSetupSettings.storyBoard.instantiateViewController(withIdentifier: "SupervisorDashboardController") as! SupervisorDashboardController
        self.navigationController?.setViewControllers([viewControl], animated: true)
        }.disposed(by: AppSetUp.disposeBag)
        return homeButton
    }()

    lazy var actionButton: UIButton = {
        let actionButton                                        = UIButton()
        actionButton.backgroundColor                            = AppSetUp.colorPrimary
        actionButton.setTitle("ACTION", for: .normal)
        actionButton.translatesAutoresizingMaskIntoConstraints  = false
        actionButton.rx.tap.subscribe { (onTap) in
            let viewControl = SupervisorSetupSettings.storyBoard.instantiateViewController(withIdentifier: "SupervisorActionController") as! SupervisorActionController
            self.navigationController?.setViewControllers([viewControl], animated: true)
        }.disposed(by: AppSetUp.disposeBag)
        return actionButton
    }()

    lazy var adMobView: GADBannerView = {
        var adMobView                                           = GADBannerView()
        adMobView.translatesAutoresizingMaskIntoConstraints     = false
        //adMobView.adUnitID = "ca-app-pub-6578362616860880/1871472685"  // original  app id
        adMobView.adUnitID = "ca-app-pub-3940256099942544/2934735716" // google test id
        adMobView.rootViewController = self
        GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers =
            [ "f55ccb73407d86d96afd90cc541bf9d9" ] // Sample device ID
        adMobView.load(GADRequest())
        return adMobView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupnav()
    }

    lazy var test: UIView = {
        let test                                        = UIView()
        test.backgroundColor                            = UIColor.red
        test.translatesAutoresizingMaskIntoConstraints  = false
        return test
    }()

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setupnav()
        self.setupCustomTabbar()
    }

    func alterColor(){
        homeButton.backgroundColor      = AppSetUp.colorPrimary
        actionButton.backgroundColor    = AppSetUp.colorAccent
    }

    func hideAd(){
        adMobView.isHidden = true
    }

    func setupnav() {
        let logo = UIImage(named: "navtitle")
        let imageView = UIImageView(image:logo)
        imageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = imageView
    }

    func setupCustomTabbar(){
        self.view.addSubview(customTabbarView)
        //      customTabbarView.topAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
        customTabbarView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        customTabbarView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        customTabbarView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        customTabbarView.heightAnchor.constraint(equalToConstant: 50).isActive = true

        self.customTabbarView.addSubview(popUpMenu)
        popUpMenu.topAnchor.constraint(equalTo: self.customTabbarView.topAnchor).isActive = true
        popUpMenu.leadingAnchor.constraint(equalTo: self.customTabbarView.leadingAnchor).isActive = true
        popUpMenu.bottomAnchor.constraint(equalTo: self.customTabbarView.bottomAnchor).isActive = true
        popUpMenu.widthAnchor.constraint(equalToConstant:75).isActive = true

        self.customTabbarView.addSubview(stackView)
        stackView.leadingAnchor.constraint(equalTo: popUpMenu.trailingAnchor).isActive = true
        stackView.bottomAnchor.constraint(equalTo: self.customTabbarView.bottomAnchor).isActive = true
        stackView.trailingAnchor.constraint(equalTo: self.customTabbarView.trailingAnchor).isActive = true
        stackView.topAnchor.constraint(equalTo: self.customTabbarView.topAnchor).isActive = true
        stackView.addArrangedSubview(homeButton)
        homeButton.addSubview(homeIcon)
        homeIcon.snp.makeConstraints { (make) in
            make.centerY.equalTo(homeButton.snp.centerY)
            make.centerX.equalTo(homeButton.snp.centerX)
            make.height.width.equalTo(30)
        }
        stackView.addArrangedSubview(actionButton)



        dropDown.backgroundColor            = AppSetUp.colorPrimaryDark
               dropDown.selectionBackgroundColor   = AppSetUp.colorPrimaryDark
               dropDown.selectedTextColor          = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
               dropDown.textColor                  = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
               dropDown.anchorView                 = popUpMenu
               dropDown.dataSource                 = ["Logout"]
               dropDown.direction                  = .top
               dropDown.topOffset                  = CGPoint(x: 0, y:-50)

               dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                   print("Selected item: \(item) at index: \(index)")
                   switch item {
                   case "Logout":
                       print("Logout")
                       if SupervisorSetupSettings().doLogOutSupervisor(){
                           let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                           let viewControl = storyBoard.instantiateViewController(withIdentifier: "ParentPageMenuViewController") as! ParentPageMenuViewController
                           self.navigationController?.setViewControllers([viewControl], animated: true)
                       }
                   default:
                       print("Default")
                   }
               }








        self.view.addSubview(adMobView)
        adMobView.bottomAnchor.constraint(equalTo: self.customTabbarView.topAnchor).isActive = true
        adMobView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        adMobView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        adMobView.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
}
