//
//  BaseViewController.swift
//  EZScreenTime
//
//  Created by cedcoss on 04/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit
import DropDown
import CoreData
import SafariServices

class BaseViewController: UIViewController {

    let dropDown = DropDown()
    lazy var customTabbarView: UIView = {
        let customTabbarView                                        = UIView()
        customTabbarView.backgroundColor                            = UIColor.red
        customTabbarView.translatesAutoresizingMaskIntoConstraints  = false
        return customTabbarView
    }()

    lazy var popUpMenu: UIButton = {
        let popUpMenu                                        = UIButton()
        popUpMenu.backgroundColor                            = AppSetUp.colorPrimaryDark
        popUpMenu.translatesAutoresizingMaskIntoConstraints  = false
        popUpMenu.rx.tap.subscribe { (onTap) in
                   self.dropDown.show()
        }.disposed(by: AppSetUp.disposeBag)
        popUpMenu.setImage(UIImage(named: "menu"), for: .normal)
        popUpMenu.tintColor                                  = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        popUpMenu.imageEdgeInsets                            = UIEdgeInsets(top: 10, left: 25, bottom: 10, right: 25)
        return popUpMenu
    }()

    lazy var homeIcon: UIImageView = {
        let homeIcon                                        = UIImageView()
        homeIcon.image                                      = UIImage(named: "homeicon")
        homeIcon.translatesAutoresizingMaskIntoConstraints  = false
        homeIcon.contentMode                                = .scaleAspectFit
        return homeIcon
    }()

    lazy var actionIcon: UIImageView = {
        let actionIcon                                        = UIImageView()
        actionIcon.image                                      = UIImage(named: "action")
        actionIcon.translatesAutoresizingMaskIntoConstraints  = false
        actionIcon.contentMode                                = .scaleAspectFit
        return actionIcon
    }()

    lazy var stackView: UIStackView = {
        let stackView                                        = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints  = false
        stackView.distribution                               = .fillEqually
        stackView.spacing                                    = 0
        stackView.alignment                                  = .fill
        stackView.axis                                       = .horizontal
        return stackView
    }()

    lazy var homeButton: UIButton = {
        let homeButton                                        = UIButton()
        homeButton.backgroundColor                            = AppSetUp.colorAccent
        homeButton.translatesAutoresizingMaskIntoConstraints  = false
        homeButton.rx.tap.subscribe { (onTap) in
            if AppSetUp.shared.areAllSettingsCompleted(){
                let viewControl:MainDashboardController = self.storyboard!.instantiateViewController()
                self.navigationController?.setViewControllers([viewControl], animated: true)
            }else{
                self.view.makeToast("Please complete all settings first")
            }
        }.disposed(by: AppSetUp.disposeBag)
        return homeButton
    }()

    lazy var actionButton: UIButton = {
        let actionButton                                        = UIButton()
        actionButton.backgroundColor                            = AppSetUp.colorPrimary
        actionButton.translatesAutoresizingMaskIntoConstraints  = false
        actionButton.rx.tap.subscribe { (onTap) in
            if AppSetUp.shared.areAllSettingsCompleted(){
                let viewControl:ActionController = self.storyboard!.instantiateViewController()
                self.navigationController?.setViewControllers([viewControl], animated: true)
            }else{
                self.view.makeToast("Please complete all settings first")
            }
        }.disposed(by: AppSetUp.disposeBag)
        return actionButton
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupnav()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setupnav()
        self.setupCustomTabbar()
        self.navigationItem.hidesBackButton = true
    }

    func alterColor(){
        homeButton.backgroundColor      = AppSetUp.colorPrimary
        actionButton.backgroundColor    = AppSetUp.colorAccent
    }

    func setupnav() {
        let logo = UIImage(named: "navtitle")
        let imageView = UIImageView(image:logo)
        imageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = imageView
    }

    func setupCustomTabbar(){
        self.view.addSubview(customTabbarView)
//      customTabbarView.topAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
        customTabbarView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        customTabbarView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        customTabbarView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        customTabbarView.heightAnchor.constraint(equalToConstant: 50).isActive = true

        self.customTabbarView.addSubview(popUpMenu)
        popUpMenu.topAnchor.constraint(equalTo: self.customTabbarView.topAnchor).isActive = true
        popUpMenu.leadingAnchor.constraint(equalTo: self.customTabbarView.leadingAnchor).isActive = true
        popUpMenu.bottomAnchor.constraint(equalTo: self.customTabbarView.bottomAnchor).isActive = true
        popUpMenu.widthAnchor.constraint(equalToConstant: 75).isActive = true

        self.customTabbarView.addSubview(stackView)
        stackView.leadingAnchor.constraint(equalTo: popUpMenu.trailingAnchor).isActive = true
        stackView.bottomAnchor.constraint(equalTo: self.customTabbarView.bottomAnchor).isActive = true
        stackView.trailingAnchor.constraint(equalTo: self.customTabbarView.trailingAnchor).isActive = true
        stackView.topAnchor.constraint(equalTo: self.customTabbarView.topAnchor).isActive = true

        stackView.addArrangedSubview(homeButton)
        homeButton.addSubview(homeIcon)
        homeIcon.snp.makeConstraints { (make) in
            make.centerY.equalTo(homeButton.snp.centerY)
            make.centerX.equalTo(homeButton.snp.centerX)
            make.height.width.equalTo(30)
        }
        stackView.addArrangedSubview(actionButton)
        actionButton.addSubview(actionIcon)
        actionIcon.snp.makeConstraints { (make) in
            make.centerY.equalTo(actionButton.snp.centerY)
            make.centerX.equalTo(actionButton.snp.centerX)
            make.height.width.equalTo(40)
        }

        dropDown.backgroundColor            = AppSetUp.colorPrimaryDark
        dropDown.selectionBackgroundColor   = AppSetUp.colorPrimaryDark
        dropDown.selectedTextColor          = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        dropDown.textColor                  = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        dropDown.anchorView                 = popUpMenu
        dropDown.dataSource                 = ["Settings","Activate kids device","Support","Parenting Tips","Logout"]
        dropDown.direction                  = .top
        dropDown.topOffset                  = CGPoint(x: 0, y:-50)
        if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular{
            dropDown.textFont               = UIFont.systemFont(ofSize: 22)
            dropDown.cellHeight             = 60
        }else{
            dropDown.textFont               = UIFont.systemFont(ofSize: 17)
        }
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            switch item {
            case "Settings":
                print("Settings")
                let viewControl:SettingsController = self.storyboard!.instantiateViewController()
                self.navigationController?.setViewControllers([viewControl], animated: true)
            case "Activate kids device":
                let viewControl:ActivateKidDeviceController = self.storyboard!.instantiateViewController()
                self.navigationController?.setViewControllers([viewControl], animated: true)
            case "Support":
                self.openUrl("https://www.facebook.com/groups/240060577373219/")
            case "Parenting Tips":
                self.openUrl("https://www.facebook.com/groups/155588992396201/")
            case "Logout":
                print("Logout")
                if AppSetUp().doLogOut(){
                    let viewControl:ParentPageMenuViewController = self.storyboard!.instantiateViewController()
                    self.navigationController?.setViewControllers([viewControl], animated: true)
                }
            default:
                print("Default")
            }
        }
    }

    func openUrl(_ url: String){
        let svc = SFSafariViewController(url: URL(string: url)!)
        svc.modalPresentationStyle = .fullScreen
        self.navigationController?.present(svc, animated: true, completion: nil)
    }
}
