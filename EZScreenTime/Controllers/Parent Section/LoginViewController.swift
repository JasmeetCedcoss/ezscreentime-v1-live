//
//  LoginViewController.swift
//  EZScreenTime
//
//  Created by cedcoss on 04/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn
import FBSDKLoginKit
import AuthenticationServices

class LoginViewController: BaseViewController {

    //  MARK: - Lazy views
    lazy var fbImageView: UIImageView = {
        let fbImageView                                        = UIImageView()
        fbImageView.image                                      = UIImage(named: "facebook")
        fbImageView.contentMode                                = .scaleAspectFit
        fbImageView.translatesAutoresizingMaskIntoConstraints  = false
        return fbImageView
    }()

    lazy var backViewfb: UIView = {
        let backViewfb                                        = UIView()
        backViewfb.backgroundColor                            = #colorLiteral(red: 0.1607843137, green: 0.231372549, blue: 0.3764705882, alpha: 1)
        backViewfb.translatesAutoresizingMaskIntoConstraints  = false
        backViewfb.layer.cornerRadius                         = 5
        backViewfb.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
        return backViewfb
    }()


    lazy var googleImageView: UIImageView = {
        let googleImageView                                        = UIImageView()
        googleImageView.image                                      = UIImage(named: "g_icon")
        googleImageView.contentMode                                = .scaleAspectFit
        googleImageView.translatesAutoresizingMaskIntoConstraints  = false
        return googleImageView
    }()

    lazy var backViewGoogle: UIView = {
        let backViewGoogle                                        = UIView()
        backViewGoogle.backgroundColor                            = #colorLiteral(red: 0.2039215686, green: 0.3411764706, blue: 0.6078431373, alpha: 1)
        backViewGoogle.translatesAutoresizingMaskIntoConstraints  = false
        backViewGoogle.layer.cornerRadius                         = 5
        backViewGoogle.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
        return backViewGoogle
    }()

    //  MARK: - Popup views
    let showKidLoginPopup           = ShowLoginPopup()
    let showSupervisorLoginPopup    = ShowLoginPopup()
    let forgotPasswordPopup         = ForgotPasswordPopup()

    //  MARK: - Data models
    var loginWelcome                : LoginWelcome?
    var configurationData           = [String:String]()
    var kidLoginModel               : KidLoginModel?
    var supervisorLoginModel        : SupervisorLoginModel?
    var window                      = UIWindow()


    @IBOutlet weak var appleSignInView: UIView!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var passView: UIView!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passField: UITextField!
    @IBOutlet weak var kidLogin: UIButton!
    @IBOutlet weak var supervisorLogin: UIButton!
    @IBOutlet weak var forgotPassword: UIButton!
    @IBOutlet weak var fbLogin: FBLoginButton!
    @IBOutlet weak var googleLogin: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        designSetUp()
        kidLoginSetup()
        checkForLoginScenarios()
        supervisorLoginSetup()
        forgotPasswordSetup()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        customTabbarView.isHidden = true
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance().delegate = self
    }

    func checkForLoginScenarios(){
        if AppSetUp.shared.isParentLogin(){
            self.navigateToHome()
        }
    }

    @IBAction func performFBLogin(_ sender: UIButton) {
        let fbLoginManager : LoginManager = LoginManager()
        //fbLoginManager.loginBehavior = .browser
        fbLoginManager.logIn(permissions: ["email"], from: self) { (result, error) -> Void in
            if (error == nil){
                print("RESULT")
                let fbloginresult : LoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions != nil
                {
                    if(fbloginresult.grantedPermissions.contains("email"))
                    {
                        self.getFBUserData()
                    }
                }
            }
        }
    }

    @IBAction func performGoogleSignIn(_ sender: UIButton) {
        print("Inside")
        GIDSignIn.sharedInstance().signIn()
    }


    func performParentLogin(_ params: [String : String]?,endPoint: String = "parent/login") {
        mageHelper().callHttpRequest(controller: self, endPoints: endPoint,parameters: params, sendAuthHeader: false,postData: true, completion: {
            data in
            do {
                let json = try JSON(data: data)
                print(json)
                if json["status"].boolValue == true{
                    self.loginWelcome = try JSONDecoder().decode(LoginWelcome.self, from: data)
                    for (key,value) in json["data"]["configuration"] {
                        print("seee",key)
                        self.configurationData[key] = value.stringValue
                    }
                    print("====",self.loginWelcome as Any)
                    print("====",self.configurationData)
                    AppSetUp.defaults.set(true, forKey: "isUserLoggedIn")
                    guard let token = self.loginWelcome?.data?.accessToken else {return}
                    AppSetUp().saveToken(token)
                    self.navigateToHome()
                }else{
                    self.view.makeToast(json["message"].stringValue, duration: 3.0)
                }
            }
            catch let error{
                print(error.localizedDescription)
            }
        })
    }

    @IBAction func loginPressed(_ sender: UIButton) {

        print("LoginPressed")
        guard let email = emailField.text, let password = passField.text else {return}

        if email.isEmpty || password.isEmpty{
            mageHelper().showAlert(view: self, msg: "All fields are required", title: "Alert") { (str) in
            }
            return
        }

        if !email.isValidEmail(email){
            self.view.makeToast("Enter a valid email address")
            return
        }


        let params: [String: String]? = ["type":"direct",
                                         "deviceType":"ios",
                                         "firebaseToken":AppSetUp.getFCMToken!,
                                         "email":email,
                                         "imei":SystemManager.getUUID(),
                                         "password":password,
                                         "tz":SystemManager.getLocalTimeZoneIdentifier()
        ]
        performParentLogin(params)
    }



    @IBAction func kidLogin(_ sender: UIButton) {
        print("dfrferfrefreferf")
    }

    @IBAction func supervisorLogin(_ sender: UIButton) {
        print("Supervisor")
    }


    func navigateToHome(){
        //  MARK: - CheckForNewAndExistingUser
        if loginWelcome?.data?.info?.isNew == "1"{
            let viewControl:SettingsController = self.storyboard!.instantiateViewController()
            viewControl.newUser = true
            self.navigationController?.setViewControllers([viewControl], animated: true)
        }else{
            if configurationData.values.contains("false"){
                let viewControl:SettingsController = self.storyboard!.instantiateViewController()
                self.navigationController?.setViewControllers([viewControl], animated: true)
            }else{
                let viewControl:MainDashboardController = self.storyboard!.instantiateViewController()
                self.navigationController?.setViewControllers([viewControl], animated: true)
            }
        }
    }

}


//  MARK: - KID LOGIN SETUP
//

extension LoginViewController{

    func kidLoginSetup(){
        kidLogin.rx.tap.subscribe { (onTap) in
            self.showKidLoginPopup.showLoginPopup()
        }.disposed(by: AppSetUp.disposeBag)

        self.showKidLoginPopup.LoginButton.rx.tap.subscribe { (onTap) in
            self.showKidLoginPopup.handleDismiss()
            guard let code = self.showKidLoginPopup.codeTextField.text else{return}

            //  MARK: - Validation
            let pasteboard = UIPasteboard.general
            self.makeKidLoginRequest(code:code)
//            if pasteboard.string == code{
//                self.makeKidLoginRequest(code:code)
//            }else{
//                self.window.makeToast("Invalid Code")
//                return
//            }
        }.disposed(by: AppSetUp.disposeBag)
    }

    func makeKidLoginRequest(code: String){
        // navigateToKidLogin()
        let params: [String: String]? = ["deviceType":"ios",
                                         "firebaseToken":AppSetUp.getFCMToken!,
                                         "imei":SystemManager.getUUID(),
                                         "code":code,
                                         "tz":SystemManager.getLocalTimeZoneIdentifier()
        ]

        mageHelper().callHttpRequest(controller: self, endPoints: "kid/login",parameters: params, sendAuthHeader: false,postData: true, completion: {
            data in
            do {
                let json = try JSON(data: data)
                print(json)
                if json["status"].boolValue == true{
                    //let token = json["data"]["access_token"].stringValue
                    self.kidLoginModel = try JSONDecoder().decode(KidLoginModel.self, from: data)
                    //  MARK: - Saving kid details to DB if kid does not exist already.
                    //
                    guard let id = self.kidLoginModel?.data?.kid?.id, let age = self.kidLoginModel?.data?.kid?.age, let icon = self.kidLoginModel?.data?.kid?.icon, let token = self.kidLoginModel?.data?.accessToken, let name = self.kidLoginModel?.data?.kid?.name else {return}
                    KidSetupSettings().checkIfKidExistInDBIfNotThenSave(id: id, icon: icon, age: age, token: token, name: name)
                    CurrentLoggedKid.id = id
                    AppSetUp.defaults.set(true, forKey: "isKidLoggedIn")
                    AppSetUp.defaults.set(id, forKey: "currentLoggedKid")
                    self.navigateToKidLogin()
                    self.view.makeToast(json["message"].stringValue, duration: 3.0)
                }else{
                    self.view.makeToast(json["message"].stringValue, duration: 3.0)
                }
            }
            catch let error{
                print(error.localizedDescription)
            }
        })
    }

    func navigateToKidLogin(){
        let storyBoard: UIStoryboard = UIStoryboard(name: "KidLoginStory", bundle: nil)
        let viewControl = storyBoard.instantiateViewController(withIdentifier: "KidLoginWelcomeScreenOne") as! KidLoginWelcomeScreenOne
        self.navigationController?.setViewControllers([viewControl], animated: true)
    }
}

//  MARK: - SUPERVISOR LOGIN SETUP
//
extension LoginViewController{

    func supervisorLoginSetup(){
        supervisorLogin.rx.tap.subscribe { (onTap) in
            self.showSupervisorLoginPopup.customLabel.text = "LOGIN AS SUPERVISOR"
            self.showSupervisorLoginPopup.showLoginPopup()
        }.disposed(by: AppSetUp.disposeBag)

        self.showSupervisorLoginPopup.LoginButton.rx.tap.subscribe { (onTap) in
            self.showSupervisorLoginPopup.handleDismiss()
            guard let code = self.showSupervisorLoginPopup.codeTextField.text else{return}
            //  MARK: - Validation
            let pasteboard = UIPasteboard.general
            self.makeSupervisorLoginRequest(code:code)
//            if pasteboard.string == code{
//                self.makeSupervisorLoginRequest(code:code)
//            }else{
//                self.window.makeToast("Invalid Code")
//                return
//            }
        }.disposed(by: AppSetUp.disposeBag)
    }

    func makeSupervisorLoginRequest(code: String){
        // navigateToKidLogin()
        let params: [String: String]? = ["deviceType":"ios",
                                         "firebaseToken":AppSetUp.getFCMToken!,
                                         "imei":SystemManager.getUUID(),
                                         "code":code,
                                         "tz":SystemManager.getLocalTimeZoneIdentifier()
        ]

        mageHelper().callHttpRequest(controller: self, endPoints: "supervisor/login",parameters: params, sendAuthHeader: false,postData: true, completion: {
            data in
            do {
                let json = try JSON(data: data)
                print(json)
                if json["status"].boolValue == true{
                    //let token = json["data"]["access_token"].stringValue
                    self.supervisorLoginModel = try JSONDecoder().decode(SupervisorLoginModel.self, from: data)
                    //                    //  MARK: - Saving kid details to DB if kid does not exist already.
                    //                    //
                    guard let supervisorToken = self.supervisorLoginModel?.data?.accessToken else {return}
                    AppSetUp.defaults.set(true, forKey: "isSupervisorLoggedIn")
                    AppSetUp.defaults.set(supervisorToken, forKey: "supervisorToken")
                    //                  self.navigateToKidLogin()

                    let viewControl = SupervisorSetupSettings.storyBoard.instantiateViewController(withIdentifier: "SupervisorDashboardController") as! SupervisorDashboardController
                    self.navigationController?.setViewControllers([viewControl], animated: true)
                    self.view.makeToast(json["message"].stringValue, duration: 3.0)
                }else{
                    self.view.makeToast(json["message"].stringValue, duration: 3.0)
                }
            }
            catch let error{
                print(error.localizedDescription)
            }
        })
    }
}
//  MARK: - FORGOT PASSWORD
//
extension LoginViewController{
    func forgotPasswordSetup(){
        forgotPassword.rx.tap.subscribe { (onTap) in
            self.forgotPasswordPopup.showLoginPopup()
        }.disposed(by: AppSetUp.disposeBag)
        var emailForOTP = String()
        self.forgotPasswordPopup.getResetCode.rx.tap.subscribe { (onTAP) in
            switch self.forgotPasswordPopup.getResetCode.currentTitle{
            case "   SEND RESET CODE   ":
                guard let email = self.forgotPasswordPopup.emailTextField.text else {return}
                let params = ["email":email]
                if !email.isValidEmail(email){
                    self.window.makeToast("Enter a valid email address")
                    return
                }
                self.window.addLoader()
                mageHelper().callHttpRequest(controller: self, endPoints: "parent/forgetPassword",parameters: params, sendAuthHeader: false,postData: true, completion: {
                    data in
                    do {
                        self.window.stopLoader()
                        let json = try JSON(data: data)
                        print(json)
                        if json["status"].boolValue == true{
                            guard let email = self.forgotPasswordPopup.emailTextField.text else {return}
                            emailForOTP = email
                            self.forgotPasswordPopup.makeOTPView()
                            self.window.makeToast(json["message"].stringValue, duration: 3.0)
                        }else{
                            self.window.makeToast(json["message"].stringValue, duration: 3.0)
                        }
                    }
                    catch let error{
                        print(error.localizedDescription)
                    }
                })
            case "  SUBMIT OTP  ":
                self.makeNetworkCallForVerifyingOTP(email:emailForOTP)
            case "  UPDATE PASSWORD  ":
                self.makeNetworkCallForUpdatingPassword(email:emailForOTP)
            default:
                print("")
            }
        }.disposed(by: AppSetUp.disposeBag)
    }


    func makeNetworkCallForVerifyingOTP(email:String){
        print("dddddddd",email)
        self.window.addLoader()
        guard let otp = self.forgotPasswordPopup.emailTextField.text else{return}
        let params = ["email":email,"otp":otp]

        mageHelper().callHttpRequest(controller: self, endPoints: "parent/verifyOtp",parameters: params, sendAuthHeader: false,postData: true, completion: {
            data in
            do {
                self.window.stopLoader()
                let json = try JSON(data: data)
                print(json)
                if json["status"].boolValue == true{
                    self.forgotPasswordPopup.makeUpdatePasswordView()
                    self.window.makeToast(json["message"].stringValue, duration: 3.0)
                }else{
                    self.window.makeToast(json["message"].stringValue, duration: 3.0)
                }
            }
            catch let error{
                print(error.localizedDescription)
            }
        })
    }

    func makeNetworkCallForUpdatingPassword(email:String){
        self.window.addLoader()
        guard let password = self.forgotPasswordPopup.emailTextField.text else{return}
        let params = ["email":email,"password":password,"varified":"true"]

        mageHelper().callHttpRequest(controller: self, endPoints: "parent/updatePassword",parameters: params, sendAuthHeader: false,postData: true, completion: {
            data in
            do {
                self.window.stopLoader()
                let json = try JSON(data: data)
                print(json)
                if json["status"].boolValue == true{
                    self.window.makeToast(json["message"].stringValue, duration: 2.0) { (_) in
                        self.forgotPasswordPopup.handleDismiss()
                        self.forgotPasswordPopup.backToBasic()
                    }
                }else{
                    self.window.makeToast(json["message"].stringValue, duration: 3.0)
                }
            }
            catch let error{
                print(error.localizedDescription)
            }
        })
    }
}

//  MARK: - Google social login

extension LoginViewController: GIDSignInDelegate{

    //MARK:Google SignIn Delegate
    // Present a view that prompts the user to sign in with Google

    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        // myActivityIndicator.stopAnimating()
    }
    
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }

    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }

    //completed sign In
    public func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {

        if (error == nil) {
            // Perform any operations on signed in user here.
            // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            let fullName = user.profile.name
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            let email = user.profile.email
            guard let userId = user.userID, let name = fullName,let emailID = email,let firstName = givenName else {return}
            print(userId)
            self.signInUsingGoogle(userId,firstName,emailID)
            // ...
        } else {
            print("\(error.localizedDescription)")
        }
    }

    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        print("Google sign in failed")
        // Perform any operations when the user disconnects from app here.
        // ...
    }

    func signInUsingGoogle(_ googleID: String,_ name: String,_ email: String){
        let params: [String: String]? = ["type":"google",
                                         "deviceType":"ios",
                                         "firebaseToken":AppSetUp.getFCMToken!,
                                         "imei":SystemManager.getUUID(),
                                         "tz":SystemManager.getLocalTimeZoneIdentifier(),
                                         "GID":googleID,"name":name,"email":email,"lang": SystemManager.getDeviceLanguage()
        ]
        performParentLogin(params)
    }
}

//  MARK: - Facebook Login Setup
extension LoginViewController:  LoginButtonDelegate{
    func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {
        print("Completed Login")
        self.getFBUserData()
    }

    func loginButtonDidLogOut(_ loginButton: FBLoginButton) {
        print("Logged Out")
    }

    func getFBUserData(){
        if((AccessToken.current) != nil){
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    //everything works print the user data
                    print(result)
                    let firstname = (result as AnyObject).value(forKey: "first_name") as! String
                    let id = (result as AnyObject).value(forKey: "id") as! String
                    let email = (result as AnyObject).value(forKey: "email") as! String
                    self.signInUsingFB(id, firstname, email)
                }
            })
        }
    }

    func signInUsingFB(_ fbID: String,_ name: String,_ email: String){
        let params: [String: String]? = ["type":"fb",
                                         "deviceType":"ios",
                                         "firebaseToken":AppSetUp.getFCMToken!,
                                         "imei":SystemManager.getUUID(),
                                         "tz":SystemManager.getLocalTimeZoneIdentifier(),
                                         "FBID":fbID,"name":name,"email":email,"lang": SystemManager.getDeviceLanguage()
        ]
        performParentLogin(params)
    }
}

//  MARK: - Apple Signin

extension LoginViewController{
    @objc func didTapAppleButton(){
        if #available(iOS 13.0, *) {
            let provider = ASAuthorizationAppleIDProvider()
            let request  = provider.createRequest()
            request.requestedScopes = [.fullName, .email]

            let controller  = ASAuthorizationController(authorizationRequests: [request])
            controller.presentationContextProvider  = self
            controller.delegate                     = self
            controller.performRequests()

        } else {
            // Fallback on earlier versions
        }
    }
}

extension LoginViewController: ASAuthorizationControllerDelegate{
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        switch authorization.credential{
        case let credentials as ASAuthorizationAppleIDCredential:
            print(credentials.user)
            print(credentials.email)
            print(credentials.fullName?.givenName)
            print(credentials.identityToken)
            if let email = credentials.email,let name = credentials.fullName?.givenName{
                signInUsingApple(credentials.user,name,email)
            }else{
                signInWithExistingAccount(credentials.user)
            }
            break
        default: break
        }
    }

    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print("Something bad happended",error)
    }

    func signInUsingApple(_ appleID: String,_ name: String,_ email: String){

        let params: [String: String]? = ["type":"apple",
                                         "deviceType":"ios",
                                         "firebaseToken":AppSetUp.getFCMToken!,
                                         "imei":SystemManager.getUUID(),
                                         "tz":SystemManager.getLocalTimeZoneIdentifier(),
                                         "apple_token":appleID,"name":name,"email":email,"lang": SystemManager.getDeviceLanguage()
        ]
        performParentLogin(params,endPoint:"parent/signup")
    }

    func signInWithExistingAccount(_ appleID:String){
        let params: [String: String]? = ["type":"apple",
                                         "deviceType":"ios",
                                         "firebaseToken":AppSetUp.getFCMToken!,
                                         "imei":SystemManager.getUUID(),
                                         "tz":SystemManager.getLocalTimeZoneIdentifier(),
                                         "apple_token":appleID,"lang": SystemManager.getDeviceLanguage()
        ]
        performParentLogin(params)
    }
}

extension LoginViewController: ASAuthorizationControllerPresentationContextProviding{
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return view.window!
    }
}

