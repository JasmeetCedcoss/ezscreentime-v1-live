//
//  LoginController+DesignExtension.swift
//  EZScreenTime
//
//  Created by cedcoss on 31/03/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import AuthenticationServices

extension LoginViewController{

    func designSetUp(){
        guard let windowadd = UIApplication.shared.keyWindow else{return}
        window = windowadd
        emailView.JSCustomView()
        passView.JSCustomView()

        fbLogin.addSubview(backViewfb)
        
        backViewfb.topAnchor.constraint(equalTo: fbLogin.topAnchor, constant: 0).isActive           = true
        backViewfb.leadingAnchor.constraint(equalTo: fbLogin.leadingAnchor, constant: 0).isActive   = true
        if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular{
            backViewfb.widthAnchor.constraint(equalToConstant: 70).isActive                             = true
        }else{
            backViewfb.widthAnchor.constraint(equalToConstant: 50).isActive                             = true
        }

        backViewfb.bottomAnchor.constraint(equalTo: fbLogin.bottomAnchor, constant: 0).isActive     = true
        backViewfb.addSubview(fbImageView)
        fbImageView.centerYAnchor.constraint(equalTo: backViewfb.centerYAnchor).isActive            = true
        fbImageView.centerXAnchor.constraint(equalTo: backViewfb.centerXAnchor).isActive            = true
        fbImageView.widthAnchor.constraint(equalToConstant: 30).isActive                            = true
        fbImageView.heightAnchor.constraint(equalToConstant: 30).isActive                           = true

        googleLogin.addSubview(backViewGoogle)
        backViewGoogle.topAnchor.constraint(equalTo: googleLogin.topAnchor, constant: 0).isActive   = true
        backViewGoogle.leadingAnchor.constraint(equalTo: googleLogin.leadingAnchor, constant: 0).isActive                                                                                                                    = true
        if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular{
            backViewGoogle.widthAnchor.constraint(equalToConstant: 70).isActive                             = true
        }else{
            backViewGoogle.widthAnchor.constraint(equalToConstant: 50).isActive                             = true
        }
        backViewGoogle.bottomAnchor.constraint(equalTo: googleLogin.bottomAnchor, constant: 0).isActive                                                                                                                      = true
        backViewGoogle.addSubview(googleImageView)
        googleImageView.centerYAnchor.constraint(equalTo: backViewGoogle.centerYAnchor).isActive    = true
        googleImageView.centerXAnchor.constraint(equalTo: backViewGoogle.centerXAnchor).isActive    = true
        googleImageView.widthAnchor.constraint(equalToConstant: 25).isActive                        = true
        googleImageView.heightAnchor.constraint(equalToConstant: 25).isActive                       = true

        //  MARK: - Apple sign in view
        if #available(iOS 13.0, *) {
            let appleButton = ASAuthorizationAppleIDButton()
            appleSignInView.addSubview(appleButton)
            appleButton.snp.makeConstraints { (make) in
                make.edges.equalTo(appleSignInView)
            }
            appleButton.addTarget(self, action: #selector(didTapAppleButton), for: .touchUpInside)
        } else {
            // Fallback on earlier versions
        }
    }
}
