//
//  SignUpViewController.swift
//  EZScreenTime
//
//  Created by cedcoss on 04/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKLoginKit
import AuthenticationServices

class SignUpViewController: UIViewController {

    //  MARK: - Lazy views
    lazy var fbImageView: UIImageView = {
        let fbImageView                                        = UIImageView()
        fbImageView.image                                      = UIImage(named: "facebook")
        fbImageView.contentMode                                = .scaleAspectFit
        fbImageView.translatesAutoresizingMaskIntoConstraints  = false
        return fbImageView
    }()

    lazy var backViewfb: UIView = {
        let backViewfb                                         = UIView()
        backViewfb.backgroundColor                             = #colorLiteral(red: 0.1607843137, green: 0.231372549, blue: 0.3764705882, alpha: 1)
        backViewfb.translatesAutoresizingMaskIntoConstraints   = false
        backViewfb.layer.cornerRadius                          = 5
        backViewfb.layer.maskedCorners                         = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
        return backViewfb
    }()


    lazy var googleImageView: UIImageView = {
        let googleImageView                                        = UIImageView()
        googleImageView.image                                      = UIImage(named: "g_icon")
        googleImageView.contentMode                                = .scaleAspectFit
        googleImageView.translatesAutoresizingMaskIntoConstraints  = false
        return googleImageView
    }()

    lazy var backViewGoogle: UIView = {
        let backViewGoogle                                        = UIView()
        backViewGoogle.backgroundColor                            = #colorLiteral(red: 0.2039215686, green: 0.3411764706, blue: 0.6078431373, alpha: 1)
        backViewGoogle.translatesAutoresizingMaskIntoConstraints  = false
        backViewGoogle.layer.cornerRadius                         = 5
        backViewGoogle.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
        return backViewGoogle
    }()

    @IBOutlet weak var appleSignInView: UIView!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var passView: UIView!
    @IBOutlet weak var confirmPassView: UIView!
    @IBOutlet weak var emailSignUpField: UITextField!
    @IBOutlet weak var passSignUpField: UITextField!
    @IBOutlet weak var fbLogin: FBLoginButton!
    @IBOutlet weak var confirmPassSignUpField: UITextField!
    @IBOutlet weak var googleLogin: GIDSignInButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        emailView.JSCustomView()
        passView.JSCustomView()
        confirmPassView.JSCustomView()
        designSetUp()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance().delegate = self
    }

    @IBAction func signUpUsingGoogle(_ sender: UIButton) {
        GIDSignIn.sharedInstance().signIn()
    }

    @IBAction func performFBSignUp(_ sender: UIButton) {
        let fbLoginManager : LoginManager = LoginManager()
        //fbLoginManager.loginBehavior = .browser
        fbLoginManager.logIn(permissions: ["email"], from: self) { (result, error) -> Void in
            if (error == nil){
                print("RESULT")
                let fbloginresult : LoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions != nil
                {
                    if(fbloginresult.grantedPermissions.contains("email"))
                    {
                        self.getFBUserData()
                    }
                }
            }
        }
    }

    func designSetUp(){

        fbLogin.addSubview(backViewfb)
        backViewfb.topAnchor.constraint(equalTo: fbLogin.topAnchor, constant: 0).isActive = true
        backViewfb.leadingAnchor.constraint(equalTo: fbLogin.leadingAnchor, constant: 0).isActive = true
        if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular{
            backViewfb.widthAnchor.constraint(equalToConstant: 70).isActive                             = true
        }else{
            backViewfb.widthAnchor.constraint(equalToConstant: 50).isActive                             = true
        }
        backViewfb.bottomAnchor.constraint(equalTo: fbLogin.bottomAnchor, constant: 0).isActive = true
        backViewfb.addSubview(fbImageView)
        fbImageView.centerYAnchor.constraint(equalTo: backViewfb.centerYAnchor).isActive = true
        fbImageView.centerXAnchor.constraint(equalTo: backViewfb.centerXAnchor).isActive = true
        fbImageView.widthAnchor.constraint(equalToConstant: 30).isActive = true
        fbImageView.heightAnchor.constraint(equalToConstant: 30).isActive = true

        googleLogin.addSubview(backViewGoogle)
        backViewGoogle.topAnchor.constraint(equalTo: googleLogin.topAnchor, constant: 0).isActive = true
        backViewGoogle.leadingAnchor.constraint(equalTo: googleLogin.leadingAnchor, constant: 0).isActive = true
        if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular{
            backViewGoogle.widthAnchor.constraint(equalToConstant: 70).isActive                             = true
        }else{
            backViewGoogle.widthAnchor.constraint(equalToConstant: 50).isActive                             = true
        }
        backViewGoogle.bottomAnchor.constraint(equalTo: googleLogin.bottomAnchor, constant: 0).isActive = true
        backViewGoogle.addSubview(googleImageView)
        googleImageView.centerYAnchor.constraint(equalTo: backViewGoogle.centerYAnchor).isActive = true
        googleImageView.centerXAnchor.constraint(equalTo: backViewGoogle.centerXAnchor).isActive = true
        googleImageView.widthAnchor.constraint(equalToConstant: 25).isActive = true
        googleImageView.heightAnchor.constraint(equalToConstant: 25).isActive = true

        //  MARK: - Apple sign in view
        if #available(iOS 13.0, *) {
            let appleButton = ASAuthorizationAppleIDButton()
            appleSignInView.addSubview(appleButton)
            appleButton.snp.makeConstraints { (make) in
                make.edges.equalTo(appleSignInView)
            }
            appleButton.addTarget(self, action: #selector(didTapAppleButton), for: .touchUpInside)

        } else {
            // Fallback on earlier versions
        }
    }

    fileprivate func performSignUp(_ params: [String : String]?) {
        mageHelper().callHttpRequest(controller: self, endPoints: "parent/signup", parameters: params, sendAuthHeader: false,postData: true, completion: {
            data in
            do {
                let json = try JSON(data: data)
                print(json)
                if json["status"].boolValue == true{
                    let viewController = SignUpSuccessController()
                    self.navigationController?.setViewControllers([viewController], animated: true)
                }else{
                    self.view.makeToast(json["message"].stringValue, duration: 3.0)
                }
            }
            catch let error{
                print(error.localizedDescription)
            }
        })
    }

    @IBAction func registerUser(_ sender: UIButton) {

        guard let emailSignUp = emailSignUpField.text, let passSignUp = passSignUpField.text, let confirmPassSignUp = confirmPassSignUpField.text else {return}

        

        if emailSignUp.isEmpty || emailSignUp.isEmpty || confirmPassSignUp.isEmpty{
            self.view.makeToast("All fields are required")
            return
        }else if passSignUp != confirmPassSignUp{
            self.view.makeToast("Password does not match")
            return
        }

        if !emailSignUp.isValidEmail(emailSignUp){
            self.view.makeToast("Enter a valid email address")
            return
        }

        let params: [String: String]? = ["type":"direct",
                                         "deviceType":"ios",
                                         "firebaseToken":AppSetUp.getFCMToken!,
                                         "email":emailSignUp,
                                         "imei":SystemManager.getUUID(),
                                         "password":passSignUp,
                                         "tz":SystemManager.getLocalTimeZoneIdentifier(),
                                         "lang": SystemManager.getDeviceLanguage()
        ]
        performSignUp(params)
    }
}

extension SignUpViewController: GIDSignInDelegate{

    //MARK:Google SignIn Delegate
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        // myActivityIndicator.stopAnimating()
    }
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }

    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }

    //completed sign In
    public func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {

        if (error == nil) {
            // Perform any operations on signed in user here.
            // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            let fullName = user.profile.name
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            let email = user.profile.email
            guard let userId = user.userID, let name = fullName,let emailID = email,let firstName = givenName else {return}
            print(userId)
            self.signInUsingGoogle(userId,firstName,emailID)
            // ...
        } else {
            print("\(error.localizedDescription)")
        }
    }

    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        print("Google sign in failed")
        // Perform any operations when the user disconnects from app here.
        // ...
    }

    func signInUsingGoogle(_ googleID: String,_ name: String,_ email: String){
        let params: [String: String]? = ["type":"google",
                                         "deviceType":"ios",
                                         "firebaseToken":AppSetUp.getFCMToken!,
                                         "imei":SystemManager.getUUID(),
                                         "tz":SystemManager.getLocalTimeZoneIdentifier(),
                                         "GID":googleID,"name":name,"email":email,"lang": SystemManager.getDeviceLanguage()
        ]
//        mageHelper().callHttpRequest(controller: self, endPoints: "parent/signup", parameters: params, sendAuthHeader: false,postData: true, completion: {
//            data in
//            do {
//                let json = try JSON(data: data)
//                print(json)
//                let viewControl:LoginViewController = self.storyboard!.instantiateViewController()
//                viewControl.performParentLogin(params,endPoint: "parent/signup")
//                self.navigationController?.setViewControllers([viewControl], animated: false)
//            }
//            catch let error{
//                print(error.localizedDescription)
//            }
//        })
        let viewControl:LoginViewController = self.storyboard!.instantiateViewController()
        viewControl.performParentLogin(params,endPoint: "parent/signup")
        self.navigationController?.setViewControllers([viewControl], animated: false)
    }
}

//  MARK: - Facebook Login Setup
extension SignUpViewController:  LoginButtonDelegate{
    func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {
        print("Completed Login")
        self.getFBUserData()
    }

    func loginButtonDidLogOut(_ loginButton: FBLoginButton) {
        print("Logged Out")
    }

    func getFBUserData(){
        if((AccessToken.current) != nil){
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    //everything works print the user data
                    print(result)
                    let firstname = (result as AnyObject).value(forKey: "first_name") as! String
                    let id = (result as AnyObject).value(forKey: "id") as! String
                    let email = (result as AnyObject).value(forKey: "email") as! String
                    self.signInUsingFB(id, firstname, email)
                }
            })
        }
    }

    func signInUsingFB(_ fbID: String,_ name: String,_ email: String){
        let params: [String: String]? = ["type":"fb",
                                         "deviceType":"ios",
                                         "firebaseToken":AppSetUp.getFCMToken!,
                                         "imei":SystemManager.getUUID(),
                                         "tz":SystemManager.getLocalTimeZoneIdentifier(),
                                         "FBID":fbID,"name":name,"email":email,"lang": SystemManager.getDeviceLanguage()
        ]
        mageHelper().callHttpRequest(controller: self, endPoints: "parent/signup", parameters: params, sendAuthHeader: false,postData: true, completion: {
            data in
            do {
                let json = try JSON(data: data)
                print(json)
                let viewControl:LoginViewController = self.storyboard!.instantiateViewController()
                viewControl.performParentLogin(params)
                self.navigationController?.setViewControllers([viewControl], animated: false)
            }
            catch let error{
                print(error.localizedDescription)
            }
        })
    }
}

extension SignUpViewController{
    @objc func didTapAppleButton(){
        if #available(iOS 13.0, *) {
            let provider = ASAuthorizationAppleIDProvider()
            let request  = provider.createRequest()
            request.requestedScopes = [.fullName, .email]

            let controller  = ASAuthorizationController(authorizationRequests: [request])
            controller.presentationContextProvider  = self
            controller.delegate                     = self
            controller.performRequests()

        } else {
            // Fallback on earlier versions
        }
    }
}

//  MARK: - Apple Signin

extension SignUpViewController: ASAuthorizationControllerDelegate{
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        switch authorization.credential{
        case let credentials as ASAuthorizationAppleIDCredential:
            print(credentials.user)
            print(credentials.email)
            print(credentials.fullName?.givenName)
            print(credentials.identityToken)
            if let email = credentials.email,let name = credentials.fullName?.givenName{
                signInUsingApple(credentials.user,name,email)
            }else{
                signInWithExistingAccount(credentials.user)
            }
            break
        default: break
        }
    }

    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print("Something bad happended",error)
    }

    func signInUsingApple(_ appleID: String,_ name: String,_ email: String){

        let params: [String: String]? = ["type":"apple",
                                         "deviceType":"ios",
                                         "firebaseToken":AppSetUp.getFCMToken!,
                                         "imei":SystemManager.getUUID(),
                                         "tz":SystemManager.getLocalTimeZoneIdentifier(),
                                         "apple_token":appleID,"name":name,"email":email,"lang": SystemManager.getDeviceLanguage()
        ]
        let viewControl:LoginViewController = self.storyboard!.instantiateViewController()
        viewControl.performParentLogin(params,endPoint:"parent/signup")
        self.navigationController?.setViewControllers([viewControl], animated: false)
    }

    func signInWithExistingAccount(_ appleID:String){
        let params: [String: String]? = ["type":"apple",
                                         "deviceType":"ios",
                                         "firebaseToken":AppSetUp.getFCMToken!,
                                         "imei":SystemManager.getUUID(),
                                         "tz":SystemManager.getLocalTimeZoneIdentifier(),
                                         "apple_token":appleID,"lang": SystemManager.getDeviceLanguage()
        ]

        let viewControl:LoginViewController = self.storyboard!.instantiateViewController()
        viewControl.performParentLogin(params)
        self.navigationController?.setViewControllers([viewControl], animated: false)
    }
}

extension SignUpViewController: ASAuthorizationControllerPresentationContextProviding{
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return view.window!
    }
}

