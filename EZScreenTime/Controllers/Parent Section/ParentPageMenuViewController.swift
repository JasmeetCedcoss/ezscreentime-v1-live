//
//  ParentPageMenuViewController.swift
//  EZScreenTime
//
//  Created by cedcoss on 04/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

class ParentPageMenuViewController: BaseViewController {

    var pageMenu : CAPSPageMenu?

    override func viewDidLoad() {
        super.viewDidLoad()
        entryCheckPoint()
    }

    func entryCheckPoint(){
        if KidSetupSettings.shared.isKidLogin(){
            let viewControl = KidSetupSettings.storyBoard.instantiateViewController(withIdentifier: "KidDashboardController") as! KidDashboardController
            CurrentLoggedKid.id = AppSetUp.defaults.value(forKey: "currentLoggedKid") as! String
            self.navigationController?.setViewControllers([viewControl], animated: true)
        }

        if SupervisorSetupSettings.shared.isSupervisorLogin(){
            let viewControl = SupervisorSetupSettings.storyBoard.instantiateViewController(withIdentifier: "SupervisorDashboardController") as! SupervisorDashboardController
            self.navigationController?.setViewControllers([viewControl], animated: true)
        }

        customTabbarView.isHidden = true

        if AppSetUp.shared.isParentLogin(){
            mageHelper().callSettings(controller: self) { (settingsCompleted) in
                if settingsCompleted{
                    let viewControl:MainDashboardController = self.storyboard!.instantiateViewController()
                    self.navigationController?.setViewControllers([viewControl], animated: true)
                    return
                }else{
                    let viewControl:SettingsController = self.storyboard!.instantiateViewController()
                    self.navigationController?.setViewControllers([viewControl], animated: true)
                    return
                }
            }
        }else{
            myPageMenuSetup()
        }
    }


    func myPageMenuSetup() {

        var controllerArray : [UIViewController] = []

        //1st
        if let controller1 : LoginViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController {
            controller1.title = "LOGIN"

            //2nd
            if let controller2 : SignUpViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController {
                controller2.title =  "SIGN UP"
                controllerArray.append(controller1)
                controllerArray.append(controller2)
            }
        }

        let parameters: [CAPSPageMenuOption] = [
            .scrollMenuBackgroundColor(UIColor.white),
            .viewBackgroundColor(UIColor.white),
            .selectionIndicatorColor(UIColor.darkGray),
            .bottomMenuHairlineColor(UIColor.lightGray),
            .menuItemFont(UIFont.systemFont(ofSize: 15)),
            .menuHeight(40.0),
            .menuItemWidth(self.view.frame.width/2),
            .centerMenuItems(true),
            .selectedMenuItemLabelColor(UIColor.black),
            .unselectedMenuItemLabelColor(UIColor.darkGray)
        ]
        
        // Initialize scroll menu
        //#warning("Temporary solution for yAxis")
        //let yAxis = SystemManager.getDeviceScreenHeight() > 667 ? 90 : 75
        let rect = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: self.view.frame.width, height: self.view.frame.height))
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: rect, pageMenuOptions: parameters)

        self.addChild(pageMenu!)
        self.view.addSubview(pageMenu!.view)
    }
}
