//
//  MainDashboardController.swift
//  EZScreenTime
//
//  Created by cedcoss on 06/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

class MainDashboardController: BaseViewController {

    let parentOverviewPopup     =  ParentOverviewPopup()
    var parentOverviewModel     :  ParentOverviewModel?
    var resfreshControl         =  UIRefreshControl()
    var overViewSelected        :  String? = "today"
    var parentDashboard         : ParentDashboardModel?{
        didSet{
            resfreshControl.endRefreshing()
            if self.parentDashboard?.data?.dayNumber == "1"{
                if AppSetUp.defaults.value(forKey: "userViewedNoti") == nil {
                    AppSetUp.defaults.set(false, forKey: "userViewedNoti")
                }
            }else{
                AppSetUp.defaults.set(nil, forKey: "userViewedNoti")
            }
            self.tableView.reloadData()
        }
    }

    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTable()
        makeNetworkCall()
        NotificationCenter.default.addObserver(self, selector: #selector(performParentDashboardUpdate(notification: )), name: .performParentDashboardUpdate, object: nil)

    }

    @objc func pullToRefresh(sender:UIRefreshControl){
        if overViewSelected == "today"{
            makeNetworkCall(endPoint: "parent/dashboard?select_type=day")
        }else{
            makeNetworkCall(endPoint: "parent/dashboard?select_type=week")
        }
    }

    @objc func performParentDashboardUpdate(notification:Notification){
        makeNetworkCall()
    }

    func setupTable(){
        tableView.delegate          = self
        tableView.dataSource        = self
        tableView.register(HeaderTVCell.self, forCellReuseIdentifier: "HeaderTVCell")
        tableView.separatorStyle    = .none
        tableView.backgroundColor   = AppSetUp.controllerBackgroundColor
        tableView.register(MondayNotiTVCell.self, forCellReuseIdentifier: "MondayNotiTVCell")
        resfreshControl.addTarget(self, action: #selector(pullToRefresh(sender:)), for: .valueChanged)
        resfreshControl.attributedTitle = NSAttributedString(string: "Loading...")
        tableView.refreshControl = resfreshControl
    }
    

    func makeNetworkCall(endPoint:String = "parent/dashboard?select_type=day"){
        mageHelper().callHttpRequest(controller: self, endPoints: endPoint, parameters: nil,sendAuthHeader: true) { (data) in
            do {
                let json = try JSON(data: data)
                print(json)
                if json["status"].boolValue == true{
                    //  MARK: - Save latest token
                    //
                    let token = json["data"]["access_token"].stringValue
                    AppSetUp().saveToken(token)
                    AppSetUp.decoder.keyDecodingStrategy = .convertFromSnakeCase
                    self.parentDashboard = try? AppSetUp.decoder.decode(ParentDashboardModel.self, from: data)
                    print("parentDashboard====",self.parentDashboard as Any)

                }else{
                    let viewControl:SettingsController = self.storyboard!.instantiateViewController()
                    self.navigationController?.setViewControllers([viewControl], animated: false)
                }
            }
            catch let error{
                print(error.localizedDescription)
            }
        }
    }
}

extension MainDashboardController: UITableViewDataSource,UITableViewDelegate{

    func numberOfSections(in tableView: UITableView) -> Int {
        guard let readyToLoad = self.parentDashboard?.status else {return 0}
        return readyToLoad ? 3 : 0
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            if AppSetUp.defaults.value(forKey: "userViewedNoti") as? Bool == false {
                return 2
            }else {
                return 1
            }
        case 1:
            return 1
        case 2:
            return self.parentDashboard?.data?.kids?.count ?? 0
        default:
            return 0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            switch indexPath.row{
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "MondayNotiTVCell", for: indexPath) as! MondayNotiTVCell
                cell.setupView()
                AppSetUp.defaults.set(true, forKey: "userViewedNoti")
                return cell
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderTVCell", for: indexPath) as! HeaderTVCell
                let data = self.parentDashboard?.data
                guard let week = data?.weekNumber, let day = data?.dayNumber else {
                    let emptyCell = UITableViewCell()
                    emptyCell.backgroundColor = AppSetUp.controllerBackgroundColor
                    return emptyCell
                }
                cell.setupView(header: "Coins Overview",image: "coinOverview",subheader: "Week \(week) - Day \(day)")
                cell.backIcon.isHidden = true
                return cell
            }
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "OverviewSwitcherTVCell", for: indexPath) as! OverviewSwitcherTVCell
            cell.backgroundColor = AppSetUp.controllerBackgroundColor
            cell.todayOverview.rx.tap.subscribe { (onTap) in
                print("Today")
                self.overViewSelected = "today"
                self.makeNetworkCall(endPoint: "parent/dashboard?select_type=day")
            }.disposed(by: cell.disposeBag)

            cell.weekOverview.rx.tap.subscribe { (onTap) in
                print("Week")
                self.overViewSelected = "week"
                self.makeNetworkCall(endPoint: "parent/dashboard?select_type=week")
            }.disposed(by: cell.disposeBag)
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ParentDashboardTVCell", for: indexPath) as! ParentDashboardTVCell
            if self.parentDashboard?.data?.kids?[indexPath.row].devices?.count == 0{
                cell.deviceAvailaibilityMessage.text = "No device assigned to kid"
            }else{
                cell.deviceAvailaibilityMessage.text = ""
            }
            let kidDevices      = self.parentDashboard?.data?.kids?[indexPath.row]
            cell.kidDevices     = kidDevices
            cell.tapgesture.rx.event.bind(onNext: { recognizer in
                self.parentOverviewSetup(kid: self.parentDashboard?.data?.kids?[indexPath.row].id ?? "", kidName:  self.parentDashboard?.data?.kids?[indexPath.row].name ?? "")
            }).disposed(by: cell.disposeBag)
            return cell
        default:
            return UITableViewCell()
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            switch indexPath.row{
            case 1: return 120
            default: return UITableView.automaticDimension
            }
        case 1:
            return 50
        case 2:
            guard let devicesount = self.parentDashboard?.data?.kids?[indexPath.row].devices?.count else {return CGFloat()}
            if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular{
                return CGFloat(((devicesount) * 83) + 180)
            }else{
                return CGFloat(((devicesount) * 73) + 130)
            }
        default:
            return UITableView.automaticDimension
        }
    }
}

extension MainDashboardController{
    func parentOverviewSetup(kid:String,kidName:String){
        mageHelper().callHttpRequest(controller: self, endPoints: "parent/dashboard/kid/usage?kidId=\(kid)", parameters: nil,sendAuthHeader: true) { (data) in
            do {
                let json = try JSON(data: data)
                print(json)
                if json["status"].boolValue == true{
                    //  MARK: - Save latest token get request
                    //
                    let token = json["data"]["access_token"].stringValue
                    AppSetUp().saveToken(token)
                    //  MARK: - Data Parsing
                    //
                    AppSetUp.decoder.keyDecodingStrategy = .convertFromSnakeCase
                    self.parentOverviewModel = try AppSetUp.decoder.decode(ParentOverviewModel.self, from: data)
                    print("parentOverviewModel====",self.parentOverviewModel as Any)
                    self.parentOverviewPopup.showLoginPopup(kidName: kidName)
                    self.parentOverviewPopup.dataPopup = self.parentOverviewModel
                }else{
                    self.view.makeToast(json["message"].stringValue, duration: 3.0)
                }
            }
            catch let error{
                print(error)
            }
        }
    }
}
