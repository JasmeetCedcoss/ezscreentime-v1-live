//
//  MondayNotiTVCell.swift
//  EZScreenTime
//
//  Created by cedcoss on 05/05/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

class MondayNotiTVCell: UITableViewCell {


    lazy var backView: UIView = {
        let backView                                        = UIView()
        backView.backgroundColor                            = #colorLiteral(red: 1, green: 0.9803921569, blue: 0.6823529412, alpha: 1)
        backView.translatesAutoresizingMaskIntoConstraints  = false
        backView.layer.borderColor                          = #colorLiteral(red: 1, green: 0.8529717198, blue: 0.07245071727, alpha: 1)
        backView.layer.cornerRadius                         = 10
        backView.layer.borderWidth                          = 2
        return backView
    }()

    lazy var customLabel: UILabel = {
        let customLabel                                        = UILabel()
        customLabel.textColor                                  = UIColor.black
        customLabel.translatesAutoresizingMaskIntoConstraints  = false
        customLabel.numberOfLines                              = 0
        customLabel.textAlignment                              = .center
        customLabel.text                                       = """
        **NOTE**
        Amount of coins
        has been reset for new week.
        """
        return customLabel
    }()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setupView(){
        self.backgroundColor = AppSetUp.controllerBackgroundColor
        self.addSubview(backView)
        backView.snp.makeConstraints { (make) in
            make.top.left.equalTo(self).offset(10)
            make.bottom.right.equalTo(self).offset(-10)
        }
        self.backView.addSubview(customLabel)
        customLabel.snp.makeConstraints { (make) in
            make.top.left.equalTo(self).offset(5)
            make.bottom.right.equalTo(self).offset(-5)
        }

    }
}
