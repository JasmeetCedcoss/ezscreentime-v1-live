//
//  ParentDashboardTVCell.swift
//  EZScreenTime
//
//  Created by cedcoss on 05/03/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

class ParentDashboardTVCell: UITableViewCell {

    let tapgesture              = UITapGestureRecognizer()
    private(set) var disposeBag = DisposeBag()

    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag() // because life cicle of every cell ends on prepare for reuse
    }

    @IBOutlet weak var deviceAvailaibilityMessage: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var kidName: UILabel!
    @IBOutlet weak var kidIcon: UIImageView!
    @IBOutlet weak var kidIconContainer: UIView!
    @IBOutlet weak var containerView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }

    func setupView(){
        collectionView.delegate                                  = self
        collectionView.dataSource                                = self
        kidIconContainer.layer.borderColor                       = #colorLiteral(red: 0.007843137255, green: 0.337254902, blue: 0.5098039216, alpha: 1)
        kidIconContainer.layer.borderWidth                       = 5
        kidIconContainer.layer.cornerRadius                      = AppSetUp.isIpad ? 120 * 0.5 : 90 * 0.5
        self.contentView.isUserInteractionEnabled                = true
        self.contentView.addGestureRecognizer(tapgesture)
        self.containerView.layer.maskedCorners                   = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        self.contentView.backgroundColor                         = AppSetUp.controllerBackgroundColor
    }

    var kidDevices: ParentDashboardKid?{
        didSet{
            kidName.text    = kidDevices?.name
            kidIcon.image   = UIImage(named: kidDevices?.icon ?? "")
            self.collectionView.reloadData()
        }
    }
}

extension ParentDashboardTVCell: UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.kidDevices?.devices?.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ParentDashboardCVCell", for: indexPath) as! ParentDashboardCVCell
        let devices     = self.kidDevices?.devices?[indexPath.row]
        cell.devices    = devices
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular{
            return CGSize(width: collectionView.frame.width, height: 75)
        }else{
            return CGSize(width: collectionView.frame.width, height: 65)
        }
    }
}


