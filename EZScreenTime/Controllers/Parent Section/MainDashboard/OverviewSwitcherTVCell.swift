//
//  OverviewSwitcherTVCell.swift
//  EZScreenTime
//
//  Created by cedcoss on 15/05/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit
import DLRadioButton

class OverviewSwitcherTVCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        todayOverview.sendActions(for: .touchUpInside)
        self.backgroundColor = AppSetUp.controllerBackgroundColor
    }

    private(set) var disposeBag = DisposeBag()

    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag() // because life cicle of every cell ends on prepare for reuse
    }

    @IBOutlet weak var todayOverview: DLRadioButton!
    @IBOutlet weak var weekOverview: DLRadioButton!

}
