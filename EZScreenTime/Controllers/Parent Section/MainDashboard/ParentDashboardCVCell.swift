//
//  ParentDashboardCVCell.swift
//  EZScreenTime
//
//  Created by cedcoss on 05/03/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

class ParentDashboardCVCell: UICollectionViewCell {
    
    @IBOutlet weak var isActiveImage: UIImageView!
    @IBOutlet weak var deviceName: UILabel!
    @IBOutlet weak var coinsDetail: UILabel!

    private(set) var disposeBag = DisposeBag()

    var devices: ParentDashboardDevice?{
        didSet{
            if devices?.isActive == 1{
                isActiveImage.backgroundColor = #colorLiteral(red: 0.6980392157, green: 0.7019607843, blue: 0.003921568627, alpha: 1)
            }else{
                isActiveImage.backgroundColor = AppSetUp.controllerBackgroundColor
            }
            deviceName.text     = devices?.name
            guard let coinsLeft = devices?.coinsAvailable, let totalCoins = devices?.coinsReceived,let totalTime = devices?.totalTimeLeft else {return}
            coinsDetail.text = coinsLeft + " coins left / " + totalCoins + "\n(\(totalTime))"
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
  
        self.contentView.JSCustomView2()
        isActiveImage.layer.cornerRadius = AppSetUp.isIpad ? 30 *  0.5 : 20 * 0.5
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag() // because life cicle of every cell ends on prepare for reuse
    }
}
