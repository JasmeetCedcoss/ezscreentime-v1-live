//
//  ParentOverviewTVCell.swift
//  EZScreenTime
//
//  Created by cedcoss on 28/03/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

class ParentOverviewTVCell: UITableViewCell {

    lazy var headerLabel: UILabel = {
        let headerLabel                                        = UILabel()
        headerLabel.textColor                                  = UIColor.black
        headerLabel.backgroundColor                            = #colorLiteral(red: 0.7540688515, green: 0.7540867925, blue: 0.7540771365, alpha: 1)
        headerLabel.translatesAutoresizingMaskIntoConstraints  = false
        headerLabel.numberOfLines                              = 0
        headerLabel.layer.cornerRadius                         = 5
        headerLabel.font                                       = UIFont(name: "Poppins-Regular", size: 17)
        return headerLabel
    }()

    lazy var collectionView: UICollectionView = {
        let layout                  = UICollectionViewFlowLayout()
        layout.scrollDirection      = .vertical
        let collection              = UICollectionView(frame: CGRect(x: 0, y: 0, width: 0, height: 0), collectionViewLayout: layout)
        collection.translatesAutoresizingMaskIntoConstraints = false
        collection.isScrollEnabled  = true
        collection.backgroundColor  = AppSetUp.controllerBackgroundColor
        return collection
    }()

    var data: DevicesUsage?{
        didSet{
            self.collectionView.reloadData()
        }
    }

    func setupView(){

        self.addSubview(headerLabel)
        headerLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 8).isActive = true
        headerLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 8).isActive = true
        headerLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -8).isActive = true
        headerLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true

        self.addSubview(collectionView)
        collectionView.topAnchor.constraint(equalTo: headerLabel.bottomAnchor, constant: 8).isActive = true
        collectionView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 8).isActive = true
        collectionView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -8).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true

        collectionView.delegate     = self
        collectionView.dataSource   = self
        let nibCell = UINib(nibName: "ParentOverviewCVCell", bundle: nil)
        collectionView.register(nibCell, forCellWithReuseIdentifier: "ParentOverviewCVCell")
    }


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
}

extension ParentOverviewTVCell: UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data?.usage?.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ParentOverviewCVCell", for: indexPath) as! ParentOverviewCVCell
        let detail = data?.usage?[indexPath.row]
        cell.timeDetail.text = """
        start   \(detail?.started ?? "")   end   \(detail?.ended ?? "")  (\(detail?.minutes ?? "")mins)
        """
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 50)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}
