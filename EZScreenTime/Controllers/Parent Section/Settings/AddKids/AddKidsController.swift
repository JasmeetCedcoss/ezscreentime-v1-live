//
//  AddKidsController.swift
//  EZScreenTime
//
//  Created by cedcoss on 13/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

class AddKidsController: BaseViewController {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var tableView: UITableView!

    var isChecked           = true
    var showKidsDevices     = true
    var selectedRowIndex    : NSIndexPath = NSIndexPath(row: -1, section: 0)

    var addKidsModel: AddKidsModel?{
        didSet{
            self.tableView.reloadData()
        }
    }

    lazy var addKidFloatingButton: UIButton = {
        let addKidFloatingButton                                        = UIButton()
        addKidFloatingButton.backgroundColor                            = AppSetUp.colorAccent
        addKidFloatingButton.translatesAutoresizingMaskIntoConstraints  = false
        addKidFloatingButton.setImage(UIImage(named: "add")?.withRenderingMode(.alwaysTemplate), for: .normal)
        addKidFloatingButton.tintColor = .white
        addKidFloatingButton.imageEdgeInsets = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        return addKidFloatingButton
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        makeNetworkCall()
        setupTable()
    }

    func setupTable(){
        tableView.delegate          = self
        tableView.dataSource        = self
        tableView.register(HeaderTVCell.self, forCellReuseIdentifier: "HeaderTVCell")
        tableView.register(NavigationCell.self, forCellReuseIdentifier: "NavigationCell")
        tableView.separatorStyle    = .none
        tableView.backgroundColor   = AppSetUp.controllerBackgroundColor

        //  MARK: - Floating button setup
        //
        self.mainView.addSubview(addKidFloatingButton)
        addKidFloatingButton.widthAnchor.constraint(equalToConstant: 70).isActive = true
        addKidFloatingButton.heightAnchor.constraint(equalToConstant: 70).isActive = true
        addKidFloatingButton.bottomAnchor.constraint(equalTo: self.mainView.bottomAnchor, constant: -20).isActive = true
        addKidFloatingButton.trailingAnchor.constraint(equalTo: self.mainView.trailingAnchor, constant: -18).isActive = true
        addKidFloatingButton.layer.cornerRadius                            = 70 * 0.5

        addKidFloatingButton.rx.tap.subscribe { (onTapCall) in
            self.callApiToAddKid()
        }.disposed(by: AppSetUp.disposeBag)
    }

    func makeNetworkCall(){

        mageHelper().callHttpRequest(controller: self, endPoints: "parent/settings/kids", parameters: nil,sendAuthHeader: true) { (data) in
            do {
                let json = try JSON(data: data)
                print(json)
                if json["status"].boolValue == true{


                    //  MARK: - Save latest token
                    //
                    let token = json["data"]["access_token"].stringValue
                    AppSetUp().saveToken(token)

                    //  MARK: - Populating Model
                    //
                    AppSetUp.decoder.keyDecodingStrategy = .convertFromSnakeCase
                    self.addKidsModel = try? AppSetUp.decoder.decode(AddKidsModel.self, from: data)
                    print("addKidsModel====",self.addKidsModel as Any)
                }else{
                    self.view.makeToast(json["message"].stringValue, duration: 3.0)
                }
            }
            catch let error{
                print(error.localizedDescription)
            }
        }
    }


    func callApiToAddKid(){
        let kidCount = ((self.addKidsModel?.data?.kids?.count ?? 0)+1)
        print("called")
        let params = ["name":"ChildName " + String(kidCount),"icon":"boy1","age":"3"]
        mageHelper().callHttpRequest(controller: self, endPoints: "parent/settings/kids",parameters: params, sendAuthHeader: true,postData: true,addStringToParam: "kid", completion: {
            data in
            do {
                let json = try JSON(data: data)
                print(json)
                if json["status"].boolValue == true{
                    let token = json["data"]["access_token"].stringValue
                    AppSetUp().saveToken(token)
                    self.view.makeToast(json["message"].stringValue, duration: 3.0)

                    //27th April
                    let viewControl:AddKidsController = self.storyboard!.instantiateViewController()
                    self.navigationController?.pushViewController(viewControl, animated: false)

                    guard let navigationController = self.navigationController else { return }
                    var navigationArray = navigationController.viewControllers // To get all UIViewController stack as Array
                    navigationArray.remove(at: navigationArray.count - 2) // To remove previous UIViewController
                    self.navigationController?.viewControllers = navigationArray
                    //end
                    //self.navigationController?.popToViewController(viewControl, animated: false)
                    //self.makeNetworkCall()
                }else{
                    self.view.makeToast(json["message"].stringValue, duration: 3.0)
                }
            }
            catch let error{
                print(error.localizedDescription)
            }
        })
    }

    //  MARK: - DELETE KID
    //
    func callApiToDeleteKid(with id: String){

        mageHelper().callHttpRequestDelete(controller: self, endPoints: "parent/settings/kids?id=\(id)", parameters: nil, sendAuthHeader: true, completion: {
            data in
            do {
                let json = try JSON(data: data)
                print(json)
                if json["status"].boolValue == true{
                    let token = json["data"]["access_token"].stringValue
                    AppSetUp().saveToken(token)
                    self.view.makeToast(json["data"]["message"].stringValue, duration: 1.0,position: .top)
                    self.makeNetworkCall()
                }else{
                    self.view.makeToast(json["data"]["message"].stringValue, duration: 1.0)
                }
            }
            catch let error{
                print(error.localizedDescription)
            }
        })
    }
}

extension AddKidsController: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let readyToLoad = self.addKidsModel?.status else {return 0}
        return readyToLoad ? 3 : 0
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 1:
            return self.addKidsModel?.data?.kids?.count ?? 0
        default:
            return 1
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderTVCell", for: indexPath) as! HeaderTVCell
            cell.setupView(header: "Add Kids",image: "settings",subheader: "Add the names, assign devices, avatar, age and coins per week")
            cell.parent = self
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddKidsTVCell", for: indexPath) as! AddKidsTVCell
            cell.setupView()
            cell.parent             = self
            cell.delegate           = self
            cell.tag                = indexPath.row
            //  MARK: - Datasource
            //
            cell.kids               = self.addKidsModel?.data?.kids?[indexPath.row]
            cell.addKidsModel       = self.addKidsModel
            print("Index===",indexPath.row)
            cell.KidDevices         = self.addKidsModel?.data?.kids?[indexPath.row].devices
            //  MARK: - Expand/Collapse section
            //
            cell.showKidDevicesButton.rx.tap.subscribe { (onTap) in
                self.isChecked = !self.isChecked
                if self.isChecked{
                    print("Closed")
                    self.selectedRowIndex = indexPath as NSIndexPath
                    tableView.beginUpdates()
                    self.showKidsDevices = false
                    tableView.endUpdates()
                }else{
                    print("Opened")
                    self.selectedRowIndex = indexPath as NSIndexPath
                    tableView.beginUpdates()
                    self.showKidsDevices = true
                    tableView.endUpdates()
                }
            }.disposed(by: cell.disposeBag)

            //  MARK: - Reactive buttons tap
            //
            cell.ageSelection.rx.tap.subscribe { (onTAp) in
                cell.ageDropDown.show()
            }.disposed(by: cell.disposeBag)

            //  MARK: -
            //
            cell.kidNameField.rx.controlEvent(.editingDidEnd)
                .asObservable()
                .subscribe(onNext: { _ in
                    print("editing state changed")
                    guard let id = self.addKidsModel?.data?.kids?[indexPath.row].id,let name = cell.kidNameField.text,let icon = cell.kidiconButton.restorationIdentifier,let age = cell.ageSelection.currentTitle else {return}
                    //self.addKidsModel?.data?.kids?[indexPath.row].name = name
                    self.fireKidsUpdate(id: id, name: name, icon: icon, age: age)
                }).disposed(by: cell.disposeBag)

            //  MARK: - RXSWIFT KIDS SELECTION/DESELECTION OBSERVER
            //
            cell.kidSelectionDeselectionEventFired.asObservable().subscribe(
                onNext: { value in
                    print(value.self)
                    value.self ? self.makeNetworkCall(): print("rxrxrxrxrxrxrxrxrxrxrxrxrxr",print(value.self))
            }
            ).disposed(by: cell.disposeBag)
            return cell

        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "NavigationCell", for: indexPath) as! NavigationCell
            cell.setupView()
            cell.parent = self
            cell.backButton.rx.tap.subscribe { (onTap) in
                self.navigationController?.popViewController(animated: true)
            }.disposed(by: cell.disposeBag)

            cell.nextButton.rx.tap.subscribe { (onTap) in
                let viewControl:RewardMessagesController = self.storyboard!.instantiateViewController()
                self.navigationController?.pushViewController(viewControl, animated: true)
            }.disposed(by: cell.disposeBag)
            return cell
        default:
            return UITableViewCell()
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 1:
            if indexPath.row == selectedRowIndex.row {
                let tableDefaultCollapsedHeight = 200
                if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular{
                    return showKidsDevices ? CGFloat((self.addKidsModel?.data?.devices?.count)! * 125) + CGFloat(tableDefaultCollapsedHeight): UITableView.automaticDimension
                }else{
                    return showKidsDevices ? CGFloat((self.addKidsModel?.data?.devices?.count)! * 108) + CGFloat(tableDefaultCollapsedHeight): UITableView.automaticDimension
                }
            }
        case 2:
            return AppSetUp.isIpad ? 70 : 60
        default:
            return UITableView.automaticDimension
        }
        return UITableView.automaticDimension
    }

    //  MARK: - Fired from 3 views textfield, age, icon
    //
    func fireKidsUpdate(id:String,name:String,icon: String,age:String){
        print("id==",id,"name==",name,"icon==",icon,"age==",age)
        let params = ["id":id,"name":name,"icon":icon,"age":age]
        mageHelper().callHttpRequest(controller: self, endPoints: "parent/settings/kids/update",parameters: params, sendAuthHeader: true,showLoader: true,postData:true, addStringToParam:"kid",completion: {
            data in
            do {
                let json = try JSON(data: data)
                print(json)
                if json["status"].boolValue == true{
                    let token = json["data"]["access_token"].stringValue
                    AppSetUp().saveToken(token)
                    // self.view.makeToast(json["message"].stringValue, duration: 3.0)
                    self.makeNetworkCall()
                }else{
                    self.view.makeToast(json["message"].stringValue, duration: 3.0)
                }
            }
            catch let error{
                print(error.localizedDescription)
            }
        })
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if indexPath.section != 0 && indexPath.section != 2 {
            return true
        }else{
            return false
        }
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            print("Delete")
            let kid  = self.addKidsModel?.data?.kids?[indexPath.row]
            self.callApiToDeleteKid(with: kid?.id ?? "")
            // handle delete (by removing the data from your array and updating the tableview)
        }
    }
}

extension AddKidsController: UITableViewDelegate{}

extension AddKidsController: fireUpdateKidSettingsDelegate{
    func fireUpdateKidSettingsDelegate(_cell: AddKidsTVCell, sender: Any, id: String, name: String, icon: String, age: String) {
//        self.addKidsModel?.data?.kids?[_cell.tag].name = name
//        self.addKidsModel?.data?.kids?[_cell.tag].icon = icon
//        self.addKidsModel?.data?.kids?[_cell.tag].age  = age
        self.fireKidsUpdate(id: id, name: name, icon: icon, age: age)
    }
}

