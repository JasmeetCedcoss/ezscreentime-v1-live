//
//  AddKidsTVCell.swift
//  EZScreenTime
//
//  Created by cedcoss on 13/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit
import DropDown

protocol fireUpdateKidSettingsDelegate {
    func fireUpdateKidSettingsDelegate(_cell:AddKidsTVCell,sender:Any,id:String,name:String,icon: String,age:String)
}

class AddKidsTVCell: UITableViewCell {

    let tapGestureIcon                      = UITapGestureRecognizer()
    let kidSelectionDeselectionEventFired   = Variable(false)
    var matchedCoins                        = [Int:String?](){
        didSet{
            print("matchedCoins====",matchedCoins)
        }
    }

    var parent                  = UIViewController()
    private(set) var disposeBag = DisposeBag()
    let ageDropDown             = DropDown()
    var isRunned                 = false
    var delegate                : fireUpdateKidSettingsDelegate?
    //  MARK: - Datasource recieved
    //
    var kids: Kid?{
        didSet{
            kidNameField.text = kids?.name
            kidiconButton.image = UIImage(named: kids?.icon ?? "placeholder")
            kidiconButton.restorationIdentifier = kids?.icon ?? "placeholder"
            ageSelection.setTitle(kids?.age, for: .normal)
        }
    }

    var addKidsModel: AddKidsModel?{
        didSet{
            collectionView.reloadData()
        }
    }

    var KidDevices: [KidDevice]?{
        didSet{
            print("See kids Devices==", KidDevices as Any)
            collectionView.reloadData()
        }
    }

    lazy var dropDownArrowImageForAge: UIImageView = {
        let dropDownArrowImageForAge                                        = UIImageView()
        dropDownArrowImageForAge.image                                      = UIImage(named: "down-arrow")
        dropDownArrowImageForAge.translatesAutoresizingMaskIntoConstraints  = false
        return dropDownArrowImageForAge
    }()

    lazy var dropDownImageForKidDevices: UIImageView = {
        let dropDownImageForKidDevices                                        = UIImageView()
        dropDownImageForKidDevices.image                                      = UIImage(named: "down-arrow")?.withRenderingMode(.alwaysTemplate)
        dropDownImageForKidDevices.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        dropDownImageForKidDevices.translatesAutoresizingMaskIntoConstraints  = false
        return dropDownImageForKidDevices
    }()

    @IBOutlet weak var kidNameField: UITextField!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var showKidDevicesButton: UIButton!
    @IBOutlet weak var ageSelection: UIButton!
    @IBOutlet weak var kidiconButton: UIImageView!
    @IBOutlet weak var kidIconContainer: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        kidIconContainer.layer.borderColor                       = #colorLiteral(red: 0.007843137255, green: 0.337254902, blue: 0.5098039216, alpha: 1)
        kidIconContainer.layer.borderWidth                       = 5
        kidIconContainer.layer.cornerRadius                      = AppSetUp.isIpad ? 120 * 0.5 : 90 * 0.5
        collectionView.delegate                               = self
        collectionView.dataSource                             = self
        headerView.layer.cornerRadius                         = 5
        mainView.layer.cornerRadius                           = 5
        showKidDevicesButton.layer.cornerRadius               = 5
        ageSelection.addDropDownImage()
        headerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }

    func setupView(){
        self.contentView.backgroundColor = AppSetUp.controllerBackgroundColor
        //  MARK: - Setting dropdown icon image
        //
        showKidDevicesButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 30)
        showKidDevicesButton.addSubview(dropDownImageForKidDevices)
        dropDownImageForKidDevices.centerYAnchor.constraint(equalTo: showKidDevicesButton.centerYAnchor, constant: 0).isActive = true
        dropDownImageForKidDevices.trailingAnchor.constraint(equalTo: showKidDevicesButton.trailingAnchor, constant: -10).isActive = true
        dropDownImageForKidDevices.widthAnchor.constraint(equalToConstant: 10).isActive = true
        dropDownImageForKidDevices.heightAnchor.constraint(equalToConstant: 10).isActive = true


        //  MARK: - DropDown Functionality
        //
        let myArray = Array(stride(from: 3, through: 18, by: 1))
        let myAgeArray = myArray.map { String($0) }
        ageDropDown.anchorView      = ageSelection
        ageDropDown.dataSource      = myAgeArray
        ageDropDown.direction       = .any
        ageDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.ageSelection.setTitle(item, for: .normal)

            guard let id = self.kids?.id,let name = self.kidNameField.text,let icon = self.kidiconButton.restorationIdentifier else {return}
            self.delegate?.fireUpdateKidSettingsDelegate(_cell: self, sender: (Any).self, id: id, name: name, icon: icon, age: item)
        }
        kidIconContainer.addGestureRecognizer(tapGestureIcon)
        tapGestureIcon.rx.event.bind(onNext: { recognizer in
            print("touches: \(recognizer.numberOfTouches)") //or whatever you like
                guard let viewControl = self.parent.storyboard?.instantiateViewController(withIdentifier: "SelectCustomDeviceBottomSheetVC") as? SelectCustomDeviceBottomSheetVC else { return }
                           let screenSize: CGRect       = UIScreen.main.bounds
                           let screenHeight             = screenSize.height
                           viewControl.height           = screenHeight/1.5
                           viewControl.topCornerRadius  = 20
                           viewControl.delegate         = self as selectedDeviceImage
                           viewControl.isFrom           = "AddKids"
                           viewControl.presentDuration  = 0.30
                           viewControl.dismissDuration  = 0.30
                           self.parent.navigationController?.present(viewControl, animated: true, completion: nil)
        }).disposed(by: disposeBag)
    }

    override func prepareForReuse(){
        super.prepareForReuse()
        disposeBag = DisposeBag() // because life cicle of every cell ends on prepare for reuse
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}

extension AddKidsTVCell: UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.addKidsModel?.data?.devices?.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddKidsDevicesCVCell", for: indexPath) as! AddKidsDevicesCVCell
        //  MARK: - Datasource for AddKidsDevicesCVCell
        //
        cell.setupView()
        let devices     = self.addKidsModel?.data?.devices?[indexPath.row]
        cell.devices    = devices
        cell.delegate   = self as updateKidDevicesCoinsDelegate

        //  MARK: - Optimize this controller on getting time !!!!!
        //  MARK: - Don't touch heavy stuff going on !!!!
        //
        if self.KidDevices?.count != 0 {
                print("isduplicatePresent")
                for val in 0..<(self.KidDevices?.count)!{
                    print("kidDeviceid===",self.KidDevices?[val].id as Any)
                    let kidID = self.KidDevices?[val].id
                    for valDev in 0..<(self.addKidsModel?.data?.devices?.count)!{
                        print("AllDeviceid===",self.addKidsModel?.data?.devices?[valDev].id as Any)
                        let devDefID = self.addKidsModel?.data?.devices?[valDev].id
                        if kidID == devDefID {
                            print("yes matched")
                            print("Matched coinID==",self.KidDevices?[val].id as Any)
                            print("Matched coinCoin==",self.KidDevices?[val].coins as Any)
                            let coin = self.KidDevices?[val].coins
                            print("devDefID==",valDev)
                            matchedCoins[valDev] = coin!
                        }
                    }
                }
            }
        print("matchdONE==",matchedCoins)
        onReceivingMatchdedCoinsViewSetup(indexPath, cell)


        //  MARK: - RXSWIFT Device select/deselect on click of device image
        //
        cell.tapGesture.rx.event.bind(onNext: { recognizer in
            guard let kid_id = self.kids?.id,let device_id = self.addKidsModel?.data?.devices?[indexPath.row].id else {return}
            let params: [String : String]
            if self.matchedCoins[indexPath.row] == nil {
                print("isActive = 0")
                params = ["kid_id":kid_id,"device_id":device_id,"coins":"10","isActive":"1"]
            }else{
                print("isActive = 1")
                params = ["kid_id":kid_id,"device_id":device_id,"coins":"10","isActive":"0"]
            }
            self.selectDeselectDevice(with: params)
        }).disposed(by: cell.disposeBag)



        //  MARK: - CointextFieldUpdate
        //
        cell.coinTextField.rx.controlEvent(.editingDidEnd)
            .asObservable()
            .subscribe(onNext: { _ in
                print("editing state changed")
                guard let device_id = devices?.id,let coin = cell.coinTextField.text,let kid_id = self.kids?.id else {return}
                let params = ["kid_id":kid_id,"device_id":device_id,"coins":coin,"isActive":"1"]
                self.selectDeselectDevice(with: params)
            }).disposed(by: cell.disposeBag)

        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 100)
    }

    fileprivate func onReceivingMatchdedCoinsViewSetup(_ indexPath: IndexPath, _ cell: AddKidsDevicesCVCell) {
        if matchedCoins[indexPath.row] == nil {
            cell.mainStack.isHidden = true
            cell.icon.tintColor     = .lightGray
            cell.name.textColor     = .lightGray
            cell.timeCalc.isHidden  = true
        }else{
            cell.mainStack.isHidden = false
            cell.icon.tintColor     = .black
            cell.name.textColor     = .black
            cell.timeCalc.isHidden  = false
            guard let match = matchedCoins[indexPath.row]! else {return}
            #warning("Do this in model later on....")
            if let index = match.range(of: ".")?.lowerBound {
                let substring = match[..<index]
                let string = String(substring)
                cell.coinTextField.text = string

                guard let intValMin = Int((self.addKidsModel?.data?.devices?[indexPath.row].valMinutes)!) else {return}
                let calc  = Int(string)! * intValMin
                cell.timeCalc.text = self.timeformatter(timeInterval: (calc * 60)) + "/week"
            }
        }
    }

    func timeformatter(timeInterval:Int)->String{
           let formatter               = DateComponentsFormatter()
           formatter.allowedUnits      = [.hour,.minute]
           formatter.unitsStyle        = .brief
           let formattedString         = formatter.string(from: TimeInterval(timeInterval))!
           return formattedString
       }
    func selectDeselectDevice(with params: [String : String]){
        print(params)
        mageHelper().callHttpRequest(controller: parent, endPoints: "parent/settings/kids/devices",parameters: params, sendAuthHeader: true,postData: true, completion: {
            data in
            do {
                let json = try JSON(data: data)
                print(json)
                if json["status"].boolValue == true{
                    //  MARK: - Deleting current data before feeding collectionview with new data
                    //
                    self.KidDevices?.removeAll()
                    self.matchedCoins.removeAll()
                    //
                    let token = json["data"]["access_token"].stringValue
                    AppSetUp().saveToken(token)
                    self.kidSelectionDeselectionEventFired.value = true
                    self.kidSelectionDeselectionEventFired.value = false
                }else{
                    self.parent.view.makeToast(json["message"].stringValue, duration: 3.0)
                }
            }
            catch let error{
                print(error.localizedDescription)
            }
        })
    }
}

extension AddKidsTVCell:selectedDeviceImage,updateKidDevicesCoinsDelegate {

    func updateKidDevicesCoinsDelegate(_cell: AddKidsDevicesCVCell, sender: Any, coin: String, device_id: String) {
        guard let kid_id = self.kids?.id else {return}
        let params = ["kid_id":kid_id,"device_id":device_id,"coins":coin,"isActive":"1"]
        self.selectDeselectDevice(with: params)
    }

    func selectedDeviceImage(_controller: SelectCustomDeviceBottomSheetVC, sender: Any, image: String) {
        print(image)
        kidiconButton.image = UIImage(named: image)
        kidiconButton.restorationIdentifier = image

        guard let id = self.kids?.id,let name = self.kidNameField.text,let age = ageSelection.currentTitle else {return}
        self.delegate?.fireUpdateKidSettingsDelegate(_cell: self, sender: (Any).self, id: id, name: name, icon: image, age: age)
    }
}



