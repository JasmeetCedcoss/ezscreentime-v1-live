//
//  AddKidsDevicesCVCell.swift
//  EZScreenTime
//
//  Created by cedcoss on 13/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit
import DropDown

protocol updateKidDevicesCoinsDelegate {
    func updateKidDevicesCoinsDelegate(_cell:AddKidsDevicesCVCell,sender:Any,coin:String,device_id:String)
}

class AddKidsDevicesCVCell: UICollectionViewCell {

    var parent                                                               = UIViewController()
    let dropDownCoin                                                         = DropDown()
    let tapGesture                                                           = UITapGestureRecognizer()
    private(set) var disposeBag                                              = DisposeBag()
    var delegate                                                             : updateKidDevicesCoinsDelegate?



    lazy var dropDownArrowImageForCoin: UIImageView = {
        let dropDownArrowImageForCoin                                        = UIImageView()
        dropDownArrowImageForCoin.image                                      = UIImage(named: "down-arrow")
        dropDownArrowImageForCoin.translatesAutoresizingMaskIntoConstraints  = false
        return dropDownArrowImageForCoin
    }()

    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var selectCoin: UIButton!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var mainStack: UIStackView!
    @IBOutlet weak var coinTextField: UITextField!
    @IBOutlet weak var addCoin: UIButton!
    @IBOutlet weak var subtractCoin: UIButton!
    @IBOutlet weak var timeCalc: UILabel!

    var devices: DataDevice?{
        didSet{
            if devices?.icon == "coin_silver"{
               icon.image = UIImage(named: devices?.icon ?? "placeholder")
            }else{
                icon.image = UIImage(named: devices?.icon ?? "placeholder")?.withRenderingMode(.alwaysTemplate)
            }
            name.text  = devices?.name
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag() // because life cicle of every cell ends on prepare for reuse
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setupView(){
        addCoin.JSDropDown()
        subtractCoin.JSDropDown()
        addCoin.rx.tap.subscribe { (onTap) in
            guard let qtyText = self.coinTextField.text else {return}
            guard  var qty = Int(qtyText) else {return}
            qty += 1
            self.coinTextField.text = String(qty)

            guard let device_id = self.devices?.id,let item = self.coinTextField.text else {return}
            self.delegate?.updateKidDevicesCoinsDelegate(_cell: self, sender: (Any).self, coin: item, device_id: device_id)
        }.disposed(by: self.disposeBag)


        subtractCoin.rx.tap.subscribe { (onTap) in
            guard let qtyText = self.coinTextField.text else {return}
            guard  var qty = Int(qtyText) else {return}
            if qty <= 1 {return}
            qty -= 1
            self.coinTextField.text = String(qty)

            guard let device_id = self.devices?.id,let item = self.coinTextField.text else {return}
            self.delegate?.updateKidDevicesCoinsDelegate(_cell: self, sender: (Any).self, coin: item, device_id: device_id)
        }.disposed(by: self.disposeBag)


        self.backgroundColor        = AppSetUp.controllerBackgroundColor
        icon.image?.withRenderingMode(.alwaysTemplate)
        icon.isUserInteractionEnabled = true
        icon.addGestureRecognizer(tapGesture)



        //  MARK: - Age DropDown Functionality
        //
        let myArray = Array(stride(from: 10, through: 50, by: 10))
        let stringArray = myArray.map { String($0) }
        dropDownCoin.anchorView      = selectCoin
        dropDownCoin.dataSource      = stringArray
        dropDownCoin.direction       = .any
        dropDownCoin.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.selectCoin.setTitle(item, for: .normal)
            guard let device_id = self.devices?.id else {return}
            self.delegate?.updateKidDevicesCoinsDelegate(_cell: self, sender: (Any).self, coin: item, device_id: device_id)
        }
    }


    func timeformatter(timeInterval:Int)->String{
        let formatter               = DateComponentsFormatter()
        formatter.allowedUnits      = [.hour,.minute]
        formatter.unitsStyle        = .brief
        let formattedString         = formatter.string(from: TimeInterval(timeInterval))!
        return formattedString
    }
}
