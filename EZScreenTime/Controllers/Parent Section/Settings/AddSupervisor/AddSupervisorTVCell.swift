//
//  AddSupervisorTVCell.swift
//  EZScreenTime
//
//  Created by cedcoss on 16/03/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit
import DropDown

protocol fireSupervisorSettingsDelegate {
    func fireSupervisorSettingsDelegate(_cell:AddSupervisorTVCell,sender:Any,id:String,name:String,icon: String,email:String,receive_email:String,status:Bool)
}

class AddSupervisorTVCell: UITableViewCell {

    let dropDownCopy                = DropDown()
    let tapgesture                  = UITapGestureRecognizer()
    let tapgestureForIcon           = UITapGestureRecognizer()
    var delegate                    : fireSupervisorSettingsDelegate?
    let dropDownMail                = DropDown()
    private(set) var disposeBag     = DisposeBag()
    var parent                      = UIViewController()

    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag() // because life cicle of every cell ends on prepare for reuse
    }

    @IBOutlet weak var supervisorIcon: UIImageView!
    @IBOutlet weak var supervisorIconContainer: UIView!
    @IBOutlet weak var supervisorName: UITextField!
    @IBOutlet weak var generateCode: UIButton!
    @IBOutlet weak var codeStatusLabel: UILabel!
    @IBOutlet weak var receiveMail: UIButton!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var containerStack: UIStackView!
    @IBOutlet weak var supervisorActivation: UISwitch!
    @IBOutlet weak var containerView: UIView!

    @IBAction func changeSupervisorStatus(_ sender: UISwitch) {
        guard let id = self.supervisor?.id,let name = self.supervisorName.text,let icon = self.supervisor?.icon,let email = self.supervisor?.email, let receive_email = self.supervisor?.receiveMail else {return}
        self.delegate?.fireSupervisorSettingsDelegate(_cell: self, sender: (Any).self, id: id, name: name, icon: icon, email: email, receive_email: receive_email,status: self.supervisorActivation.isOn)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        supervisorIconContainer.layer.borderColor                       = #colorLiteral(red: 0.007843137255, green: 0.337254902, blue: 0.5098039216, alpha: 1)
        supervisorIconContainer.layer.borderWidth                       = 5
        if AppSetUp.isIpad{
            supervisorIconContainer.layer.cornerRadius                      = 120 * 0.5
        }else{
            supervisorIconContainer.layer.cornerRadius                      = 90 * 0.5
        }
        generateCode.layer.cornerRadius                        = 5
        receiveMail.layer.cornerRadius                         = 5
        receiveMail.addDropDownImage()
        //  MARK: - DROPDOWN MAIL
        //
        dropDownMail.anchorView             = receiveMail
        dropDownMail.dataSource             = ["Yes","No"]
        dropDownMail.direction              = .any
        containerView.layer.maskedCorners   = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }

    func setupReactiveView(){

        self.receiveMail.rx.tap.subscribe { (onTap) in
            self.dropDownMail.show()
        }.disposed(by: self.disposeBag)

        //  MARK: - REACTIVE SUPERVISORICON
        //
        supervisorIconContainer.addGestureRecognizer(tapgestureForIcon)
        tapgestureForIcon.rx.event.bind(onNext: { recognizer in
            print("touches: \(recognizer.numberOfTouches)") //or whatever you like
                guard let viewControl = self.parent.storyboard?.instantiateViewController(withIdentifier: "SelectCustomDeviceBottomSheetVC") as? SelectCustomDeviceBottomSheetVC else { return }
                let screenSize: CGRect       = UIScreen.main.bounds
                let screenHeight             = screenSize.height
            viewControl.height           = screenHeight/1.5
                viewControl.topCornerRadius  = 20
                viewControl.delegate         = self as selectedDeviceImage
                viewControl.isFrom           = "AddKids"
                viewControl.presentDuration  = 0.30
                viewControl.dismissDuration  = 0.30
                self.parent.navigationController?.present(viewControl, animated: true, completion: nil)
        }).disposed(by: disposeBag)


        //  MARK: - REACTIVE SUPERVISOR EMAIL TEXTFIELD
        //
        emailField.rx.controlEvent(.editingDidEnd)
            .asObservable()
            .subscribe(onNext: { text in
                print("editing state changed")
                guard let id = self.supervisor?.id,let name = self.supervisorName.text,let icon = self.supervisorIcon.restorationIdentifier,let email = self.emailField.text else {return}
                self.delegate?.fireSupervisorSettingsDelegate(_cell: self, sender: (Any).self, id: id, name: name, icon: icon, email: email, receive_email: "1",status: self.supervisorActivation.isOn)
            }).disposed(by: self.disposeBag)

        //  MARK: - REACTIVE SUPERVISOR NAME TEXTFIELD
        //
        supervisorName.rx.controlEvent(.editingDidEnd)
            .asObservable()
            .subscribe(onNext: { text in
                print("editing state changed")
                guard let id = self.supervisor?.id,let name = self.supervisorName.text,let icon = self.supervisorIcon?.restorationIdentifier,let email = self.supervisor?.email, let receive_email = self.supervisor?.receiveMail else {return}
                self.delegate?.fireSupervisorSettingsDelegate(_cell: self, sender: (Any).self, id: id, name: name, icon: icon, email: email, receive_email: receive_email,status: self.supervisorActivation.isOn)
            }).disposed(by: self.disposeBag)

        codeStatusLabel.isUserInteractionEnabled   = true
        codeStatusLabel.addGestureRecognizer(tapgesture)
        self.dropDownCopy.width                    = 150
        self.dropDownCopy.anchorView               = codeStatusLabel
        self.dropDownCopy.dataSource               = ["Copy"]
        self.dropDownCopy.direction                = .any
        self.dropDownCopy.selectionAction          = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            let pasteboard                         = UIPasteboard.general
            pasteboard.string                      = self.codeStatusLabel.text
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    var supervisor: SupervisorListViewModel?{
        didSet{
            supervisorName.text = supervisor?.name
            supervisorIcon.image = UIImage(named: supervisor?.icon ?? "")
            supervisorIcon.restorationIdentifier = supervisor?.icon ?? ""

            generateCode.setTitle(supervisor?.generateCodeText, for: .normal)
            receiveMail.setTitle(supervisor?.receiveMailCustom, for: .normal)
            emailField.isHidden = supervisor?.receiveMail == "0" ? true : false
            emailField.text     = supervisor?.email
            supervisorActivation.isOn = supervisor?.activationStatus ?? false
        }
    }
}

extension AddSupervisorTVCell:selectedDeviceImage {
    func selectedDeviceImage(_controller: SelectCustomDeviceBottomSheetVC, sender: Any, image: String) {
        print(image)
        supervisorIcon.image = UIImage(named: image)
        supervisorIcon.restorationIdentifier = image

        guard let id = self.supervisor?.id,let name = self.supervisorName.text,let email = self.supervisor?.email, let receive_email = self.supervisor?.receiveMail else {return}
        self.delegate?.fireSupervisorSettingsDelegate(_cell: self, sender: (Any).self, id: id, name: name, icon: image, email: email, receive_email: receive_email,status: supervisorActivation.isOn)
    }
}


