//
//  AddSupervisorController.swift
//  EZScreenTime
//
//  Created by cedcoss on 16/03/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

class AddSupervisorController: BaseViewController {

    var supervisorModel             : SupervisorModel?
    var supervisorListViewModel     = [SupervisorListViewModel](){
        didSet{
            self.tableView.reloadData()
        }
    }

    @IBOutlet weak var mainView: UIView!

    lazy var addSupervisorFloatingButton: UIButton = {
        let addSupervisorFloatingButton                                        = UIButton()
        addSupervisorFloatingButton.backgroundColor                            = AppSetUp.colorPrimary
        addSupervisorFloatingButton.translatesAutoresizingMaskIntoConstraints  = false
        addSupervisorFloatingButton.setImage(UIImage(named: "add")?.withRenderingMode(.alwaysTemplate), for: .normal)
        addSupervisorFloatingButton.tintColor = .white
        addSupervisorFloatingButton.imageEdgeInsets = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        addSupervisorFloatingButton.floatButtonShadow()
        return addSupervisorFloatingButton
    }()


    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTable()
        makeNetworkCall()
        setupDesign()
    }

    func setupDesign(){
        self.mainView.addSubview(addSupervisorFloatingButton)
        addSupervisorFloatingButton.widthAnchor.constraint(equalToConstant: 70).isActive = true
        addSupervisorFloatingButton.heightAnchor.constraint(equalToConstant: 70).isActive = true
        addSupervisorFloatingButton.bottomAnchor.constraint(equalTo: self.mainView.bottomAnchor, constant: -20).isActive = true
        addSupervisorFloatingButton.trailingAnchor.constraint(equalTo: self.mainView.trailingAnchor, constant: -18).isActive = true
        addSupervisorFloatingButton.layer.cornerRadius = 70 * 0.5

        addSupervisorFloatingButton.rx.tap.subscribe { (onTap) in
            print("Button tapped")
            self.callApiToAddSupervisor()
        }.disposed(by: AppSetUp.disposeBag)
    }

    func setupTable(){
        tableView.dataSource      = self
        tableView.delegate        = self
        tableView.register(HeaderTVCell.self, forCellReuseIdentifier: "HeaderTVCell")
        tableView.register(NavigationCell.self, forCellReuseIdentifier: "NavigationCell")
        tableView.separatorStyle  = .none
        tableView.backgroundColor = AppSetUp.controllerBackgroundColor
    }
}
//  MARK: - NETWORK LAYER
//


extension AddSupervisorController{

    func makeNetworkCall(){

        mageHelper().callHttpRequest(controller: self, endPoints: "parent/settings/supervisors", parameters: nil,sendAuthHeader: true) { (data) in
            do {
                let json = try JSON(data: data)
                print(json)
                if json["status"].boolValue == true{
                    //  MARK: - Save latest token get request
                    //
                    let token = json["data"]["access_token"].stringValue
                    AppSetUp().saveToken(token)
                    //  MARK: - Data Parsing
                    //
                    AppSetUp.decoder.keyDecodingStrategy = .convertFromSnakeCase
                    self.supervisorModel = try AppSetUp.decoder.decode(SupervisorModel.self, from: data)

                    guard let supervisorList = self.supervisorModel?.data?.supervisors else {return}
                    self.supervisorListViewModel.removeAll()
                    self.supervisorListViewModel         += supervisorList.map({
                        return SupervisorListViewModel(supervisor: $0)
                    })
                }else{
                    self.view.makeToast(json["message"].stringValue, duration: 3.0)
                }
            }
            catch let error{
                print(error.localizedDescription)
            }
        }
    }

    //  MARK: - Fired from 3 views textfield, age, icon
    func fireSupervisorUpdate(id:String,name:String,icon: String,email:String,receive_email: String,status:Bool){
        let finalStatus = status == true ? "1" : "0"
        let params = ["id":id,"name":name,"icon":icon,"email":email,"receive_email":receive_email,"status":finalStatus]
        print(params)

        if !email.isValidEmail(email) && receive_email == "1"{
            self.view.makeToast("Enter a valid email address")
            return
        }
        mageHelper().callHttpRequest(controller: self, endPoints: "parent/settings/supervisors/update",parameters: params, sendAuthHeader: true,postData:true,completion: {
            data in
            do {
                let json = try JSON(data: data)
                print(json)
                if json["status"].boolValue == true{
                    let token = json["data"]["access_token"].stringValue
                    AppSetUp().saveToken(token)
                    self.view.makeToast(json["data"]["message"].stringValue, duration: 1.0)
                    self.makeNetworkCall()
                }else{
                    self.view.makeToast(json["message"].stringValue, duration: 1.0)
                }
            }
            catch let error{
                print(error.localizedDescription)
            }
        })
    }

    func callApiToAddSupervisor(){
        //http://185.59.17.33/~ezscreentimer/api/parent/settings/supervisors
        let name  = "Supervisor " + "\(self.supervisorListViewModel.count + 1)"
        print(name)
        let params = ["name":name,"icon":"new_icon","email":"","receive_email":"0","status":"1"]
        mageHelper().callHttpRequest(controller: self, endPoints: "parent/settings/supervisors",parameters: params, sendAuthHeader: true,postData:true,completion: {
            data in
            do {
                let json = try JSON(data: data)
                print(json)
                if json["status"].boolValue == true{
                    let token = json["data"]["access_token"].stringValue
                    AppSetUp().saveToken(token)
                    self.view.makeToast(json["message"].stringValue, duration: 1.0)
                    self.makeNetworkCall()
                }else{
                    self.view.makeToast(json["message"].stringValue, duration: 1.0)
                }
            }
            catch let error{
                print(error.localizedDescription)
            }
        })
    }
    //  MARK: - DELETE SUPERVISOR
    //
    func callApiToDeleteSupervisor(with id: String){

        mageHelper().callHttpRequestDelete(controller: self, endPoints: "parent/settings/supervisors/delete?id=\(id)", parameters: nil, sendAuthHeader: true, completion: {
            data in
            do {
                let json = try JSON(data: data)
                print(json)
                if json["status"].boolValue == true{
                    let token = json["data"]["access_token"].stringValue
                    AppSetUp().saveToken(token)
                    self.view.makeToast(json["message"].stringValue, duration: 1.0)
                    self.makeNetworkCall()
                }else{
                    self.view.makeToast(json["message"].stringValue, duration: 1.0)
                }
            }
            catch let error{
                print(error.localizedDescription)
            }
        })
    }

    func getSupervisorActivationCode(sid: String,indexPath: IndexPath){
        mageHelper().callHttpRequest(controller: self, endPoints: "parent/settings/supervisors/session/generate",parameters: ["sId":sid], sendAuthHeader: true,postData:true,completion: {
            data in
            do {
                let json = try JSON(data: data)
                print(json)
                if json["status"].boolValue == true{
                    let token = json["data"]["access_token"].stringValue
                    AppSetUp().saveToken(token)
                    let sessionCode = json["data"]["sessionCode"].stringValue
                    let cell = self.tableView.cellForRow(at: indexPath) as! AddSupervisorTVCell
                    cell.codeStatusLabel.text = sessionCode
                    self.view.makeToast(json["message"].stringValue, duration: 1.0)
                }else{
                    self.view.makeToast(json["message"].stringValue, duration: 1.0)
                }
            }
            catch let error{
                print(error.localizedDescription)
            }
        })
    }
}

extension AddSupervisorController:  UITableViewDelegate,UITableViewDataSource{

    func numberOfSections(in tableView: UITableView) -> Int {
        guard let readyToLoad = self.supervisorModel?.status else {return 0}
        return readyToLoad ? 3 : 0
        //return 3
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 1:
            return supervisorListViewModel.count
        default:
            return 1
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderTVCell", for: indexPath) as! HeaderTVCell
            cell.setupView(header: "Activate Supervisor",image: "settings",subheader: "Supervisor can track behaviour of children")
            cell.backgroundColor = AppSetUp.controllerBackgroundColor
            cell.parent = self
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddSupervisorTVCell", for: indexPath) as! AddSupervisorTVCell
            cell.setupReactiveView()
            let supervisor  = self.supervisorListViewModel[indexPath.row]
            cell.supervisor = supervisor
            cell.parent     = self
            cell.delegate   = self as fireSupervisorSettingsDelegate
            cell.dropDownMail.selectionAction = { [unowned self] (index: Int, item: String) in
                print("Selected mail option: \(item) at index: \(index)")
                cell.receiveMail.setTitle(item, for: .normal)
                if item == "No"{
                    guard let id = cell.supervisor?.id,let name = cell.supervisorName.text,let icon = cell.supervisorIcon.restorationIdentifier else {return}
                    self.fireSupervisorUpdate(id: id, name: name, icon: icon, email: "", receive_email: "0",status:cell.supervisorActivation.isOn)
                }
                self.updateUI(item, indexPath)
                tableView.reloadData()
            }
            cell.generateCode.rx.tap.subscribe { (onTap) in
                self.getSupervisorActivationCode(sid: supervisor.id ?? "", indexPath: indexPath)
            }.disposed(by: cell.disposeBag)

            cell.tapgesture.rx.event.bind(onNext: { recognizer in
                print("touches: \(recognizer.numberOfTouches)") //or whatever you like
                cell.dropDownCopy.show()
            }).disposed(by: cell.disposeBag)
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "NavigationCell", for: indexPath) as! NavigationCell
            cell.setupView()
            cell.parent = self
            cell.backButton.rx.tap.subscribe { (onTap) in
                self.navigationController?.popViewController(animated: true)
            }.disposed(by: cell.disposeBag)

            cell.nextButton.setTitle("SAVE", for: .normal)
            cell.nextButton.rx.tap.subscribe { (onTap) in
                print("Next")
                let viewControl:SettingsController = self.storyboard!.instantiateViewController()
                self.navigationController?.setViewControllers([viewControl], animated: true)
            }.disposed(by: cell.disposeBag)
            cell.backgroundColor = AppSetUp.controllerBackgroundColor
            return cell
        default:
            return UITableViewCell()
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return UITableView.automaticDimension
        case 1:
            return self.supervisorListViewModel[indexPath.row].cellHeight ?? 0
        case 2:
            return AppSetUp.isIpad ? 70 : 60
        default:
            return 0
        }
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if indexPath.section != 0 && indexPath.section != 2 {
            return true
        }else{
            return false
            }
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            print("Delete")
            let supervisor  = self.supervisorListViewModel[indexPath.row]
            self.callApiToDeleteSupervisor(with: supervisor.id ?? "")
            // handle delete (by removing the data from your array and updating the tableview)
        }
    }

    fileprivate func updateUI(_ item: String,_ indexPath: IndexPath) {
        //  MARK: - TO BE DONE IN VIEW MODEL BUT WAS IN HURRY :):)
        //
        let show = item == "Yes" ? true : false
        if show{
            self.supervisorListViewModel[indexPath.row].cellHeight          = AppSetUp.isIpad ? 420 : 340
            self.supervisorListViewModel[indexPath.row].receiveMail         = "1"
            self.supervisorListViewModel[indexPath.row].receiveMailCustom   = "Yes"
        }else{
            self.supervisorListViewModel[indexPath.row].cellHeight          = AppSetUp.isIpad ? 370 : 290
            self.supervisorListViewModel[indexPath.row].receiveMail         = "0"
            self.supervisorListViewModel[indexPath.row].receiveMailCustom   = "No"
        }
    }
}

extension AddSupervisorController:fireSupervisorSettingsDelegate {
    func fireSupervisorSettingsDelegate(_cell: AddSupervisorTVCell, sender: Any, id: String, name: String, icon: String, email: String, receive_email: String,status:Bool) {
        print("Debug==",icon)
        self.fireSupervisorUpdate(id: id, name: name, icon: icon, email: email, receive_email: receive_email, status: status)
    }
}
