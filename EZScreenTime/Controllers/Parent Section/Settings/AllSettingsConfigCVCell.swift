//
//  AllSettingsConfigCVCell.swift
//  EZScreenTime
//
//  Created by cedcoss on 07/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

class AllSettingsConfigCVCell: UICollectionViewCell {

    var parent = UIViewController()
    let showPremiumPopUp = PremiumPopUpHandler()

    lazy var backView: UIView = {
        let backView                                        = UIView()
        backView.backgroundColor                            = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        backView.translatesAutoresizingMaskIntoConstraints  = false
        backView.layer.cornerRadius                         = 5
        return backView
    }()

    lazy var stackView: UIStackView = {
        let stackView                                        = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints  = false
        stackView.distribution                               = .fillEqually
        stackView.spacing                                    = 12
        stackView.alignment                                  = .fill
        stackView.axis                                       = .vertical
        return stackView
    }()

    lazy var mainActionButton: UIButton = {
        let mainActionButton                                        = UIButton()
        mainActionButton.backgroundColor                            = AppSetUp.colorAccent
        if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular{
            mainActionButton.titleLabel?.font                       = UIFont(name: "Poppins-SemiBold", size: 20)
        }else{
            mainActionButton.titleLabel?.font                       = UIFont(name: "Poppins-SemiBold", size: 15)
        }
        mainActionButton.translatesAutoresizingMaskIntoConstraints  = false
        mainActionButton.rx.tap.subscribe { (onTap) in
            switch mainActionButton.currentTitle{
            case "ACTIVATE KIDS DEVICE":
                let viewControl:ActivateKidDeviceController = (self.parent.storyboard?.instantiateViewController())!
                self.parent.navigationController?.pushViewController(viewControl, animated: true)
            case "START CONFIGURATION":
                print("START CONFIGURATION")
            default:
                print("")
            }
        }.disposed(by: AppSetUp.disposeBag)
        return mainActionButton
    }()

    func setupView(){
        self.addSubview(backView)
        backView.topAnchor.constraint(equalTo: self.topAnchor, constant: 10).isActive = true
        backView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10).isActive = true
        backView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10).isActive = true

        backView.addSubview(stackView)
        stackView.topAnchor.constraint(equalTo: backView.topAnchor, constant: 10).isActive = true
        stackView.leadingAnchor.constraint(equalTo: backView.leadingAnchor, constant: 10).isActive = true
        stackView.trailingAnchor.constraint(equalTo: backView.trailingAnchor, constant: -10).isActive = true
        stackView.bottomAnchor.constraint(equalTo: backView.bottomAnchor, constant: -10).isActive = true

        self.addSubview(mainActionButton)
        mainActionButton.topAnchor.constraint(equalTo: backView.bottomAnchor, constant: 20).isActive = true
        mainActionButton.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        mainActionButton.heightAnchor.constraint(equalToConstant: 65).isActive = true
        mainActionButton.widthAnchor.constraint(equalToConstant: self.frame.width/2).isActive = true
        mainActionButton.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -5).isActive = true
        mainActionButton.layer.cornerRadius = 5
    }


    func createConfigButton(name:String, status: String){
        let checkImage                                          = UIImageView()
        checkImage.translatesAutoresizingMaskIntoConstraints    = false
        checkImage.contentMode                                  = .scaleToFill
        let customButton                                        = UIButton()
        customButton.backgroundColor                            = AppSetUp.controllerBackgroundColor
        customButton.translatesAutoresizingMaskIntoConstraints  = false
        customButton.setTitleColor(AppSetUp.text_icon_gray, for: .normal)
        customButton.layer.borderColor                          = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
        customButton.layer.borderWidth                          = 1
        if name == "6. Penalty Messages"{
            customButton.setTitle("6. Discipline Messages", for: .normal)
        }else{
            customButton.setTitle(name, for: .normal)
        }
        customButton.contentHorizontalAlignment                 = .left
        if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular{
            customButton.titleLabel?.font                           = UIFont(name:"Poppins-Regular",size:21)
        }else{
            customButton.titleLabel?.font                           = UIFont(name:"Poppins-Medium",size:16)
        }
        customButton.tintColor                                  = #colorLiteral(red: 0, green: 0.5628422499, blue: 0.3188166618, alpha: 1)
        customButton.titleLabel?.textColor = AppSetUp.textColor
        customButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        customButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 00, bottom: 0, right: 100)
        customButton.addSubview(checkImage)
        checkImage.trailingAnchor.constraint(equalTo: customButton.trailingAnchor, constant: -10).isActive = true
        checkImage.centerYAnchor.constraint(equalTo: customButton.centerYAnchor, constant: 0).isActive = true
        checkImage.heightAnchor.constraint(equalToConstant: 30).isActive = true
        checkImage.widthAnchor.constraint(equalToConstant: 30).isActive  = true
        checkImage.bringSubviewToFront(checkImage)

        if #available(iOS 13.0, *) {
            checkImage.image = status == "true" ? UIImage(named: "checked") : UIImage(named: "checked_grey")
        } else {
            // Fallback on earlier versions
        }
        stackView.addArrangedSubview(customButton)


        //  MARK: - Button click mgmt
        //
        customButton.rx.tap.subscribe { (onTAp) in
            var getButtonTitle =  String.SubSequence()
            if let range = customButton.currentTitle?.range(of: ". ") {
                let buttonTitle = customButton.currentTitle?[range.upperBound...]
                getButtonTitle = buttonTitle!
            }
            switch getButtonTitle {
            case "My Profile":
                let viewControl:ProfileController = (self.parent.storyboard?.instantiateViewController())!
                self.parent.navigationController?.pushViewController(viewControl, animated: true)
            case "Add Devices":
                let viewControl:AddDevicesController = (self.parent.storyboard?.instantiateViewController())!
                self.parent.navigationController?.pushViewController(viewControl, animated: true)
            case "Coin Settings":
                let viewControl:CoinSettingController = (self.parent.storyboard?.instantiateViewController())!
                self.parent.navigationController?.pushViewController(viewControl, animated: true)
            case "Add Kids":
                let viewControl:AddKidsController = (self.parent.storyboard?.instantiateViewController())!
                self.parent.navigationController?.pushViewController(viewControl, animated: true)
            case "Reward Messages":
                let viewControl:RewardMessagesController = (self.parent.storyboard?.instantiateViewController())!
                self.parent.navigationController?.pushViewController(viewControl, animated: true)
            case "Penalty Messages","Discipline Messages":
                let viewControl:PenaltyMessageController = (self.parent.storyboard?.instantiateViewController())!
                self.parent.navigationController?.pushViewController(viewControl, animated: true)
            case "Time Management":
                let viewControl:TimeMgmtController = (self.parent.storyboard?.instantiateViewController())!
                self.parent.navigationController?.pushViewController(viewControl, animated: true)
            case "Add Supervisor":
                if AppSetUp.isPremium{
                    let viewControl:AddSupervisorController = (self.parent.storyboard?.instantiateViewController())!
                    self.parent.navigationController?.pushViewController(viewControl, animated: true)
                }else{
                    self.makeToast("You dont have premium app")
                    self.showPremiumPopUp.showLoginPopup()
                }
            case "Notification":
                if AppSetUp.isPremium{
                   let viewControl: SettingsPremiumNotificationController = (self.parent.storyboard?.instantiateViewController())!
                    self.parent.navigationController?.pushViewController(viewControl, animated: true)
                }else{
                    self.makeToast("You dont have premium app")
                    self.showPremiumPopUp.showLoginPopup()
                }
            default:
                print("Default")
            }
        }.disposed(by: AppSetUp.disposeBag)
    }
}


