//
//  RewardMessagesController.swift
//  EZScreenTime
//
//  Created by cedcoss on 17/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

class RewardMessagesController: BaseViewController {

    var rewardMessageModel: RewardMessageModel?{
        didSet{
            tableView.reloadData()
        }
    }

    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        makeNetworkCall()
        setupTable()
    }

    func setupTable(){
        tableView.delegate          = self
        tableView.dataSource        = self
        tableView.separatorStyle    = .none
        tableView.backgroundColor   = AppSetUp.controllerBackgroundColor
        tableView.register(HeaderTVCell.self, forCellReuseIdentifier: "HeaderTVCell")
        tableView.register(NavigationCell.self, forCellReuseIdentifier: "NavigationCell")
    }

    func makeNetworkCall(){
        mageHelper().callHttpRequest(controller: self, endPoints: "parent/settings/messages/reward", parameters: nil,sendAuthHeader: true) { (data) in
            do {
                let json = try JSON(data: data)
                print(json)
                if json["status"].boolValue == true{
                    //  MARK: - Save latest token
                    //
                    let token = json["data"]["access_token"].stringValue
                    AppSetUp().saveToken(token)
                    //  MARK: - Populating Model
                    //
                    AppSetUp.decoder.keyDecodingStrategy = .convertFromSnakeCase
                    self.rewardMessageModel = try? AppSetUp.decoder.decode(RewardMessageModel.self, from: data)
                    //  MARK: - Static data for additional sections
                    //
                    self.rewardMessageModel?.data?.categories?.insert(Category(id: "", name: "First Cell", messages: []), at: 0)

                    self.rewardMessageModel?.data?.categories?.append(Category(id: "", name: "Last Cell", messages: []))

                    for val in 0..<(self.rewardMessageModel?.data?.categories?.count ?? 0) {
                        self.rewardMessageModel?.data?.categories?[val].messages?.append(Message(selected: nil, catID: nil, id: nil, isCustom: nil, message: nil))
                    }
                    print("rewardMessageModel====",self.rewardMessageModel as Any)
                }else{
                    self.view.makeToast(json["message"].stringValue, duration: 3.0,position: .top)
                }
            }
            catch let error{
                print(error.localizedDescription)
            }
        }
    }

    func enableDisableRewardMessage(with params: [String: String],endPoint: String){

        mageHelper().callHttpRequest(controller: self, endPoints: endPoint,parameters: params, sendAuthHeader: true,postData: true, completion: {
            data in
            do {
                let json = try JSON(data: data)
                print(json)
                if json["status"].boolValue == true{
                    let token = json["data"]["access_token"].stringValue
                    AppSetUp().saveToken(token)
                    self.view.makeToast(json["message"].stringValue, duration: 3.0,position: .top)
                    self.makeNetworkCall()
                }else{
                    self.view.makeToast(json["message"].stringValue, duration: 3.0,position: .top)
                }
            }
            catch let error{
                print(error.localizedDescription)
            }
        })
    }

    func deleteRewardMessage(id: String){
        mageHelper().callHttpRequestDelete(controller: self, endPoints: "parent/settings/messages/reward?id=\(id)", parameters: nil,sendAuthHeader: true) { (data) in
            do {
                let json = try JSON(data: data)
                print(json)

                if json["status"].boolValue == true{
                    //  MARK: - Save latest token
                    //
                    let token = json["data"]["access_token"].stringValue
                    AppSetUp().saveToken(token)
                    self.makeNetworkCall()
                    self.view.makeToast(json["message"].stringValue, duration: 3.0,position: .top)
                }else{
                    self.view.makeToast(json["message"].stringValue, duration: 3.0,position: .top)
                }
            }
            catch let error{
                print(error.localizedDescription)
            }
        }
    }
}

extension RewardMessagesController: UITableViewDataSource,UITableViewDelegate{

    func numberOfSections(in tableView: UITableView) -> Int {
        let catCount = (self.rewardMessageModel?.data?.categories?.count ?? 0)
        return catCount
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case (self.rewardMessageModel?.data?.categories?.count ?? 0) - 1:
            return 1
        default:
            return (self.rewardMessageModel?.data?.categories?[section].messages?.count ?? 0)
        }
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label               = UILabel()
        label.text              = "   " + (self.rewardMessageModel?.data?.categories?[section].name ?? "")
        label.textColor         = .black
        label.textAlignment     = .left
        label.font              = UIFont(name: "Poppins-SemiBold", size: 18)
        label.translatesAutoresizingMaskIntoConstraints = false
        let containerView       = UIView()
        containerView.backgroundColor = AppSetUp.controllerBackgroundColor
        containerView.addSubview(label)
        label.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
        return containerView
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section{
        case 0,(self.rewardMessageModel?.data?.categories?.count ?? 0) - 1:
            return 0
        default:
            return 50
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderTVCell", for: indexPath) as! HeaderTVCell
            cell.setupView(header: "Reward Messages",image: "settings",subheader: "Select here the appropriate chores you want to use to reward them\n\nThese chores will be shown in reward section. Below you can add custom chores.")
            cell.parent = self
            return cell

        case (self.rewardMessageModel?.data?.categories?.count ?? 0) - 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "NavigationCell", for: indexPath) as! NavigationCell
            cell.setupView()
            cell.parent = self
            cell.backButton.rx.tap.subscribe { (onTap) in
                self.navigationController?.popViewController(animated: true)
            }.disposed(by: cell.disposeBag)

            cell.nextButton.rx.tap.subscribe { (onTap) in
                let viewControl:PenaltyMessageController = self.storyboard!.instantiateViewController()
                self.navigationController?.pushViewController(viewControl, animated: true)
            }.disposed(by: cell.disposeBag)
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "RewardMessageTVCell", for: indexPath) as! RewardMessageTVCell
            cell.messages = self.rewardMessageModel?.data?.categories?[indexPath.section].messages?[indexPath.row]
            cell.delegate = self as enableDisableRewardMessageDelegate
            cell.addNewMessage.rx.tap.subscribe { (onTap) in
                print(self.rewardMessageModel?.data?.categories?[indexPath.section].id)
                print(cell.message.text)
                guard let cat_id = self.rewardMessageModel?.data?.categories?[indexPath.section].id, let message = cell.message.text else {return}
                let params = ["cat_id":cat_id,"message":message]
                self.enableDisableRewardMessage(with: params, endPoint: "parent/settings/messages/reward/add")
            }.disposed(by: cell.disposeBag)
            cell.setupDelete()
            return cell
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case (self.rewardMessageModel?.data?.categories?.count ?? 0) - 1:
            return AppSetUp.isIpad ? 70 : 60
        case 0:
            return UITableView.automaticDimension
        default:
            return AppSetUp.isIpad ? 70 : UITableView.automaticDimension
        }
    }
}


extension RewardMessagesController: enableDisableRewardMessageDelegate{
    func enableDisableRewardMessageDelegate(_cell: RewardMessageTVCell, sender: Any, params: [String : String],endPoint:String) {
        print(params)
        self.enableDisableRewardMessage(with: params, endPoint: endPoint)
    }

    func deleteRewardMessageDelegate(_cell: RewardMessageTVCell, sender: Any, id: String) {
        self.deleteRewardMessage(id: id)
    }
}
