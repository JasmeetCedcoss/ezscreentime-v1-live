//
//  ProfileTVCell.swift
//  EZScreenTime
//
//  Created by cedcoss on 12/03/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit
import SearchTextField

class ProfileTVCell: UITableViewCell,UITextFieldDelegate {

    let tapGesture                  = UITapGestureRecognizer()
    private(set) var disposeBag     = DisposeBag()
    var parentController            = UIViewController()

    @IBOutlet weak var timeStack:   UIStackView!
    @IBOutlet weak var parentName:  UITextField!
    @IBOutlet weak var appLanguage: UIButton!
    @IBOutlet weak var appTimeZone: SearchTextField!
    @IBOutlet weak var parentImage: UIImageView!
    @IBOutlet weak var parentImageContainer: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()

        parentImageContainer.layer.borderColor                       = #colorLiteral(red: 0.007843137255, green: 0.337254902, blue: 0.5098039216, alpha: 1)
        parentImageContainer.layer.borderWidth                       = 5
        parentImageContainer.layer.cornerRadius                      = AppSetUp.isIpad ? 130 * 0.5 : 100 * 0.5
        self.selectionStyle                                          = .none
        appTimeZone.theme.bgColor                                    = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        appLanguage.JSCustomView2()
        appTimeZone.startVisible = true

        parentImageContainer.addGestureRecognizer(tapGesture)
        tapGesture.rx.event.bind(onNext: { recognizer in
            print("touches: \(recognizer.numberOfTouches)") //or whatever you like
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            guard let viewControl = storyBoard.instantiateViewController(withIdentifier: "SelectCustomDeviceBottomSheetVC") as? SelectCustomDeviceBottomSheetVC else { return }
            let screenSize: CGRect       = UIScreen.main.bounds
            let screenHeight             = screenSize.height
            viewControl.height           = screenHeight/1.5
            viewControl.topCornerRadius  = 20
            viewControl.delegate         = self as selectedDeviceImage
            viewControl.isFrom           = "AddKids"
            viewControl.presentDuration  = 0.30
            viewControl.dismissDuration  = 0.30
            self.parentController.navigationController?.present(viewControl, animated: true, completion: nil)
        }).disposed(by: disposeBag)
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag                       = DisposeBag()
    }

    var parent: ProfileData?{
        didSet{
            parentImage.image = UIImage(named: parent?.icon ?? "new_icon")
            parentImage.restorationIdentifier = parent?.icon
            appLanguage.setTitle("English", for: .normal)
            parentName.text  = parent?.name
            self.appTimeZone.text = parent?.tz ?? ""
        }
    }

    func setup(){
        appTimeZone.filterStrings(SystemManager.getTimeZoneIdentifiers())
        appTimeZone.theme.font = UIFont.systemFont(ofSize: 17)
    
        appTimeZone.itemSelectionHandler = { filteredResults, itemPosition in
            // Just in case you need the item position
            let item = filteredResults[itemPosition]
            print("Item at position \(itemPosition): \(item.title)")

            // Do whatever you want with the picked item
            self.appTimeZone.text = item.title
        }
    }
}

extension ProfileTVCell: selectedDeviceImage{
    func selectedDeviceImage(_controller: SelectCustomDeviceBottomSheetVC, sender: Any, image: String) {
        parentImage.image = UIImage(named: image)
        parentImage.restorationIdentifier = image
    }
}
