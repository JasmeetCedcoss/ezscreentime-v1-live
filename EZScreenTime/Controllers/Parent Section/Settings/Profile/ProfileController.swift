//
//  ProfileController.swift
//  EZScreenTime
//
//  Created by cedcoss on 12/03/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit


class ProfileController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!

    var profileModel: ProfileModel?{
        didSet{
            tableView.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        makeNetworkCall()
        setupTable()
    }

    func makeNetworkCall(){
        mageHelper().callHttpRequest(controller: self, endPoints: "parent/myprofile", parameters: nil,sendAuthHeader: true) { (data) in
            do {
                let json = try JSON(data: data)
                print(json)
                if json["status"].boolValue == true{
                    //  MARK: - Save latest token
                    //
                    let token = json["data"]["access_token"].stringValue
                    AppSetUp().saveToken(token)
                    AppSetUp.decoder.keyDecodingStrategy = .convertFromSnakeCase
                    self.profileModel = try AppSetUp.decoder.decode(ProfileModel.self, from: data)
                    print("profileModel====",self.profileModel)
                    print("====",self.profileModel)
                }else{
                    self.view.makeToast(json["message"].stringValue, duration: 3.0)
                }
            }
            catch let error{
                print(error.localizedDescription)
            }
        }
    }

    func setupTable(){
        tableView.delegate          = self
        tableView.dataSource        = self
        tableView.register(HeaderTVCell.self, forCellReuseIdentifier: "HeaderTVCell")
        tableView.register(NavigationCell.self, forCellReuseIdentifier: "NavigationCell")
        tableView.separatorStyle    = .none
        tableView.backgroundColor   = AppSetUp.controllerBackgroundColor
    }

    func updateProfileRequest(){
        let cell = tableView.cellForRow(at: [1,0]) as! ProfileTVCell
        guard let icon = cell.parentImage.restorationIdentifier,let name  = cell.parentName.text,let lang = cell.appLanguage.currentTitle,let tz = cell.appTimeZone.text else {return}

        if SystemManager.getTimeZoneIdentifiers().contains(tz){
            print("Valid Time Zone")
        }else{
            self.view.makeToast("Invalid TimeZone")
            return
        }

        let params = ["icon":icon,"name":name,"lang":lang,"tz":tz]

        mageHelper().callHttpRequest(controller: self, endPoints: "parent/myprofile",parameters: params, sendAuthHeader: true,postData: true, completion: {
            data in
            do {
                let json = try JSON(data: data)
                print(json)
                if json["status"].boolValue == true{
                    let token = json["data"]["access_token"].stringValue
                    AppSetUp().saveToken(token)
                    self.view.makeToast(json["message"].stringValue, duration: 1.0){ _ in
                        let viewControl:AddDevicesController = self.storyboard!.instantiateViewController()
                        self.navigationController?.pushViewController(viewControl, animated: true)
                    }
                }else{
                    self.view.makeToast(json["message"].stringValue, duration: 3.0)
                }
            }
            catch let error{
                print(error.localizedDescription)
            }
        })
    }
}

extension ProfileController: UITableViewDataSource,UITableViewDelegate{

    func numberOfSections(in tableView: UITableView) -> Int {
        guard let readyToLoad = self.profileModel?.status else {return 0}
        return readyToLoad ? 3 : 0
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderTVCell", for: indexPath) as! HeaderTVCell
            cell.setupView(header: "My Profile",image: "settings",subheader: "Please add your name")
            cell.parent = self
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "NavigationCell", for: indexPath) as! NavigationCell
            cell.setupView()
            cell.parent = self
            cell.backButton.rx.tap.subscribe { (onTap) in
                self.navigationController?.popViewController(animated: true)
            }.disposed(by: cell.disposeBag)

            cell.nextButton.rx.tap.subscribe { (onTap) in
                self.updateProfileRequest()
            }.disposed(by: cell.disposeBag)
            return cell

        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileTVCell", for: indexPath) as! ProfileTVCell
            let parent              = self.profileModel?.data?.profileData
            cell.parentController   = self
            cell.parent             = parent
            cell.setup()
            return cell
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 1:
            return AppSetUp.isIpad ? 430 : 330
        case 2:
            return AppSetUp.isIpad ? 70 : 60
        default:
            return UITableView.automaticDimension
        }
    }
}
