//
//  TimeSavedSettingsTVCell.swift
//  EZScreenTime
//
//  Created by cedcoss on 19/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

class TimeSavedSettingsTVCell: UITableViewCell {

    var dropDownSelectedDuration = String()

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var dayHeader: UILabel!

    @IBOutlet weak var containerViewForDayHeader: UIView!
    @IBOutlet weak var dayHeaderHeight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionView.delegate                         = self
        collectionView.dataSource                       = self
        containerViewForDayHeader.layer.borderColor     = #colorLiteral(red: 0.662745098, green: 0.662745098, blue: 0.662745098, alpha: 0.8008052147)
        containerViewForDayHeader.layer.borderWidth     = 1
        containerViewForDayHeader.layer.cornerRadius    = 5
        containerView.JSCustomView2()
    }

    var data: TimeMgmtDataClass?{
        didSet{
            self.collectionView.reloadData()
        }
    }

    var durationType: String {
        if self.dropDownSelectedDuration.isEmpty{
            return self.data?.savedSettings?.durationType ?? "";
        }else{
            return self.dropDownSelectedDuration
        }
    }

    var durationValueForCase1: [String:String]?{
        didSet{
            self.collectionView.reloadData()
        }
    }

    var savedValForDevices: [String : String]?{
        didSet{
            self.collectionView.reloadData()
        }
    }
}

extension TimeSavedSettingsTVCell:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.data?.parentDevices?.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TimeSaveSettingsCVCell", for: indexPath) as! TimeSaveSettingsCVCell
        cell.setupView()
        cell.parentDevices.text = self.data?.parentDevices?[indexPath.row].name
        let parentId            = self.data?.parentDevices?[indexPath.row].id
        cell.id                 = parentId

        //  MARK: - Setting Duration Value
        //
        print("DurationType*****",durationType)
        switch durationType{
        case "1":
            if self.durationValueForCase1?[parentId!] != nil{
                cell.dropDownMins.setTitle(self.durationValueForCase1?[parentId!], for: .normal)
            }else{
                cell.dropDownMins.setTitle("0", for: .normal)
            }
        case "2":
            print("")
            cell.day                = dayHeader.text
            cell.deleagate          = self as deviceSettingCase2Delegate
            let getDeviceIdByDays   = (self.data?.parentDevices?[indexPath.row].id)!

            if savedValForDevices == nil{
                cell.dropDownMins.setTitle("0", for: .normal)
            }else{
                cell.dropDownMins.setTitle(savedValForDevices?[getDeviceIdByDays], for: .normal)
            }

        default:
            print("Default")
        }

        cell.dropDownMins.rx.tap.subscribe { (onTap) in
            cell.dropDownMinsDropdown.show()
        }.disposed(by: cell.disposeBag)
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.frame.width, height: 80)
    }
}

extension TimeSavedSettingsTVCell:deviceSettingCase2Delegate{
    func deviceSettingCase2Delegate(_cell: TimeSaveSettingsCVCell, sender: Any, params: [String : String],day: String,item: String,id: String) {
        print(params)
        let min = params["max_minutes"]
        TimeMgmtSingleton.saveDurationValueForCase2[day]?[id] = min
        print(TimeMgmtSingleton.saveDurationValueForCase2[day] as Any)
    }
}


