//
//  TimeMgmtController.swift
//  EZScreenTime
//
//  Created by cedcoss on 18/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

class TimeMgmtController: BaseViewController {

    var periodDurationData                  = [String:[String:String]]()
    var durationValueForCase1               = [String:String]()
    var durationType                        : String? = "1"
    var durationTypeSelectedFromDropDown    = String()
    var custom = ["Same settings for all days","Different setting per day","Kids may manage time"]
    var timeMgmtModel: TimeMgmtModel?{
        didSet{
            self.tableView.reloadData()
        }
    }

    func getArrayOfValues(forValue: String)->[String]{
        var period_DurationArray = [String]()
        let keys = Array((self.periodDurationData[forValue] ?? [:]).keys).sorted()
        keys.forEach({
            period_DurationArray.append((self.periodDurationData[forValue]?[$0])!)
        })
        return period_DurationArray
    }

    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        makeNetworkCall()
        setupTable()
    }

    func setupTable(){
        tableView.delegate          = self
        tableView.dataSource        = self
        tableView.separatorStyle    = .none
        tableView.backgroundColor   = AppSetUp.controllerBackgroundColor
        tableView.register(HeaderTVCell.self, forCellReuseIdentifier: "HeaderTVCell")
        tableView.register(NavigationCell.self, forCellReuseIdentifier: "NavigationCell")
    }

    func makeNetworkCall(){
        mageHelper().callHttpRequest(controller: self, endPoints: "parent/settings/time", parameters: nil,sendAuthHeader: true) { (data) in
            do {
                let json = try JSON(data: data)
                print(json)
                if json["status"].boolValue == true{
                    //  MARK: - Save latest token
                    //
                    let token = json["data"]["access_token"].stringValue
                    AppSetUp().saveToken(token)
                    //  MARK: - Populating Model
                    //
                    AppSetUp.decoder.keyDecodingStrategy = .convertFromSnakeCase
                    self.timeMgmtModel = try? AppSetUp.decoder.decode(TimeMgmtModel.self, from: data)
                    print("timeMgmtModel====",self.timeMgmtModel as Any)

                    //  MARK: - Dictionary parsing
                    //
                    for (mainKey,jsonValue) in json["data"]["systemSettings"] {
                        var temp = [String:String]()
                        for (_,val) in jsonValue{
                            temp[val["key"].stringValue] = val["value"].stringValue
                        }
                        self.periodDurationData[mainKey] = temp
                    }

                    //  MARK: - Parsing Duration Value based on duration type
                    //
                    switch self.timeMgmtModel?.data?.savedSettings?.durationType{
                    case "1":
                        for (_,jsonValue) in json["data"]["savedSettings"]["duration_value"] {
                            self.durationValueForCase1[jsonValue["device_id"].stringValue] = jsonValue["allDays"].stringValue
                        }

                        AppSetUp.weeks.forEach { (day) in
                            var myArr = [String: String]()
                            self.timeMgmtModel?.data?.parentDevices?.forEach({ (devices) in
                                myArr[devices.id!] = "0"
                            })
                            TimeMgmtSingleton.saveDurationValueForCase2[day] = myArr
                        }
                    case "2":
                        for (_,jsonValue) in json["data"]["savedSettings"]["duration_value"] {
                            var temp = [String:String]()
                            for (key,val) in jsonValue{
                                for (_,valin) in val{
                                    temp[valin["device_id"].stringValue] =
                                        valin["max_minutes"].stringValue
                                }
                                TimeMgmtSingleton.saveDurationValueForCase2[key] = temp
                            }
                        }
                    default:
                        print("no value found default block executed")
                    }
                }else{
                    self.view.makeToast(json["message"].stringValue, duration: 3.0)
                }
            }
            catch let error{
                print(error.localizedDescription)
            }
        }
    }
}

extension TimeMgmtController: UITableViewDataSource,UITableViewDelegate{

    func numberOfSections(in tableView: UITableView) -> Int {
        guard let readyToLoad = self.timeMgmtModel?.status else {return 0}
        return readyToLoad ? 5 : 0
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 3:
            if self.durationTypeSelectedFromDropDown.isEmpty{
                durationType = self.timeMgmtModel?.data?.savedSettings?.durationType
            }else{
                durationType = self.durationTypeSelectedFromDropDown
            }
            switch durationType{
            case "1":
                return 1
            case "2":
                return 7
            case "3":
                return 0
            default:
                return 1
            }
        default:
            return 1
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
            //  MARK: - HeaderTVCell
        //
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderTVCell", for: indexPath) as! HeaderTVCell
            cell.setupView(header: "Time Management",image: "settings",subheader: "Select period")
            cell.parent = self
            return cell
            //  MARK: - PeriodTVCell
        //
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "PeriodDurationTVCell", for: indexPath) as! PeriodDurationTVCell
            cell.periodDurationValuesSelect.JSDropDown()
            cell.periodDurationLabel.text   = "Periods"
            cell.setupView(period_DurationArray: getArrayOfValues(forValue: "periods"),headerString:"This is the period kids need to manage their coins",period:(self.timeMgmtModel?.data?.savedSettings?.period))
//            cell.periodDurationValuesSelect.rx.tap.subscribe { (onTap) in
//                cell.dropDownPeriod.show()
//            }.disposed(by: cell.disposeBag)
            return cell
        //  MARK: - DurationTVCell
        //
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "PeriodDurationTVCell", for: indexPath) as! PeriodDurationTVCell
            cell.periodDurationValuesSelect.addDropDownImage()
            cell.periodDurationLabel.isHidden = true
            cell.delegate                     = self
            var periodToSend: String?
            periodToSend = durationTypeSelectedFromDropDown.isEmpty ? self.timeMgmtModel?.data?.savedSettings?.durationType : durationTypeSelectedFromDropDown
            cell.setupView(period_DurationArray: getArrayOfValues(forValue: "durations"),headerString:"Duration\nGaming time per device",period:periodToSend)
            cell.periodDurationValuesSelect.rx.tap.subscribe { (onTap) in
                cell.dropDownPeriod.show()
            }.disposed(by: cell.disposeBag)
            return cell

        //  MARK: - TimeSavedSettingsTVCell
        //
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TimeSavedSettingsTVCell", for: indexPath) as! TimeSavedSettingsTVCell
            let dataSource                    = self.timeMgmtModel?.data
            cell.dayHeader.text               = ""
            cell.data                         = dataSource

            if self.durationTypeSelectedFromDropDown.isEmpty{
                durationType                  = self.timeMgmtModel?.data?.savedSettings?.durationType
            }else{
                durationType = self.durationTypeSelectedFromDropDown
                cell.dropDownSelectedDuration = self.durationTypeSelectedFromDropDown
                cell.durationValueForCase1    = self.durationValueForCase1
            }

            switch durationType {
            case "1":
                cell.durationValueForCase1 = self.durationValueForCase1
                cell.dayHeaderHeight.constant = 0
            case "2":
                print("Send Data for durationValueForCase2")
                let day                = AppSetUp.weeks[indexPath.row]
                cell.dayHeader.text    = day
                let savedValForDevices = TimeMgmtSingleton.saveDurationValueForCase2[day]
                cell.savedValForDevices = savedValForDevices
                cell.dayHeaderHeight.constant = 40
            case "3":
                print("Nothing to send")
            default:
                cell.dayHeaderHeight.constant = 0
            }
            return cell
            //  MARK: - NavigationCell
            //
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "NavigationCell", for: indexPath) as! NavigationCell
            cell.setupView()
            cell.parent = self
            cell.backButton.rx.tap.subscribe { (onTap) in
                self.navigationController?.popViewController(animated: true)
            }.disposed(by: cell.disposeBag)

            //  MARK: - SAVE TIME MGMT SETTINGS
            //
            //cell.nextButton.setTitle("SAVE", for: .normal)
            cell.nextButton.rx.tap.subscribe { (onTap) in
                self.saveSettings()
            }.disposed(by: cell.disposeBag)
            return cell
        default:
            return UITableViewCell()
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 4:
            return 60
        case 3:
            let height          = CGFloat(((self.timeMgmtModel?.data?.parentDevices?.count ?? 0) + 1) * 90)
            return durationType == "1" ? height - CGFloat(50) : height
        default:
            return UITableView.automaticDimension
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath)
    }

    func saveSettings(){
        var duration_type           : String?
        let period                  = "1"
        var postDurationValueCase1  = [[String:String]]()

        if self.durationTypeSelectedFromDropDown.isEmpty{
            duration_type = self.timeMgmtModel?.data?.savedSettings?.durationType
        }else{
            duration_type  = self.durationTypeSelectedFromDropDown
        }

        switch duration_type{
        case "1":
            let cell = tableView.cellForRow(at: [3,0]) as! TimeSavedSettingsTVCell
            guard let parentDeviceCount = self.timeMgmtModel?.data?.parentDevices?.count else {return}
            for val in 0..<parentDeviceCount{
                var temp = [String:String]()
                let cvCell = cell.collectionView.cellForItem(at: [0,val]) as! TimeSaveSettingsCVCell
                temp["device_id"] = self.timeMgmtModel?.data?.parentDevices?[val].id
                temp["allDays"]   = cvCell.dropDownMins.currentTitle
                postDurationValueCase1.append(temp)
            }
            print(postDurationValueCase1)
            guard let duration_type = duration_type else {return}
            let paramsToBeConverted = ["period":period,"duration_type":duration_type,"duration_value":postDurationValueCase1] as [String : Any]
            let postParams = AppSetUp().convertParamsToJSON(params: paramsToBeConverted)

            //  MARK: - NETWORK CALL FOR SAVING TIME SETTINGS FOR SAME DAY
            //
            makeNetworkCallForSavingSettings(postParams)

        case "2":
            let finalist = [AppSetUp().getDurationValueForCaseTwo()]
            guard let duration_type = duration_type else {return}
            let paramsToBeConverted = ["period":period,"duration_type":duration_type,"duration_value":finalist] as [String : Any]
            let postParams = AppSetUp().convertParamsToJSON(params: paramsToBeConverted)
            print("postParams===",postParams)
            //  MARK: - NETWORK CALL FOR SAVING TIME SETTINGS FOR DIFFERENT DAY
            //
            makeNetworkCallForSavingSettings(postParams)
        default:
            print("")
            guard let duration_type = duration_type else {return}
            let paramsToBeConverted = ["period":period,"duration_type":duration_type,"duration_value":[]] as [String : Any]
            let postParams = AppSetUp().convertParamsToJSON(params: paramsToBeConverted)
            print("postParams===",postParams)
            makeNetworkCallForSavingSettings(postParams)
        }
    }

    fileprivate func makeNetworkCallForSavingSettings(_ postParams: String) {
        mageHelper().callHttpRequestForTimeMgmt(controller: self, endPoints: "parent/settings/time",parameters: postParams, sendAuthHeader: true,postData:true,completion: {
            data in
            do {
                let json = try JSON(data: data)
                print(json)
                if json["status"].boolValue == true{
                    let token = json["data"]["access_token"].stringValue
                    AppSetUp().saveToken(token)
                    self.view.makeToast(json["message"].stringValue, duration: 1.0) { (_) in
                        let viewControl:SettingsPremiumNotificationController = self.storyboard!.instantiateViewController()
                        self.navigationController?.pushViewController(viewControl, animated: true)
                    }
                }else{
                    self.view.makeToast(json["message"].stringValue, duration: 1.0)
                }
            }
            catch let error{
                print(error.localizedDescription)
            }
        })
    }
}

extension TimeMgmtController: showSavedSettingDelegate{
    func showSavedSettingDelegate(_cell: PeriodDurationTVCell, sender: Any, durationTypeSelected: String) {
        print(durationTypeSelected)
        durationTypeSelectedFromDropDown = durationTypeSelected
        if self.timeMgmtModel?.data?.savedSettings?.durationType == durationTypeSelected{
            self.makeNetworkCall()
        }else{
            self.durationValueForCase1.removeAll()
            self.tableView.reloadSections([3], with: .automatic)
        }
    }
}





