//
//  TimeSaveSettingsCVCell.swift
//  EZScreenTime
//
//  Created by cedcoss on 19/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit
import DropDown

protocol deviceSettingCase2Delegate {
    func deviceSettingCase2Delegate(_cell:TimeSaveSettingsCVCell,sender: Any,params: [String:String],day:String,item: String,id:String)
}

class TimeSaveSettingsCVCell: UICollectionViewCell {

    @IBOutlet weak var dropDownMins: UIButton!
    @IBOutlet weak var parentDevices: UILabel!

    var paramsForCase2 = [String:[String:String]]()
    let dropDownMinsDropdown                        = DropDown()
    private(set) var disposeBag                     = DisposeBag()
    var deleagate                                   : deviceSettingCase2Delegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        dropDownMins.addDropDownImage()
    }

    override func prepareForReuse(){
        super.prepareForReuse()
        disposeBag = DisposeBag() // because life cicle of every cell ends on prepare for reuse
    }

    var id: String?{
        didSet{
            print("id",id as Any)
        }
    }

    var day:String?{
        didSet{
            print("day",day as Any)
        }
    }

    func setupView(){
        let myArray = Array(stride(from: 0, through: 480, by: 10))
        let myMinsArray = myArray.map { String($0) }
        dropDownMinsDropdown.anchorView      = dropDownMins
        dropDownMinsDropdown.dataSource      = myMinsArray
        dropDownMinsDropdown.direction       = .any
        dropDownMinsDropdown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("CheckparamsForCase2===",self.paramsForCase2)
            print("Selected item: \(item) at index: \(index)")
            self.dropDownMins.setTitle(item, for: .normal)
            print("Mins==",item)
            print("Deviceid==",self.id as Any)
            print("Day==",self.day as Any)
            var temp                = [String:String]()
            temp["device_id"]       = self.id
            temp["max_minutes"]     = item
            self.deleagate?.deviceSettingCase2Delegate(_cell: self, sender: (Any).self, params: temp,day:self.day!,item: item,id: self.id!)
        }
    }
}
