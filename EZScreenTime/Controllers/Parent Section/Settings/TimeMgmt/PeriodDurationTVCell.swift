//
//  PeriodDurationTVCell.swift
//  EZScreenTime
//
//  Created by cedcoss on 18/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit
import DropDown

protocol showSavedSettingDelegate {
    func showSavedSettingDelegate(_cell: PeriodDurationTVCell,sender:Any,durationTypeSelected: String)
}

class PeriodDurationTVCell: UITableViewCell {

    let dropDownPeriod                              = DropDown()
    var delegate                                    : showSavedSettingDelegate?
    private(set) var disposeBag                     = DisposeBag()

    @IBOutlet weak var containerView                : UIView!
    @IBOutlet weak var periodDurationLabel          : UILabel!
    @IBOutlet weak var periodDurationValuesSelect   : UIButton!
    @IBOutlet weak var Header: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        containerView.JSCustomView2()
    }

    override func prepareForReuse(){
        super.prepareForReuse()
        disposeBag = DisposeBag() // because life cicle of every cell ends on prepare for reuse
    }

    func setupView(period_DurationArray:[String],headerString: String,period:String?){
        Header.text                    = headerString
        if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular{
            Header.font                    = UIFont(name:"Poppins-Regular",size:17)
        }else{
            Header.font                    = UIFont(name:"Poppins-Regular",size:15)
        }
        dropDownPeriod.anchorView      = periodDurationValuesSelect
        dropDownPeriod.dataSource      = period_DurationArray
        dropDownPeriod.direction       = .any
        if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular{
            dropDownPeriod.textFont               = UIFont.systemFont(ofSize: 19)
        }else{
            dropDownPeriod.textFont               = UIFont.systemFont(ofSize: 15)
        }
        if period != nil{
            self.periodDurationValuesSelect.setTitle(period_DurationArray[(Int(period!)!) - 1], for: .normal)
        }else{
            self.periodDurationValuesSelect.setTitle(period_DurationArray.first, for: .normal)
        }
        dropDownPeriod.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.periodDurationValuesSelect.setTitle(item, for: .normal)
            self.delegate?.showSavedSettingDelegate(_cell: self, sender: (Any).self, durationTypeSelected: String(index + 1))
        }
    }
}

