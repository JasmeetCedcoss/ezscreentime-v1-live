//
//  SettingsController.swift
//  EZScreenTime
//
//  Created by cedcoss on 06/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

class SettingsController: BaseViewController {

    var newUser                     = false
    var settingsModel               : SettingsModel?
    var settingConfigurationData    = [String:String](){
        didSet{setupTable()}
    }
    var incompleteArray             = [String]()

    @IBOutlet weak var collectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.backgroundColor = AppSetUp.controllerBackgroundColor
        checkForUser()
        makeNetworkCall()
    }

    func setupTable(){
        collectionView.dataSource = self
        collectionView.delegate   = self
        collectionView.register(HeaderCVCell.self, forCellWithReuseIdentifier: "HeaderCVCell")
        collectionView.register(AllSettingsConfigCVCell.self, forCellWithReuseIdentifier: "AllSettingsConfigCVCell")
    }

    func checkForUser(){
        newUser ? displayWelcomeView() : print("Existing User")
    }

    func displayWelcomeView(){
        let welcomeView = WelcomeView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height))
        welcomeView.startConfigurationButton.rx.tap.subscribe { (onTap) in
            let viewControl:SettingsController = self.storyboard!.instantiateViewController()
            self.navigationController?.setViewControllers([viewControl], animated: true)
        }.disposed(by: AppSetUp.disposeBag)
        self.view.addSubview(welcomeView)
    }

    func makeNetworkCall(){
        mageHelper().callHttpRequest(controller: self, endPoints: "parent/settings/configuration", parameters: nil,sendAuthHeader: true) { (data) in
            do {
                let json = try JSON(data: data)
                print(json)
                if json["status"].boolValue == true{
                    //  MARK: - Save latest token
                    //
                    let token = json["data"]["access_token"].stringValue
                    AppSetUp().saveToken(token)
                    for (key,value) in json["data"]["configuration"] {
                        print("seee",key)
                        self.settingConfigurationData[key] = value.stringValue
                    }
                    let decoder = JSONDecoder()
                    decoder.keyDecodingStrategy = .convertFromSnakeCase
                    self.settingsModel = try decoder.decode(SettingsModel.self, from: data)
                    print("====",self.settingConfigurationData)
                    AppSetUp.settingConfigurationData = self.settingConfigurationData

                    //  MARK: - Setting up premium check
                    AppSetUp.isPremium = self.settingsModel?.data?.premiumUser ?? false
                }else{
                    self.view.makeToast(json["message"].stringValue, duration: 3.0)
                }
            }
            catch let error{
                print(error.localizedDescription)
            }
        }
    }
}


extension SettingsController: UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        switch indexPath.section{
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HeaderCVCell", for: indexPath) as! HeaderCVCell
            let subHeadingText = settingConfigurationData.values.contains("false") ? "Please complete these steps" : "Congratulations!\n All steps were successfully configured."
            cell.setupView(header: "Settings",image: "settings",subheader: subHeadingText)
            return cell
        case 1:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AllSettingsConfigCVCell", for: indexPath) as! AllSettingsConfigCVCell
            cell.setupView()
            let text = settingConfigurationData.values.contains("false") ? "START CONFIGURATION" : "ACTIVATE KIDS DEVICE"
            cell.mainActionButton.setTitle(text, for: .normal)
            cell.parent = self
            for (index, element) in (self.settingsModel?.data?.configurationArray?.enumerated())! {
                cell.createConfigButton(name: "\(index + 1). " + element, status: settingConfigurationData[element]!)
                //  MARK: - Start config button click setup 6 may
                if settingConfigurationData[element] == "false"{
                    incompleteArray.append(element)
                }
            }
            return cell
        default:
            return UICollectionViewCell()
        }
    }

    func collectionView(_ collectionVie0w: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch indexPath.section{
        case 1:
            if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular{
                return CGSize(width: self.view.frame.width, height: CGFloat(self.settingConfigurationData.count)*90)
            }else{
                return CGSize(width: self.view.frame.width, height: CGFloat(self.settingConfigurationData.count)*75)
            }
        default:
            if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular{
                return CGSize(width: self.view.frame.width, height: 150)
            }else{
                return CGSize(width: self.view.frame.width, height: 130)
            }
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}
