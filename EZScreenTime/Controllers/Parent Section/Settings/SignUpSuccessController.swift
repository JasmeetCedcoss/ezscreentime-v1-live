//
//  SignUpSuccessController.swift
//  EZScreenTime
//
//  Created by cedcoss on 04/04/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

class SignUpSuccessController: UIViewController {

    lazy var backView: UIView = {
        let backView                                        = UIView()
        backView.translatesAutoresizingMaskIntoConstraints  = false
        backView.backgroundColor                            = AppSetUp.controllerBackgroundColor
        backView.layer.cornerRadius                         = 5
        return backView
    }()

    lazy var LoginButton: UIButton = {
        let LoginButton                                        = UIButton()
        LoginButton.backgroundColor                            = AppSetUp.colorAccent
        LoginButton.setTitle("LOGIN", for: .normal)
        LoginButton.translatesAutoresizingMaskIntoConstraints  = false
        LoginButton.layer.cornerRadius                         = 5
        LoginButton.rx.tap.subscribe { (onTap) in
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewControl = storyBoard.instantiateViewController(withIdentifier: "ParentPageMenuViewController") as! ParentPageMenuViewController
            self.navigationController?.setViewControllers([viewControl], animated: true)
        }.disposed(by: AppSetUp.disposeBag)
        return LoginButton
    }()

    lazy var customImageView: UIImageView = {
        let customImageView                                        = UIImageView()
        customImageView.image                                      = UIImage(named: "signupsuccess")
        customImageView.translatesAutoresizingMaskIntoConstraints  = false
        customImageView.backgroundColor                            = .red
        return customImageView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        designSetup()
    }

    func designSetup(){
        self.view.addSubview(backView)
        backView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
        backView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        backView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        backView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true

        backView.addSubview(customImageView)
        customImageView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
        customImageView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 20).isActive = true
        customImageView.widthAnchor.constraint(equalToConstant: self.view.frame.width - 50).isActive = true
        customImageView.heightAnchor.constraint(equalToConstant: self.view.frame.height/1.5).isActive = true

        backView.addSubview(LoginButton)
        LoginButton.topAnchor.constraint(equalTo: customImageView.bottomAnchor, constant: 30).isActive = true
        LoginButton.centerXAnchor.constraint(equalTo: backView.centerXAnchor).isActive = true
        LoginButton.widthAnchor.constraint(equalToConstant: 170).isActive = true
        LoginButton.heightAnchor.constraint(equalToConstant: 50).isActive = true


    }
}
