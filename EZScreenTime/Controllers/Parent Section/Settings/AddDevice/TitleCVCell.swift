//
//  TitleCVCell.swift
//  EZScreenTime
//
//  Created by cedcoss on 11/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

class TitleCVCell: UICollectionViewCell {
    
    lazy var backView: UIView = {
        let backView                                        = UIView()
        backView.translatesAutoresizingMaskIntoConstraints  = false
        return backView
    }()

    lazy var customLabel: UILabel = {
        let customLabel                                        = UILabel()
        customLabel.textColor                                  = UIColor.black
        customLabel.translatesAutoresizingMaskIntoConstraints  = false
        customLabel.numberOfLines                              = 0
        customLabel.textAlignment                              = .left
        customLabel.font                                       = UIFont(name: "Poppins-SemiBold", size: 20)
        return customLabel
    }()

    override init(frame: CGRect) {
        super.init(frame:frame)
        setupView()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("")
    }

    func setupView(){
           self.addSubview(backView)
           backView.topAnchor.constraint(equalTo: self.topAnchor,constant: 0 ).isActive = true
           backView.leadingAnchor.constraint(equalTo: self.leadingAnchor,constant: 0).isActive = true
           backView.trailingAnchor.constraint(equalTo: self.trailingAnchor,constant: 0).isActive = true
           backView.bottomAnchor.constraint(equalTo: self.bottomAnchor,constant: 0).isActive = true

        backView.addSubview(customLabel)
        customLabel.topAnchor.constraint(equalTo: backView.topAnchor,constant: 5 ).isActive = true
        customLabel.leadingAnchor.constraint(equalTo: backView.leadingAnchor,constant: 5).isActive = true
        customLabel.trailingAnchor.constraint(equalTo: backView.trailingAnchor,constant: -5).isActive = true
        customLabel.bottomAnchor.constraint(equalTo: backView.bottomAnchor,constant: -5).isActive = true
    }
}
