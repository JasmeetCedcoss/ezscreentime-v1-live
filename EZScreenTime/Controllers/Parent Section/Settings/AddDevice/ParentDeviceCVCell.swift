//
//  ParentDeviceCVCell.swift
//  EZScreenTime
//
//  Created by cedcoss on 10/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

class ParentDeviceCVCell: UICollectionViewCell {
    private(set) var disposeBag = DisposeBag()

       override func prepareForReuse() {
           super.prepareForReuse()
           disposeBag = DisposeBag() // because life cicle of every cell ends on prepare for reuse
       }
    
    lazy var backView: UIView = {
        let backView                                        = UIView()
        backView.translatesAutoresizingMaskIntoConstraints  = false
        return backView
    }()

    lazy var deleteDevice: UIButton = {
           let deleteDevice                                        = UIButton()

        deleteDevice.setImage(UIImage(named: "cancel"), for: .normal)
           deleteDevice.translatesAutoresizingMaskIntoConstraints  = false
           return deleteDevice
       }()

    lazy var customImageView: UIImageView = {
        let customImageView                                        = UIImageView()
        customImageView.image                                      = UIImage(named: "placeholder")
        customImageView.contentMode                                = .scaleAspectFit
        customImageView.translatesAutoresizingMaskIntoConstraints  = false
        return customImageView
    }()

    lazy var customLabel: UILabel = {
        let customLabel                                        = UILabel()
        customLabel.textColor                                  = UIColor.black
        customLabel.translatesAutoresizingMaskIntoConstraints  = false
        customLabel.numberOfLines                              = 0
        customLabel.textAlignment                              = .center
        return customLabel
    }()

    func setupView(){

        self.backgroundColor  = AppSetUp.controllerBackgroundColor
        self.addSubview(backView)
        backView.topAnchor.constraint(equalTo: self.topAnchor,constant: 5 ).isActive = true
        backView.leadingAnchor.constraint(equalTo: self.leadingAnchor,constant: 5).isActive = true
        backView.trailingAnchor.constraint(equalTo: self.trailingAnchor,constant: -5).isActive = true
        backView.bottomAnchor.constraint(equalTo: self.bottomAnchor,constant: -5).isActive = true

        self.backView.addSubview(deleteDevice)
        deleteDevice.layer.cornerRadius = 25 * 0.5
        deleteDevice.snp.makeConstraints { (make) in
            make.width.height.equalTo(25)
            make.top.equalTo(backView.snp.top).offset(0)
            make.right.equalTo(backView.snp.right).offset(0)
        }

        self.backView.addSubview(customLabel)
        customLabel.leadingAnchor.constraint(equalTo: self.backView.leadingAnchor).isActive = true
        customLabel.trailingAnchor.constraint(equalTo: self.backView.trailingAnchor, constant: -20).isActive = true
        customLabel.bottomAnchor.constraint(equalTo: self.backView.bottomAnchor).isActive = true
        customLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true

        self.backView.addSubview(customImageView)
        customImageView.leadingAnchor.constraint(equalTo: self.backView.leadingAnchor).isActive = true
        customImageView.trailingAnchor.constraint(equalTo: self.backView.trailingAnchor, constant: -20).isActive = true
        customImageView.bottomAnchor.constraint(equalTo: self.customLabel.topAnchor).isActive = true
        customImageView.topAnchor.constraint(equalTo: self.backView.topAnchor, constant: 20).isActive = true
    }

    var parentDevices: ParentDevice?{
        didSet{
            if parentDevices?.icon == "coin_silver"{
                customImageView.image = UIImage(named: parentDevices?.icon ?? "placeholder")
            }else{
                customImageView.image = UIImage(named: parentDevices?.icon ?? "placeholder")?.withRenderingMode(.alwaysTemplate)
                customImageView.tintColor = .black
            }
            customLabel.text      = parentDevices?.name
        }
    }
}
