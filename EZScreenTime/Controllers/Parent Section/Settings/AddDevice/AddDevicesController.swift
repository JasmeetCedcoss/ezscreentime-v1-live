//
//  AddDevicesController.swift
//  EZScreenTime
//
//  Created by cedcoss on 08/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

class AddDevicesController: BaseViewController {

    var addDeviceData: AddDevicesModel?{
        didSet{
            tableView.reloadData()
        }
    }

    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = AppSetUp.controllerBackgroundColor
        makeNetworkCall()
        setupTable()
        NotificationCenter.default.addObserver(self, selector: #selector(deviceAdded(notification: )), name: .deviceAdded, object: nil)
    }

    @objc func deviceAdded(notification:Notification){
        print("Device Added")
        makeNetworkCall()
    }

    func setupTable(){
        tableView.dataSource = self
        tableView.delegate   = self
        tableView.register(HeaderTVCell.self, forCellReuseIdentifier: "HeaderTVCell")
        tableView.register(NavigationCell.self, forCellReuseIdentifier: "NavigationCell")
        tableView.separatorStyle = .none
        tableView.backgroundColor = AppSetUp.controllerBackgroundColor
    }


    func makeNetworkCall(){

        mageHelper().callHttpRequest(controller: self, endPoints: "parent/devices", parameters: nil,sendAuthHeader: true) { (data) in
            do {
                let json = try JSON(data: data)
                print(json)
                if json["status"].boolValue == true{
                    //  MARK: - Save latest token get request
                    //
                    let token = json["data"]["access_token"].stringValue
                    AppSetUp().saveToken(token)

                    //  MARK: - Data Parsing
                    //
                    AppSetUp.decoder.keyDecodingStrategy = .convertFromSnakeCase
                    self.addDeviceData = try AppSetUp.decoder.decode(AddDevicesModel.self, from: data)
                    print("SettingsModel====",self.addDeviceData as Any)
                }else{
                    self.view.makeToast(json["message"].stringValue, duration: 3.0)
                }
            }
            catch let error{
                print(error.localizedDescription)
            }
        }
    }
}


extension AddDevicesController: UITableViewDelegate,UITableViewDataSource{

    func numberOfSections(in tableView: UITableView) -> Int {
        guard let readyToLoad = self.addDeviceData?.status else {return 0}
        return readyToLoad ? 3 : 0
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section{
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderTVCell", for: indexPath) as! HeaderTVCell
            cell.setupView(header: "Add devices",image: "settings",subheader: "Select the device your kids are using.\n\nLater on you can assign the same devices multiple times and rename them.")
            cell.parent = self
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SuggestedDeviceTVCell", for: indexPath) as! SuggestedDeviceTVCell
            cell.gotAddDeviceData = addDeviceData
            cell.parent           = self
            cell.delegate         = self as expandSection
            return cell
        case 2:
        let cell = tableView.dequeueReusableCell(withIdentifier: "NavigationCell", for: indexPath) as! NavigationCell
        cell.setupView()
        cell.parent = self

        cell.backButton.rx.tap.subscribe { (onTap) in
            self.navigationController?.popViewController(animated: true)
        }.disposed(by: cell.disposeBag)

        cell.nextButton.rx.tap.subscribe { (onTap) in
            let viewControl:CoinSettingController = self.storyboard!.instantiateViewController()
            self.navigationController?.pushViewController(viewControl, animated: true)
        }.disposed(by: cell.disposeBag)
        return cell
        default:
            return UITableViewCell()
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        switch indexPath.section{
        case 0:
            return UITableView.automaticDimension
        case 2:
            return AppSetUp.isIpad ? 70 : 60
        default:
            let totalCountofSuggestedDevice = (self.addDeviceData?.data?.suggestedDevices?.count ?? 6)
            print("totalCount==",totalCountofSuggestedDevice)
            let totalCountofParentDevice = (self.addDeviceData?.data?.parentDevices?.count ?? 6)
            print("totalCountofParentDevice==",totalCountofParentDevice)
            if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular{
                return (CGFloat(((totalCountofSuggestedDevice + 1))/3) * 250) + CGFloat((totalCountofParentDevice + 3)/4 * 160)
            }else{
                return (CGFloat(((totalCountofSuggestedDevice + 1))/3) * 250) + CGFloat((totalCountofParentDevice + 2)/3 * 160)
            }

        }
    }
}

extension AddDevicesController: expandSection{
    func reloadData(_cell: SuggestedDeviceTVCell, sender: Any) {
        print("Reloading")
        self.tableView.reloadData()
    }

    func expandSection(_cell: SuggestedDeviceTVCell, sender: Any) {
        print("Delegate")
        guard let _ = sender as? UIButton else {return}
    }
}


