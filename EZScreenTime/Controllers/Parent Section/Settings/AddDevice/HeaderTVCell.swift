//
//  HeaderTVCell.swift
//  EZScreenTime
//
//  Created by cedcoss on 08/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

class HeaderTVCell: UITableViewCell {

    lazy var backView: UIView = {
        let backView                                        = UIView()
        backView.backgroundColor                            = UIColor.white
        backView.layer.borderColor                          = #colorLiteral(red: 0.7540688515, green: 0.7540867925, blue: 0.7540771365, alpha: 1)
        backView.layer.borderWidth                          = 2
        backView.layer.cornerRadius                         = 5
        backView.translatesAutoresizingMaskIntoConstraints  = false
        return backView
    }()

    lazy var customImageView: UIImageView = {
        let customImageView                                        = UIImageView()
        customImageView.image                                      = UIImage(named: "placeholder")
        customImageView.contentMode                                = .scaleAspectFit
        customImageView.translatesAutoresizingMaskIntoConstraints  = false
        return customImageView
    }()

    lazy var customLabel: UILabel = {
        let customLabel                                        = UILabel()
        customLabel.textColor                                    = AppSetUp.textColor
        customLabel.translatesAutoresizingMaskIntoConstraints  = false
        customLabel.numberOfLines                              = 0
        if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular{
            customLabel.font = UIFont.systemFont(ofSize: 25, weight: .medium)
        }else{
            customLabel.font = UIFont.systemFont(ofSize: 22, weight: .medium)
        }
        return customLabel
    }()

    lazy var subheadingLabel: UILabel = {
        let subheadingLabel                                        = UILabel()
        subheadingLabel.textColor                                  = AppSetUp.textColor
        subheadingLabel.translatesAutoresizingMaskIntoConstraints  = false
        subheadingLabel.numberOfLines                              = 0
        if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular{
            subheadingLabel.font = UIFont(name:"Poppins-Regular",size:19)
        }else{
            subheadingLabel.font = UIFont(name:"Poppins-Regular",size:15)
        }
        return subheadingLabel
    }()


    lazy var backIcon: UIButton = {
        let backIcon                                        = UIButton()
        backIcon.setImage(UIImage(named:"backicon"), for: .normal)
        backIcon.translatesAutoresizingMaskIntoConstraints  = false
        return backIcon
    }()

    var parent                  = UIViewController()

    private(set) var disposeBag = DisposeBag()

    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag() // because life cicle of every cell ends on prepare for reuse
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    func setupView(header: String = "", image: String = "placeholder",subheader:String = ""){
        self.backgroundColor    = AppSetUp.controllerBackgroundColor
        self.selectionStyle     = .none
        self.addSubview(backView)
        backView.topAnchor.constraint(equalTo: self.topAnchor, constant: 10).isActive                       = true
        backView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10).isActive               = true
        backView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10).isActive            = true
        if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular{
            backView.heightAnchor.constraint(equalToConstant: 65).isActive                                      = true
        }else{
            backView.heightAnchor.constraint(equalToConstant: 50).isActive                                      = true
        }


        self.backView.addSubview(customImageView)
        customImageView.topAnchor.constraint(equalTo: backView.topAnchor, constant: 10).isActive             = true
        customImageView.leadingAnchor.constraint(equalTo: backView.leadingAnchor, constant: 5).isActive     = true
        customImageView.bottomAnchor.constraint(equalTo: backView.bottomAnchor, constant: -10).isActive      = true
        customImageView.widthAnchor.constraint(equalToConstant: 50).isActive                                = true

        self.backView.addSubview(customLabel)
        customLabel.topAnchor.constraint(equalTo: backView.topAnchor).isActive                              = true
        customLabel.leadingAnchor.constraint(equalTo: customImageView.trailingAnchor,constant: 5).isActive  = true
        customLabel.bottomAnchor.constraint(equalTo: backView.bottomAnchor).isActive                        = true
        customLabel.trailingAnchor.constraint(equalTo: backView.trailingAnchor).isActive                    = true
        customLabel.text        = header
        customImageView.image   = UIImage(named: image)

        self.backView.addSubview(backIcon)
        backIcon.snp.makeConstraints { (make) in
            make.centerY.equalTo(backView.snp.centerY)
            make.height.equalTo(40)
            make.width.equalTo(30)
            make.right.equalTo(backView.snp.right).offset(-10)
        }

        self.addSubview(subheadingLabel)
        subheadingLabel.topAnchor.constraint(equalTo: backView.bottomAnchor, constant: 10).isActive         = true
        subheadingLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10).isActive        = true
        subheadingLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10).isActive     = true
        subheadingLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10).isActive         = true
        subheadingLabel.text = subheader
        subheadingLabel.numberOfLines = 0
        subheadingLabel.textAlignment = .center

        backIcon.rx.tap.subscribe { (onTap) in
            self.parent.navigationController?.popViewController(animated: true)
        }.disposed(by: self.disposeBag)
    }
}
