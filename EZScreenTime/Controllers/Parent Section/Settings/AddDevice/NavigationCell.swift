//
//  NavigationCell.swift
//  EZScreenTime
//
//  Created by cedcoss on 11/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

class NavigationCell: UITableViewCell {

    private(set) var disposeBag = DisposeBag()
    var parent                  = UIViewController()
    lazy var stackView: UIStackView = {
        let stackView                                        = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints  = false
        stackView.distribution                               = .fillEqually
        stackView.spacing                                    = 10
        stackView.alignment                                  = .fill
        stackView.axis                                       = .horizontal
        return stackView
    }()

    lazy var backButton: UIButton = {
        let backButton                                        = UIButton()
        backButton.backgroundColor                            = AppSetUp.controllerBackgroundColor
        backButton.setTitle("<< BACK", for: .normal)
        backButton.translatesAutoresizingMaskIntoConstraints  = false
        backButton.setTitleColor(.black, for: .normal)
        backButton.layer.borderColor                          = #colorLiteral(red: 0.7540688515, green: 0.7540867925, blue: 0.7540771365, alpha: 1)
        backButton.layer.borderWidth                          = 1
        backButton.layer.cornerRadius                         = 5
        backButton.titleLabel?.font                           = UIFont(name: "Poppins-SemiBold", size: 15)
        if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular{
            backButton.titleLabel?.font                           = UIFont(name: "Poppins-SemiBold", size: 18)
        }else{
            backButton.titleLabel?.font                           = UIFont(name: "Poppins-SemiBold", size: 15)
        }
        backButton.titleLabel?.textColor                      = AppSetUp.textColor
        return backButton
    }()

    lazy var nextButton: UIButton = {
        let nextButton                                        = UIButton()
        nextButton.backgroundColor                            = AppSetUp.colorAccent
        nextButton.setTitle("NEXT >>", for: .normal)
        nextButton.translatesAutoresizingMaskIntoConstraints  = false
        nextButton.layer.borderColor                          = #colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1)
        nextButton.layer.borderWidth                          = 1
        nextButton.layer.cornerRadius                         = 5

        if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular{
            nextButton.titleLabel?.font                           = UIFont(name: "Poppins-SemiBold", size: 18)
        }else{
            nextButton.titleLabel?.font                           = UIFont(name: "Poppins-SemiBold", size: 15)
        }
        return nextButton
    }()

    override func prepareForReuse() {
       super.prepareForReuse()
       disposeBag = DisposeBag() // because life cicle of every cell ends on prepare for reuse
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }

    func setupView(){
        self.backgroundColor = AppSetUp.controllerBackgroundColor
        self.selectionStyle  = .none
        self.addSubview(stackView)
        stackView.topAnchor.constraint(equalTo: self.topAnchor, constant: 10).isActive = true
        stackView.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        stackView.widthAnchor.constraint(equalToConstant: 210).isActive = true
        stackView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10).isActive = true
        stackView.addArrangedSubview(backButton)
        stackView.addArrangedSubview(nextButton)
    }
}
