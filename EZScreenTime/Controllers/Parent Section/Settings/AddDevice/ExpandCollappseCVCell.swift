//
//  ExpandCollappseCVCell.swift
//  EZScreenTime
//
//  Created by cedcoss on 10/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

class ExpandCollappseCVCell: UICollectionViewCell {

    lazy var expandCollapseButton: UIButton = {
        let expandCollapseButton                                        = UIButton()
        expandCollapseButton.translatesAutoresizingMaskIntoConstraints  = false
        expandCollapseButton.layer.cornerRadius                         = 5
        expandCollapseButton.setTitle("My Devices", for: .normal)
        expandCollapseButton.contentHorizontalAlignment = .left
        expandCollapseButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        expandCollapseButton.backgroundColor = AppSetUp.colorPrimary
        return expandCollapseButton
    }()

    lazy var otherButton: UIButton = {
           let otherButton                                        = UIButton()
           otherButton.backgroundColor                            = AppSetUp.controllerBackgroundColor
           otherButton.setTitle("Other", for: .normal)
           otherButton.translatesAutoresizingMaskIntoConstraints  = false
           otherButton.setTitleColor(.systemBlue, for: .normal)
        
           return otherButton
       }()

    lazy var backView: UIView = {
        let backView                                        = UIView()
        backView.backgroundColor                            = AppSetUp.controllerBackgroundColor
        backView.translatesAutoresizingMaskIntoConstraints  = false
        return backView
    }()

    func setupView(){
        self.addSubview(backView)
        backView.topAnchor.constraint(equalTo: self.topAnchor,constant: 5 ).isActive = true
        backView.leadingAnchor.constraint(equalTo: self.leadingAnchor,constant: 5).isActive = true
        backView.trailingAnchor.constraint(equalTo: self.trailingAnchor,constant: -5).isActive = true
        backView.bottomAnchor.constraint(equalTo: self.bottomAnchor,constant: -5).isActive = true

        backView.addSubview(expandCollapseButton)
        expandCollapseButton.topAnchor.constraint(equalTo: backView.topAnchor).isActive = true
        expandCollapseButton.leadingAnchor.constraint(equalTo: backView.leadingAnchor).isActive = true
        expandCollapseButton.trailingAnchor.constraint(equalTo: backView.trailingAnchor).isActive = true
        expandCollapseButton.bottomAnchor.constraint(equalTo: backView.bottomAnchor).isActive = true

    }


    func setupViewOtherButton(){
        self.addSubview(backView)
        backView.topAnchor.constraint(equalTo: self.topAnchor,constant: 5 ).isActive = true
        backView.leadingAnchor.constraint(equalTo: self.leadingAnchor,constant: 5).isActive = true
        backView.trailingAnchor.constraint(equalTo: self.trailingAnchor,constant: -5).isActive = true
        backView.bottomAnchor.constraint(equalTo: self.bottomAnchor,constant: -5).isActive = true

        backView.addSubview(otherButton)
        otherButton.topAnchor.constraint(equalTo: backView.topAnchor).isActive = true
        otherButton.leadingAnchor.constraint(equalTo: backView.leadingAnchor).isActive = true
        otherButton.trailingAnchor.constraint(equalTo: backView.trailingAnchor).isActive = true
        otherButton.bottomAnchor.constraint(equalTo: backView.bottomAnchor).isActive = true

    }
}
