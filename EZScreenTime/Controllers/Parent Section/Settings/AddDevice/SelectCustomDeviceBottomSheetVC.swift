//
//  SelectCustomDeviceBottomSheetVC.swift
//  EZScreenTime
//
//  Created by cedcoss on 11/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit
import BottomPopup

protocol selectedDeviceImage {
    func selectedDeviceImage(_controller:SelectCustomDeviceBottomSheetVC,sender:Any,image:String)
}


class SelectCustomDeviceBottomSheetVC: BottomPopupViewController {

    var kidIconsArray               = ["boy1", "boy2", "boy3", "boy4", "boy5", "boy6", "boy7", "boy8", "boy9", "boy10", "boy11", "boy12", "boy13", "boy14", "boy15", "boy16", "boy17", "boy18", "boy19", "boy20", "boy21", "boy24", "boy25", "boy26", "boy27", "boy28", "boy29", "boy30", "boy31", "boy32", "boy33", "boy34", "boy35", "boy36", "boy37", "boy38",
    "girl1", "girl2", "girl3", "girl4", "girl5", "girl6", "girl7", "girl8", "girl9", "girl10", "girl11", "girl12", "girl13", "girl14", "girl15", "girl16", "girl17", "girl18", "girl19", "girl20", "girl21", "girl22", "girl23", "girl24", "girl25", "girl26", "girl27", "girl28", "girl29", "girl30", "girl31", "girl32", "girl33", "girl34"]

    var suggestedDevices            : [SuggestedDevice]?
    lazy var collectionView: UICollectionView = {
        let layout                  = UICollectionViewFlowLayout()
        layout.scrollDirection      = .vertical
        let collection              = UICollectionView(frame: CGRect(x: 0, y: 0, width: 0, height: 0), collectionViewLayout: layout)
        collection.translatesAutoresizingMaskIntoConstraints = false
        collection.isScrollEnabled  = true
        collection.backgroundColor  = UIColor.white
        return collection
    }()

    var isFrom          = String()
    var delegate       :selectedDeviceImage?
    var height: CGFloat?
    var topCornerRadius: CGFloat?
    var presentDuration: Double?
    var dismissDuration: Double?
    var shouldDismissInteractivelty: Bool?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()

        // Do any additional setup after loading the view.
    }

    func getPopupHeight() -> CGFloat {
        return height ?? CGFloat(300)
    }

    override var popupHeight: CGFloat { return height ?? CGFloat(300) }

    func getPopupTopCornerRadius() -> CGFloat {
        return topCornerRadius ?? CGFloat(10)
    }

    func getPopupPresentDuration() -> Double {
        return presentDuration ?? 1.0
    }

    func getPopupDismissDuration() -> Double {
        return dismissDuration ?? 1.0
    }

    func shouldPopupDismissInteractivelty() -> Bool {
        return shouldDismissInteractivelty ?? true
    }

    func setupView(){
        self.view.addSubview(collectionView)
        collectionView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 5).isActive       = true
        collectionView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -5).isActive   = true
        collectionView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 5).isActive    = true
        collectionView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -5).isActive = true
        collectionView.register(customDeviceBottomSheetCVCell.self, forCellWithReuseIdentifier: "customDeviceBottomSheetCVCell")
        collectionView.register(TitleCVCell.self, forCellWithReuseIdentifier: "TitleCVCell")
        collectionView.delegate     = self
        collectionView.dataSource   = self
    }
}

extension SelectCustomDeviceBottomSheetVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case 1:
            switch isFrom{
            case "AddKids":
            return kidIconsArray.count
            default:
                return suggestedDevices?.count ?? 0
            }

        default:
            return 1
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        switch indexPath.section {
        case 1:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "customDeviceBottomSheetCVCell", for: indexPath) as! customDeviceBottomSheetCVCell
            cell.setupView()
            switch isFrom{
            case "AddKids":
            cell.kidIcons = self.kidIconsArray[indexPath.item]
            default:
            cell.suggestedDevices = self.suggestedDevices?[indexPath.item]
            }
            return cell
        default:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TitleCVCell", for: indexPath) as! TitleCVCell
            cell.setupView()
            cell.customLabel.text = "Select an image"
            return cell

        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch indexPath.section {
        case 1:
            return CGSize(width: collectionView.frame.width/3 - 10 , height: 100)
        default:
            return CGSize(width: collectionView.frame.width , height: 100)
        }
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        switch indexPath.section {
        case 1:
                switch isFrom{
                case "AddKids":
                    dismiss(animated: true, completion: {
                        print("completion block")
                        self.delegate?.selectedDeviceImage(_controller: self, sender: (Any).self, image: self.kidIconsArray[indexPath.item])
                    })
                default:
                    dismiss(animated: true, completion: {
                        print("completion block")
                        self.delegate?.selectedDeviceImage(_controller: self, sender: (Any).self, image: self.suggestedDevices?[indexPath.item].icon ?? "")
                    })
                }
        default:
            print("default")
        }
    }
}
