//
//  customDeviceBottomSheetCVCell.swift
//  EZScreenTime
//
//  Created by cedcoss on 11/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

class customDeviceBottomSheetCVCell: UICollectionViewCell {

    lazy var backView: UIView = {
        let backView                                        = UIView()
        backView.translatesAutoresizingMaskIntoConstraints  = false
        return backView
    }()

    lazy var customImageView: UIImageView = {
        let customImageView                                        = UIImageView()
        customImageView.contentMode                                = .scaleAspectFit
        customImageView.clipsToBounds                              = true
        customImageView.translatesAutoresizingMaskIntoConstraints  = false
        return customImageView
    }()

    override init(frame: CGRect) {
        super.init(frame:frame)
        setupView()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("")
    }

    func setupView(){
        self.addSubview(backView)
        backView.topAnchor.constraint(equalTo: self.topAnchor,constant: 0 ).isActive = true
        backView.leadingAnchor.constraint(equalTo: self.leadingAnchor,constant: 0).isActive = true
        backView.trailingAnchor.constraint(equalTo: self.trailingAnchor,constant: 0).isActive = true
        backView.bottomAnchor.constraint(equalTo: self.bottomAnchor,constant: 0).isActive = true

        backView.addSubview(customImageView)
        customImageView.topAnchor.constraint(equalTo: backView.topAnchor,constant: 0).isActive = true
        customImageView.leadingAnchor.constraint(equalTo: backView.leadingAnchor,constant: 0).isActive = true
        customImageView.trailingAnchor.constraint(equalTo: backView.trailingAnchor,constant: 0).isActive = true
        customImageView.bottomAnchor.constraint(equalTo: backView.bottomAnchor,constant: 0).isActive = true
    }

    var suggestedDevices: SuggestedDevice?{
        didSet{
            customImageView.image = UIImage(named: suggestedDevices?.icon ?? "placeholder")
        }
    }


    var kidIcons: String?{
        didSet{
            customImageView.image = UIImage(named: kidIcons ?? "placeholder")
        }
    }
}
