//
//  SuggestedDeviceCVCell.swift
//  EZScreenTime
//
//  Created by cedcoss on 08/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

class SuggestedDeviceCVCell: UICollectionViewCell {

    lazy var backView: UIView = {
        let backView                                        = UIView()
        backView.backgroundColor                            = AppSetUp.controllerBackgroundColor
        backView.translatesAutoresizingMaskIntoConstraints  = false
        return backView
    }()

    lazy var customImageView: UIImageView = {
        let customImageView                                        = UIImageView()
        customImageView.image                                      = UIImage(named: "placeholder")
        customImageView.contentMode                                = .scaleAspectFit
        customImageView.translatesAutoresizingMaskIntoConstraints  = false
        return customImageView
    }()

    lazy var customLabel: UILabel = {
        let customLabel                                        = UILabel()
        customLabel.textColor                                  = UIColor.black
        customLabel.translatesAutoresizingMaskIntoConstraints  = false
        customLabel.numberOfLines                              = 0
        customLabel.textAlignment                              = .center
        if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular{
            customLabel.font                   = UIFont(name: "Poppins-Regular", size: 19)
        }else{
            customLabel.font                   = UIFont(name: "Poppins-Regular", size: 15)
        }
        return customLabel
    }()


    func setupView(){
        self.addSubview(backView)
        backView.topAnchor.constraint(equalTo: self.topAnchor,constant: 5 ).isActive = true
        backView.leadingAnchor.constraint(equalTo: self.leadingAnchor,constant: 5).isActive = true
        backView.trailingAnchor.constraint(equalTo: self.trailingAnchor,constant: -5).isActive = true
        backView.bottomAnchor.constraint(equalTo: self.bottomAnchor,constant: -5).isActive = true

        self.backView.addSubview(customLabel)
        customLabel.leadingAnchor.constraint(equalTo: self.backView.leadingAnchor).isActive = true
        customLabel.trailingAnchor.constraint(equalTo: self.backView.trailingAnchor).isActive = true
        customLabel.bottomAnchor.constraint(equalTo: self.backView.bottomAnchor).isActive = true
        customLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true

        self.backView.addSubview(customImageView)
        customImageView.leadingAnchor.constraint(equalTo: self.backView.leadingAnchor).isActive = true
        customImageView.trailingAnchor.constraint(equalTo: self.backView.trailingAnchor).isActive = true
        customImageView.bottomAnchor.constraint(equalTo: self.customLabel.topAnchor).isActive = true
        customImageView.topAnchor.constraint(equalTo: self.backView.topAnchor).isActive = true
    }
    
    var suggestedDevices: SuggestedDevice?{
        didSet{
            customImageView.image = UIImage(named: suggestedDevices?.icon ?? "placeholder")?.withRenderingMode(.alwaysTemplate)
            customImageView.tintColor = #colorLiteral(red: 0.6862745098, green: 0.6862745098, blue: 0.6862745098, alpha: 1)
            customLabel.text      = suggestedDevices?.name
        }
    }
}
