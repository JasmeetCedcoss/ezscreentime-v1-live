//
//  SuggestedDeviceTVCell.swift
//  EZScreenTime
//
//  Created by cedcoss on 08/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

protocol expandSection {
    func expandSection(_cell:SuggestedDeviceTVCell,sender:Any)
    func reloadData(_cell:SuggestedDeviceTVCell,sender: Any)
}

class SuggestedDeviceTVCell: UITableViewCell {

    var gotAddDeviceData: AddDevicesModel?{
        didSet{
            print(gotAddDeviceData)
            collectionView.reloadData()
        }
    }

    @IBOutlet weak var collectionView: UICollectionView!
    var deletePopup = CustomDevice()
    var bgCView = UIView()
    var showParentDevice        = true
    var receivedNewParentDevice = false
    var delegate                :expandSection?
    var parent                  = UIViewController()
    var imageReceived           = String()

    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.delegate         = self
        collectionView.dataSource       = self
        collectionView.register(ParentDeviceCVCell.self, forCellWithReuseIdentifier: "ParentDeviceCVCell")
        collectionView.register(ExpandCollappseCVCell.self, forCellWithReuseIdentifier: "ExpandCollappseCVCell")
        collectionView.backgroundColor  = AppSetUp.controllerBackgroundColor
    }
}


extension SuggestedDeviceTVCell:UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 3
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        switch section{
        case 0:
            return gotAddDeviceData?.data?.suggestedDevices?.count ?? 0
        case 2:
            return gotAddDeviceData?.data?.parentDevices?.count ?? 0
        case 1:
            return 2
        default:
            return 0
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        switch indexPath.section{
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SuggestedDeviceCVCell", for: indexPath) as! SuggestedDeviceCVCell
            cell.setupView()
            cell.suggestedDevices = self.gotAddDeviceData?.data?.suggestedDevices?[indexPath.item]
            cell.backgroundColor  = AppSetUp.controllerBackgroundColor
            return cell

        case 1:
            switch indexPath.item {
            case 1:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ExpandCollappseCVCell", for: indexPath) as! ExpandCollappseCVCell
                cell.setupView()
                cell.expandCollapseButton.addTarget(self, action: #selector(expandTapNow(_:)), for: .touchUpInside)
                return cell
            default:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ExpandCollappseCVCell", for: indexPath) as! ExpandCollappseCVCell
                cell.otherButton.addTarget(self, action: #selector(popUpDisplay(_:)), for: .touchUpInside)
                cell.setupViewOtherButton()
                return cell
            }
        case 2:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ParentDeviceCVCell", for: indexPath) as! ParentDeviceCVCell
            cell.setupView()
            cell.parentDevices = self.gotAddDeviceData?.data?.parentDevices?[indexPath.item]

//            //  MARK: - Delete device activity
//            //
//            let longPressRecognizer = UILongPressGestureRecognizer()
//            cell.backView.addGestureRecognizer(longPressRecognizer)
//            longPressRecognizer.rx.event.bind(onNext: { recognizer in
//                if recognizer.state == .ended{
//
//                    print("touches: \(recognizer.numberOfTouches)") //or whatever you like
//
//                    //                      MARK: - Delete AlertView
//
//                    let alertView = UIAlertController(title: cell.customLabel.text, message: "Are you sure you want to delete this device ?", preferredStyle: .alert)
//                    alertView.addAction(UIAlertAction(title: "Yes", style: .default, handler: {
//                        Void in
//                        guard let id = self.gotAddDeviceData?.data?.parentDevices?[indexPath.item].id else {return}
//                        mageHelper().callHttpRequestDelete(controller: self.parent, endPoints: "parent/devices?id=\(id)", parameters: nil,sendAuthHeader: true) { (data) in
//                            do {
//                                let json = try JSON(data: data)
//                                print(json)
//
//                                if json["status"].boolValue == true{
//
//                                    //  MARK: - Save latest token
//                                    //
//                                    let token = json["data"]["access_token"].stringValue
//                                    AppSetUp().saveToken(token)
//                                    NotificationCenter.default.post(name: .deviceAdded, object: nil)
//                                }else{
//                                    self.parent.view.makeToast(json["message"].stringValue, duration: 3.0)
//                                }
//                            }
//                            catch let error{
//                                print(error.localizedDescription)
//                            }
//                        }
//                    }))
//                    alertView.addAction(UIAlertAction(title: "No", style: .cancel, handler: { Void in
//                    }))
//                    self.parent.present(alertView, animated: true, completion: nil)
//                }
//            }).disposed(by: AppSetUp.disposeBag)

            cell.deleteDevice.rx.tap.subscribe { (onTap) in

                let alertView = UIAlertController(title: cell.customLabel.text, message: "Are you sure you want to delete this device ?", preferredStyle: .alert)
                alertView.addAction(UIAlertAction(title: "Yes", style: .default, handler: {
                    Void in
                    guard let id = self.gotAddDeviceData?.data?.parentDevices?[indexPath.item].id else {return}
                    mageHelper().callHttpRequestDelete(controller: self.parent, endPoints: "parent/devices?id=\(id)", parameters: nil,sendAuthHeader: true) { (data) in
                        do {
                            let json = try JSON(data: data)
                            print(json)

                            if json["status"].boolValue == true{

                                //  MARK: - Save latest token
                                //
                                let token = json["data"]["access_token"].stringValue
                                AppSetUp().saveToken(token)
                                NotificationCenter.default.post(name: .deviceAdded, object: nil)
                            }else{
                                self.parent.view.makeToast(json["message"].stringValue, duration: 3.0)
                            }
                        }
                        catch let error{
                            print(error.localizedDescription)
                        }
                    }
                }))
                alertView.addAction(UIAlertAction(title: "No", style: .cancel, handler: { Void in
                }))
                self.parent.present(alertView, animated: true, completion: nil)
            }.disposed(by: cell.disposeBag)
        return cell

        default:
        return UICollectionViewCell()
    }
}

func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

    switch indexPath.section {
    case 1:
        return CGSize(width: collectionView.frame.width, height: 60)
    default:
        if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular{
            return CGSize(width: collectionView.frame.width/4-8, height: 150)
        }else{
            return CGSize(width: collectionView.frame.width/3-8, height: 150)
        }
    }
}

func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    switch indexPath.section{
    case 0:
        guard let devicename = self.gotAddDeviceData?.data?.suggestedDevices?[indexPath.item].name, let deviceicon = self.gotAddDeviceData?.data?.suggestedDevices?[indexPath.item].icon  else {return}
        print("deviceSelected===",deviceicon)
        let params: [String: String]? = ["name":devicename,"icon":deviceicon]
        callAPIToAddDevice(params)
    default:
        print("default")
    }
}

@objc func expandTapNow(_ sender: UIButton){
    self.delegate?.expandSection(_cell: self, sender: sender)
}


//  MARK: - Add custom device
//
@objc func popUpDisplay(_ sender: UIButton){
    deletePopup = CustomDevice(frame: CGRect(x:0 , y: 0, width: self.parent.view.bounds.width - 50, height: 200))
    let newcgpoint = CGPoint(x: self.parent.view.center.x + 4, y:  self.parent.view.center.y - 10)
    deletePopup.center                  = newcgpoint
    //  MARK: - Display BottomSheet
    //
//    deletePopup.selectDevice.rx.tap.subscribe { (onTap) in
//        guard let viewControl = self.parent.storyboard?.instantiateViewController(withIdentifier: "SelectCustomDeviceBottomSheetVC") as? SelectCustomDeviceBottomSheetVC else { return }
//        let screenSize: CGRect       = UIScreen.main.bounds
//        let screenHeight             = screenSize.height
//        viewControl.height           = screenHeight/2.5
//        viewControl.topCornerRadius  = 20
//        viewControl.delegate         = self
//        viewControl.suggestedDevices = self.gotAddDeviceData?.data?.suggestedDevices
//        viewControl.presentDuration  = 0.30
//        viewControl.dismissDuration  = 0.30
//        self.parent.navigationController?.present(viewControl, animated: true, completion: nil)
//    }.disposed(by: AppSetUp.disposeBag)

    //  MARK: - Add selected device
    //
    deletePopup.addCustomDevice.rx.tap.subscribe { (onTap) in
        print("ADD THIS DEVICE")
        let icon = "\(String(describing: self.deletePopup.selectDevice.currentImage))"
        print("icon====",icon)
        self.callAPIToAddDevice(["icon":"coin_silver","name":self.deletePopup.deviceNameField.text ?? ""],isPopUpPresented: true)
    }.disposed(by: AppSetUp.disposeBag)

    bgCView.isUserInteractionEnabled    = true
    bgCView.tag                         = 500
    bgCView.center                      = self.parent.view.center
    bgCView.backgroundColor             = UIColor(red:0, green:0, blue:0, alpha:0.5);
    bgCView.autoresizingMask            = [UIView.AutoresizingMask.flexibleHeight , UIView.AutoresizingMask.flexibleWidth]
    let tap                             = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
    bgCView.addGestureRecognizer(tap)
    let window = UIApplication.shared.keyWindow
    window?.addSubview(bgCView)
    bgCView.frame                       = (window?.frame)!
    bgCView.addSubview(deletePopup)
}

@objc func handleTap(_ sender: UITapGestureRecognizer) {
    bgCView.removeFromSuperview()
    if let deletePopup = self.parent.view.viewWithTag(500) as? CustomDevice {
        bgCView.removeFromSuperview()
        deletePopup.removeFromSuperview()
    }
}

fileprivate func callAPIToAddDevice(_ params: [String : String]?,isPopUpPresented:Bool = false) {
    mageHelper().callHttpRequest(controller: parent, endPoints: "parent/devices",parameters: params, sendAuthHeader: true,postData: true, completion: {
        data in
        do {
            let json = try JSON(data: data)
            print(json)
            if json["status"].boolValue == true{
                let token = json["data"]["access_token"].stringValue
                AppSetUp().saveToken(token)
                self.parent.view.makeToast(json["message"].stringValue, duration: 3.0)
                NotificationCenter.default.post(name: .deviceAdded, object: nil)
                self.collectionView.reloadData()

                if isPopUpPresented{
                    self.bgCView.removeFromSuperview()
                    if let deletePopup = self.parent.view.viewWithTag(500) as? CustomDevice {
                        self.bgCView.removeFromSuperview()
                        deletePopup.removeFromSuperview()
                    }
                }else{
                    print("Pop up not present")
                }
                self.parent.view.makeToast(json["message"].stringValue, duration: 3.0)
            }else{
                self.parent.view.makeToast(json["message"].stringValue, duration: 3.0)
            }
        }
        catch let error{
            print(error.localizedDescription)
        }
    })
}
}

extension SuggestedDeviceTVCell: selectedDeviceImage{
    func selectedDeviceImage(_controller: SelectCustomDeviceBottomSheetVC, sender: Any, image: String) {
        print("Yesyesyeysyesyes",image)
        deletePopup.selectDevice.setImage(UIImage(named: image), for: .normal)
        imageReceived = image
    }
}
