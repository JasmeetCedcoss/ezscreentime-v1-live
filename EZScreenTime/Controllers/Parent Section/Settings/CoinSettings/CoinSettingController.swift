//
//  CoinSettingController.swift
//  EZScreenTime
//
//  Created by cedcoss on 11/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

class CoinSettingController: BaseViewController {

    var updatedMinutes      = String()
    var coinSettingsModel   : CoinSettingsModel?{
        didSet{
            tableView.reloadData()
        }
    }

    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = AppSetUp.controllerBackgroundColor
        makeNetworkCall(showLoader: true)
        setupTable()
    }

    func setupTable(){
        tableView.dataSource      = self
        tableView.delegate        = self
        tableView.register(HeaderTVCell.self, forCellReuseIdentifier: "HeaderTVCell")
        tableView.register(NavigationCell.self, forCellReuseIdentifier: "NavigationCell")
        tableView.separatorStyle  = .none
        tableView.backgroundColor = AppSetUp.controllerBackgroundColor
    }

    func makeNetworkCall(showLoader:Bool = false){

        mageHelper().callHttpRequest(controller: self, endPoints: "parent/settings/devices/coins", parameters: nil,sendAuthHeader: true,showLoader: showLoader) { (data) in
            do {
                let json = try JSON(data: data)
                print(json)
                if json["status"].boolValue == true{
                    //  MARK: - Save latest token get request
                    //
                    let token = json["data"]["access_token"].stringValue
                    AppSetUp().saveToken(token)
                    //  MARK: - Data Parsing
                    //
                    AppSetUp.decoder.keyDecodingStrategy = .convertFromSnakeCase
                    self.coinSettingsModel = try AppSetUp.decoder.decode(CoinSettingsModel.self, from: data)
                    print("coinSettingsModel====",self.coinSettingsModel as Any)
                }else{
                    self.view.makeToast(json["message"].stringValue, duration: 3.0)
                }
            }
            catch let error{
                print(error.localizedDescription)
            }
        }
    }

    //  MARK: - Fire coin settings update: Called from 3 reactive views ===  Textfield, color button, minute button
    //
    func fireCoinSettingUpdate(id:String,name:String,color: String,val_minutes:String){
        print("id==",id,"name==",name,"color==",color,"val_minutes==",val_minutes)

        let params = ["id":id,"name":name,"color":color,"val_minutes":val_minutes]
        mageHelper().callHttpRequest(controller: self, endPoints: "parent/settings/devices/coins",parameters: params,sendAuthHeader: true,showLoader: false, postData:true,addStringToParam:"device",completion: {
            data in
            do {
                let json = try JSON(data: data)
                print(json)
                if json["status"].boolValue == true{
                    let token = json["data"]["access_token"].stringValue
                    AppSetUp().saveToken(token)
                   // self.makeNetworkCall()
                }else{
                    self.view.makeToast(json["message"].stringValue, duration: 3.0)
                }
            }
            catch let error{
                print(error.localizedDescription)
            }
        })
    }
}

extension CoinSettingController:  UITableViewDelegate,UITableViewDataSource{

    func numberOfSections(in tableView: UITableView) -> Int {
        guard let readyToLoad = self.coinSettingsModel?.status else {return 0}
        return readyToLoad ? 3 : 0
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 1:
            return self.coinSettingsModel?.data?.devices?.count ?? 0
        default:
            return 1
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderTVCell", for: indexPath) as! HeaderTVCell
            cell.setupView(header: "Coin Settings",image: "settings",subheader: "Select a coin color and add a value per coin.")
            cell.parent = self
            cell.backgroundColor = AppSetUp.controllerBackgroundColor
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CoinSettingDevicesTVCell", for: indexPath) as! CoinSettingDevicesTVCell
            cell.setupView()
            cell.devices        = self.coinSettingsModel?.data?.devices?[indexPath.row]
            //cell.selectMin.tag  = indexPath.row
            cell.delegate       = self as fireUpdateCoinSettings
            cell.parent         = self
            cell.tag            = indexPath.row
            //  MARK: - Reactive coin button
            //
            cell.selectColor.rx.tap.subscribe { (onTap) in
                cell.dropDownCoin.show()
            }.disposed(by: cell.disposeBag)

            //  MARK: -  Reactive textfield
            //
            cell.deviceName.rx.controlEvent(.editingDidEnd)
                .asObservable()
                .subscribe(onNext: { _ in
                    print("editing state changed")
                    let a = cell.selectColor.currentImage
                    print(a as Any)
                    guard let id = self.coinSettingsModel?.data?.devices?[indexPath.row].id,let name = cell.deviceName.text,let color = cell.selectColor.currentTitle,let val_minutes = cell.minTextField.text else {return}
                    //  MARK: - Updating Model then firing delegate
                    self.coinSettingsModel?.data?.devices?[indexPath.row].name = name
                    self.fireCoinSettingUpdate(id: id, name: name, color: color, val_minutes: val_minutes)
                }).disposed(by: cell.disposeBag)
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "NavigationCell", for: indexPath) as! NavigationCell
            cell.setupView()
            cell.parent = self
            cell.backButton.rx.tap.subscribe { (onTap) in
                self.navigationController?.popViewController(animated: true)
            }.disposed(by: cell.disposeBag)

            cell.nextButton.rx.tap.subscribe { (onTap) in
                print("Next")
                let viewControl:AddKidsController = self.storyboard!.instantiateViewController()
                self.navigationController?.pushViewController(viewControl, animated: true)
            }.disposed(by: cell.disposeBag)
            cell.backgroundColor = AppSetUp.controllerBackgroundColor
            return cell
        default:
            return UITableViewCell()
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return UITableView.automaticDimension
        case 2:
            return AppSetUp.isIpad ? 70 : 60
        default:
            return AppSetUp.isIpad ? 211 : 175
        }
    }
}

extension CoinSettingController: fireUpdateCoinSettings{

    func fireUpdateCoinSettings(_cell: CoinSettingDevicesTVCell, sender: Any, id: String, name: String, color: String, val_minutes: String) {
        //  MARK: - Updating Model then firing delegate
        self.coinSettingsModel?.data?.devices?[_cell.tag].id = id
        self.coinSettingsModel?.data?.devices?[_cell.tag].name = name
        self.coinSettingsModel?.data?.devices?[_cell.tag].color = color
        self.coinSettingsModel?.data?.devices?[_cell.tag].valMinutes = val_minutes
        self.fireCoinSettingUpdate(id: id, name: name, color: color, val_minutes: val_minutes)
    }
}

