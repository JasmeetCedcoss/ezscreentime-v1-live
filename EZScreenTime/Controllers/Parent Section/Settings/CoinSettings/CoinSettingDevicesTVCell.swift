//
//  CoinSettingDevicesTVCell.swift
//  EZScreenTime
//
//  Created by cedcoss on 12/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit
import DropDown

protocol fireUpdateCoinSettings {
    func fireUpdateCoinSettings(_cell:CoinSettingDevicesTVCell,sender:Any,id:String,name:String,color: String,val_minutes:String)
}

class CoinSettingDevicesTVCell: UITableViewCell {

    private(set) var disposeBag = DisposeBag()
    let dropDownMin             = DropDown()
    let dropDownCoin            = DropDown()
    var delegate                : fireUpdateCoinSettings?
    var parent                  = UIViewController()


    @IBOutlet weak var deviceIcon: UIImageView!
    @IBOutlet weak var deviceName: UITextField!
    @IBOutlet weak var selectColor: UIButton!
    @IBOutlet weak var addMin: UIButton!
    @IBOutlet weak var subtractMin: UIButton!
    @IBOutlet weak var minTextField: UITextField!

    var devices: Device?{
        didSet{
            if devices?.icon == "coin_silver"{
                deviceIcon.image = UIImage(named: devices?.icon ?? "placeholder")
            }else{
                deviceIcon.image = UIImage(named: devices?.icon ?? "placeholder")?.withRenderingMode(.alwaysTemplate)
                deviceIcon.tintColor = .black
            }
            deviceName.text  = devices?.name
            selectColor.setTitle(devices?.color, for: .normal)
            selectColor.setImage(UIImage(named: devices?.color ?? "")?.withRenderingMode(.alwaysOriginal), for: .normal)
            guard let word = devices?.valMinutes else {return}
            if let index = word.range(of: ".")?.lowerBound {
                let substring = word[..<index]
                let string = String(substring)

                minTextField.text = string
                print(string)  // "ora"
            }
        }
    }


    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        addMin.JSDropDown()
        subtractMin.JSDropDown()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag() // because life cicle of every cell ends on prepare for reuse
    }

    func setupView(){

        addMin.rx.tap.subscribe { (onTap) in
            guard let qtyText = self.minTextField.text else {return}
            guard  var qty = Int(qtyText) else {return}
            qty += 1
            self.minTextField.text = String(qty)
            guard let id = self.devices?.id,let name = self.deviceName.text,let color = self.selectColor.currentTitle,let val_minutes = self.minTextField.text else {return}
            self.delegate?.fireUpdateCoinSettings(_cell: self, sender: (Any).self, id: id,name:name,color:color,val_minutes:val_minutes)
        }.disposed(by: self.disposeBag)

        subtractMin.rx.tap.subscribe { (onTap) in
            guard let qtyText = self.minTextField.text else {return}
            guard  var qty = Int(qtyText) else {return}
            if qty <= 1 {return}
            qty -= 1
            self.minTextField.text = String(qty)
            guard let id = self.devices?.id,let name = self.deviceName.text,let color = self.selectColor.currentTitle,let val_minutes = self.minTextField.text else {return}
            self.delegate?.fireUpdateCoinSettings(_cell: self, sender: (Any).self, id: id,name:name,color:color,val_minutes:val_minutes)
        }.disposed(by: self.disposeBag)


        //  MARK: - Designing stuff farzi ka kaam
        //
        deviceName.backgroundColor          = AppSetUp.controllerBackgroundColor
        selectColor.addDropDownImage()
        selectColor.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 33)

        //  MARK: - Dropdown click functionality
        //
        selectColor.setTitleColor(.clear, for: .normal)


        //  MARK: - Dropdown select color 
        //
        let coinImage           = ["coin_blue","coin_bronze","coin_gold","coin_green","coin_pink","coin_silver"]
        dropDownCoin.anchorView = selectColor
        dropDownCoin.dataSource = coinImage
        dropDownCoin.cellNib    = UINib(nibName: "MyCell", bundle: nil)
        dropDownCoin.customCellConfiguration = { (index: Int, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? MyCell else { return }
            // Setup your custom UI components
            cell.coinImage.image = UIImage(named: coinImage[index])
            cell.optionLabel.isHidden = true
        }
        dropDownCoin.width      = 50
        dropDownCoin.direction  = .any
        dropDownCoin.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.selectColor.setTitle(item, for: .normal)
            self.selectColor.setImage(UIImage(named: item)?.withRenderingMode(.alwaysOriginal), for: .normal)




            guard let id = self.devices?.id,let name = self.deviceName.text,let val_minutes = self.minTextField.text else {return}
            self.delegate?.fireUpdateCoinSettings(_cell: self, sender: (Any).self, id: id,name:name,color:item,val_minutes:val_minutes)
        }
    }
}
