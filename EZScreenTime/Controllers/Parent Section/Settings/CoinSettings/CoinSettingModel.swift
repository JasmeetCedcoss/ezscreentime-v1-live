//
//  CoinSettingModel.swift
//  EZScreenTime
//
//  Created by cedcoss on 11/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//


import Foundation

// MARK: - Welcome
struct CoinSettingsModel: Codable {
    let response: Bool?
    let message: String?
    let status: Bool?
    var data: CoinDataClass?
}

// MARK: - DataClass
struct CoinDataClass: Codable {
    var devices: [Device]?
    let accessToken: String?
}

// MARK: - Device
struct Device: Codable {
    var valMinutes, name, icon, color: String?
    var id: String?
}

