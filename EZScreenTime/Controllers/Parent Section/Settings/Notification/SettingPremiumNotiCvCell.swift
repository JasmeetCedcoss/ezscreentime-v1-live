//
//  SettingPremiumNotiCvCell.swift
//  EZScreenTime
//
//  Created by cedcoss on 26/03/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit
import DropDown

class SettingPremiumNotiCvCell: UICollectionViewCell {
    
    @IBOutlet weak var dropDownMins: UIButton!
    @IBOutlet weak var parentDevices: UILabel!
    let dropDownMinsDropdown                        = DropDown()
    private(set) var disposeBag                     = DisposeBag()

    override func awakeFromNib() {
           super.awakeFromNib()
           // Initialization code
           dropDownMins.addDropDownImage()
       }

    override func prepareForReuse(){
           super.prepareForReuse()
           disposeBag = DisposeBag() // because life cicle of every cell ends on prepare for reuse
       }

        func setupView(){
            if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular{
                dropDownMinsDropdown.textFont               = UIFont.systemFont(ofSize: 19)
            }else{
                dropDownMinsDropdown.textFont               = UIFont.systemFont(ofSize: 15)
            }
            dropDownMinsDropdown.anchorView      = dropDownMins
            dropDownMinsDropdown.dataSource      = ["5","10","15"]
            dropDownMinsDropdown.direction       = .any
            dropDownMinsDropdown.selectionAction = { [unowned self] (index: Int, item: String) in

                print("Selected item: \(item) at index: \(index)")
                self.dropDownMins.setTitle(item, for: .normal)
                print("Mins==",item)

            }
        }
}
