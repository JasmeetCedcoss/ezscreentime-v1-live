//
//  SettingsPremiumNotificationController.swift
//  EZScreenTime
//
//  Created by cedcoss on 20/03/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

class SettingsPremiumNotificationController: BaseViewController {

    var notificationEnabled                        : String? = "1"
    var premiumNotificationModel                   : PremiumNotificationModel?{
        didSet{
            self.tableView.reloadData()
        }
    }
    var notificationData = [String:String]()

    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        makeNetworkCall()
        setupTable()
    }

    func makeNetworkCall(){

        mageHelper().callHttpRequest(controller: self, endPoints: "parent/getNotificationSettings", parameters: nil,sendAuthHeader: true) { (data) in
            do {
                let json = try JSON(data: data)
                print(json)
                if json["status"].boolValue == true{
                    //  MARK: - Save latest token get request
                    //
                    let token = json["data"]["access_token"].stringValue
                    AppSetUp().saveToken(token)
                    //  MARK: - Data Parsing
                    //
                    AppSetUp.decoder.keyDecodingStrategy = .convertFromSnakeCase
                    self.premiumNotificationModel = try AppSetUp.decoder.decode(PremiumNotificationModel.self, from: data)

                    if json["data"]["notification_data"].exists(){
                        print("yoyoyoyyoyoyoy")
                        for (_,jsonValue) in json["data"]["notification_data"] {
                            self.notificationData[jsonValue["device_id"].stringValue] = jsonValue["time_before"].stringValue
                        }
                        self.notificationEnabled = "2"
                        self.tableView.reloadSections([2], with: .none)
                    }
                    print("premiumNotificationModel====",self.premiumNotificationModel as Any)
                }else{
                    self.view.makeToast(json["message"].stringValue, duration: 3.0)
                }
            }
            catch let error{
                print(error.localizedDescription)
            }
        }
    }


    func setupTable(){
        tableView.delegate          = self
        tableView.dataSource        = self
        tableView.separatorStyle    = .none
        tableView.backgroundColor   = AppSetUp.controllerBackgroundColor
        tableView.register(HeaderTVCell.self, forCellReuseIdentifier: "HeaderTVCell")
        tableView.register(NavigationCell.self, forCellReuseIdentifier: "NavigationCell")
    }
}

extension SettingsPremiumNotificationController: UITableViewDataSource,UITableViewDelegate{

    func numberOfSections(in tableView: UITableView) -> Int {
        guard let readyToLoad = self.premiumNotificationModel?.status else {return 0}
        return readyToLoad ? 4 : 0
        //return 4
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 2
        case 2:
            return notificationEnabled == "1" ? 0 : 1
        default:
            return 1
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
            //  MARK: - HeaderTVCell
        //
        case 0:
            switch indexPath.row{
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderTVCell", for: indexPath) as! HeaderTVCell
                cell.setupView(header: "Notification",image: "settings",subheader: "")
                cell.parent = self
                return cell
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "SettingPremiumHeaderTVCell", for: indexPath) as! SettingPremiumHeaderTVCell
                return cell
            }
            //  MARK: - PeriodTVCell
        //
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "PeriodDurationTVCell", for: indexPath) as! PeriodDurationTVCell
            cell.periodDurationLabel.text   = "Setting"
            cell.periodDurationValuesSelect.addDropDownImage()
            cell.setupView(period_DurationArray: ["No Notification","Sound Notification"],headerString:"Set a notification per device so kids will be notified on their device the max time is nearly reached", period: notificationEnabled)
            cell.periodDurationValuesSelect.rx.tap.subscribe { (onTap) in
                cell.dropDownPeriod.show()
            }.disposed(by: cell.disposeBag)
            cell.delegate = self as showSavedSettingDelegate
            return cell
            //  MARK: - TimeSavedSettingsTVCell
        //
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SettingPremiumNotiTVCell", for: indexPath) as! SettingPremiumNotiTVCell
            let devices     = self.premiumNotificationModel?.data?.devices
            cell.devices    = devices
            cell.notificationData = self.notificationData
            return cell
            //  MARK: - NavigationCell
        //
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "NavigationCell", for: indexPath) as! NavigationCell
            cell.setupView()
            cell.parent = self
            cell.backButton.rx.tap.subscribe { (onTap) in
                self.navigationController?.popViewController(animated: true)
            }.disposed(by: cell.disposeBag)

            //  MARK: - SAVE TIME MGMT SETTINGS
            //
            cell.nextButton.setTitle("NEXT >>", for: .normal)
            cell.nextButton.rx.tap.subscribe { (onTap) in
                self.saveSettings()
            }.disposed(by: cell.disposeBag)
            return cell
        default:
            return UITableViewCell()
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return indexPath.row == 1 ?  200 : UITableView.automaticDimension
        case 3:
            return AppSetUp.isIpad ? 70 : 60
        case 2:
            return CGFloat((self.premiumNotificationModel?.data?.devices?.count ?? 0) * 100)
        default:
            return UITableView.automaticDimension
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath)
    }

    func saveSettings(){
        switch notificationEnabled{
        case "1":
            let paramsToBeConverted = ["isActive":"0"]
            let postParams = AppSetUp().convertParamsToJSON(params: paramsToBeConverted)
            makeNetworkCallForSavingSettings(postParams)
        default:
            var setNotificationParams = [[String:String]]()
            let cell = tableView.cellForRow(at: [2,0]) as! SettingPremiumNotiTVCell
            guard let parentDeviceCount = self.premiumNotificationModel?.data?.devices?.count else {return}
            for val in 0..<parentDeviceCount{
                var temp            = [String:String]()
                let cvCell          = cell.collectionView.cellForItem(at: [0,val]) as! SettingPremiumNotiCvCell
                temp["device_id"]   = self.premiumNotificationModel?.data?.devices?[val].id
                temp["time"]        = cvCell.dropDownMins.currentTitle
                setNotificationParams.append(temp)
            }
            print(setNotificationParams)

            let paramsToBeConverted = ["isActive":"1","data":setNotificationParams] as [String : Any]
            let postParams = AppSetUp().convertParamsToJSON(params: paramsToBeConverted)
            print("postParams===",postParams)
            makeNetworkCallForSavingSettings(postParams)
        }
    }

    fileprivate func makeNetworkCallForSavingSettings(_ postParams: String) {
            mageHelper().callHttpRequestForTimeMgmt(controller: self, endPoints: "parent/setNotificationSettings",parameters: postParams, sendAuthHeader: true,postData:true,completion: {
                data in
                do {
                    let json = try JSON(data: data)
                    print(json)
                    if json["status"].boolValue == true{
                        let token = json["data"]["access_token"].stringValue
                        AppSetUp().saveToken(token)
                        self.view.makeToast(json["message"].stringValue, duration: 1.0) { (_) in
                            let viewControl:AddSupervisorController = self.storyboard!.instantiateViewController()
                            self.navigationController?.pushViewController(viewControl, animated: true)
                        }
                    }else{
                        self.view.makeToast(json["message"].stringValue, duration: 1.0)
                    }
                }
                catch let error{
                    print(error.localizedDescription)
                }
            })
        }
    }


extension SettingsPremiumNotificationController: showSavedSettingDelegate{

    func showSavedSettingDelegate(_cell: PeriodDurationTVCell, sender: Any, durationTypeSelected: String) {
        print(durationTypeSelected)
        notificationEnabled = durationTypeSelected
        self.tableView.reloadSections([2], with: .automatic)
    }
}
