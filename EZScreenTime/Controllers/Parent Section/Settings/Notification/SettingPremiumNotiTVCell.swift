//
//  SettingPremiumNotiTVCell.swift
//  EZScreenTime
//
//  Created by cedcoss on 26/03/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

class SettingPremiumNotiTVCell: UITableViewCell {

    var devices: [NotiDevice]?{
        didSet{
            self.collectionView.reloadData()
        }
    }

    var notificationData:[String:String]?{
        didSet{
            self.collectionView.reloadData()
        }
    }



    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var containerView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.delegate     = self
        collectionView.dataSource   = self
        containerView.JSCustomView2()
        // Initialization code
    }
}


extension SettingPremiumNotiTVCell:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.devices?.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SettingPremiumNotiCvCell", for: indexPath) as! SettingPremiumNotiCvCell
        cell.setupView()
        cell.parentDevices.text = self.devices?[indexPath.row].name

        let parentId            = self.devices?[indexPath.row].id
        if self.notificationData?[parentId!] != nil{
            cell.dropDownMins.setTitle(self.notificationData?[parentId!], for: .normal)
        }else{
            cell.dropDownMins.setTitle("0", for: .normal)
        }

        cell.dropDownMins.rx.tap.subscribe { (onTap) in
                   cell.dropDownMinsDropdown.show()
               }.disposed(by: cell.disposeBag)
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.frame.width, height: 80)
    }
}
