//
//  PenaltyMessageTVCell.swift
//  EZScreenTime
//
//  Created by cedcoss on 18/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

protocol enableDisablePenaltyMessageDelegate {
    func enableDisablePenaltyMessageDelegate(_cell: PenaltyMessageTVCell,sender: Any, params: [String:String],endPoint: String)
     func deletePenaltyMessageDelegate(_cell: PenaltyMessageTVCell,sender: Any, id: String)
}


class PenaltyMessageTVCell: UITableViewCell {

     private(set) var disposeBag = DisposeBag()
     var delegate                : enableDisablePenaltyMessageDelegate?
    @IBOutlet weak var checkBox: BEMCheckBox!
      @IBOutlet weak var message: UITextField!
      @IBOutlet weak var editMessage: UIButton!
      @IBOutlet weak var addNewMessage: UIButton!
      @IBOutlet weak var deleteMessage: UIButton!
      @IBOutlet weak var stackViewWidth: NSLayoutConstraint!
      @IBOutlet weak var mainView: UIView!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        self.contentView.backgroundColor = AppSetUp.controllerBackgroundColor
        mainView.layer.cornerRadius = 5
        editMessage.JSDropDown()
        message.backgroundColor     = AppSetUp.controllerBackgroundColor
        addNewMessage.JSDropDown()
        checkBox.boxType            = .square
        checkBox.onAnimationType    = .bounce
        checkBox.offAnimationType   = .bounce
        checkBox.delegate           = self as BEMCheckBoxDelegate
        if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular{
            message.font                = UIFont(name: "Poppins-Regular", size: 18)
        }else{
            message.font                = UIFont(name: "Poppins-Regular", size: 14)
        }
    }


    override func prepareForReuse(){
           super.prepareForReuse()
           disposeBag = DisposeBag() // because life cicle of every cell ends on prepare for reuse
       }


    var messages: PenaltyMessage?{
        didSet{
            checkBox.on                             = messages?.selected ?? false
            message.text                            = messages?.message
            if messages?.isCustom == "1"{
                editMessage.isHidden                = false
                deleteMessage.isHidden              = false
                addNewMessage.isHidden              = true
                stackViewWidth.constant             = 85
                message.isUserInteractionEnabled    = true
                checkBox.isHidden                   = false
            }else if messages?.message == nil && messages?.isCustom == nil{
                deleteMessage.isHidden              = false
                addNewMessage.isHidden              = false
                editMessage.isHidden                = true
                stackViewWidth.constant             = 85
                message.isUserInteractionEnabled    = true
                message.placeholder                 = "Add custom message"
                checkBox.isHidden                   = true
            }else{
                checkBox.isHidden                   = false
                message.isUserInteractionEnabled    = false
                editMessage.isHidden                = true
                deleteMessage.isHidden              = true
                addNewMessage.isHidden              = true
                stackViewWidth.constant             = 85
            }
        }
    }

    func setupDelete(){
           self.deleteMessage.rx.tap.subscribe { (onTap) in
               self.delegate?.deletePenaltyMessageDelegate(_cell: self, sender: (Any).self, id: self.messages?.id ?? "")
           }.disposed(by: self.disposeBag)

        self.editMessage.rx.tap.subscribe { (onTap) in
            print("tap*/*/*/*/*/*/*/")
                   guard let id = self.messages?.id , let message = self.message.text else {return}
                   let params = ["id":id,"message": message]
                   self.delegate?.enableDisablePenaltyMessageDelegate(_cell: self, sender: (Any).self, params: params, endPoint: "parent/settings/messages/discipline/update")
               }.disposed(by: self.disposeBag)
            }
        }

extension PenaltyMessageTVCell: BEMCheckBoxDelegate{
    func didTap(_ checkBox: BEMCheckBox) {
        guard let id = messages?.id , let status = (messages?.selected) else {return}
        let statusMod = status ? "0" : "1"
        let params = ["id":id,"status": statusMod]
        self.delegate?.enableDisablePenaltyMessageDelegate(_cell: self, sender: (Any).self, params: params, endPoint: "parent/settings/messages/discipline")
    }
}

