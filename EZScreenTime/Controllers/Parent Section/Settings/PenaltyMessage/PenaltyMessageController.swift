//
//  PenaltyMessageController.swift
//  EZScreenTime
//
//  Created by cedcoss on 18/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

class PenaltyMessageController: BaseViewController {


    var penaltyMessageModel: PenaltyMessageModel?{
        didSet{
            self.tableView.reloadData()
        }
    }

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        makeNetworkCall()
        setupTable()
    }

    

    func setupTable(){
        tableView.delegate =        self
        tableView.dataSource        = self
        tableView.register(HeaderTVCell.self, forCellReuseIdentifier: "HeaderTVCell")
        tableView.register(NavigationCell.self, forCellReuseIdentifier: "NavigationCell")
        tableView.separatorStyle    = .none
        tableView.backgroundColor   = AppSetUp.controllerBackgroundColor

    }

    func makeNetworkCall(){
        mageHelper().callHttpRequest(controller: self, endPoints: "parent/settings/messages/discipline", parameters: nil,sendAuthHeader: true) { (data) in
            do {
                let json = try JSON(data: data)
                print(json)
                if json["status"].boolValue == true{
                    //  MARK: - Save latest token
                    //
                    let token = json["data"]["access_token"].stringValue
                    AppSetUp().saveToken(token)
                    //  MARK: - Populating Model
                    //
                    AppSetUp.decoder.keyDecodingStrategy = .convertFromSnakeCase
                    self.penaltyMessageModel = try? AppSetUp.decoder.decode(PenaltyMessageModel.self, from: data)
                    //  MARK: - Static data for additional sections
                    //
                    self.penaltyMessageModel?.data?.messages?.append(PenaltyMessage(isCustom: nil, message: nil, selected: nil, id: nil))
                    print("penaltyMessageModel====",self.penaltyMessageModel as Any)
                }else{
                    self.view.makeToast(json["message"].stringValue, duration: 3.0,position:.top)
                }
            }
            catch let error{
                print(error.localizedDescription)
            }
        }
    }

    func enableDisablePenaltyMessage(with params: [String: String],endPoint: String){

        mageHelper().callHttpRequest(controller: self, endPoints: endPoint,parameters: params, sendAuthHeader: true,postData: true, completion: {
            data in
            do {
                let json = try JSON(data: data)
                print(json)
                if json["status"].boolValue == true{
                    let token = json["data"]["access_token"].stringValue
                    AppSetUp().saveToken(token)
                    self.view.makeToast(json["message"].stringValue, duration: 3.0,position:.top)
                    self.makeNetworkCall()
                }else{
                    self.view.makeToast(json["message"].stringValue, duration: 3.0,position:.top)
                }
            }
            catch let error{
                print(error.localizedDescription)
            }
        })
    }

    func deletePenaltyMessage(id: String){
        mageHelper().callHttpRequestDelete(controller: self, endPoints: "parent/settings/messages/discipline?id=\(id)", parameters: nil,sendAuthHeader: true) { (data) in
            do {
                let json = try JSON(data: data)
                print(json)

                if json["status"].boolValue == true{
                    //  MARK: - Save latest token
                    //
                    let token = json["data"]["access_token"].stringValue
                    AppSetUp().saveToken(token)
                    self.makeNetworkCall()
                    self.view.makeToast(json["message"].stringValue, duration: 3.0,position:.top)
                }else{
                    self.view.makeToast(json["message"].stringValue, duration: 3.0,position:.top)
                }
            }
            catch let error{
                print(error.localizedDescription)
            }
        }
    }
}

extension PenaltyMessageController: UITableViewDataSource{

    func numberOfSections(in tableView: UITableView) -> Int {
        guard let load = self.penaltyMessageModel?.data?.messages?.isEmpty else {return 0}
        return load ? 0 : 3
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return (self.penaltyMessageModel?.data?.messages?.count ?? 0)
        default:
            return 1
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderTVCell", for: indexPath) as! HeaderTVCell
            cell.setupView(header: "Discipline Messages",image: "settings",subheader: "Select here the message you want to use.\n Below you can add custom message")
            cell.parent = self
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "PenaltyMessageTVCell", for: indexPath) as! PenaltyMessageTVCell
            cell.messages = self.penaltyMessageModel?.data?.messages?[indexPath.row]
            cell.delegate = self as enableDisablePenaltyMessageDelegate
            cell.addNewMessage.rx.tap.subscribe { (onTap) in
                guard let message = cell.message.text else {return}
                let params = ["message":message]
                self.enableDisablePenaltyMessage(with: params, endPoint: "parent/settings/messages/discipline/add")
            }.disposed(by: cell.disposeBag)
            cell.setupDelete()
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "NavigationCell", for: indexPath) as! NavigationCell
            cell.setupView()
            cell.parent = self
            cell.backButton.rx.tap.subscribe { (onTap) in
                self.navigationController?.popViewController(animated: true)
            }.disposed(by: cell.disposeBag)

            cell.nextButton.rx.tap.subscribe { (onTap) in
                let viewControl:TimeMgmtController = self.storyboard!.instantiateViewController()
                self.navigationController?.pushViewController(viewControl, animated: true)
            }.disposed(by: cell.disposeBag)
            return cell
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 2:
            return AppSetUp.isIpad ? 70 : 60
        case 0: return UITableView.automaticDimension
        default:
            return AppSetUp.isIpad ? 70 : UITableView.automaticDimension
        }
    }
}


extension PenaltyMessageController: UITableViewDelegate{}

extension PenaltyMessageController: enableDisablePenaltyMessageDelegate{
    func deletePenaltyMessageDelegate(_cell: PenaltyMessageTVCell, sender: Any, id: String) {
        self.deletePenaltyMessage(id: id)
    }

    func enableDisablePenaltyMessageDelegate(_cell: PenaltyMessageTVCell, sender: Any, params: [String : String],endPoint:String) {
        print(params)
        print("called")
        self.enableDisablePenaltyMessage(with: params, endPoint: endPoint)
    }
}
