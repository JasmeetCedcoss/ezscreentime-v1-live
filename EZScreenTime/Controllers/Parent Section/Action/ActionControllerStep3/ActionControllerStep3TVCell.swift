//
//  ActionControllerStep3TVCell.swift
//  EZScreenTime
//
//  Created by cedcoss on 25/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit
import SearchTextField
import DropDown

protocol actionMessageSelectedDelegate {
    func actionMessageSelectedDelegate(_cell : ActionControllerStep3TVCell, sender: Any, message: String)
}

protocol checkBoxSelectedDelegate {
    func checkBoxSelectedDelegate(_cell : ActionControllerStep3TVCell, sender: Any, checkbox: String)
}

protocol checkBoxForEditableMessageDelegate {
    func checkBoxForEditableMessageDelegate(_cell : ActionControllerStep3TVCell, sender: Any, checkbox: String,editedMessage:String)
}

class ActionControllerStep3TVCell: UITableViewCell, BEMCheckBoxDelegate {

    @IBOutlet weak var kidIConView: UIView!
    @IBOutlet weak var kidName: UILabel!
    @IBOutlet weak var kidIconImage: UIImageView!
    @IBOutlet weak var deviceName: UILabel!
    @IBOutlet weak var coinsDeductLabel: UILabel!
    @IBOutlet weak var sendNotificationToChildCheckbox: BEMCheckBox!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var headerViewContainer: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var kidNameHeaderView: UIView!
    @IBOutlet weak var mySearchField: SearchTextField!
    @IBOutlet weak var masterView   : UIView!


    var delegate                    : actionMessageSelectedDelegate?
    var checkboxDelegate            : checkBoxSelectedDelegate?
    var editMessageDelegate         : checkBoxForEditableMessageDelegate?
    var isFromReward                = false
    private(set) var disposeBag     = DisposeBag()


    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.layer.maskedCorners        = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        sendNotificationToChildCheckbox.delegate = self as BEMCheckBoxDelegate
        mySearchField.theme.bgColor              = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        headerViewContainer.JSCustomView2()

        kidIConView.layer.borderColor            = #colorLiteral(red: 0.007843137255, green: 0.337254902, blue: 0.5098039216, alpha: 1)
        kidIConView.layer.borderWidth            = 5

        if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular{
            kidIConView.layer.cornerRadius       = 120 * 0.5
        }else{
            kidIConView.layer.cornerRadius       = 90 * 0.5
        }
        sendNotificationToChildCheckbox.boxType = .square
        mySearchField.startVisible              = true
        if self.traitCollection.horizontalSizeClass == .regular && self.traitCollection.verticalSizeClass == .regular{
            mySearchField.theme.font            = UIFont.systemFont(ofSize: 22)
        }else{
            mySearchField.theme.font            = UIFont.systemFont(ofSize: 17)
        }
        kidNameHeaderView.layer.maskedCorners   = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }

    func setupView(){
      
        mySearchField.itemSelectionHandler = { filteredResults, itemPosition in
            // Just in case you need the item position
            let item = filteredResults[itemPosition]
            print("Item at position \(itemPosition): \(item.title)")

            // Do whatever you want with the picked item
            self.mySearchField.text = item.title
            self.delegate?.actionMessageSelectedDelegate(_cell: self, sender: (Any).self, message: item.title)
        }
    }


    override func prepareForReuse(){
        super.prepareForReuse()
        disposeBag = DisposeBag() // because life cicle of every cell ends on prepare for reuse
    }

    let dropDownActionMessages = DropDown()

    var selectedKidDataSource: [String: String]?{
        didSet{
            kidName.text                = selectedKidDataSource?["selectedChild"]
            kidIconImage.image          = UIImage(named: selectedKidDataSource?["kidIconSelected"] ?? "")
            deviceName.text             = selectedKidDataSource?["deviceName"]

            if isFromReward{
                coinsDeductLabel.text       = "Added \(selectedKidDataSource?["coinsToReward"] ?? "") coins"
                detailLabel.text            = "Send message to \(selectedKidDataSource?["selectedChild"] ?? "") \n\n Congrats! \n \(selectedKidDataSource?["coinsToReward"] ?? "") coins has been added!"
            }else{
                coinsDeductLabel.text       = "Deducted \(selectedKidDataSource?["coinsToDeduct"] ?? "") coins"
                detailLabel.text            = "Send message to \(selectedKidDataSource?["selectedChild"] ?? "") \n\n \(selectedKidDataSource?["coinsToDeduct"] ?? "") coins consumed for \(selectedKidDataSource?["deviceName"] ?? "")"
            }
        }
    }

    var actionMessagesPassed: [String]?{
        didSet{
            mySearchField.filterStrings(actionMessagesPassed ?? [])
            //mySearchField.text = actionMessagesPassed?.first
            mySearchField.placeholder = "Enter your message here..."
        }
    }
}

extension ActionControllerStep3TVCell{
    func didTap(_ checkBox: BEMCheckBox) {
            let checkBoxStatusBool = checkBox.on
            let checkBoxStatus = checkBoxStatusBool ? "1" : "0"
            print("Send nOTI",checkBoxStatus)
            self.checkboxDelegate?.checkBoxSelectedDelegate(_cell: self, sender: (Any).self, checkbox: checkBoxStatus)
    }
}

