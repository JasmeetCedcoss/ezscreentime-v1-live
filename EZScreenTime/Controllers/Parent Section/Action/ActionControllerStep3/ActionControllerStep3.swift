//
//  ActionControllerStep3.swift
//  EZScreenTime
//
//  Created by cedcoss on 25/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

class ActionControllerStep3: BaseViewController {

    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTable()
    }

    func setupTable(){
        tableView.dataSource        = self
        tableView.delegate          = self
        tableView.backgroundColor   = AppSetUp.controllerBackgroundColor
        tableView.separatorStyle    = .none
        tableView.register(NavigationCell.self, forCellReuseIdentifier: "NavigationCell")
        self.alterColor()
    }

    var selectedKidDataSource      : [String: String]?
    var actionMessagesPassed       : [String: String]?
    var isfromReward               = false

}

extension ActionControllerStep3:UITableViewDelegate,UITableViewDataSource{

    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section{
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ActionHeaderTVCell", for: indexPath) as! ActionHeaderTVCell
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ActionControllerStep3TVCell", for: indexPath) as! ActionControllerStep3TVCell
            cell.isFromReward          = isfromReward
            cell.actionMessagesPassed  = self.actionMessagesPassed?.map({ $0.key })
            cell.selectedKidDataSource = selectedKidDataSource
            cell.delegate              = self as actionMessageSelectedDelegate
            cell.checkboxDelegate      = self as checkBoxSelectedDelegate
            cell.editMessageDelegate   = self as checkBoxForEditableMessageDelegate
            cell.setupView()

            cell.mySearchField.rx.controlEvent(.editingDidEnd)
                .asObservable()
                .subscribe(onNext: { _ in
                    print("editing state changed")
                    self.selectedKidDataSource?["remark"] = cell.mySearchField.text
                }).disposed(by: cell.disposeBag)

            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "NavigationCell", for: indexPath) as! NavigationCell
            cell.setupView()
            cell.parent = self

            cell.backButton.rx.tap.subscribe { (onTap) in
                self.navigationController?.popViewController(animated: true)
            }.disposed(by: cell.disposeBag)

            cell.nextButton.setTitle("SEND", for: .normal)
            cell.nextButton.rx.tap.subscribe { (onTap) in
                self.isfromReward ? self.callSaveActionForReward() : self.callSaveActionForConsumeAndDiscipline()
            }.disposed(by: cell.disposeBag)
            return cell
        default:
            return UITableViewCell()
        }
    }


    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section{
        case 1:
            return AppSetUp.isIpad ? 373 : 330
        case 2:
            return AppSetUp.isIpad ? 70 : 60
        default:
            return UITableView.automaticDimension
        }
    }

    func callSaveActionForConsumeAndDiscipline(){
       
        switch selectedKidDataSource?["generalDeducion"]{
        case "true":

            guard let kid_id = selectedKidDataSource?["kidID"],let type =  selectedKidDataSource?["actionSelected"], let coins = selectedKidDataSource?["coinsToDeduct"], let device = selectedKidDataSource?["deviceId"] else {return}
            let send_msg: String = selectedKidDataSource?["send_msg"] ?? "0"

            //  MARK: - Check for edit message
            //let remark: String = selectedKidDataSource?["remark"] ?? (self.actionMessagesPassed?.keys.first)!
            let remark: String = selectedKidDataSource?["remark"] ?? ""
            if remark.isEmpty{
                self.view.makeToast("Please enter or select message")
                return
            }

            let edit =  actionMessagesPassed?.keys.contains(remark) ?? false ? "0" : "1"

            let params = ["kid_id":kid_id,"type":type,"coins":coins,"device_id":device,"remark":remark,"send_msg":send_msg,"edit":edit,"msg_id":(self.actionMessagesPassed?[remark]) ?? "0"]
            print(params)
            performAction(params)
        case "false":
           // settings/message/scheduledcoinDeduction
            guard let kid_id = selectedKidDataSource?["kidID"],let type =  selectedKidDataSource?["actionSelected"], let coins = selectedKidDataSource?["coinsToDeduct"], let device = selectedKidDataSource?["deviceId"],let date = selectedKidDataSource?["date"] else {return}
            let send_msg: String = selectedKidDataSource?["send_msg"] ?? "0"

            //  MARK: - Check for edit message
            let remark: String = selectedKidDataSource?["remark"] ?? ""
            if remark.isEmpty{
                self.view.makeToast("Please enter or select message")
                return
            }

            let edit =  actionMessagesPassed?.keys.contains(remark) ?? false ? "0" : "1"

            let params = ["kid_id":kid_id,"type":type,"coins":coins,"device_id":device,"remark":remark,"send_msg":send_msg,"edit":edit,"msg_id":(self.actionMessagesPassed?[remark]) ?? "0","date":date,"customChoice":"0"]
            print(params)
            performAction(params,"parent/settings/message/scheduledcoinDeduction")
            default:
                print("")
        }
    }

    func callSaveActionForReward(){
        print("Save reward")
        switch selectedKidDataSource?["customChoice"]{
        case "0": //Parent may decide
            print("")
            guard let kid_id = selectedKidDataSource?["kidID"],let type =  selectedKidDataSource?["actionSelected"], let coins = selectedKidDataSource?["coinsToReward"],let customChoice = selectedKidDataSource?["customChoice"], let device_id = selectedKidDataSource?["deviceId"]else {return}

            let send_msg: String = selectedKidDataSource?["send_msg"] ?? "0"
            //let edit: String = selectedKidDataSource?["edit"] ?? "0"

            //  MARK: - Check for edit message
            let remark: String = selectedKidDataSource?["remark"] ?? ""
            if remark.isEmpty{
                self.view.makeToast("Please enter or select message")
                return
            }

            let edit =  actionMessagesPassed?.keys.contains(remark) ?? false ? "0" : "1"
            let params = ["kid_id":kid_id,"type":type,"coins":coins,"remark":remark,"send_msg":send_msg,"customChoice":customChoice,"device_id":device_id,"edit":edit,"msg_id":(self.actionMessagesPassed?[remark]) ?? "0"]
            performAction(params)
        case "1": // kid may decide
            print("")
            guard let kid_id = selectedKidDataSource?["kidID"],let type =  selectedKidDataSource?["actionSelected"], let coins = selectedKidDataSource?["coinsToReward"],let customChoice = selectedKidDataSource?["customChoice"] else {return}

            let send_msg: String = selectedKidDataSource?["send_msg"] ?? "0"
            //  MARK: - Check for edit message
            let remark: String = selectedKidDataSource?["remark"] ?? ""
            if remark.isEmpty{
                self.view.makeToast("Please enter or select message")
                return
            }
            let edit =  actionMessagesPassed?.keys.contains(remark) ?? false ? "0" : "1"
            let params = ["kid_id":kid_id,"type":type,"coins":coins,"remark":remark,"send_msg":send_msg,"customChoice":customChoice,"edit":edit,"msg_id":(self.actionMessagesPassed?[remark]) ?? "0"]
            performAction(params)
        default:
            print("")
        }
    }

    fileprivate func performAction(_ params: [String : String],_ endPoints: String = "parent/actions") {
        mageHelper().callHttpRequest(controller: self, endPoints: endPoints,parameters: params, sendAuthHeader: true,postData: true, completion: {
            data in
            do {
                let json = try JSON(data: data)
                print(json)
                if json["status"].boolValue == true{
                    let token = json["data"]["access_token"].stringValue
                    AppSetUp().saveToken(token)
                    self.view.makeToast(json["message"].stringValue, duration: 2.0){ (_) in
                        let viewControl:ActionController = self.storyboard!.instantiateViewController()
                        self.navigationController?.setViewControllers([viewControl], animated: true)
                    }
                }else{
                    self.view.makeToast(json["message"].stringValue, duration: 3.0)
                }
            }
            catch let error{
                print(error.localizedDescription)
            }
        })
    }
}

extension ActionControllerStep3: actionMessageSelectedDelegate{
    func actionMessageSelectedDelegate(_cell: ActionControllerStep3TVCell, sender: Any, message: String) {
        print(message)
        selectedKidDataSource?["remark"] = message
    }
}

extension ActionControllerStep3:checkBoxSelectedDelegate,checkBoxForEditableMessageDelegate{
    func checkBoxForEditableMessageDelegate(_cell: ActionControllerStep3TVCell, sender: Any, checkbox: String,editedMessage: String) {
        print(checkbox)
        selectedKidDataSource?["edit"] = checkbox
    }

    func checkBoxSelectedDelegate(_cell: ActionControllerStep3TVCell, sender: Any, checkbox: String) {
        print(checkbox)
        selectedKidDataSource?["send_msg"] = checkbox
    }
}


