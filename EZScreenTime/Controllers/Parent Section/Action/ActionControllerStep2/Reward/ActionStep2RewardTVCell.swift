//
//  ActionStep2RewardTVCell.swift
//  EZScreenTime
//
//  Created by cedcoss on 26/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

protocol deviceSelectedDelegate {
    func deviceSelectedDelegate(_cell:ActionStep2RewardTVCell, sender: Any, deviceId: String,deviceName: String)
}

class ActionStep2RewardTVCell: UITableViewCell {

    let tapGesture              = UITapGestureRecognizer()
    private(set) var disposeBag = DisposeBag()
    var getDeviceIDDelegate     : deviceSelectedDelegate?
    var parent                  = UIViewController()

    @IBOutlet weak var kidDecideView: UIView!
    @IBOutlet weak var deviceAvailaibilityLabel: UILabel!
    @IBOutlet weak var kidCoinTextField: UITextField!
    @IBOutlet weak var childCaseLabel: UILabel!
    @IBOutlet weak var kidMayDecide: UIButton!
    @IBOutlet weak var parentMayDecide: UIButton!
    @IBOutlet weak var kidNameHeaderView: UIView!
    @IBOutlet weak var iconView: UIView!
    @IBOutlet weak var kidIcon: UIImageView!
    @IBOutlet weak var kidName: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var reduceCoin: UIButton!
    @IBOutlet weak var addCoin: UIButton!
    @IBOutlet weak var containerViewForCoin: UIView!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        iconView.layer.borderColor           = #colorLiteral(red: 0.007843137255, green: 0.337254902, blue: 0.5098039216, alpha: 1)
        iconView.layer.borderWidth           = 5
        if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular{
            iconView.layer.cornerRadius          = 120 * 0.5
        }else{
            iconView.layer.cornerRadius          = 90 * 0.5
        }
        collectionView.delegate              = self
        collectionView.dataSource            = self
        collectionView.register(ActionStep2RewardCVCell.self, forCellWithReuseIdentifier: "ActionStep2RewardCVCell")
        reduceCoin.JSDropDown()
        addCoin.JSDropDown()
        containerViewForCoin.JSCustomView2()
        kidNameHeaderView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        kidDecideView.addGestureRecognizer(tapGesture)
    }


    @IBAction func reduceCoin(_ sender: UIButton) {
        guard let qtyText = self.kidCoinTextField.text else {return}
        guard  var qty = Int(qtyText) else {return}
        if qty <= 1 {
            self.parent.view.makeToast("Minimum quantity cannot be less than 1")
            return
        }
        qty -= 1
        self.kidCoinTextField.text = String(qty)
    }

    @IBAction func addCoin(_ sender: UIButton) {
        print("add")
        guard let qtyText = self.kidCoinTextField.text else {return}
        guard var qty = Int(qtyText) else {return}
        qty += 1
        self.kidCoinTextField.text = String(qty)
    }

    @IBAction func kidDecide(_ sender: UIButton) {
        kidsDecide()
    }

    @IBAction func parentDecide(_ sender: UIButton) {
        parentDecide()
    }

    @objc func kidsDecide(){
        self.kidMayDecide.setImage(UIImage(named: "radio-on"), for: .normal)
        self.parentMayDecide.setImage(UIImage(named: "circle"), for: .normal)
    }

    func parentDecide(){
        self.parentMayDecide.setImage(UIImage(named: "radio-on"), for: .normal)
        self.kidMayDecide.setImage(UIImage(named: "circle"), for: .normal)
    }

    override func prepareForReuse(){
        super.prepareForReuse()
        disposeBag = DisposeBag() // because life cicle of every cell ends on prepare for reuse
    }

    var selectedKidDataSource: [String:String]?{
        didSet{
            kidIcon.image   = UIImage(named: selectedKidDataSource?["kidIconSelected"] ?? "")
            kidName.text    = selectedKidDataSource?["selectedChild"]
        }
    }

//    func setupView(){
//        tapGesture.rx.event.bind(onNext: { recognizer in
//            self.kidsDecide()
//        }).disposed(by: self.disposeBag)
//    }

    var dataSource: [ActionKidDevice]?{
        didSet{
            self.collectionView.reloadData()
            if dataSource?.count == 0{
                deviceAvailaibilityLabel.text = "No device assigned to kid."
            }else{
                deviceAvailaibilityLabel.text = ""
            }
        }
    }
}

extension ActionStep2RewardTVCell: UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate{

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource?.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        switch indexPath.section {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ActionStep2RewardCVCell", for: indexPath) as! ActionStep2RewardCVCell
            cell.setupView()
            let feedData    = self.dataSource?[indexPath.item]
            cell.feedData   = feedData
            return cell
        default:
            return UICollectionViewCell()
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/3 - 8, height: 150)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //parentDecide()
        let feedData    = self.dataSource?[indexPath.item]
        guard let deviceId = feedData?.id ,let deviceName = feedData?.name else {return}
        self.getDeviceIDDelegate?.deviceSelectedDelegate(_cell: self, sender: (Any).self, deviceId: deviceId, deviceName: deviceName)
    }
}
