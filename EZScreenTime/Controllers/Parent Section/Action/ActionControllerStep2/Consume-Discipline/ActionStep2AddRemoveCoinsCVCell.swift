//
//  ActionStep2AddRemoveCoinsCVCell.swift
//  EZScreenTime
//
//  Created by cedcoss on 25/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

class ActionStep2AddRemoveCoinsCVCell: UICollectionViewCell {

    var parent  = UIViewController()
    private(set) var disposeBag = DisposeBag()

    @IBOutlet weak var kidCoinTextField: UITextField!
    @IBOutlet weak var reduceCoin: UIButton!
    @IBOutlet weak var addCoin: UIButton!
    @IBOutlet weak var radioSelectDeselect: UIImageView!
    @IBOutlet weak var deviceName: UILabel!
    @IBOutlet weak var backView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        //Initialization code
        backView.JSCustomView2()
        reduceCoin.JSDropDown()
        addCoin.JSDropDown()
        kidCoinTextField.textAlignment = .center
        
        self.reduceCoin.rx.tap.subscribe { (onTap) in
            guard let qtyText = self.kidCoinTextField.text else {return}
            guard  var qty = Int(qtyText) else {return}
            if qty <= 1 {
                self.parent.view.makeToast("Minimum quantity cannot be less than 1")
                return
            }
            qty -= 1
            self.kidCoinTextField.text = String(qty)
        }.disposed(by: self.disposeBag)

        self.addCoin.rx.tap.subscribe { (onTap) in
            print("add")
            guard let qtyText = self.kidCoinTextField.text else {return}
            //var max = String()
//            if  let index = self.feedData?.coinsAvailable?.range(of: ".")?.lowerBound {
//                let substring = self.feedData?.coinsAvailable?[..<index]
//                let string = String(substring ??  String.SubSequence())
//                max = string
//            }
            guard let max = self.feedData?.coinsAvailable else {return}
            guard let maxCoin = Int(max) else {return}
            guard var qty = Int(qtyText) else {return}
            print(qty)
            if qty >= maxCoin {
                self.parent.view.makeToast("You cannot add more coins than kid's allotted coins.")
                return
            }
            qty += 1
            self.kidCoinTextField.text = String(qty)
        }.disposed(by: self.disposeBag)
    }

    override func prepareForReuse(){
        super.prepareForReuse()
        disposeBag = DisposeBag() // because life cicle of every cell ends on prepare for reuse
    }

    var feedData: ActionKidDevice?{
        didSet{
            guard let coins = feedData?.coinsAvailable else {return}
            deviceName.text = (feedData?.name ?? "") + " (\(coins))"
            kidCoinTextField.text = "0"
        }
    }

    override var isSelected: Bool {
        didSet {
            if isSelected { // Selected cell
                radioSelectDeselect.image               = UIImage(named: "radio-on")
            } else { // Normal cell
                radioSelectDeselect.image               = UIImage(named: "circle")
            }
        }
    }
}
