//
//  ActionControllerStep2.swift
//  EZScreenTime
//
//  Created by cedcoss on 24/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

class ActionControllerStep2: BaseViewController {

    var actionStepTwoModel: ActionStepTwoModel?{
        didSet{
            self.tableView.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        makeNetworkCall()
        setupTable()
    }

    func makeActionMessageArray(){
        guard let action = selectedKidDataSource?["actionSelected"] else {return}
        switch action {
        case "consume":
            print("")
            guard let getCount = self.actionStepTwoModel?.data?.actionMessages?.consume?.count else {return}
            for val in 0..<getCount{
                guard let message = self.actionStepTwoModel?.data?.actionMessages?.consume?[val].message else {return}
                guard let id = self.actionStepTwoModel?.data?.actionMessages?.consume?[val].id else {return}
                self.actionMessages[message] = id
            }
            print(actionMessages)
        case "discipline":
            print("")
            guard let getCount = self.actionStepTwoModel?.data?.actionMessages?.discipline?.count  else {return}
            for val in 0..<getCount{
                guard let message = self.actionStepTwoModel?.data?.actionMessages?.discipline?[val].message else {return}
                guard let id = self.actionStepTwoModel?.data?.actionMessages?.consume?[val].id else {return}
                self.actionMessages[message] = id
            }
        case "reward":
            guard let getCount = self.actionStepTwoModel?.data?.actionMessages?.reward?.count else {return}
            for val in 0..<getCount{
                guard let message = self.actionStepTwoModel?.data?.actionMessages?.reward?[val].message else {return}
                guard let id = self.actionStepTwoModel?.data?.actionMessages?.consume?[val].id else {return}
                self.actionMessages[message] = id
            }
        default:
            print("")
        }
    }

    func setupTable(){
        tableView.dataSource        = self
        tableView.delegate          = self
        tableView.backgroundColor   = AppSetUp.controllerBackgroundColor
        tableView.separatorStyle    = .none
        tableView.register(NavigationCell.self, forCellReuseIdentifier: "NavigationCell")
        self.alterColor()
        selectedKidDataSource?["generalDeducion"]   = "true"
    }

    var selectedKidDataSource: [String: String]?
    var actionMessages       = [String:String]()
    var showCalendarView       : Bool = false

    @IBOutlet weak var tableView: UITableView!

    func makeNetworkCall(){
        guard let id = selectedKidDataSource?["kidID"] else {return}
        mageHelper().callHttpRequest(controller: self, endPoints: "parent/actions/kids/devices?id=\(id)", parameters: nil,sendAuthHeader: true) { (data) in
            do {
                let json = try JSON(data: data)
                print(json)
                if json["status"].boolValue == true{
                    //  MARK: - Save latest token
                    //
                    let token = json["data"]["access_token"].stringValue
                    AppSetUp().saveToken(token)

                    //  MARK: - Populating Model
                    //
                    AppSetUp.decoder.keyDecodingStrategy = .convertFromSnakeCase
                    self.actionStepTwoModel = try? AppSetUp.decoder.decode(ActionStepTwoModel.self, from: data)
                    self.makeActionMessageArray()
                    print("actionStepTwoModel====",self.actionStepTwoModel as Any)
                }else{
                    self.view.makeToast(json["message"].stringValue, duration: 3.0)
                }
            }
            catch let error{
                print(error.localizedDescription)
            }
        }
    }
}


extension ActionControllerStep2:UITableViewDelegate,UITableViewDataSource{

    func numberOfSections(in tableView: UITableView) -> Int {
        guard let readyToLoad = self.actionStepTwoModel?.status else {return 0}
        return readyToLoad ? 4 : 0
        //return 3
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section{
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ActionHeaderTVCell", for: indexPath) as! ActionHeaderTVCell
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ActionStep2AddRemoveCoinsTVCell", for: indexPath) as! ActionStep2AddRemoveCoinsTVCell
            let dataSource              = self.actionStepTwoModel?.data?.kidDevices
            cell.dataSource             = dataSource
            cell.selectedKidDataSource  = selectedKidDataSource
            cell.parent                 = self
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "NavigationCell", for: indexPath) as! NavigationCell
            cell.setupView()
            cell.parent = self

            cell.backButton.rx.tap.subscribe { (onTap) in
                self.navigationController?.popViewController(animated: true)
            }.disposed(by: cell.disposeBag)

            cell.nextButton.rx.tap.subscribe { (onTap) in
                self.goToStepThree()
            }.disposed(by: cell.disposeBag)
            return cell

        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CoinSchedularTVCell", for: indexPath) as! CoinSchedularTVCell
            cell.setupUI()
            cell.selectDeductionOption.rx.tap.subscribe { (onTap) in
                cell.dropDownSchedular.show()
            }.disposed(by: cell.disposeBag)
            cell.reloadTable = self.reloadTable
            return cell
        default:
            return UITableViewCell()
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section{
        case 1:
            var staticHeight = 163
            staticHeight = AppSetUp.isIpad ? 195 : 163
            return CGFloat((self.actionStepTwoModel?.data?.kidDevices?.count ?? 0) * 72) + CGFloat(staticHeight)
        case 3:
            return AppSetUp.isIpad ? 70 : 60
        case 2:
            return showCalendarView ? 140 : 90
        case 0:
            return 60
        default:
            return UITableView.automaticDimension
        }
    }


    func goToStepThree(){
        //  MARK: - Retreiving data before going to step 3 through selected collectionview cell
        //
        let cell = tableView.cellForRow(at: [1,0]) as! ActionStep2AddRemoveCoinsTVCell

        let cellCoin = tableView.cellForRow(at: [2,0]) as! CoinSchedularTVCell
        selectedKidDataSource?["date"] = cellCoin.showCalendar.currentTitle

        guard let index = cell.collectionView.indexPathsForSelectedItems?.first else {
            self.view.makeToast("Please select a device to proceed", duration: 1.0)
            return
        }

        let cvCell = cell.collectionView.cellForItem(at: index) as! ActionStep2AddRemoveCoinsCVCell
        guard let coinData = cvCell.kidCoinTextField.text ,let deviceName = cvCell.deviceName.text,let deviceId = cvCell.feedData?.id,let maxCoins  = cvCell.feedData?.coinsAvailable else {return}

        guard let intCoin = Int(coinData),let intMaxCoins =  Int(maxCoins)else {return}
        let coinsToDeduct = coinData
        //  MARK: -  If Validation checks passed go to step 3
        //
        if intCoin > intMaxCoins{
            self.view.makeToast("You cannot add more coins than kid's alloted coins.", duration: 1.0)
            return
        }else{
            selectedKidDataSource?["coinData"]          = coinData
            selectedKidDataSource?["deviceName"]        = deviceName
            selectedKidDataSource?["deviceId"]          = deviceId
            selectedKidDataSource?["coinsToDeduct"]     = String(coinsToDeduct)

            let viewControl:ActionControllerStep3       = self.storyboard!.instantiateViewController()
            viewControl.selectedKidDataSource           = selectedKidDataSource
            viewControl.actionMessagesPassed            = actionMessages
            self.navigationController?.pushViewController(viewControl, animated: true)
        }
    }

    func reloadTable(_ cell:CoinSchedularTVCell,_ val:Bool){
        tableView.beginUpdates()
        selectedKidDataSource?["generalDeducion"]   = String(!val)
        showCalendarView                            = val
        cell.stackView.isHidden                     = !val
        tableView.endUpdates()
        showCalendarView = false
    }
}



