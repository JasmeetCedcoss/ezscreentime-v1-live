//
//  ActionStep2AddRemoveCoinsTVCell.swift
//  EZScreenTime
//
//  Created by cedcoss on 24/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

class ActionStep2AddRemoveCoinsTVCell: UITableViewCell {

    private(set) var disposeBag = DisposeBag()
    override func prepareForReuse(){
        super.prepareForReuse()
        disposeBag = DisposeBag() // because life cicle of every cell ends on prepare for reuse
    }

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var kidNameHeaderView: UIView!
    @IBOutlet weak var iconView: UIView!
    @IBOutlet weak var kidIcon: UIImageView!
    @IBOutlet weak var kidName: UILabel!
    @IBOutlet weak var deviceAvailaibilityLabel: UILabel!

    var parent  = UIViewController()

    var dataSource: [ActionKidDevice]?{
        didSet{
            self.collectionView.reloadData()
            if dataSource?.count == 0{
                deviceAvailaibilityLabel.text = "No device assigned to kid."
            }else{
                deviceAvailaibilityLabel.text = ""
            }
        }
    }

    var selectedKidDataSource: [String:String]?{
        didSet{
            kidIcon.image   = UIImage(named: selectedKidDataSource?["kidIconSelected"] ?? "")
            kidName.text    = selectedKidDataSource?["selectedChild"]
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        iconView.layer.borderColor           = #colorLiteral(red: 0.007843137255, green: 0.337254902, blue: 0.5098039216, alpha: 1)
        iconView.layer.borderWidth           = 5
        if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular{
            iconView.layer.cornerRadius          = 120 * 0.5
        }else{
            iconView.layer.cornerRadius          = 90 * 0.5
        }
        collectionView.delegate              = self
        collectionView.dataSource            = self
        kidNameHeaderView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        collectionView.allowsMultipleSelection = false
    }
}


extension ActionStep2AddRemoveCoinsTVCell: UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate{

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource?.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ActionStep2AddRemoveCoinsCVCell", for: indexPath) as! ActionStep2AddRemoveCoinsCVCell
        let feedData    = self.dataSource?[indexPath.item]
        cell.feedData   = feedData
        cell.parent     = parent


        cell.addCoin.rx.tap.subscribe { (onTap) in
            collectionView.selectItem(at: indexPath, animated: false, scrollPosition: .bottom)
        }.disposed(by: cell.disposeBag)

        cell.reduceCoin.rx.tap.subscribe { (onTap) in
            collectionView.selectItem(at: indexPath, animated: false, scrollPosition: .bottom)
        }.disposed(by: cell.disposeBag)
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 65)
    }
}
