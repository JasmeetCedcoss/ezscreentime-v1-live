//
//  CoinSchedularTVCell.swift
//  EZScreenTime
//
//  Created by cedcoss on 21/04/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit
import DropDown
import FSCalendar

class CoinSchedularTVCell: UITableViewCell {

    var calendarView = CalendarPopup()

    let dropDownSchedular               = DropDown()
    let parent                          = UIViewController()
    private(set) var disposeBag         = DisposeBag()
    var reloadTable                     : ((CoinSchedularTVCell,Bool) -> Void)?

    @IBOutlet weak var selectDeductionOption: UIButton!
    @IBOutlet weak var showCalendar: UIButton!
    @IBOutlet weak var stackView: UIStackView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag() // because life cicle of every cell ends on prepare for reuse
    }


    func setupUI(){
        stackView.isHidden = true
        selectDeductionOption.addDropDownImage()
        dropDownSchedular.anchorView      = selectDeductionOption
        dropDownSchedular.dataSource      = AppSetUp.coinsDeductionChoice
        dropDownSchedular.direction       = .any

        self.selectDeductionOption.setTitle(AppSetUp.coinsDeductionChoice.first, for: .normal)
        dropDownSchedular.selectionAction = { [unowned self] (index: Int, item: String) in
            self.selectDeductionOption.setTitle(item, for: .normal)
            let val = item == AppSetUp.coinsDeductionChoice.first ? false: true
            self.reloadTable?(self, val)
        }

        showCalendar.rx.tap.subscribe { (onTap) in
            self.calendarView.showCalendarPopup()
            self.calendarView.calendarView.delegate = self
        }.disposed(by: disposeBag)
    }
}


extension CoinSchedularTVCell: FSCalendarDelegate{
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        self.calendarView.handleDismiss()
        let customDate = AppSetUp.shared.getDateInSpecificFormat("dd-MM-yyyy", date)
        showCalendar.setTitle(customDate, for: .normal)
        
    }
}
