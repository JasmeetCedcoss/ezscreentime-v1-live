//
//  ActionSelectedForKidTVCell.swift
//  EZScreenTime
//
//  Created by cedcoss on 22/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit
import DLRadioButton

protocol kidsCoinActionDelegate {
    func kidsCoinActionDelegate(_cell:ActionSelectedForKidTVCell ,sender:Any,action:String?)
}

class ActionSelectedForKidTVCell: UITableViewCell {

    @IBOutlet weak var consumeButton: DLRadioButton!
    @IBOutlet weak var rewardButton: DLRadioButton!
    @IBOutlet weak var disciplineButton: DLRadioButton!
    @IBOutlet weak var childSelected: UILabel!
    @IBOutlet weak var mainStack: UIStackView!
    @IBOutlet var backView: [UIView]!
    @IBOutlet weak var containerView: UIView!
    var delegate: kidsCoinActionDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        containerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        backView.forEach { (view) in
            view.JSCustomView2()
        }
        consumeButton.selected()
    }

    @IBAction func kidsCoinAction(_ sender: DLRadioButton) {

        self.delegate?.kidsCoinActionDelegate(_cell: self, sender: (Any).self, action: sender.currentTitle ?? "consume")
    }
}
