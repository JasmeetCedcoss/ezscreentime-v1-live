//
//  ActionKidListingCVCell.swift
//  EZScreenTime
//
//  Created by cedcoss on 22/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

class ActionKidListingCVCell: UICollectionViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var kidIcon: UIImageView!
    @IBOutlet weak var kidName: UILabel!
    @IBOutlet weak var borderView: UIView!

    let tapGesture              = UITapGestureRecognizer()
    private(set) var disposeBag = DisposeBag()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        borderView.layer.borderColor           = #colorLiteral(red: 0.007843137255, green: 0.337254902, blue: 0.5098039216, alpha: 1)
        borderView.layer.borderWidth           = 5
        if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular{
            borderView.layer.cornerRadius          = 120 * 0.5
        }else{
            borderView.layer.cornerRadius          = 100 * 0.5
        }
        kidIcon.addGestureRecognizer(tapGesture)
        containerView.alpha = 0.5
    }

    override func prepareForReuse(){
        super.prepareForReuse()

        disposeBag = DisposeBag() // because life cicle of every cell ends on prepare for reuse
    }

    override var isSelected: Bool {
        didSet {
            if isSelected { // Selected cell
                containerView.alpha = 1.0
            } else { // Normal cell
                containerView.alpha = 0.5
           }
        }
    }


    var dataSourceRceived: KidForAction?{
        didSet{
            kidIcon.image = UIImage(named: dataSourceRceived?.icon ?? "")
            kidName.text  = dataSourceRceived?.name
        }
    }
}
