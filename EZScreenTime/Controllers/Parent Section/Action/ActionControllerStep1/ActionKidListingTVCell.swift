//
//  ActionKidListingTVCell.swift
//  EZScreenTime
//
//  Created by cedcoss on 22/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

protocol selecetedChildDelegate {
    func selecetedChildDelegate(_cell: ActionKidListingTVCell,sender: Any,kidId: String,kidName: String,kidIcon: String)
}

class ActionKidListingTVCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    private(set) var disposeBag = DisposeBag()
    var delegate        : selecetedChildDelegate?
    var kidsDataSource  : [KidForAction]?{
        didSet{
            self.collectionView.reloadData()
        }
    }

    override func prepareForReuse(){
        super.prepareForReuse()
        disposeBag = DisposeBag() // because life cicle of every cell ends on prepare for reuse
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionView.delegate     = self
        collectionView.dataSource   = self
    }
}

extension ActionKidListingTVCell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.kidsDataSource?.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ActionKidListingCVCell", for: indexPath) as! ActionKidListingCVCell
        let dataSource          = self.kidsDataSource?[indexPath.row]
        cell.dataSourceRceived  = dataSource
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular{
            return CGSize(width: 150, height: 200)
        }else{
            return CGSize(width: 100, height: 150)
        }
    }

   

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let dataSource = self.kidsDataSource?[indexPath.row]
        guard let kidID = dataSource?.id , let kidName = dataSource?.name , let kidIcon = dataSource?.icon else {return}
        self.delegate?.selecetedChildDelegate(_cell: self, sender: (Any).self, kidId: kidID, kidName: kidName,kidIcon: kidIcon)
    }
}
