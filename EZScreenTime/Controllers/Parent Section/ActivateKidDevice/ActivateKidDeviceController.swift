//
//  ActivateKidDeviceController.swift
//  EZScreenTime
//
//  Created by cedcoss on 27/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

class ActivateKidDeviceController: BaseViewController {

    var activateKidDeviceModel: ActivateKidDeviceModel?{
        didSet{
            self.tableView.reloadData()
        }
    }

    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        makeNetworkCall()
        setupTable()
    }

    func makeNetworkCall(){
        mageHelper().callHttpRequest(controller: self, endPoints: "parent/settings/kids/session", parameters: nil,sendAuthHeader: true) { (data) in
            do {
                let json = try JSON(data: data)
                print(json)
                if json["status"].boolValue == true{
                    //  MARK: - Save latest token
                    //
                    let token = json["data"]["access_token"].stringValue
                    AppSetUp().saveToken(token)
                    AppSetUp.decoder.keyDecodingStrategy = .convertFromSnakeCase
                    self.activateKidDeviceModel = try? AppSetUp.decoder.decode(ActivateKidDeviceModel.self, from: data)
                    print("activateKidDeviceModel====",self.activateKidDeviceModel as Any)
                }else{
                    self.view.makeToast(json["message"].stringValue, duration: 3.0)
                }
            }
            catch let error{
                print(error.localizedDescription)
            }
        }
    }

    func setupTable(){
           tableView.dataSource        = self
           tableView.delegate          = self
           tableView.backgroundColor   = AppSetUp.controllerBackgroundColor
           tableView.separatorStyle    = .none
           tableView.register(HeaderTVCell.self, forCellReuseIdentifier: "HeaderTVCell")
           self.alterColor()
       }


    func makeCodeRequest(kidId:String,indexPath: IndexPath){
        let params = ["kidId": kidId]
        mageHelper().callHttpRequest(controller: self, endPoints: "parent/settings/kids/session/generate",parameters: params, sendAuthHeader: true,postData: true, completion: {
            data in
            do {
                let json = try JSON(data: data)
                print(json)
                if json["status"].boolValue == true{
                    let token = json["data"]["access_token"].stringValue
                    AppSetUp().saveToken(token)
                    //  MARK: - Session code received and updating label
                    //
                    let sessionCode = json["data"]["sessionCode"].stringValue
                    let cell = self.tableView.cellForRow(at: indexPath) as! ActivateKidDeviceTVCell
                    cell.codeLabel.text = sessionCode
                    self.view.makeToast(json["message"].stringValue, duration: 3.0)
                }else{
                    self.view.makeToast(json["message"].stringValue, duration: 3.0)
                }
            }
            catch let error{
                print(error.localizedDescription)
            }
        })
    }


    
}

extension ActivateKidDeviceController: UITableViewDataSource,UITableViewDelegate{

    func numberOfSections(in tableView: UITableView) -> Int {
        guard let readyToLoad = self.activateKidDeviceModel?.status else {return 0}
        return readyToLoad ? 2 : 0
        //return 2
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section{
        case 0:
            return 1
        case 1:
            return self.activateKidDeviceModel?.data?.kidsSessions?.count ?? 0
        default:
            return 0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section{
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderTVCell", for: indexPath) as! HeaderTVCell
            cell.setupView(header: "Activate Kids Device",image: "settings",subheader: "Go to the device of your kid, install EZScreen Timer and put the code to connect the device.")
            cell.backIcon.isHidden = true
            cell.backgroundColor = AppSetUp.controllerBackgroundColor
            return cell

        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ActivateKidDeviceTVCell", for: indexPath) as! ActivateKidDeviceTVCell
            let kid     = self.activateKidDeviceModel?.data?.kidsSessions?[indexPath.row]
            cell.kid    = kid
            cell.parent = self
            cell.generateCodeButton.rx.tap.subscribe { (onTap) in
                guard let kidId = kid?.id else {return}
                print("Genearte Code For id ==",kidId)
                self.makeCodeRequest(kidId: kidId, indexPath: indexPath)
            }.disposed(by: cell.disposeBag)

            cell.tapgesture.rx.event.bind(onNext: { recognizer in
                print("touches: \(recognizer.numberOfTouches)") //or whatever you like
                cell.dropDownCopy.show()
            }).disposed(by: cell.disposeBag)
            return cell
        default:
            return UITableViewCell()
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section{
        case 1:
            if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular{
                return CGFloat(320)
            }else{
                return CGFloat(265)
            }

        default:
            return UITableView.automaticDimension
        }
    }
}
