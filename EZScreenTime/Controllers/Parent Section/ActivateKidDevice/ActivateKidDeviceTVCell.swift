//
//  ActivateKidDeviceTVCell.swift
//  EZScreenTime
//
//  Created by cedcoss on 27/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit
import DropDown

class ActivateKidDeviceTVCell: UITableViewCell {

    @IBOutlet weak var kidicon: UIImageView!

    @IBOutlet weak var kidIconContainer: UIView!
    @IBOutlet weak var generateCodeButton: UIButton!
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var isActivatedCheckMark: UIImageView!
    @IBOutlet weak var kidName: UILabel!
    @IBOutlet weak var containerView: UIView!

    let tapgesture                      = UITapGestureRecognizer()
    var parent                          = UIViewController()
    let dropDownCopy                    = DropDown()
    private(set) var disposeBag         = DisposeBag()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        designSetup()

        self.dropDownCopy.width = 150
        self.dropDownCopy.anchorView      = codeLabel
        self.dropDownCopy.dataSource      = ["Copy"]
        self.dropDownCopy.direction       = .any
        self.dropDownCopy.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            let pasteboard = UIPasteboard.general
            pasteboard.string = self.codeLabel.text
        }
    }

    func designSetup(){
        kidIconContainer.layer.borderColor                       = #colorLiteral(red: 0.007843137255, green: 0.337254902, blue: 0.5098039216, alpha: 1)
        kidIconContainer.layer.borderWidth                       = 5
        if AppSetUp.isIpad{
            kidIconContainer.layer.cornerRadius                  = 120 * 0.5
        }else{
            kidIconContainer.layer.cornerRadius                  = 90 * 0.5
        }
        generateCodeButton.backgroundColor              = AppSetUp.colorAccent
        generateCodeButton.layer.borderColor            = #colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1)
        generateCodeButton.layer.borderWidth            = 1
        generateCodeButton.layer.cornerRadius           = 5
        codeLabel.JSCustomView2()
        codeLabel.isUserInteractionEnabled              = true
        codeLabel.addGestureRecognizer(tapgesture)
        containerView.layer.maskedCorners               = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag() // because life cicle of every cell ends on prepare for reuse
    }

    let longPressRecognizer = UILongPressGestureRecognizer()

    var kid: KidsSession?{
        didSet{
            kidicon.image = UIImage(named: kid?.icon ?? "placeholder")
            kidName.text = kid?.name
            if ((kid?.isNew!)!){
                codeLabel.text = "**NEW CODE**"
                generateCodeButton.setTitle("   GENERATE   ", for: .normal)
                isActivatedCheckMark.isHidden   = true
            }else{
                if kid?.isActivated == "0"{
                    isActivatedCheckMark.isHidden   = true
                    codeLabel.text                  = "**NEW CODE**"
                    generateCodeButton.setTitle("   RESET   ", for: .normal)
                }else if kid?.isActivated == "1"{
                    isActivatedCheckMark.isHidden   = false
                    codeLabel.text                  = "**ACTIVATED**"
                    generateCodeButton.setTitle("   RESET   ", for: .normal)
                }else{
                    isActivatedCheckMark.isHidden   = true
                    codeLabel.text                  = "**NEW CODE**"
                    generateCodeButton.setTitle("   GENERATE   ", for: .normal)
                }
            }
        }
    }
}

