//
//  SupervisorDashboardController.swift
//  EZScreenTime
//
//  Created by cedcoss on 24/03/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

class SupervisorDashboardController: BaseViewControllerSupervisor {
    var overViewSelected        :  String? = "today"
    var resfreshControl         =  UIRefreshControl()
    var supervisorDashboard: ParentDashboardModel?{
        didSet{
            resfreshControl.endRefreshing()
            self.tableView.reloadData()
        }
    }

    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTable()
        makeNetworkCall()
        hideAd()
        NotificationCenter.default.addObserver(self, selector: #selector(performParentDashboardUpdate(notification: )), name: .performParentDashboardUpdate, object: nil)
    }
    @objc func performParentDashboardUpdate(notification:Notification){
         makeNetworkCall()
    }

    func setupTable(){
        tableView.delegate          = self
        tableView.dataSource        = self
        tableView.register(HeaderTVCell.self, forCellReuseIdentifier: "HeaderTVCell")
        tableView.separatorStyle    = .none
        tableView.backgroundColor   = AppSetUp.controllerBackgroundColor
        resfreshControl.addTarget(self, action: #selector(pullToRefresh(sender:)), for: .valueChanged)
        resfreshControl.attributedTitle = NSAttributedString(string: "Loading...")
        tableView.refreshControl = resfreshControl
    }

    @objc func pullToRefresh(sender:UIRefreshControl){
        if overViewSelected == "today"{
            makeNetworkCall(endPoint: "supervisor/dashboard?select_type=day")
        }else{
            makeNetworkCall(endPoint: "supervisor/dashboard?select_type=week")
        }
    }

    func makeNetworkCall(endPoint:String = "supervisor/dashboard?select_type=day"){
        mageHelper().callHttpRequestSupervisor(controller: self, endPoints: endPoint, parameters: nil,sendAuthHeader: true) { (data) in
            do {
                let json = try JSON(data: data)
                print(json)
                if json["status"].boolValue == true{
                    AppSetUp.decoder.keyDecodingStrategy = .convertFromSnakeCase
                    self.supervisorDashboard = try? AppSetUp.decoder.decode(ParentDashboardModel.self, from: data)
                    print("supervisorDashboard====",self.supervisorDashboard as Any)
                }else{
                    self.view.makeToast(json["message"].stringValue, duration: 3.0)
                }
            }
            catch let error{
                print(error.localizedDescription)
            }
        }
    }
}


extension SupervisorDashboardController: UITableViewDataSource,UITableViewDelegate{

    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0,1:
            return 1
        case 2:
            return self.supervisorDashboard?.data?.kids?.count ?? 0
        default:
            return 0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderTVCell", for: indexPath) as! HeaderTVCell
            cell.setupView(header: "Coins Overview",image: "settings",subheader: "")
            cell.backIcon.isHidden = true
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "OverviewSwitcherTVCell", for: indexPath) as! OverviewSwitcherTVCell
            cell.backgroundColor = AppSetUp.controllerBackgroundColor
            cell.todayOverview.rx.tap.subscribe { (onTap) in
                print("Today")
                self.overViewSelected = "today"
                self.makeNetworkCall(endPoint: "supervisor/dashboard?select_type=day")
            }.disposed(by: cell.disposeBag)

            cell.weekOverview.rx.tap.subscribe { (onTap) in
                print("Week")
                self.overViewSelected = "week"
                self.makeNetworkCall(endPoint: "supervisor/dashboard?select_type=week")
            }.disposed(by: cell.disposeBag)
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ParentDashboardTVCell", for: indexPath) as! ParentDashboardTVCell
            if self.supervisorDashboard?.data?.kids?[indexPath.row].devices?.count == 0{
                cell.deviceAvailaibilityMessage.text = "No device assigned to kid"
            }else{
                cell.deviceAvailaibilityMessage.text = ""
            }
            let kidDevices      = self.supervisorDashboard?.data?.kids?[indexPath.row]
            cell.kidDevices     = kidDevices
            return cell
        default:
            return UITableViewCell()
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 1:
            return 50
        case 2:
            guard let devicesount = self.supervisorDashboard?.data?.kids?[indexPath.row].devices?.count else {return CGFloat()}
            if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular{
                return CGFloat(((devicesount) * 83) + 180)
            }else{
                return CGFloat(((devicesount) * 73) + 130)
            }
        default:
            return UITableView.automaticDimension
        }
    }
}

