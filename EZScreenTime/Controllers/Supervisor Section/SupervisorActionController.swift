//
//  SupervisorActionController.swift
//  EZScreenTime
//
//  Created by cedcoss on 24/03/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

class SupervisorActionController: BaseViewControllerSupervisor {

    var actionModel: AcctionModel?{
        didSet{
            self.tableView.reloadData()
        }
    }

    @IBOutlet weak var tableView: UITableView!

    var actionSelected      :String? = "consume"
    var kidIDSelected       :String?
    var kidIconSelected     :String?

    var selectedChild       = String(){
        didSet{
            tableView.reloadSections([2], with: .none)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTable()
        makeNetworkCall()
        hideAd()

    }

    func setupTable(){
        tableView.dataSource        = self
        tableView.delegate          = self
        tableView.backgroundColor   = AppSetUp.controllerBackgroundColor
        tableView.separatorStyle    = .none
        tableView.register(NavigationCell.self, forCellReuseIdentifier: "NavigationCell")
        alterColor()
    }

    func makeNetworkCall(){
        mageHelper().callHttpRequestSupervisor(controller: self, endPoints: "supervisor/actions", parameters: nil,sendAuthHeader: true) { (data) in
            do {
                let json = try JSON(data: data)
                print(json)
                if json["status"].boolValue == true{
                    //  MARK: - Save latest token
                    //
                    let decoder = JSONDecoder()
                    decoder.keyDecodingStrategy = .convertFromSnakeCase
                    self.actionModel = try decoder.decode(AcctionModel.self, from: data)
                    print("actionModel====",self.actionModel as Any)
                    print("====",self.actionModel as Any)
                }else{
                    self.view.makeToast(json["message"].stringValue, duration: 3.0)
                }
            }
            catch let error{
                print(error.localizedDescription)
            }
        }
    }
}

extension SupervisorActionController: UITableViewDataSource,UITableViewDelegate{

    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section{
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ActionHeaderTVCell", for: indexPath) as! ActionHeaderTVCell
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ActionKidListingTVCell", for: indexPath) as! ActionKidListingTVCell
            cell.kidsDataSource     = self.actionModel?.data?.kids
            cell.delegate           = self as selecetedChildDelegate
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ActionSelectedForKidTVCell", for: indexPath) as! ActionSelectedForKidTVCell
            cell.childSelected.text = self.selectedChild.isEmpty ? self.actionModel?.data?.kids?.first?.name : selectedChild
            cell.delegate           = self as kidsCoinActionDelegate
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "NavigationCell", for: indexPath) as! NavigationCell
            cell.setupView()
            cell.parent = self
            cell.backButton.isHidden = true
            cell.stackView.widthAnchor.constraint(equalToConstant: 110).isActive = true
            cell.nextButton.rx.tap.subscribe { (onTap) in

                guard let kidID = self.kidIDSelected else {
                    self.view.makeToast("Please select kid")
                    return
                }
                var selectedKidDataSource                   = [String: String]()
                selectedKidDataSource["kidID"]              = kidID
                selectedKidDataSource["selectedChild"]      = self.selectedChild
                selectedKidDataSource["kidIconSelected"]    = self.kidIconSelected
                selectedKidDataSource["actionSelected"]     = self.actionSelected

                if self.actionSelected == "reward"{
                    let viewControl = SupervisorSetupSettings.storyBoard.instantiateViewController(withIdentifier: "SupervisorActionStep2Reward") as! SupervisorActionStep2Reward
                    viewControl.selectedKidDataSource           = selectedKidDataSource
                    self.navigationController?.pushViewController(viewControl, animated: true)
                }else{
                    let viewControl = SupervisorSetupSettings.storyBoard.instantiateViewController(withIdentifier: "SupervisorActionControllerStep2") as! SupervisorActionControllerStep2
                    viewControl.selectedKidDataSource           = selectedKidDataSource
                    self.navigationController?.pushViewController(viewControl, animated: true)
                }
            }.disposed(by: cell.disposeBag)
            return cell
        default:
            return UITableViewCell()
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section{
        case 1:
            return AppSetUp.isIpad ? 210 : 165
        case 2:
            //return AppSetUp.isIpad ? 306 : 250
            return AppSetUp.isIpad ? 262 : 206
        case 3:
            return AppSetUp.isIpad ? 70 : 60
        default:
            return 65
        }
    }
}

extension SupervisorActionController :selecetedChildDelegate,kidsCoinActionDelegate{
    func kidsCoinActionDelegate(_cell: ActionSelectedForKidTVCell, sender: Any, action: String?) {
        actionSelected = action?.lowercased()
    }

    func selecetedChildDelegate(_cell: ActionKidListingTVCell, sender: Any, kidId: String, kidName: String,kidIcon: String) {
        selectedChild           = kidName
        self.kidIDSelected      = kidId
        self.kidIconSelected    = kidIcon
    }
}
