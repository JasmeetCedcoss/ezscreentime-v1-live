//
//  SupervisorActionStep2Reward.swift
//  EZScreenTime
//
//  Created by cedcoss on 24/03/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit

class SupervisorActionStep2Reward: BaseViewControllerSupervisor {

    var actionStepTwoModel: ActionStepTwoModel?{
        didSet{
            self.tableView.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        makeNetworkCall()
        setupTable()
        hideAd()
    }

    func makeActionMessageArray(){
        guard let action = selectedKidDataSource?["actionSelected"] else {return}
        switch action {

          case "reward":
                    guard let getCount = self.actionStepTwoModel?.data?.actionMessages?.reward?.count else {return}
                    for val in 0..<getCount{
                        guard let message = self.actionStepTwoModel?.data?.actionMessages?.reward?[val].message else {return}
                        guard let id = self.actionStepTwoModel?.data?.actionMessages?.reward?[val].id else {return}
                        self.actionMessages[message] = id
                    }
        default:
            print("")
        }












    }

    func setupTable(){
        tableView.dataSource        = self
        tableView.delegate          = self
        tableView.backgroundColor   = AppSetUp.controllerBackgroundColor
        tableView.separatorStyle    = .none
        tableView.register(NavigationCell.self, forCellReuseIdentifier: "NavigationCell")
        self.alterColor()
    }

    var selectedKidDataSource: [String: String]?
    var actionMessages       = [String: String]()


    @IBOutlet weak var tableView: UITableView!

    func makeNetworkCall(){
        guard let id = selectedKidDataSource?["kidID"] else {return}
        mageHelper().callHttpRequestSupervisor(controller: self, endPoints: "supervisor/actions/kids/devices?id=\(id)", parameters: nil,sendAuthHeader: true) { (data) in
            do {
                let json = try JSON(data: data)
                print(json)
                if json["status"].boolValue == true{

                    //  MARK: - Populating Model
                    //
                    AppSetUp.decoder.keyDecodingStrategy = .convertFromSnakeCase
                    self.actionStepTwoModel = try? AppSetUp.decoder.decode(ActionStepTwoModel.self, from: data)
                    self.makeActionMessageArray()
                    print("actionStepTwoModel====",self.actionStepTwoModel as Any)
                }else{
                    self.view.makeToast(json["message"].stringValue, duration: 3.0)
                }
            }
            catch let error{
                print(error.localizedDescription)
            }
        }
    }
}


extension SupervisorActionStep2Reward:UITableViewDelegate,UITableViewDataSource{

    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section{
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ActionHeaderTVCell", for: indexPath) as! ActionHeaderTVCell
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ActionStep2RewardTVCell", for: indexPath) as! ActionStep2RewardTVCell
            let dataSource              = self.actionStepTwoModel?.data?.kidDevices
            cell.dataSource             = dataSource
            cell.selectedKidDataSource  = selectedKidDataSource
            cell.parent                 = self
            guard let childName         = selectedKidDataSource?["selectedChild"] else {return UITableViewCell()}
            cell.childCaseLabel.text    = "\(childName) may choose how to spend"

            cell.kidMayDecide.rx.tap.subscribe { (onTap) in
                print("kiddddddd")
                self.selectedKidDataSource?["customChoice"] = "1"
            }.disposed(by: cell.disposeBag)

            cell.parentMayDecide.rx.tap.subscribe { (ontap) in
                print("parent")
                self.selectedKidDataSource?["customChoice"] = "0"
            }.disposed(by: cell.disposeBag)
            cell.getDeviceIDDelegate = self as deviceSelectedDelegate
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "NavigationCell", for: indexPath) as! NavigationCell
            cell.setupView()
            cell.parent = self

            cell.backButton.rx.tap.subscribe { (onTap) in
                self.navigationController?.popViewController(animated: true)
            }.disposed(by: cell.disposeBag)

            cell.nextButton.rx.tap.subscribe { (onTap) in
                self.goToStepThree()
            }.disposed(by: cell.disposeBag)
            return cell
        default:
            return UITableViewCell()
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section{
        case 1:
            var staticHeight = AppSetUp.isIpad ? 300 : 250
            return CGFloat((((self.actionStepTwoModel?.data?.kidDevices?.count ?? 0)+2)/3) * 150) + CGFloat(staticHeight)
        case 2:
            return AppSetUp.isIpad ? 70 : 60
        default:
            return UITableView.automaticDimension
        }
    }
    
    func goToStepThree(){
        //  MARK: - Retreiving data before going to step 3 through selected collectionview cell
        //
        let cell = tableView.cellForRow(at: [1,0]) as! ActionStep2RewardTVCell
        guard let coinData = cell.kidCoinTextField.text else {return}
        selectedKidDataSource?["coinsToReward"]     = coinData
        //  MARK: - Validation checks
        //
        switch selectedKidDataSource?["customChoice"]{
        case "0":
            print("selectedKidDataSource===CaseParent====",selectedKidDataSource as Any)
            if selectedKidDataSource?["deviceId"] == nil{
                self.view.makeToast("Please select device to proceed")
                return
            }
        case "1":
            selectedKidDataSource?.removeValue(forKey: "deviceId")
            selectedKidDataSource?.removeValue(forKey: "deviceName")
            print("selectedKidDataSource===CaseKid=====",selectedKidDataSource as Any)
        default:
            print("")
            if selectedKidDataSource?["deviceId"] == nil{
                self.view.makeToast("Please select option to proceed")
                return
            }
        }

        let viewControl = SupervisorSetupSettings.storyBoard.instantiateViewController(withIdentifier: "SupervisorActionControllerStep3") as! SupervisorActionControllerStep3
        viewControl.selectedKidDataSource           = selectedKidDataSource
        viewControl.actionMessagesPassed            = actionMessages
        viewControl.isfromReward                    = true
        self.navigationController?.pushViewController(viewControl, animated: true)
    }
}

extension SupervisorActionStep2Reward:deviceSelectedDelegate{
    func deviceSelectedDelegate(_cell: ActionStep2RewardTVCell, sender: Any, deviceId: String, deviceName: String) {
        selectedKidDataSource?["deviceId"]       = deviceId
        selectedKidDataSource?["deviceName"]     = deviceName
    }
}
