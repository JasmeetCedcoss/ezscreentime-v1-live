//
//  mageUsefulSingleton.swift
//  EZScreenTime
//
//  Created by cedcoss on 18/03/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import Foundation

class NotificationKidUIUpdater {
    private let _labelUpdate = PublishSubject<NotificationModel?>()
    let onUpdateLabel: Observable<NotificationModel?>?

    // call this method whenever you need to change text
    func triggerLabelUpdate(newValue: NotificationModel?) {
        _labelUpdate.onNext(newValue)
    }

    init() {
        onUpdateLabel = _labelUpdate.share(replay: 1)
    }
    static let sharedInstance = NotificationKidUIUpdater()
}
