//
//  mageSupervisor.swift
//  EZScreenTime
//
//  Created by cedcoss on 24/03/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import Foundation

class SupervisorSetupSettings{

    static let shared = SupervisorSetupSettings()
    static let storyBoard: UIStoryboard     = UIStoryboard(name: "SupervisorStory", bundle: nil)
    static var getsupervisorToken:            String?{
    return AppSetUp.defaults.value(forKey: "supervisorToken") as? String
    }

    //  MARK: - CheckForLogin
    //
    func isSupervisorLogin()->Bool{
        return AppSetUp.defaults.bool(forKey: "isSupervisorLoggedIn")
    }

    // MARK: - DoLogout
    //
    func doLogOutSupervisor()->Bool{
        AppSetUp.defaults.set(false, forKey: "isSupervisorLoggedIn")
        AppSetUp.defaults.removeObject(forKey: "supervisorToken")
        return true
    }
}
