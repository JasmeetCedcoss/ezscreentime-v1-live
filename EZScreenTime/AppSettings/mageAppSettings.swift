//
//  mageAppSettings.swift
//  EZScreenTime
//
//  Created by cedcoss on 04/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import Foundation
import DropDown
import GoogleSignIn
import FBSDKLoginKit

final class AppSetUp {

    //  MARK: - Major app functionality

    static let baseUrl            = "https://ezscreentimer.com/api/"
    //static let baseUrl          = "http://185.59.17.33/~ezscreentimer/api/"
    static var getFCMToken: String?{
        guard let myfcmToken    = Messaging.messaging().fcmToken else {return nil}
        return myfcmToken
    }

    //  MARK: - CheckForLogin
    //
    func isParentLogin()->Bool{
        return AppSetUp.defaults.bool(forKey: "isUserLoggedIn")
    }

    //----------------------------------
    // MARK: - doLogout
    //
    func doLogOut()->Bool{
        AppSetUp.defaults.set(false, forKey: "isKidLoggedIn")
        AppSetUp.defaults.removeObject(forKey: "currentLoggedKid")
        AppSetUp.defaults.set(false, forKey: "isUserLoggedIn")

        //7may
        AppSetUp.defaults.removeObject(forKey: "areAllSettingsCompleted")
        //7 may end
        deleteOldTokensFromCoreDB()
        AppSetUp.settingConfigurationData = [:]
        //  MARK: - FB Logout
        LoginManager().logOut()
        //  MARK: - Google Logout
        GIDSignIn.sharedInstance()?.signOut()
        return true
    }

    //  MARK: - Save latest token
    //
    func saveToken(_ token: String) {
        AppSetUp().deleteOldTokensFromCoreDB()
        let AppInformation = AppInfo(context: PersistenceService.context)
        AppInformation.getLatestToken = token
        PersistenceService.saveContext()
        AppSetUp().showCurrentTokenInCoreDB()
    }

    //  MARK: - Delete old tokens from Core Database
    //
    func deleteOldTokensFromCoreDB(){
        let demo =  try! PersistenceService.context.fetch(AppInfo.fetchRequest()) as? [AppInfo]
        demo?.forEach({
            PersistenceService.context.delete($0)
        })
    }

    //  MARK: - Show current tokens
    //
    func showCurrentTokenInCoreDB(){
        let demo =  try! PersistenceService.context.fetch(AppInfo.fetchRequest()) as? [AppInfo]
        demo?.forEach({
            print("CurrentTokenIs==",$0.getLatestToken)
        })
    }

    //  MARK: - FetchToken from DB
    //
    func getLatestToken()->String?{
        let fetchRequest: NSFetchRequest<AppInfo> = AppInfo.fetchRequest()
        do{
            let latestToken = try? PersistenceService.context.fetch(fetchRequest)
            return latestToken?.first?.getLatestToken
        }catch(let error){
            print(error.localizedDescription)
            return nil
        }
    }

    //  MARK: - Time management Stuff

    func convertParamsToJSON(params:[String : Any])->String{
        var jsonString = String()
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: params,
            options: [.prettyPrinted]) {
            let theJSONText = String(data: theJSONData,
                                     encoding: .ascii)
            print("JSON string = \(theJSONText!)")
            guard let theJSONTextFinal = theJSONText else {return ""}
            jsonString = theJSONTextFinal
        }
        return jsonString
    }


    func getDurationValueForCaseTwo()->[String:[[String:String]]]{
        var paramsFinal = [String:[[String:String]]]()
        let send = TimeMgmtSingleton.saveDurationValueForCase2
        AppSetUp.weeks.forEach { (day) in
            let dayT = send[day]
            print("Day===",day)
            var postDurationValueCase2  = [[String:String]]()
            for (key,val) in dayT ?? [:]{
                var temp = [String: String]()
                temp["device_id"]   = key
                temp["max_minutes"] = val
                postDurationValueCase2.append(temp)
            }
            paramsFinal[day] = postDurationValueCase2
        }
        return paramsFinal
    }


    let areAllSettingsCompleted = { () in AppSetUp.settingConfigurationData.values.contains("false") ? false : true }
    
    //  MARK: - Design Logic
    func dropDownImage()->UIImageView{
        let dropDownImage                                        = UIImageView()
        dropDownImage.image                                      = UIImage(named: "down-arrow")
        dropDownImage.translatesAutoresizingMaskIntoConstraints  = false
        return dropDownImage
    }


    //  MARK: - Formatter
    typealias please_provide_date_format_in_string = String
    func getDateInSpecificFormat(_ dateFormat: please_provide_date_format_in_string,_ date: Date)->String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        let string = dateFormatter.string(from: date)
        return string
    }


    //  MARK: - Check trait
    static let isIpad: Bool = {
        return UIScreen.main.traitCollection.horizontalSizeClass == .regular && UIScreen.main.traitCollection.verticalSizeClass == .regular
    }()
    static var showMondayNotification       = false
    static var isMondayNotificationSeenbyUser = false

    static let defaults                     = UserDefaults.standard
    static let disposeBag                   = DisposeBag()
    static let dropDown                     = DropDown()
    static let colorPrimary                 = #colorLiteral(red: 0.007843137255, green: 0.337254902, blue: 0.5098039216, alpha: 1)
    static let colorPrimaryDark             = #colorLiteral(red: 0.2117647059, green: 0.2784313725, blue: 0.3098039216, alpha: 1)
    static let colorAccent                  = #colorLiteral(red: 0.007843137255, green: 0.4901960784, blue: 0.7254901961, alpha: 1)
    static let text_icon_gray               = #colorLiteral(red: 0.262745098, green: 0.262745098, blue: 0.2549019608, alpha: 1)
    static let controllerBackgroundColor    = #colorLiteral(red: 0.9333333333, green: 0.9333333333, blue: 0.9333333333, alpha: 1)
    static let textColor                    = #colorLiteral(red: 0.1294117647, green: 0.1294117647, blue: 0.1294117647, alpha: 1)
    static let decoder                      = JSONDecoder()
    static let weeks                        = ["monday","tuesday","wednesday","thursday","friday","saturday","sunday"]
    static let coinsDeductionChoice         = ["Deduct coins in general","Deduct coins on specific date"]
    static var isPremium                    = Bool()
    static var settingConfigurationData     = [String:String]()
    static let shared                       = AppSetUp()
}

