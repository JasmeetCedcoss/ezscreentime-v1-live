//
//  mageKidsettings.swift
//  EZScreenTime
//
//  Created by cedcoss on 29/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import Foundation

final class KidSetupSettings {

    static let storyBoard: UIStoryboard     = UIStoryboard(name: "KidLoginStory", bundle: nil)
    static let shared                       = KidSetupSettings()

    //  MARK: - SAVE KID DETAIL TO DB
    //
    func checkIfKidExistInDBIfNotThenSave(id:String,icon:String,age:String,token: String,name: String) {

        let fetchRequest: NSFetchRequest<MultipleKidLoginDB> = MultipleKidLoginDB.fetchRequest()
        do{
            let allKids = try? PersistenceService.context.fetch(fetchRequest)
            allKids?.forEach({ (kid) in
                if id == kid.kid_id{
                    print("KidToUpdate",kid.kid_name as Any)
                    PersistenceService.context.delete(kid)
                }
            })
            KidSetupSettings().saveKidDetailToDB(id: id, icon: icon, age: age, token: token, name: name)
        }catch(let error){
            print(error.localizedDescription)
        }
    }

    //  MARK: - Save new kid to db
    //
    func saveKidDetailToDB(id:String,icon:String,age:String,token: String,name: String) {
        let kidDetail       = MultipleKidLoginDB(context: PersistenceService.context)
        kidDetail.kid_id    = id
        kidDetail.kid_age   = age
        kidDetail.kid_icon  = icon
        kidDetail.kid_token = token
        kidDetail.kid_name  = name
        PersistenceService.saveContext()
        KidSetupSettings().showCurrentKidsInCoreDB()
    }

    //  MARK: - SHOW CURRENT KIDS IN DB
    //
    func showCurrentKidsInCoreDB(){
        let demo =  try! PersistenceService.context.fetch(MultipleKidLoginDB.fetchRequest()) as? [MultipleKidLoginDB]
        demo?.forEach({
            print("CurrentkidIndb==",$0.kid_name,"id",$0.kid_id,"token==",$0.kid_token)
        })
    }

    //  MARK: - FetchAllKids From DB
    //
    func getAllKidsFromDB()->[MultipleKidLoginDB]?{
        let fetchRequest: NSFetchRequest<MultipleKidLoginDB> = MultipleKidLoginDB.fetchRequest()
        do{
            let allKids = try? PersistenceService.context.fetch(fetchRequest)
            return allKids

        }catch(let error){
            print(error.localizedDescription)
            return nil
        }
    }

    //  MARK: - gettokenforkid From DB
    //
    func getTokenForKidFromDB(with id: String)->String?{
        let fetchRequest: NSFetchRequest<MultipleKidLoginDB> = MultipleKidLoginDB.fetchRequest()
        do{
            var token = String()
            let allKids = try? PersistenceService.context.fetch(fetchRequest)
            allKids?.forEach({ (kid) in
                if id == kid.kid_id{
                    print(kid)
                    print(kid.kid_token as Any)
                    token = kid.kid_token ?? ""
                }
            })
            print("kidToken==",token)
            return token
        }catch(let error){
            print(error.localizedDescription)
            return nil
        }
    }

    //  MARK: - getDataForKidFromDB From DB
    //
    func getDataForKidFromDB(with id: String)->MultipleKidLoginDB?{
        let fetchRequest: NSFetchRequest<MultipleKidLoginDB> = MultipleKidLoginDB.fetchRequest()
        do{
            var singleKidDetail: MultipleKidLoginDB?
            let allKids = try? PersistenceService.context.fetch(fetchRequest)
            allKids?.forEach({ (kid) in
                print("Matched kid==",kid)
                if id == kid.kid_id{
                    singleKidDetail = kid
                }
            })
            return singleKidDetail
        }catch(let error){
            print(error.localizedDescription)
            return nil
        }
    }


    //  MARK: - UPDATE KID DB ICON
    //
    func updateKidIcon(icon:String,id:String) {
        let fetchRequest: NSFetchRequest<MultipleKidLoginDB> = MultipleKidLoginDB.fetchRequest()
        do{
            let allKids = try? PersistenceService.context.fetch(fetchRequest)
            allKids?.forEach({ (kid) in
                if id == kid.kid_id{
                    print("KidToUpdate",kid.kid_name as Any)
                    kid.setValue(icon, forKey: "kid_icon")
                }
            })
        }catch(let error){
            print(error.localizedDescription)
        }
    }

    //  MARK: - CheckForLogin
    //
    func isKidLogin()->Bool{
        return AppSetUp.defaults.bool(forKey: "isKidLoggedIn")
    }

    //  MARK: - Delete kid from DB when kid logs out

    func deleteKidLoggingOut(_ id:String){
        let fetchRequest: NSFetchRequest<MultipleKidLoginDB> = MultipleKidLoginDB.fetchRequest()
        do{
            let allKids = try? PersistenceService.context.fetch(fetchRequest)
            allKids?.forEach({ (kid) in
                if id == kid.kid_id{
                    print("KidToUpdate",kid.kid_name as Any)
                    PersistenceService.context.delete(kid)
                }
            })
        }catch(let error){
            print(error.localizedDescription)
        }
    }
}

class CurrentLoggedKid{
    let shared = CurrentLoggedKid()
    private init() {}
    static var id = String()

}



//class ADLNProcess{
//    let shared = ADLNProcess()
//    private init() {}
//    static var isInProcess = false
//
//}
