//
//  AppDelegate.swift
//  EZScreenTime
//
//  Created by cedcoss on 04/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import UIKit
import CoreData
import Firebase
import UserNotificationsUI
import UserNotifications
import Messages
import RxSwift
import GoogleMobileAds
import GoogleSignIn
import FBSDKLoginKit

protocol AutoDeactivationDelegate {
    func AutoDeactivationDelegate(_ param: [AnyHashable : Any])
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow? 
    let gcmMessageIDKey = "gcm_message_id"

    //  MARK: LocalNotification Setup
    //
    let notificationCenter          = UNUserNotificationCenter.current()
    var autoDeactivationDelegate    : AutoDeactivationDelegate?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        print("DB Path:==",FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!)

        //  MARK: - google sign in
        UIApplication.shared.applicationIconBadgeNumber = 0
        GIDSignIn.sharedInstance().clientID = "249002385789-lj5vduuprkkkg8flem6ddcrhb75j9s2s.apps.googleusercontent.com"

        //  MARK: - Facebook Setup
        ApplicationDelegate.shared.application(
            application,
            didFinishLaunchingWithOptions: launchOptions
        )

        //  MARK: - other setup
        IQKeyboardManager.shared.enable = true

        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self

            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()

        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        Messaging.messaging().fcmToken
        // Initialize the Google Mobile Ads SDK.
        GADMobileAds.sharedInstance().start(completionHandler: nil)
        scheduleLocalNotification()
        return true
    }

    //4may
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return ApplicationDelegate.shared.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }

    //this function is added only
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
       return GIDSignIn.sharedInstance().handle(url)
        
    }

    //4may end
    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    //  MARK: - CLOUD MESSAGING
    //

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification

        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)

        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        // Print full message.
        print(userInfo)
    }

    // [END receive_message]
    // 25april
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }

    // This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
    // If swizzling is disabled then this function must be implemented so that the APNs token can be paired to
    // the FCM registration token.
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("APNs token retrieved: \(deviceToken)")
        // With swizzling disabled you must set the APNs token here.
        Messaging.messaging().apnsToken = deviceToken
    }
    // [END receive_message]

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification

        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)

        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        // Print full message.
        print(userInfo)
        completionHandler(UIBackgroundFetchResult.newData)
    }
}

//  MARK: - Setup MessagingDelegate
//
extension AppDelegate: MessagingDelegate {

    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token====\(fcmToken)")
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
    }

    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Message Data", remoteMessage.appData)
    }
}

//  MARK: - Setup UNUserNotificationCenterDelegate
//

@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {

    // Receive displayed notifications for iOS 10 devices.

    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        var notificationModel: NotificationModel?
        let userInfo = notification.request.content.userInfo
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)

        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }

        // Print full message.
        print(userInfo)

        //22 may start
        //  MARK: - Local Notification Handling for Auto Deactivation // 22maystart
        if notification.request.identifier.contains("ADLN"){
            print("See latest news===",notification.request.content)
            let param = notification.request.content.userInfo
            autoDeactivationDelegate?.AutoDeactivationDelegate(param)
            //NotificationCenter.default.post(name: .performAutoDeactivation, object: nil, userInfo: param as [AnyHashable : Any])
        }
        //22may end
        //  MARK: - PARSING NOTIFICATION JSON
        //
        guard let test = userInfo["aps"] as? Dictionary<String,Any>
            else{
                completionHandler([.alert,.sound])
                print("not able to convert (userInfo[aps])")
                return
        }

        guard let main = test["alert"] as? Dictionary<String,Any>
            else{
                print("not able to convert (userInfo[alert])")
                return
        }

        //  MARK: - FIRING NOTIFICATION
        //
        let notificationContent = main["title"] as? String

        switch notificationContent {
        case "Activated","Deactivated","Icon Changed":
            NotificationCenter.default.post(name: .performParentDashboardUpdate, object: nil)
            KidSetupSettings.shared.isKidLogin() ? print("Notification for parent"): completionHandler([.alert,.sound])
        case "Penalty","Awarded","Discipline":
                guard let title =  main["title"] as? String,
                let parentIcon           = userInfo["gcm.notification.parentIcon"] as? String,
                let message              = userInfo["gcm.notification.message"] as? String,
                let reason               = userInfo["gcm.notification.reason"] as? String,
                let _                    = userInfo["gcm.notification.pageNavigation"] as? String,
                let kidId                = userInfo["gcm.notification.kidId"] as? String,
                let typeOfDistribution   = userInfo["gcm.notification.typeOfDistribution"] as? String,
                let numberOfCoins        = userInfo["gcm.notification.numberOfCoins"] as? String
                else {return}

            let temp = NotificationModel(title: title, message: message, reason: reason, parentIcon: parentIcon,typeOfDistribution: typeOfDistribution,kidID: kidId,numberOfCoins: numberOfCoins)
            notificationModel = temp
            let param = ["PenaltyCoinUpdate":notificationModel]
            NotificationCenter.default.post(name: .performKidDashUpdateForCoin, object: nil, userInfo: param as [AnyHashable : Any])
            KidSetupSettings.shared.isKidLogin() ? completionHandler([.alert,.sound]): print("Notification for kiddashboard")
        case "Time will end soon":
            completionHandler([.alert,.sound])
        default:
            print("Default notification case")
        }
        //completionHandler([.alert,.sound])
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        var notificationModel: NotificationModel?
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        // Print full message.
        print("DidReceiveNotification===",userInfo)

        //  MARK: - PARSING NOTIFICATION JSON
        //
        guard let test = userInfo["aps"] as? Dictionary<String,Any>
            else{
                print("not able to convert (userInfo[aps]) in didreceive")
                //AppSetUp.defaults.set(nil, forKey: "userViewedNoti")
                return
        }
        guard let main = test["alert"] as? Dictionary<String,Any>
            else{
                print("not able to convert (userInfo[alert])in didreceive")
                return
        }

        //  MARK: - FIRING NOTIFICATION
        //
        let notificationContent = main["title"] as? String
        //  MARK: - Push notfication handling when tapped in background
        let application = UIApplication.shared
        if(application.applicationState == .active){
             print("user tapped the notification bar when the app is in foreground")
        }else if (application.applicationState == .inactive) {
             print("user tapped the notification bar when the app is in background")
            if AppSetUp.shared.isParentLogin(){
                print("Yes parent Logged in")
            }else if KidSetupSettings.shared.isKidLogin(){
                print("Kid logged in")
                switch notificationContent {
                case "Penalty","Awarded","Discipline":
                    guard let title =  main["title"] as? String,
                        let parentIcon           = userInfo["gcm.notification.parentIcon"] as? String,
                        let message              = userInfo["gcm.notification.message"] as? String,
                        let reason               = userInfo["gcm.notification.reason"] as? String,
                        let _                    = userInfo["gcm.notification.pageNavigation"] as? String,
                        let kidId                = userInfo["gcm.notification.kidId"] as? String,
                        let typeOfDistribution   = userInfo["gcm.notification.typeOfDistribution"] as? String,
                        let numberOfCoins        = userInfo["gcm.notification.numberOfCoins"] as? String
                        else {return}
                    let temp = NotificationModel(title: title, message: message, reason: reason, parentIcon: parentIcon,typeOfDistribution: typeOfDistribution,kidID: kidId,numberOfCoins: numberOfCoins)
                    notificationModel = temp
                    let param = ["PenaltyCoinUpdate":notificationModel]
                    NotificationCenter.default.post(name: .performKidDashUpdateForCoin, object: nil, userInfo: param as [AnyHashable : Any])
                default:
                    print("Default notification case")
                }
            }
        }

        //  MARK: - Local Notification Handling
        if response.notification.request.identifier == "Local Notification" {
            response.notification.request.content
        }
        //25april mod end
        completionHandler()
    }
}

//  MARK: - Test Local Notification
//
extension AppDelegate{

    fileprivate func scheduleLocalNotification() {

        let options: UNAuthorizationOptions = [.alert, .sound]

        notificationCenter.requestAuthorization(options: options) {
            (didAllow, error) in
            if !didAllow {
                print("User has declined notifications")
            }
        }

        let content = UNMutableNotificationContent()
        content.title = "EZST"
        content.body  = "Amount of coins has been reset for new week!"
        content.sound = UNNotificationSound.default

        var calendarComponents = DateComponents()
                    calendarComponents.weekday  = 2
                    calendarComponents.hour     = 6
                    calendarComponents.second   = 0
                    calendarComponents.minute   = 0

        let trigger     = UNCalendarNotificationTrigger(dateMatching: calendarComponents, repeats: true)
        let identifier  = "Local Notification"
        let request     = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)

        notificationCenter.add(request) { (error) in
            if let error = error {
                print("Error \(error.localizedDescription)")
            }
        }
    }
}



//[AnyHashable("gcm.notification.typeOfMessage"): 0, AnyHashable("gcm.notification.numberOfCoins"): , AnyHashable("data"): {"timestamp":"2020-05-15 11:43:02"}, AnyHashable("gcm.notification.kidId"): 5, AnyHashable("gcm.notification.typeOfDistribution"): , AnyHashable("gcm.message_id"): 1589535782265259, AnyHashable("google.c.sender.id"): 249002385789, AnyHashable("google.c.a.e"): 1, AnyHashable("aps"): {
//    alert =     {
//        body = "Pontiac your today limit for TV will end in next 5 minutes";
//        title = "Time will end soon";
//    };
//}, AnyHashable("gcm.notification.pageNavigation"): kidDashboard, AnyHashable("gcm.notification.kidName"): Pontiac, AnyHashable("gcm.notification.deviceName"): TV, AnyHashable("gcm.notification.reason"): Limit will reached soon, AnyHashable("gcm.notification.message"): Your device time end soon]
