//
//  UsefulExtensions.swift
//  EZScreenTime
//
//  Created by cedcoss on 05/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//

import Foundation

extension Dictionary{
    func convtToJson() -> NSString {
        do {
            let json = try JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions.prettyPrinted)
            return NSString(data: json, encoding: String.Encoding.utf8.rawValue)!
        }catch {
            return ""
        }
    }
}

extension UIView {
    func addLoader(){
        let activity: UIActivityIndicatorView
        if #available(iOS 13.0, *) {
            activity = UIActivityIndicatorView(style: .large)
        } else {
            // Fallback on earlier versions
            activity = UIActivityIndicatorView(style: .whiteLarge)
        }
        activity.frame.size = CGSize(width: 100, height: 100)
        activity.color = AppSetUp.colorPrimary
        activity.center = self.center
        activity.tag = 12334
        activity.hidesWhenStopped = true
        activity.startAnimating()
        self.addSubview(activity)
    }

    func stopLoader(){
        if let activity = self.viewWithTag(12334) as? UIActivityIndicatorView {
            activity.stopAnimating()
            activity.removeFromSuperview()
        }
    }
}

extension Notification.Name{
    static let deviceAdded                  = Notification.Name("deviceAdded")
    static let performLogOut                = Notification.Name("performLogOut")
    static let performParentDashboardUpdate = Notification.Name("performParentDashboardUpdate")
    static let performKidDashUpdateForCoin  = Notification.Name("performKidDashUpdateForCoin")
    static let performAutoDeactivation      = Notification.Name("performAutoDeactivation")


}

extension UIView{
    public func JSShadowUIView(){
        self.layer.shadowColor              = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        self.layer.shadowOffset             = CGSize(width: 0, height: 3)
        self.layer.shadowOpacity            = 1.0
        self.layer.shadowRadius             = 10.0
        self.layer.masksToBounds            = false
    }
}



extension UIButton{
    public func JSDropDown(){
        self.layer.borderWidth              = 1
        self.layer.cornerRadius             = 5
        self.tintColor                      = .black
        self.layer.borderColor              = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        self.backgroundColor                = AppSetUp.controllerBackgroundColor

    }
}

extension Dictionary where Value : Equatable {
    func allKeysForValue(val : Value) -> [Key] {
        return self.filter { $1 == val }.map { $0.0 }
    }
}


extension UIButton{
    public func addDropDownImage(){
        let image = AppSetUp().dropDownImage()
        self.layer.borderWidth              = 1
        self.layer.cornerRadius             = 5
        self.tintColor                      = .black
        self.layer.borderColor              = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        self.backgroundColor                = AppSetUp.controllerBackgroundColor
        self.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 30)
        self.addSubview(image)
        image.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        image.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10).isActive = true
        image.widthAnchor.constraint(equalToConstant: 10).isActive = true
        image.heightAnchor.constraint(equalToConstant: 10).isActive = true
    }
}

extension UIButton{
    public func floatButtonShadow(){
        self.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
        self.layer.masksToBounds = false
    }
}


extension String{
    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
}
