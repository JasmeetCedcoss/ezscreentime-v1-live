//
//  Storyboard+ViewController.swift
//  EZScreenTime
//
//  Created by cedcoss on 04/02/20.
//  Copyright © 2020 cedcoss. All rights reserved.
//


import UIKit

extension UIStoryboard {

    func instantiateViewController<T: UIViewController>() -> T {

        let viewController = self.instantiateViewController(withIdentifier: T.className)
        guard let typedViewController = viewController as? T else {
            fatalError("Unable to cast view controller of type (\(type(of: viewController))) to (\(T.className))")
        }
        return typedViewController
    }
}

extension UIView{
    public func JSCustomView(){
        self.layer.borderColor     = #colorLiteral(red: 0.662745098, green: 0.662745098, blue: 0.662745098, alpha: 1)
        self.layer.borderWidth     = 2
        self.layer.cornerRadius    = 5
    }
}

extension UIView{
    public func JSCustomView2(){
        self.layer.borderColor     = #colorLiteral(red: 0.662745098, green: 0.662745098, blue: 0.662745098, alpha: 0.8008052147)
        self.layer.borderWidth     = 1
        self.layer.cornerRadius    = 5
    }
}

extension UIView{
    public func cornerRadius(usingCorners corners: UIRectCorner,cornerRadii:CGSize){
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: cornerRadii)
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        self.layer.mask = maskLayer
    }
}
